# The Jason White Slim Frame

[Current JWSF residence](http://jwsf.michigansocial.com/features "Click to visit JWSF's current home")

* Please note, that is the default, vanilla install
and only has development themes.  It has seen hundreds
of interfaces, tons of custom plugins to merchant
APIs, dynamic video processing and more.  I evolved
it over many years and poured a whole lot of love
into it.
* I take great pride in the code, read through the
_public_html, it's pretty. :>

There is a *ton* of documentation on that site, as
well as in the source code (which lives almost
entirely in _public_html because I am a security-first
minded kinda guy).

This has never before been seen by other eyes (Well,
except that *one* time, garr).  Enjoy.

I suggest starting in public_html/index.php and then
proceeding into the _public_html folder.  This baby
is so flexible and smart it's unreal.  You can drag
and drop it around (or scp/mv like me) like apps in
MacOS 9. :D

If anyone has time, go through old Wordpress commits
and see when they got my file loading & dynamic theme
code! lol
