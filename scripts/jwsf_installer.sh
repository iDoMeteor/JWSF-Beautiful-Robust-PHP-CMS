#!/bin/bash


##### JWFS Installer v0.7 ######################################################
#
# Synopsis: Bash installer for JWFS CMS
# Version:	0.7
# Code Name:Beta 2
# Editors:	thims, arthax0r
# Date:	    20091030
# Comments: Cleaned up code structure
#           Renamed most variables to self-documenting names
#           Added second password and output both to install log
#           Added more install.log messages
#           Completed curl statements
#           Removed stale code and comments
#           Made additional adjustments and clarification for differences
#               between public and private sites
#           Updated htaccess/password stuff
#           File regex subustition string updated
# TODO:
#           Check syntax
#           Verify htpasswd/access
#           ????
#           Profit! (test) :>
#
################################################################################

# Start Log
touch ./install.log
echo "Beginning JWSF Installation: $`date`"

# Define package meta data
palias=jwfs-latest-stable.tar.gz
source_link=http://stable.lookintomycode.com/$palias


##### Password Generator #######################################################
#   Generates 12 Random Chars
#   Pass To:
#       curl'd form fields for database/user creation
#       _public_html/constants/Cons_Db.php
#   Output To:
#       some text file in site root
################################################################################
function genPasswd() {
    alph=(a b c d e f g h i j k l m n o p q r s t u v w x y z
        A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
        0 1 2 3 4 5 6 7 8 9)
    lenAlph=${#alph[*]}
    ranPasswd=
    for i in $(seq 12)
        do
            ranPasswd=${passwd}${alph[$((RANDOM % lenAlph))]}
            echo $ranPasswd
        done
}
dbPasswordAdmin=$(genPasswd)
dbPasswordScript=$(genPasswd)
echo $dbPasswordAdmin >> ./install.log
echo $dbPasswordScript >> ./install.log

##### Parse CLI Arguments ######################################################
while [ $# -gt 0 ]
    do
        case "$1" in
            "-c"|"--config")
                if [ ! -e $2 ] ;then
                    echo "Error: file $2 doesnt exist."
                    exit 1
                else
                    config_file="$2"
                fi
                ;;
            "-h"|"--help")
                cat << EOF
                $0 installer
                Usage: ./$0 [args]

                -c, --config (filename) - Specify the path to the configuration file containing credentials
                -h, --help              - Print this help and exit
                EOF
                ;;
            esac
        shift
    done


##### File Acquisition/Expansion ###############################################
#
#   In home dir, create archive directory
#   Download source
#   Expand
#
################################################################################
if [ -z $config_file ] ;then
    echo "Error: No configuration file specified"
    exit 1
else
	# loop every line, parse values, and setup enviroment with values
    for lines in $(cat $config_file)
    do
        domainName=$(echo $lines | awk -F":" '{print $1}')
        domainUserName=$(echo $lines | awk -F":" '{print $2}')
        domainPassword=$(echo $lines | awk -F":" '{print $3}')
        domainHome="/home/$domainUserName"
        previewHome="/home/$domainUserName/subdomains/preview"

        # make sure home dir exists
        if [ ! -d $domainHome ] ;then
            echo "Error: $domainHome does not exist."
            echo "Error: $domainHome does not exist." >> ./install.log
            exit 1
        else
            cd $domainHome
        fi

        # get source, expand it, and log the output
        wget -o ./install.log -O ./$palias $source_link
        tar xvfz $palias >> ./install.log

        # create preview subdomain
        formLocation="http://$domainName:2082/frontend/x3/subdomain/doadddomain.html"
        formDomainSubValue=$domainName
        formDirValue=subdomains/preview/public_html
        curl -A Mozilla/4.0 --basic --connect-timeout 5 --data domain=$formDomainSubValue --data rootdomain=$domainName --data dir=$formDirValue --data go='Create' --user $webUser:"'"$webPasswd"'" $formLocation

        # create primary and secondary databases
        formAction="http://$domainName:2082/frontend/x3/sql/addb.html"
        formNewDBFieldName=db
        formName=mainform
        formNewDBValue1=Site
        formNewDBValue2=preview
        formSubmitFieldName=submit
        formSubmitValue='Create Database'
        curl -A Mozilla/4.0 --basic --connect-timeout 5 --data $formName=true --data $formNewDBFieldName=$formNewDBValue1 --data $formSubmitFieldName=$formSubmitFieldValue --user $webUser:$webPasswd $formAction
        curl -A Mozilla/4.0 --basic --connect-timeout 5 --data $formName=true --data $formNewDBFieldName=$formNewDBValue2 --data $formSubmitFieldName=$formSubmitFieldValue --user $webUser:$webPasswd $formAction


        # create database users for web and scripted access
        formAction="http://$domainName:2082/frontend/x3/sql/adduser.html"
        formName=userform
        formNewUserFieldName=user
        formNewUserValue1=admin
        formNewUserValue2=scripty
        formNewUserPasswordFieldName=pass
        formNewUserPasswordVerifyFieldName=pass2
        formNewUserPasswordValue1=$dbPasswordAdmin
        formNewUserPasswordValue2=$dbPasswordScript
        formSubmitFieldName=submit
        formSubmitValue='Create User'
        curl -A Mozilla/4.0 --basic --connect-timeout 5 --data $formName=true --data $formNewUserFieldName=$formNewUserValue1 --data formNewUserPasswordFieldName=$formNewUserPasswordValue1 --data $formNewUserPasswordVerifyFieldName=$formNewUserPasswordValue1 --data $formSubmitFieldName=$formSubmitFieldValue --user $webUser:$webPasswd $formAction
        curl -A Mozilla/4.0 --basic --connect-timeout 5 --data $formName=true --data $formNewUserFieldName=$formNewUserValue2 --data formNewUserPasswordFieldName=$formNewUserPasswordValue2 --data $formNewUserPasswordVerifyFieldName=$formNewUserPasswordValue2 --data $formSubmitFieldName=$formSubmitFieldValue --user $webUser:$webPasswd $formAction


        # adding both users to both databases
        formAction="http://$domainName:2082/frontend/x3/sql/addusertodb.html"
        formName=adduserdb
        formUserFieldName=user
        formUserValueAdmin=$domainUserName_admin
        formUserValueScript=$domainUserName_scripty
        formDbFieldName=db
        formDbValueSite=$domainUserName_Site
        formDbValuePreview=$domainUserName_preview
#		formUpdateFieldName=update # trying without
#		formUpdateValue=''
#       --data update=''  # from curl statement
        formSubmitFieldName=submit
        formSubmitValue='Make Changes'

        #admin - to both databases with full perms
        curl -A Mozilla/4.0 --basic --connect-timeout 5 --data $formName=true --data $formUserFieldName=$formUserValueAdmin --data $formDbFieldName=$formDbValueSite --data SELECT=SELECT --data CREATE=CREATE --data INSERT=INSERT --data ALTER=ALTER --data UPDATE=UPDATE --data DROP=DROP --data DELETE=DELETE --data LOCKTABLES=LOCK --data INDEX=INDEX --data REFERENCES=REFERENCES --data CREATETEMPORARYTABLES=TEMPORARY --data CREATEROUTINE=CREATEROUTINE  --data $formSubmitFieldName=$formSubmitValue --user $webUser:$webPasswd $formAction
        curl -A Mozilla/4.0 --basic --connect-timeout 5 --data $formName=true --data $formUserFieldName=$formUserValueAdmin --data $formDbFieldName=$formDbValuePreview --data SELECT=SELECT --data CREATE=CREATE --data INSERT=INSERT --data ALTER=ALTER --data UPDATE=UPDATE --data DROP=DROP --data DELETE=DELETE --data LOCKTABLES=LOCK --data INDEX=INDEX --data REFERENCES=REFERENCES --data CREATETEMPORARYTABLES=TEMPORARY --data CREATEROUTINE=CREATEROUTINE  --data $formSubmitFieldName=$formSubmitValue --user $webUser:$webPasswd $formAction

        #scripty - to both databases with limited perms
        curl -A Mozilla/4.0 --basic --connect-timeout 5 --data $formName=true --data $formUserFieldName=$formUserValueScript --data $formDbFieldName=$formDbValueSite --data SELECT=SELECT --data INSERT=INSERT --data UPDATE=UPDATE --data DELETE=DELETE --data INDEX=INDEX --data $formSubmitFieldName=$formSubmitValue --user $webUser:$webPasswd $formAction
        curl -A Mozilla/4.0 --basic --connect-timeout 5 --data $formName=true --data $formUserFieldName=$formUserValueScript --data $formDbFieldName=$formDbValuePreview --data SELECT=SELECT --data INSERT=INSERT --data UPDATE=UPDATE --data DELETE=DELETE --data INDEX=INDEX --data $formSubmitFieldName=$formSubmitValue --user $webUser:$webPasswd $formAction

        ### end curling ###


        ## lets change some credentials in files
        # a list containing filename:linenumber of files requiring $whomai
        # to be replaced with a value reflecting the current enviroment
        filesChangeUserName=(
            _public_html/constants/Cons_Db.php:23
            _public_html/constants/Cons_Db.php:25
            subdomains/preview/_public_html/constants/Cons_Db.php:23
            subdomains/preview/_public_html/constants/Cons_Db.php:25
            subdomains/preview/public_html/.htaccess:7
        )

        # list is same as above list, only with $passwd instead
        filesChangePassword=(
            _public_html/constants/Cons_Db.php:27
            subdomains/preview/_public_html/constants/Cons_Db.php:27
        )

        # s/$whoami/$USERNAME/g
        for i in ${filesChangeUserName[*]}
            do
                file=$(echo i | awk -F":" '{print $1}')
                line_num=$(echo i | awk -F":" '{print $2}')
                sed -i $line_number's/XXX/$domainUserName/g' $file
            done

        # s/$passwd/$ranPasswd
        for x in ${filesChangePassword[*]}
            do
                file=$(echo x | awk -F":" '{print $1}')
                line_num=$(echo x | awk -F":" '{print $2}')
                sed -i $line_number's/XXX/'$dbPasswordScript'/g' $file
            done


        ## Setup the .htaccess files and populate both of them
        htadminPublic=$domainHome/public_html/admin/.htaccess
        if [ ! -e $htadminPublic ] ;then
            touch $htadminPublic
            chown 700 $htadminPublic
            # I realize how syntactically incorrect these next 6 lines are
            # hideous right? but bash doesnt parse them right following
            # proper code format.
            cat >> $htadminPublic << EOF
                AuthType Basic
                AuthName "JWSF Admin Area"
                AuthUserFile "/home/$domainName/.htpasswds/public_html/admin/passwd"
                require valid-user
                EOF
        else
            echo "Notice: $htadminPublic exists!"
            echo "Notice: $htadminPublic exists!" >> ./install.log
        fi

        htadminPreview=$domainHome/subdomains/preview/public_html/.htaccess
        if [ ! -e $htadminPreview ] ;then
            touch $htadminPreview
            chown 700 $htadminPreview
            # same story as above
            cat >> $htadminPreview << EOF
                AuthType Basic
                AuthName "JWSF Preview Area"
                AuthUserFile "/home/$domainName/.htpasswds/subdomains/preview/public_html/passwd"
                require valid-user
                EOF
        else
            echo Notice: $htadminPreview exists!
        fi

        # add user to authentication DB
        htpasswd -b -c $htadminPublic $domainUserName $domainPassword
        htpasswd -b -c $htadminPreview $domainUserName $domainPassword

        # Fix the permissions
        find $domainHome/public_html | xargs chmod 755
        find $domainHome/_public_html | xargs chmod 755
        chmod 775 $domainHome/dumps
        chmod 775 $domainHome/_public_html/sessions
        chmod 775 $domainHome/_public_html/smarty/cache
        chmod 775 $domainHome/_public_html/smarty/templates_c
        chmod 775 $domainHome/public_html/sitemap.xml
        chmod 444 $domainHome/_public_html/constants/Cons_Db.php
        chmod 444 $domainHome/public_html/.htaccess
        chmod 444 $domainHome/public_html/admin/.htaccess
        find $previewHome/public_html | xargs chmod 755
        find $previewHome/_public_html | xargs chmod 755
        chmod 775 $previewHome/dumps
        chmod 775 $previewHome/_public_html/sessions
        chmod 775 $previewHome/_public_html/smarty/cache
        chmod 775 $previewHome/_public_html/smarty/templates_c
        chmod 775 $previewHome/public_html/sitemap.xml
        chmod 444 $previewHome/_public_html/constants/Cons_Db.php
        chmod 444 $previewHome/public_html/.htaccess

        # Report
        echo "If there are no errors above, JWSF has been installed!"
        echo "Visit http://$domainName to view the public site"
        echo "Visit http://$domainName/admin to administer the public site"
        echo "Visit http://preview.$domainName to view the development site"
        echo "Visit http://preview.$domainName/admin to administer the public site"
        echo "Visit http://lookintomycode.com for documentation and author details"
        # Report to install.log
        echo "If there are no errors above, JWSF has been installed!" >> ./install.log
        echo "Visit http://$domainName to view the public site" >> ./install.log
        echo "Visit http://$domainName/admin to administer the public site" >> ./install.log
        echo "Visit http://preview.$domainName to view the development site" >> ./install.log
        echo "Visit http://preview.$domainName/admin to administer the public site" >> ./install.log
        echo "Visit http://lookintomycode.com for documentation and author details" >> ./install.log

        # Archive install log
        mv ./install.log $domainHome/dumps
        chmod 400 $domainHome/dumps/install.log

        #cleanup
        rm $palias
    done
    # End configuration file processing loop
fi
