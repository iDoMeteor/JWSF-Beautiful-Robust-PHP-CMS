<?php

/*******************************************************************************
 *  JWSF Slim Frame Multi-Level Contact Module
 *
 *  $Revision: 1.2 $
 *
 *  First run through we need to grab configuration parameters from the db...
 *      Recipient List
 *      Subject List
 *      BCC List
 *      Email Headers ... possibly in constants
 *      Assign Smarty values
 *
 *  Second pass performs validation
 *      Upon successful
 *          Log to database
 *          Send email
 *          Assign Smarty values
 *          Display receipt
 *      Upon failure
 *          Assign Smarty values
 *          Back to step one with user entered data
 *
 *  7/15/2009 1:53:36 AM
 *      Added Captcha
 *      Added ip logging
 *      Database - added ip, failed captchas, captcha config switch
 *
 *  TODO:
 *      Globalize captcha
 *
 ******************************************************************************/

DEFINE ('TableContactRecips', 'JWSF_ModuleContactUsRecips');
DEFINE ('TableContactSubjects', 'JWSF_ModuleContactUsSubjs');
DEFINE ('TableContactBCC', 'JWSF_ModuleContactUsBCC');
DEFINE ('TableContactLog', 'JWSF_ModuleContactUsLog');
DEFINE ('TableContactLogFail', 'JWSF_ModuleContactUsLogFail');
DEFINE ('ContactMsgPrompt', 'Please enter your comments here.');
DEFINE ('ContactInvalidRecip', 'You must choose a recipient from the list');
DEFINE ('ContactInvalidSubject', 'You must choose a subject from the list');
DEFINE ('ContactInvalidComment', 'You must enter a new message');
DEFINE ('ContactInvalidName', 'You must enter a valid name');
DEFINE ('ContactInvalidEmail', 'You must enter a valid email address');


class jwsfContactObject {

    function jwsfContactObject () {

        $sql = NULL;
        $result = NULL;
        $data = NULL;
        $this->recips = array();
        $this->subjects = array();
        $this->bcc = null;
        $this->validRecipIds = array();
        $this->validSubjectIds = array();

    }

    // Return an array(id => name)
    // Assigns $this->validRecipIds an array(id)
    //      and $this->recips an array (id => name)
    function getRecipientList () {
        $sql = "SELECT id, item, value FROM `"
                . TableContactRecips
                . "` ORDER BY item ASC;";
        $result = $GLOBALS['db']->query($sql, __FILE__, __LINE__);
        while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
            $id = $row['id'];
            $this->validRecipIds[] = $id;
            $this->recips[$id] = $row['item'];
            // This makes data available to receipt upon submission
            if (isset($_POST['contactRecipient'])
                    && ($_POST['contactRecipient'] == $id)
                    ) {
                $this->recip = $row['item'];
                $this->recipEmail = $row['value'];
            }
        }
        return $this->recips;
    }

    // Return an array(id => name)
    // Assigns $this->validRecipIds an array(id)
    //      and $this->recips an array (id => name)
    function getSubjectList () {
        // Read subject list
        $sql = "SELECT id, item FROM `"
                . TableContactSubjects
                . "` ORDER BY item ASC;";
        $result = $GLOBALS['db']->query($sql, __FILE__, __LINE__);
        while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
            $id = $row['id'];
            $this->validSubjectIds[] = $id;
            $this->subjects[$id] = $row['item'];
            // This makes data available to receipt upon submission
            if (isset($_POST['contactSubject'])
                    && ($_POST['contactSubject'] == $id)
                    ) {
                $this->subject = $row['item'];
            }
        }
        return $this->subjects;
    }

    function getBccList () {
        // Read bcc list
        $sql = "SELECT id, value FROM `"
                . TableContactBCC
                . "` ORDER BY value ASC;";
        $result = $GLOBALS['db']->query($sql, __FILE__, __LINE__);
        while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
            $id = $row['id'];
            $this->bcc .= $row['value'] . ',';
        }
        $this->bcc = substr($this->bcc, 0, -1);
        return $this->bcc;
    }

}


/***** Instantiate Contact Information Object *********************************/
$jwsfContactObject = new jwsfContactObject ();

/***** Construct Form Elements *************************************************
 *
 *  Form elements we wish to display:
 *
 *      Message         contactComments     (required)
 *      Recipient       contactRecipient    (required)
 *      Subject         contactSubject      (required)
 *      Name            contactName         (required)
 *      Email           contactEmail        (required)
 *      Company Name    contactCompany
 *      Telephone       contactTelephone
 *      Fax             contactFax
 *      Address         contactAddress
 *      City            contactCity
 *      State/Province  contactState
 *      Country         contactCountry
 *      Zip/Postal      contactZip
 *      Send            contactSubmit
 *
 ******************************************************************************/


$contactFormTags = array();
$contactFormTags[] = '<div id="id_containerContactForm">';
    $contactFormTags['contactFormHeader'] = '<h2>Contact Us Directly Today!</h2>';
    $contactFormTags['openForm'] = jwsfGenerateForm (
                                            'contactForm',
                                            'Use this form to contact us!'
                                            );
        $contactFormTags [] = '<div id="id_containerContactFormMessage">';
            $contactFormTags['openFieldsetMessage'] = jwsfGenerateFieldsetWithLegend (
                                                    'Please Enter Your Message Below'
                                                    );
                $contactFormTags['div01'] = '<div id="id_containerRecipient">';
                    $contactFormTags['contactRecipientLabel'] = jwsfGenerateLabel (
                                                            'contactRecipient',
                                                            'Select a Recipient:'
                                                            );
                    $contactFormTags['contactRecipient'] = jwsfGenerateSelectFromArray (
                                                            $jwsfContactObject->getRecipientList(),
                                                            'contactRecipient'
                                                            );
                $contactFormTags['div02'] = '</div><div id="id_containerSubject">';
                    $contactFormTags['contactSubjectLabel'] = jwsfGenerateLabel (
                                                            'contactSubject',
                                                            'Select a Subject:'
                                                            );
                    $contactFormTags['contactSubject'] = jwsfGenerateSelectFromArray (
                                                            $jwsfContactObject->getSubjectList(),
                                                            'contactSubject'
                                                            );
                $contactFormTags['div03'] = '</div><div id="id_containerComments">';
                    $contactFormTags['contactCommentsLabel'] = jwsfGenerateLabel (
                                                            'contactComments',
                                                            '*Enter Your Comments:'
                                                            );
                    $contactFormTags['contactComments'] = jwsfGenerateInputArea (
                                                            'contactComments'
                                                            );
                $contactFormTags['div01a'] = '</div>';
            $contactFormTags['closeFieldsetMessage'] = jwsfCloseFieldset ();
        $contactFormTags [] = '</div>';

        // Display fields for unknown users
        if (!isset($GLOBALS['userObject'])
                || ($GLOBALS['userObject']->acslvl < 1)
                ) {
            $contactFormTags [] = '<div id="id_containerContactFormPersonal">';
                $contactFormTags['openFieldsetUserContact'] = jwsfGenerateFieldsetWithLegend (
                                                        'About You'
                                                        );
                    $contactFormTags['div04'] = '<div id="id_containerName">';
                        $contactFormTags['contactNameLabel'] = jwsfGenerateLabel (
                                                                'contactName',
                                                                '*Your Name:'
                                                                );
                        $contactFormTags['contactName'] = jwsfGenerateInputBox (
                                                                'contactName'
                                                                );
                    $contactFormTags['div05'] = '</div><div id="id_containerEmail">';
                        $contactFormTags['contactEmailLabel'] = jwsfGenerateLabel (
                                                                'contactEmail',
                                                                '*Your Email:'
                                                                );
                        $contactFormTags['contactEmail'] = jwsfGenerateInputBox (
                                                                'contactEmail'
                                                                );
                    $contactFormTags['div06'] = '</div><div id="id_containerCompany">';
                        $contactFormTags['contactCompanyLabel'] = jwsfGenerateLabel (
                                                                'contactCompany',
                                                                'Your Company:'
                                                                );
                        $contactFormTags['contactCompany'] = jwsfGenerateInputBox (
                                                                'contactCompany'
                                                                );
                    $contactFormTags['div07'] = '</div><div id="id_containerTelephone">';
                        $contactFormTags['contactTelephoneLabel'] = jwsfGenerateLabel (
                                                                'contactTelephone',
                                                                'Your Telephone:'
                                                                );
                        $contactFormTags['contactTelephone'] = jwsfGenerateInputBox (
                                                                'contactTelephone'
                                                                );
                    $contactFormTags['div08'] = '</div><div id="id_containerFax">';
                        $contactFormTags['contactFaxLabel'] = jwsfGenerateLabel (
                                                                'contactFax',
                                                                'Your Fax:'
                                                                );
                        $contactFormTags['contactFax'] = jwsfGenerateInputBox (
                                                                'contactFax'
                                                                );
                    $contactFormTags['div08a'] = '</div>';
                $contactFormTags['closeFieldsetUserContact'] = jwsfCloseFieldset ();
            $contactFormTags [] = '</div>';
            $contactFormTags [] = '<div id="id_containerContactFormLocation">';
                $contactFormTags['openFieldsetLocation'] = jwsfGenerateFieldsetWithLegend (
                                                        'About Your Location'
                                                        );
                    $contactFormTags['div09'] = '<div id="id_containerAddress">';
                        $contactFormTags['contactAddressLabel'] = jwsfGenerateLabel (
                                                                'contactAddress',
                                                                'Address Line 1:'
                                                                );
                        $contactFormTags['contactAddress'] = jwsfGenerateInputBox (
                                                                'contactAddress'
                                                                );
                    $contactFormTags['div10'] = '</div><div id="id_containerAddress2">';
                        $contactFormTags['contactAddress2Label'] = jwsfGenerateLabel (
                                                                'contactAddress2',
                                                                'Address Line 2:'
                                                                );
                        $contactFormTags['contactAddress2'] = jwsfGenerateInputBox (
                                                                'contactAddress2'
                                                                 );
                    $contactFormTags['div11'] = '</div><div id="id_containerCity">';
                        $contactFormTags['contactCityLabel'] = jwsfGenerateLabel (
                                                                'contactCity',
                                                                'Your City:'
                                                                );
                        $contactFormTags['contactCity'] = jwsfGenerateInputBox (
                                                                'contactCity'
                                                                );
                    $contactFormTags['div12'] = '</div><div id="id_containerState">';
                        $contactFormTags['contactStateLabel'] = jwsfGenerateLabel (
                                                                'contactState',
                                                                'Your State:'
                                                                );
                        $contactFormTags['contactState'] = jwsfGenerateStateProvinceSelect (
                                                                'ON',
                                                                'contactState'
                                                                );
                    $contactFormTags['div13'] = '</div><div id="id_containerCountry">';
                        $contactFormTags['contactCountryLabel'] = jwsfGenerateLabel (
                                                                'contactCountry',
                                                                'Your Country:'
                                                                );
                        $contactFormTags['contactCountry'] = jwsfGenerateCountrySelect (
                                                                'CA',
                                                                'contactCountry'
                                                                );
                    $contactFormTags['div14'] = '</div><div id="id_containerZip">';
                        $contactFormTags['contactZipLabel'] = jwsfGenerateLabel (
                                                                'contactZip',
                                                                'Your Zip Code:'
                                                                );
                        $contactFormTags['contactZip'] = jwsfGenerateInputBox (
                                                                'contactZip'
                                                                );
                    $contactFormTags['div15'] = '</div>';

                $contactFormTags['closeFieldsetUserLocation'] = jwsfCloseFieldset ();
            $contactFormTags [] = '</div>';
            $contactFormTags['div16'] = '<div id="id_containerSpecialOffers">';
                $contactFormTags['contactSpecialOffersLabel'] = jwsfGenerateLabel (
                                                        'contactSpecialOffers',
                                                        'Receive important email from this site only'
                                                        );
                $contactFormTags['contactSpecialOffers'] = jwsfGenerateCheckRadio (
                                                        'contactSpecialOffers',
                                                        'true',
                                                        '',
                                                        '',
                                                        true
                                                        );
            // Check for captcha config switch
            if ($GLOBALS['jwsfConfig']->ProtectionCaptcha == 'true') {
                // Create captcha values
                $_SESSION['captcha'] = array(rand(0,9), rand(0,9));
                // Output image, form tags
                        $contactFormTags['contactCaptchaLabel'] = jwsfGenerateLabel (
                                                                'contactCaptcha',
                                                                'Please provide sufficient evidence'
                                                                . ' that you are not a robot by solving'
                                                                . ' the following mathematical puzzle:'
                                                                );
                        $contactFormTags['contactCaptchaImage'] = '<img'
                                                            . ' src="/_img/php/captcha.php"'
                                                            . ' title="Image verification"'
                                                            . ' alt="Image verification"'
                                                            . ' />';
                        $contactFormTags['contactCaptchaInput'] = jwsfGenerateInputBox (
                                                                'contactCaptcha'
                                                                );
            }
            $contactFormTags['div16a'] = '</div>';
        }
        $contactFormTags['div17'] = '<div id="id_containerSubmit">';
            $contactFormTags['contactSubmit'] = jwsfGenerateInputButton (
                                                    'contactSubmit'
                                                );
        $contactFormTags['div18'] = '</div>';
    $contactFormTags['closeForm'] = jwsfCloseForm ();
$contactFormTags[] = "</div>";

$GLOBALS['smarty']->assign ('contactForm', $contactFormTags);


/***** Validate Form Elements **************************************************
 *
 *
 *
 ******************************************************************************/
if (isset($_POST['contactSubmit'])) {
    // Validate data (This could be stricter)
    $contactErrors = array();

    // Required fields
    if (!isset($_POST['contactRecipient'])
            || !is_numeric($_POST['contactRecipient'])
            || !in_array($_POST['contactRecipient'],
                    $jwsfContactObject->validRecipIds))
        $contactErrors[] = ContactInvalidRecip;

    if (!isset($_POST['contactSubject'])
            || !is_numeric($_POST['contactSubject'])
            || !in_array($_POST['contactSubject'],
                    $jwsfContactObject->validSubjectIds))
        $contactErrors[] = ContactInvalidSubject;

    if (!isset($_POST['contactComments'])
            || ($_POST['contactComments'] == null)
            || ($_POST['contactComments'] == ContactMsgPrompt))
        $contactErrors[] = ContactInvalidComment;

    if ($GLOBALS['userObject']->acslvl < 1) {
        if (!isset($_POST['contactName'])
                || ($_POST['contactName'] == null))
            $contactErrors[] = ContactInvalidName;

        if (!isset($_POST['contactEmail'])
                || (eregi('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,6})$', $_POST['contactEmail']) == false))
            $contactErrors[] = ContactInvalidEmail;
    }

    // Captcha
    if (($GLOBALS['jwsfConfig']->ProtectionCaptcha == 'true')
            && ($GLOBALS['userObject']->acslvl < 1)
            ){
        // Get validation code from session
        $validation = $_SESSION['captchaValidation'];
        if (!isset($_POST['contactCaptcha'])
                || ($_POST['contactCaptcha'] == null)
                || ($_POST['contactCaptcha'] != $validation))
            $contactErrors[] = 'You must enter the proper validation code';
    }

    // Extraneous field validation would go here

    // Proceed to log and send comments if everything is ok
    if (empty($contactErrors)) {
        /* Upon success
            log to database
            send email
            set contactSuccess
            set receipt variables according to selected values
            display receipt (template logic)
        */
        if ($GLOBALS['userObject']->acslvl > 0) {
            $name = $GLOBALS['userObject']->nameFirst
                    . ' ' . $GLOBALS['userObject']->nameLast;
            $email = $GLOBALS['userObject']->email;
            $company = $GLOBALS['userObject']->nameCompany;
            $telephone = $GLOBALS['userObject']->telephone;
            $address = $GLOBALS['userObject']->address;
            $address2 = $GLOBALS['userObject']->address2;
            $city = $GLOBALS['userObject']->city;
            $state = $GLOBALS['userObject']->state;
            $country = $GLOBALS['userObject']->country;
            $zip = $GLOBALS['userObject']->zip;
            $fax = $GLOBALS['userObject']->fax;
        } else {
            $name = $_POST['contactName'];
            $email = $_POST['contactEmail'];
            $company = $_POST['contactCompany'];
            $telephone = $_POST['contactTelephone'];
            $address = $_POST['contactAddress'];
            $address2 = $_POST['contactAddress2'];
            $city = $_POST['contactCity'];
            $state = $_POST['contactState'];
            $country = $_POST['contactCountry'];
            $zip = $_POST['contactZip'];
            $fax = $_POST['contactFax'];
        }

        $sql = "INSERT INTO `"
                . TableContactLog
                . "` (
                    recipient,
                    subject,
                    name,
                    email,
                    comments,
                    company,
                    telephone,
                    address,
                    address2,
                    city,
                    state,
                    country,
                    zip,
                    fax,
                    ip
                ) VALUES (
                    '" . $_POST['contactRecipient'] . "',
                    '" . $_POST['contactSubject'] . "',
                    '" . $name . "',
                    '" . $email . "',
                    '" . $_POST['contactComments'] . "',
                    '" . $company . "',
                    '" . $telephone . "',
                    '" . $address . "',
                    '" . $address2 . "',
                    '" . $city . "',
                    '" . $state . "',
                    '" . $country . "',
                    '" . $zip . "',
                    '" . $fax . "',
                    '" . $_SERVER['REMOTE_ADDR'] . "'
                    );";
        $result = $GLOBALS['db']->query($sql, __FILE__, __LINE__);

        // Construct email
        $to = $jwsfContactObject->recip
                . " <" . $jwsfContactObject->recipEmail . ">";
        $subject = $GLOBALS['jwsfConfig']->SiteTitleShort . ' :: '
                . $jwsfContactObject->subject;
        $message = strip_tags($_POST['contactComments']);
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/plain; charset=us-ascii' . "\r\n";
        $headers .= 'From: ' . $name
                . ' <' . $email . '>' . "\r\n";
        $headers .= 'Reply-To: ' . $name
                . ' <' . $email . '>' . "\r\n";
        $headers .= 'Bcc: ' . $jwsfContactObject->getBccList();
        $headers .= "\r\n";

        // Mail it
        if (mail($to, $subject, $message, $headers) == true) {
            $GLOBALS['smarty']->assign('contactSuccess', 1);
            $GLOBALS['smarty']->assign('contactRecip',
                                            $jwsfContactObject->recip);
            $GLOBALS['smarty']->assign('contactSubject',
                                            $jwsfContactObject->subject);
            $GLOBALS['messagesSuccess'][] = 'Thank you for your message!';
        } else {
            $contactErrors[] =
                    "Message was not accepted by server!  Please try again.";
        }
    }

    // Register errors
    if (!empty($contactErrors)) {
        // Make errors available externally
        $GLOBALS['messagesError'] =
                    array_merge($GLOBALS['messagesError'], $contactErrors);
    }


}
?>
