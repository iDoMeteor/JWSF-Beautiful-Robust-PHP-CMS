<?php

/*******************************************************************************
 *  JWSF Slim Frame User Sign-up Module
 *
 *  $Revision: 1.1.1.1 $
 *
 *  TODO:
 *      Incorporate pay features and make a switch for them
 *
 ******************************************************************************/

/* Set method of operation */
$type = 'create';
$nochange = false;

/* These should be moved to the language table.. not the strings table.. */
$arrayLabels = array();
$arrayLabels['formId'] = 'id_formUserCreate';
$arrayLabels['formTitle'] = 'Use this form to create a user account!';
$arrayLabels['legend'] = 'Create a user account';
$arrayLabels['nameFirst'] = '*First name:';
$arrayLabels['nameMiddle'] = 'Middle name:';
$arrayLabels['nameLast'] = '*Last name:';
$arrayLabels['nameCompany'] = 'Company Name:';
$arrayLabels['email'] = '*Primary email:';
$arrayLabels['emailv'] = '*Verify primary:';
$arrayLabels['email2'] = 'Secondary email:';
$arrayLabels['telephone'] = '*Primary telephone';
$arrayLabels['telephone2'] = 'Secondary telephone:';
$arrayLabels['fax'] = 'Fax:';
$arrayLabels['address'] = '*Address 1:';
$arrayLabels['address2'] = 'Address  2:';
$arrayLabels['city'] = '*City:';
$arrayLabels['state'] = '*State:';
$arrayLabels['zip'] = '*Zip:';
$arrayLabels['country'] ='*Country:';
$arrayLabels['language'] = 'Primary Language:';
$arrayLabels['language2'] = 'Secondary Language:';
$arrayLabels['password'] = 'Create Password:';
$arrayLabels['passwordv'] = '*Verify password:';
$arrayLabels['secretQuestion'] = 'Secret question:';
$arrayLabels['secretAnswer'] = 'Secret answer:';
$arrayLabels['secretAnswerv'] = 'Verify answer:';
$arrayLabels['specialOffers'] = 'Receive special offers:';
$arrayLabels['submit'] = 'Register';


/***** Begin Program Flow *****************************************************/
/***** Process User Activation *************************************************
 *
 *  Check activation key
 *      If valid
 *          Set active flag to true
 *          Set access level to 1 (standard user)
 *          Send welcome email template
 *          Display verification, link to login
 *      If invalid
 *          Log to database
 *          Direct to tech support
 *
 ******************************************************************************/

if (isset($_GET['activationKey'])) {

    // De-crypt activation key
    $email = str_rot13(rawurldecode($_GET['activationKey']));
    $email = explode ('|', $email);
    $email = strlen($email[0] > 320) ? null : $email[0];

    // Check account existence
    $user = array();
    $sql = 'SELECT id, active, acslvl FROM ' . TableUsers .
        ' WHERE email="' . $email . '";';
    $result = $GLOBALS['db']->query ($sql, __FILE__, __LINE__);
    $row = $GLOBALS['db']->fetch(__FILE__, __LINE__);
    foreach ($row as $key => $value) {
        $user[$key] = $value;
    }

    if (($user['id'] > 0) && ($user['active'] == 0) && ($user['acslvl'] == 0)) {
        // We have a valid user account and activation key

        $sql = 'UPDATE ' . TableUsers . ' SET active=1, acslvl=1'
            . ' WHERE id=' . $user['id'] . ' LIMIT 1;';
        $GLOBALS['db']->query($sql, __FILE__, __LINE__);
        if ($GLOBALS['db']->rowsAffected() > 0) {

            $GLOBALS['messagesSuccess'][] = 'Account activated, you may now log in';

            // Send welcome email
            $welcomeMessage =
                "Thank you for activating your account!"
                . "\n\r"
                . "Sincerely,\n\r" . $GLOBALS['jwsfConfig']->NewUserEmailTagline
                ;
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/plain; charset=us-ascii' . "\r\n";
            $headers .= 'From: ' . $GLOBALS['jwsfConfig']->NewUserEmailFrom . "\r\n";
            $headers .= 'Reply-To: ' . $GLOBALS['jwsfConfig']->NewUserEmailFrom . "\r\n";
            /* This should be filled from BCC database table
            $headers .= 'Bcc: ';
            */
            $headers .= "\r\n";

            // Mail it
            if (mail($email, $GLOBALS['jwsfConfig']->NewUserSubjectWelcome, $welcomeMessage, $headers) == false) {
                $GLOBALS['messagesError'][] = 'Your welcome message was not accepted by server!'
                    . ' Please contact technical support as your account has been'
                    . ' created but not activated.';
            } else {
                $GLOBALS['messagesSuccess'][] = 'Your welcome email has been sent';
            }
            $GLOBALS['smarty']->assign('templ', 'activationSuccess');

        } else {
            $GLOBALS['smarty']->assign('templ', 'activationFailure');
        }
    } else {
        // We have an invalid validation attempt

        $GLOBALS['messagesError'] = ($user['id'] != 0)
            ?
            $GLOBALS['messagesError'][] = 'You may only activate your account once'
            :
            $GLOBALS['messagesError'][] = 'Your activation key was invalid';

        $sql = 'INSERT INTO `' . TableUserFailedActivation
            . '` (email, crypt) VALUES ("' . $email . '", "'
            . $_GET['activationKey'] . '");';
        $GLOBALS['db']->query($sql, __FILE__, __LINE__);
        $GLOBALS['smarty']->assign('templ', 'activationFailure');
    }


} else {
    $errors = array();
    if (($GLOBALS['userObject']->id == 0)
            && ($GLOBALS['userObject']->acslvl == 0)
            ) {
        // Create temp user object
        $tempUserObject = new userObject ();
        // Process form submission
        if (isset($_POST['userFormSubmit'])) {
            // This should maybe be improved.. may be succeptable to buffer overflow
            foreach ($_POST as $key => $value) {
                $tempUserObject->$key = $value;
            }
            // Validate form information
            $tempUserObject->validateFormUserData($type);
            if (empty($tempUserObject->errors)) {
                // Create user
                if ($tempUserObject->create() == true) {
                    // Let user know we succeeded
                    $GLOBALS['messagesSuccess'][] = 'Account created';
                    $GLOBALS['smarty']->assign ('templ', 'creationSuccess');
                } else {
                    $nochange = true;
                    $errors[] = 'The was an error and your account was not'
                        . ' created.  Please use the contact form to alert'
                        . ' technical support!';
                }
            } else {
                $errors = array_merge($errors, $tempUserObject->errors);
                // Set posted values to reload in form
            }
        } else {
            $tempUserObject->setGuestTemplate();
            $tempUserObject->active = $GLOBALS['jwsfConfig']->UserDefaultActive;
            $tempUserObject->acslvl = $GLOBALS['jwsfConfig']->UserDefaultLevel;
            $tempUserObject->nameFirst = null;
            $tempUserObject->secretQuestion = null;
            $tempUserObject->specialOffers = true;
            $tempUserObject->welcomeEmail = true;
        }
    $tempUserObject->userFormLabels = $arrayLabels;
    // Construct form
    $tempUserObject->constructUserForm ($type);
    $GLOBALS['smarty']->assign ('template', 'userDataForm');
    } else {
        $nochange = true;
        $errors[] = "You cannot register a new user if you are logged in";
    }
}
/***** Register Errors ********************************************************/
if (!empty($errors)) {
    if ($nochange == false) {
        // Give warning about reset fields
        $errors[] = 'Your submission contained errors, please try again';
    }
    // Make errors available externally
    $GLOBALS['messagesError'] = array_merge($GLOBALS['messagesError'], $errors);
}




?>
