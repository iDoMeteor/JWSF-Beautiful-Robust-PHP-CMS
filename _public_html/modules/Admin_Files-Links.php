<?php

/* ***** File & Link Administration ********************************************
 *
 *  $Revision: 1.1.1.1 $
 *
 *  The goal of this area is to provide an effective, easy to manage way of
 *  maintaining file and link resources in a continuous way throughout the
 *  site.  A user can add new or existing link/file resources via a concise
 *  two step process or edit/reassign/remove the resources.  Each resource
 *  may be assigned to any number of pages and/or content blocks.  The access
 *  level required for that particular page or block will also be applied to
 *  the resource.  All resources will be listed on the download or link page
 *  as appropriate, regardless of access level.
 *
 *  TODO:
 *      Integrate access level assignment when needed
 *
 ******************************************************************************/

// Get pages for assocation
$GLOBALS['jwsfPage']->getActives();
$GLOBALS['smarty']->assign('pagesActive', $GLOBALS['jwsfPage']->actives);
$GLOBALS['jwsfPage']->getInactives();
$GLOBALS['smarty']->assign('pagesInactive', $GLOBALS['jwsfPage']->inactives);
$GLOBALS['jwsfPage']->getOrphans();
$GLOBALS['smarty']->assign('pagesOrphan', $GLOBALS['jwsfPage']->orphans);

if (isset($_POST['activate'])) {
    $GLOBALS['jwsfPage']->resourceActivate();
} elseif (isset($_POST['assignment_delete'])) {
    $GLOBALS['jwsfPage']->resourceAssignmentDelete();
} elseif ((isset($_POST['assignment_edit'])
        || isset($_POST['assID']))
        && !isset($_POST['assignment_deleteConfirmation'])
        ) {
    $GLOBALS['jwsfPage']->resourceAssignmentEdit();
    $GLOBALS['jwsfPage']->id = (isset($_POST['editID']))
                            ? $_POST['editID']
                            : $GLOBALS['jwsfPage']->resourceAssignment['pid'];
    $GLOBALS['jwsfPage']->populate(); // Ensures updates are applied
    $GLOBALS['jwsfPage']->editContent();
    $GLOBALS['smarty']->register_object('page', $GLOBALS['jwsfPage']);
    $GLOBALS['smarty']->assign('data', $GLOBALS['jwsfPage']->content);
} elseif (isset($_POST['deactivate'])) {
    $GLOBALS['jwsfPage']->resourceDeactivate();
} elseif (isset($_POST['file_deleteAll'])) {
    $GLOBALS['jwsfPage']->fileDeleteValid();
} elseif (isset($_POST['file_deleteLost'])) {
    $GLOBALS['jwsfPage']->fileDeleteLost();
} elseif (isset($_POST['file_deleteOrphan'])) {
    $GLOBALS['jwsfPage']->fileDeleteOrphan();
} elseif (isset($_POST['hide'])) {
    $GLOBALS['jwsfPage']->resourceHide();
} elseif (isset($_POST['file_editValid'])) {
    $GLOBALS['jwsfPage']->fileEditValid();
} elseif (isset($_POST['meta_save'])) {
    $GLOBALS['jwsfPage']->metaSave();
} elseif (isset($_POST['show'])) {
    $GLOBALS['jwsfPage']->resourceShow();
}

if (isset($_POST['meta_repair'])) {
    $GLOBALS['jwsfPage']->filesDetermineMetaValues();
    $GLOBALS['jwsfPage']->fileInsertMeta();
}

if (isset($_POST['file_upload'])) {
    $GLOBALS['jwsfPage']->filesDetermineUploadValues();
    $GLOBALS['jwsfPage']->fileUploadStandard();
}

if (isset($_POST['action'])) {
    switch ($_POST['action']) {
        case 'Save':
            $GLOBALS['jwsfPage']->filesDetermineUploadValues();
            $GLOBALS['jwsfPage']->filesDetermineMetaValues();
            $GLOBALS['jwsfPage']->fileInsert();
            break;
        case 'Update':
            if (isset($_POST['assID'])) {
                $GLOBALS['jwsfPage']->resourceAssignmentUpdate();
            } else {
                $GLOBALS['jwsfPage']->filesDetermineUploadValues();
                $GLOBALS['jwsfPage']->filesDetermineMetaValues();
                $GLOBALS['jwsfPage']->fileUpdateMeta();
            }
            break;
        case 'Next':
            // Get page info for content assignment
            $GLOBALS['jwsfPage']->populate(); // Ensures updates are applied
            $GLOBALS['jwsfPage']->editContent();
            $GLOBALS['smarty']->register_object('page', $GLOBALS['jwsfPage']);
            $GLOBALS['smarty']->assign('data', $GLOBALS['jwsfPage']->content);
            // Assignment editor, second stage
            if (isset($_POST['preExistingResource'])
                    && ($_POST['preExistingResource'] == 1)
                    ) {
            // Add file, using a pre-existing resource
                switch ($_GET['action']) {
                    case 'add-files':
                        // Acquire and categorize existing files
                        $GLOBALS['jwsfPage']->filesListValid();
                        break;
                    case 'add-links':
                        $GLOBALS['jwsfPage']->linksListValid();
                        break;
                }
            }
            break;
    }
}

if ($_GET['action'] == 'edit-files') {
    $GLOBALS['jwsfPage']->filesEditAll();
}


?>