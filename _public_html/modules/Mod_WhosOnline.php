<?php

/*******************************************************************************
 *  JWSF Slim Frame User Administration - List Users
 *
 *  $Revision: 1.1.1.1 $
 *
 *  This module will list all registered users, seperated into groups by
 *  access level, sorted alphabetically.
 *
 *  This list should show the following for each user:
 *      Active Status
 *      Access Level (Will be in group headers)
 *      First & Last Name (as href to edit user)
 *      Company Name
 *      Email address (as href)
 *      Telephone
 *
 ******************************************************************************/


$userListObject = new userObject();
$userListObject->whosOnline();
$GLOBALS['smarty']->assign ('online', $userListObject->online);

?>
