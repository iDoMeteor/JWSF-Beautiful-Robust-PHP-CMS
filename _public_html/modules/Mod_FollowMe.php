<?php

/***** Follow Me - A JWSF Module ***********************************************
 *
 *
 *
 * ****************************************************************************/

DEFINE ('TableFollowMeTypes', 'JWSF_FollowMeTypes');
DEFINE ('TableFollowMeData', 'JWSF_FollowMe');

$types = array();
$data = array();
$out = array();

// Get types
$fields = array('id','value');
$clause = 'ORDER BY so';
$GLOBALS['db']->select($fields, TableFollowMeTypes, $clause, 
    __FILE__, __LINE__);
while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
    $key = $row['id'];
    $types[$key] = $row['value'];
}
// Get data
$fields = array('idType', 'title', 'link', 'icon', 'description');
$clause = 'ORDER BY title';
$GLOBALS['db']->select($fields, TableFollowMeData, $clause, 
    __FILE__, __LINE__);
while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
    $data[] = $row;
}
// Send to Smarty
$GLOBALS['smarty']->assign('followMeTypes', $types);
$GLOBALS['smarty']->assign('followMeData', $data);

?>
