<?php

if (isset($_POST['delete-data'])) {
    select_data();
    delete_data();
    $_GET['action'] = 'select-data';
} elseif (isset($_POST['delete-files'])) {
    select_files();
    delete_files();
    $_GET['action'] = 'select-files';
} elseif (isset($_POST['restore-data'])) {
    select_data();
    restore_data();
    $_GET['action'] = 'select-data';
} elseif (isset($_POST['restore-files'])) {
    select_files();
    restore_files();
    $_GET['action'] = 'select-files';
}

switch ($_GET['action']) {
    case 'backup-data':
        backup_data();
        break;
    case 'backup-files':
        backup_files();
        break;
    case 'select-data':
        select_data();
        break;
    case 'select-files':
        select_files();
        break;

}

function backup_data () {
        $base = 'JWSF-DB-';
        $date = time();
        $location = SiteRoot . '/../dumps/';
        $file = $base . $date . '.sql';
        $command_dump = '/usr/bin/mysqldump -v --user='
                        . dbUser
                        . ' --password='
                        . dbPass
                        . ' '
                        . dbName
                        . ' > '
                        . $location
                        . $file;
        $command_compress = '/usr/bin/gzip -v ' . $location . $file;
        $last_dump = system($command_dump, $result_dump);
        $last_compress = system($command_compress, $result_compress);
        $GLOBALS['messagesNotice'][] = 'Your request is processing in the'
                                        . ' background and may take a moment to'
                                        . ' appear below';
        $GLOBALS['messagesSuccess'][] = 'Database archive '
                                        . $file
                                        . ' created!';
}

function backup_files () {
        $base = 'JWSF-FS-';
        $date = time();
        $location = SiteRoot . '/../dumps/';
        $file = $base . $date . '.tar.gz';
        $targets = SiteRoot
                    . '/../_public_html'
                    . ' '
                    . SiteRoot
                    . '/../public_html'; // Gotta do it this goofy way
        $command_compress = 'tar -czf '
                        . $location
                        . $file
                        . ' '
                        . $targets;
        $last_compress = system($command_compress, $result_compress);
        $GLOBALS['messagesNotice'][] = 'Your request is processing in the'
                                        . ' background and may take a moment to'
                                        . ' appear below';
        $GLOBALS['messagesSuccess'][] = 'File archive '
                                        . $file
                                        . ' created!';
}

function delete_data () {
        $location = SiteRoot . '/../dumps/';
        $id = $_POST['delete-data'];
        $file = $GLOBALS['jwsfPage']->dataBackupFiles[$id];
        $command_remove = 'rm ' . $location . $file;
        $last_remove = system($command_remove, $result_remove);
        $GLOBALS['messagesSuccess'][] = 'Data set '
                                        . $file
                                        . ' removed!';
}

function delete_files () {
        $location = SiteRoot . '/../dumps/';
        $id = $_POST['delete-files'];
        $file = $GLOBALS['jwsfPage']->fileBackupFiles[$id];
        $command_remove = 'rm ' . $location . $file;
        $last_remove = system($command_remove, $result_remove);
        $GLOBALS['messagesSuccess'][] = 'File set '
                                        . $file
                                        . ' removed!';
}

function restore_data () {
        $location = SiteRoot . '/../dumps/';
        $id = $_POST['restore-data'];
        $file = $GLOBALS['jwsfPage']->dataBackupFiles[$id];
        $command_decompress = 'gunzip ' . $location . $file;
        $file = substr($file, 0, -3);
        $command_restore = 'mysql --user='
                        . dbUser
                        . ' --password='
                        . dbPass
                        . ' '
                        . dbName
                        . ' < '
                        . $location
                        . $file;
        $command_compress = 'gzip ' . $location . $file;
        $last_decompress = system($command_decompress, $result_decompress);
        $last_restore = system($command_restore, $result_restore);
        $last_compress = system($command_compress, $result_compress);
        $GLOBALS['messagesSuccess'][] = 'Data set '
                                        . $file
                                        . ' restored!';
}

function restore_files () {
        $location = SiteRoot . '/../dumps/';
        $id = $_POST['restore-files'];
        $file = $GLOBALS['jwsfPage']->fileBackupFiles[$id];
        $command_decompress = 'tar -xzf '
                                . $location
                                . $file
                                . ' --directory '
                                . SiteRoot
                                . '/../';
        $last_decompress = system($command_decompress, $result_decompress);
        $GLOBALS['messagesNotice'][] = 'No files are removed in this process';
        $GLOBALS['messagesSuccess'][] = 'File set '
                                        . $file
                                        . ' restored!';
}

function select_data () {
    $valid = array();
    // Read available files into array
    $location = SiteRoot . '/../dumps/';
    $base = 'JWSF-DB-';
    $ext = '.gz';
    $extInvalid = '.tar.gz';
    $files = dir_as_file_array($location);
    // Eliminate superfluous entries
    foreach ($files as $file) {
        if (strpos($file, $base) === 0) {
            if(strpos($file, $ext)
                    && !strpos($file, $extInvalid)
                    ) {
                $valid[] = $file;
            }
        }
    }
    // Register array with smarty
    $GLOBALS['jwsfPage']->dataBackupFiles = array_reverse($valid);
}

function select_files () {
        $valid = array();
        // Read available files into array
        $location = SiteRoot . '/../dumps/';
        $base = 'JWSF-FS-';
        $ext = '.tar.gz';
        $files = dir_as_file_array($location);
        // Eliminate superfluous entries
        foreach ($files as $file) {
            if (strpos($file, $base) === 0) {
                if(strpos($file, $ext)) {
                    $valid[] = $file;
                }
            }
        }
        // Register array with smarty
        $GLOBALS['jwsfPage']->fileBackupFiles = array_reverse($valid);
}

?>
