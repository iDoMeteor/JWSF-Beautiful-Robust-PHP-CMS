<?php

/*******************************************************************************
 *  JWSF Slim Frame User Sign-up Module
 *
 *  $Revision: 1.1.1.1 $
 *
 *  This should be kept and moved to the admin area
 *
 *  First run through we simply display the form
 *
 *  Second pass performs validation
 *      Upon successful
 *          Log to database
 *          Send welcome email
 *          Assign Smarty values
 *          Display receipt
 *      Upon failure
 *          Assign Smarty values
 *          Back to step one with user entered data
 *
 *  TODO:
 *      Keep in mind this will be ported to a 'user control panel'!!
 *
 ******************************************************************************/


/* Set method of operation */
$type = 'update';
$update = false;
$nochange = false;

/*
    Determine user id to operate on, if it is not being set via
    get (coming from user list) or post (coming from search or update)
    then set it to the current user's id (so they can edit themselves)
*/
// Need to ensure regular user cannot load or submit data for another id
if (isset($_POST['editId'])) {
    $id = $_POST['editId'];
    $update = true;
} elseif (isset($_GET['UserId'])) {
    $id = $_GET['UserId'];
} else {
    $id = 0;
}

/*  This would be for user cp, not for admins
    otherwise they would edit themselves on
    every first visit to page
 else
    $id = $GLOBALS['userObject']->id;
*/

/* These should be moved to the language table.. not the strings table.. */
$arrayLabels = array();
$arrayLabels['formId'] = 'id_formUserEdit';
$arrayLabels['formTitle'] = 'Use this form to edit a user account!';
$arrayLabels['legend'] = 'Edit a user account';
$arrayLabels['active'] = 'Active:';
$arrayLabels['acslvl'] = 'Access Level:';
$arrayLabels['nameFirst'] = '*First name:';
$arrayLabels['nameMiddle'] = 'Middle name:';
$arrayLabels['nameLast'] = '*Last name:';
$arrayLabels['nameCompany'] = 'Company Name:';
$arrayLabels['email'] = '*Primary email:';
$arrayLabels['emailv'] = '*Verify primary:';
$arrayLabels['email2'] = 'Secondary email:';
$arrayLabels['telephone'] = 'Primary telephone';
$arrayLabels['telephone2'] = 'Secondary telephone:';
$arrayLabels['fax'] = 'Fax:';
$arrayLabels['address'] = '*Address 1:';
$arrayLabels['address2'] = 'Address  2:';
$arrayLabels['city'] = '*City:';
$arrayLabels['state'] = '*State:';
$arrayLabels['zip'] = '*Zip:';
$arrayLabels['country'] ='*Country:';
$arrayLabels['language'] = 'Language:';
$arrayLabels['language2'] = 'Language 2:';
$arrayLabels['password'] = 'Change Password:';
$arrayLabels['passwordv'] = '*Verify password:';
$arrayLabels['secretQuestion'] = 'Secret question:';
$arrayLabels['secretAnswer'] = 'Secret answer:';
$arrayLabels['secretAnswerv'] = 'Verify answer:';
$arrayLabels['welcomeEmail'] = 'Send welcome email:';
$arrayLabels['specialOffers'] = 'Special offers:';
$arrayLabels['stampCreated'] = 'Creation Date:';
$arrayLabels['stampLogin'] = 'Last Login:';
$arrayLabels['stampModified'] = 'Last Page View:';
$arrayLabels['loginsFailed'] = 'Recent Failed Logins:';
$arrayLabels['loginsFailedTotal'] = 'Lifetime Failed Logins:';
$arrayLabels['loginsSuccessTotal'] = 'Lifetime Successful Logins:';
$arrayLabels['deleteUser'] = 'Delete this user permanently:';
$arrayLabels['submit'] = 'Update';


/***** Begin Program Flow *****************************************************/
$errors = array();

/***** Unlocking a User? ******************************************************/
if (isset($_GET['unlock'])
        && ($_GET['unlock'] == 'true')
        ) {
    $sql = 'UPDATE '
        . TableUsers
        . ' SET loginsFailed=0'
        . ' WHERE id='
        . $_GET['UserId']
        . ' LIMIT 1;'
        ;
    $result = $GLOBALS['db']->query ($sql, __FILE__, __LINE__);
}

// Request to update user data
if (($id > 0)
        && ($GLOBALS['userObject']->checkAdmin() == true)
        ) {
    // Validate edit id against valid user ids
    if ($GLOBALS['userObject']->validateId ($id) == true) {
        // ID is valid, create user object
        $tempUserObject = new userObject ();
        // Load user
        $tempUserObject->edit($id);
        // Make sure user doesn't go over their head,
        // but can still edit themselves
        if ($tempUserObject->id == $GLOBALS['userObject']->id) {
            $errors[] = "To edit yourself, use the 'Your Settings' page";
        } elseif ($tempUserObject->acslvl < $GLOBALS['userObject']->acslvl) {
            // Process form submission
            if ($update == true) {
                // Validate form information
                $tempUserObject->validateFormUserData($type);
                if (empty($tempUserObject->errors)) {
                    // Update user
                    if ($tempUserObject->update() == true) {
                        // Load modified information
                        $tempUserObject->edit($id);
                        // Let user know we succedded
                        $GLOBALS['smarty']->assign ('success', true);
                    } else {
                        $nochange = true;
                        $errors[] = 'Your information was *not* updated';
                    }
                } else
                    $errors = array_merge($errors, $tempUserObject->errors);
            }
            if (!isset($_POST['deleteUser'])
                    || ($_POST['deleteUser'] != 'true')) {
                // Assign form labels
                $tempUserObject->userFormLabels = $arrayLabels;
                // Quick hack
                $tempUserObject->welcomeEmail = 'false';
                // Construct form
                $tempUserObject->constructUserForm ($type);
                $GLOBALS['smarty']->assign ('template', 'userDataForm');
            }
        } else {
            $nochange = true;
            $errors[] = 'You cannot edit a user equal to or greater'
                    . ' than your own level';
        }
    } else {
        // Invalid id, set error
        $nochange = true;
        $errors[] = 'Invalid user id';
    }
} elseif (isset($_POST['userSearch'])
        && isset($_POST['searchTerm'])
        ) {
    if ($GLOBALS['userObject']->searchUsers($_POST['searchTerm']) == true) {
        $GLOBALS['smarty']->assign ('template', 'searchResults');
        $GLOBALS['smarty']->assign ('results',
                $GLOBALS['userObject']->searchResults);
    } else {
        $errors[] = 'Your search did not return any results';
    }

} elseif ($GLOBALS['userObject']->checkAdmin() == true) {
    // No request, display admin list
    $GLOBALS['userObject']->adminList();
    $GLOBALS['smarty']->assign ('admins', $GLOBALS['userObject']->users);
    $GLOBALS['smarty']->assign ('template', 'admins');
} else {
    $errors[] = 'Only administrators may use this';
    $nochange = true;
    $GLOBALS['smarty']->assign ('template', 'none');
}
// Always display
if ($GLOBALS['userObject']->checkAdmin() == true) {
    // Always display search form
    $GLOBALS['userObject']->generateUserSearchForm();
}


/***** Register Errors ********************************************************/
if (!empty($errors)) {
    if ($nochange == false) {
        // Give warning about reset fields
        $errors[] = 'Your submission contained errors, please try again';
    }
    // Make errors available externally
    $GLOBALS['messagesError'] = array_merge($GLOBALS['messagesError'], $errors);
}



?>
