<?php

/*******************************************************************************
 *  JWSF Slim Frame User Sign-up Module
 *
 *  $Revision: 1.1.1.1 $
 *
 *  TODO:
 *      Incorporate pay features and make a switch for them
 *
 ******************************************************************************/

/* Set method of operation */
$type = 'create';
$nochange = false;

/* These should be moved to the language table.. not the strings table.. */
$arrayLabels = array();
$arrayLabels['formId'] = 'id_formUserCreate';
$arrayLabels['formTitle'] = 'Use this form to create a user account!';
$arrayLabels['legend'] = 'Create a user account';
$arrayLabels['active'] = 'Active:';
$arrayLabels['acslvl'] = 'Access Level:';
$arrayLabels['nameFirst'] = '*First name:';
$arrayLabels['nameMiddle'] = 'Middle name:';
$arrayLabels['nameLast'] = '*Last name:';
$arrayLabels['nameCompany'] = 'Company Name:';
$arrayLabels['email'] = '*Primary email:';
$arrayLabels['emailv'] = '*Verify primary:';
$arrayLabels['email2'] = 'Secondary email:';
$arrayLabels['telephone'] = 'Primary telephone';
$arrayLabels['telephone2'] = 'Secondary telephone:';
$arrayLabels['fax'] = 'Fax:';
$arrayLabels['address'] = '*Address 1:';
$arrayLabels['address2'] = 'Address  2:';
$arrayLabels['city'] = '*City:';
$arrayLabels['state'] = '*State:';
$arrayLabels['zip'] = '*Zip:';
$arrayLabels['country'] ='*Country:';
$arrayLabels['language'] = 'Language:';
$arrayLabels['language2'] = 'Language 2:';
$arrayLabels['password'] = 'Create Password:';
$arrayLabels['passwordv'] = '*Verify password:';
$arrayLabels['secretQuestion'] = 'Secret question:';
$arrayLabels['secretAnswer'] = 'Secret answer:';
$arrayLabels['secretAnswerv'] = 'Verify answer:';
$arrayLabels['welcomeEmail'] = 'Send welcome email:';
$arrayLabels['specialOffers'] = 'Special offers:';
$arrayLabels['submit'] = 'Create';


/***** Begin Program Flow *****************************************************/
$errors = array();

// Request to update user data
if ($GLOBALS['userObject']->checkAdmin() == true) {
    // Create temp user object
    $tempUserObject = new userObject ();
    // Process form submission
    if (isset($_POST['userFormSubmit'])) {
        // This should maybe be improved.. may be succeptable to buffer overflow
        foreach ($_POST as $key => $value) {
            $tempUserObject->$key = $value;
        }
        // Validate form information
        $tempUserObject->validateFormUserData($type);
        if (empty($tempUserObject->errors)) {
            // Create user
            if ($tempUserObject->create() == true) {
                // Let user know we succedded
                $GLOBALS['smarty']->assign ('success', true);
                // Fix this on re-write!

                $tempUserObject->setGuestTemplate();
                $tempUserObject->active = $GLOBALS['jwsfConfig']->UserDefaultActive;
                $tempUserObject->acslvl = $GLOBALS['jwsfConfig']->UserDefaultLevel;
                $tempUserObject->nameFirst = null;
                $tempUserObject->secretQuestion = null;
                $tempUserObject->specialOffers = true;
                $tempUserObject->welcomeEmail = true;
            } else {
                $nochange = true;
                $errors[] = 'The user was *not* created';
            }
        } else {
            $errors = array_merge($errors, $tempUserObject->errors);
            // Set posted values to reload in form
            $tempUserObject->active = $GLOBALS['jwsfConfig']->UserDefaultActive;
            $tempUserObject->acslvl = $GLOBALS['jwsfConfig']->UserDefaultLevel;
        }
    } else {
        $tempUserObject->setGuestTemplate();
        $tempUserObject->active = $GLOBALS['jwsfConfig']->UserDefaultActive;
        $tempUserObject->acslvl = $GLOBALS['jwsfConfig']->UserDefaultLevel;
        $tempUserObject->nameFirst = null;
        $tempUserObject->secretQuestion = null;
        $tempUserObject->specialOffers = true;
        $tempUserObject->welcomeEmail = true;
    }
    $tempUserObject->userFormLabels = $arrayLabels;
    // Construct form
    $tempUserObject->constructUserForm ($type);
    $GLOBALS['smarty']->assign ('template', 'userDataForm');
} else {
    $errors[] = 'Only administrators may use this';
    $GLOBALS['smarty']->assign ('template', 'none');
}

/***** Register Errors ********************************************************/
if (!empty($errors)) {
    if ($nochange == false) {
        // Give warning about reset fields
        $errors[] = 'Your submission contained errors, please try again';
    }
    // Make errors available externally
    $GLOBALS['messagesError'] = array_merge($GLOBALS['messagesError'], $errors);
}


?>
