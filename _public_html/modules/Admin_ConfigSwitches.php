<?php

/* ************* System Configuration Editor ***********************************
 *
 *  $Revision: 1.1.1.1 $
 *
 *  Upon entering the administration area, the system-
 *  wide configuration editor will be presented by
 *  default.  This is strictly for my pleasure.  ;)
 *
 *  TODO: Add a 'Flush Smarty Cache' button
 ******************************************************************************/

$table = TableConfigSwitches;

if (isset($_POST['submitConfig'])) {
    $GLOBALS['jwsfConfig']->update($table);
}

$GLOBALS['jwsfConfig']->read($table);
$GLOBALS['smarty']->assign('config', $GLOBALS['jwsfConfig']->editData);


?>
