<?php

/* ************* Page Editor ***************************************************
 *
 *  $Revision: 1.1.1.1 $
 *
 *  If a page ID has not been set, this should just
 *  display the page selector drop box (add an orphan
 *  section based on OPTGROUPs).  If a page ID is set,
 *  load that page's meta data and content for editing.
 *
 *  TODO:
 *      2   Sort the page selector
 *      3   Add OPTGROUPS for Active, Inactive, & Orphan
 ******************************************************************************/

$editPage = new pageObject ();

if (isset($_POST['saveEdit']) && ($_POST['saveEdit'] == true)) {
    $editPage->update();
}

$so = array();
$so = generate_numbers_array (1, 255);
$editPage->getActives();
$editPage->getInactives();
$editPage->getOrphans();
$editPage->getInactiveContent();
$editPage->load_content_templates();
$GLOBALS['smarty']->assign('data', $editPage->inactiveContent);
$GLOBALS['smarty']->assign('content_templates', $editPage->contentTemplates);
$GLOBALS['smarty']->assign('so_array', $so);
$GLOBALS['smarty']->assign('pagesActive', $editPage->actives);
$GLOBALS['smarty']->assign('pagesInactive', $editPage->inactives);
$GLOBALS['smarty']->assign('pagesOrphan', $editPage->orphans);
unset ($editPage);


?>