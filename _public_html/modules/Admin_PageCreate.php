<?php

/*
 *  $Revision: 1.1.1.1 $
 */


if (isset($_POST['pageCreate'])) {
    // Check defaults
    if (isset($_POST['page_titleNav'])
            && ($_POST['page_titleNav'] !=
                    $GLOBALS['jwsfConfig']->PageDefaultTitleNav)
            && isset($_POST['page_titleFull'])
            && ($_POST['page_titleFull'] !=
                    $GLOBALS['jwsfConfig']->PageDefaultTitle)
            ) {
        // Insert into database
        $pageEditObj = new pageObject();
        $pageEditObj->insert ();
        unset ($pageEditObj);
    } else {
        $GLOBALS['messagesError'][] =
                "You must modify the page and navigation titles";
    }
}

/* ************* Page Creator **************************
 *  This will not require a page ID, but will require
 *  a list of available parents.
 *******************************************************/
// Load up the page template
$temp = new pageObject;
$temp->getActives();
$GLOBALS['smarty']->assign('pagesActive', $temp->actives);
$temp->getInactives();
$GLOBALS['smarty']->assign('pagesInactive', $temp->inactives);
$temp->getOrphans();
$GLOBALS['smarty']->assign('pagesOrphan', $temp->orphans);
// Get available content templates
$temp->load_content_templates();
$GLOBALS['smarty']->assign('content_templates', $temp->contentTemplates);
// Generate values for standard numeric select menus
$so = array();
$so = generate_numbers_array (1, 255);
$GLOBALS['smarty']->assign('so_array', $so);
// Get available sitemap frequency/id pairs
$temp->load_sitemap_frequency_array ();
$GLOBALS['smarty']->assign('frequency_array', $temp->frequencies);
// Generate values for sitemap priority
$priorities = array();
$priorities = generate_numbers_array (0, 10);
$GLOBALS['smarty']->assign('priority_array', $priorities);
// Safety
unset ($temp);
?>
