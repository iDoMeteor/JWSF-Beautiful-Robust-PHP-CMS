<?php

if (isset($_POST['delete-state'])) {
    select_state();
    delete_state();
    $_GET['action'] = 'select-state';
} elseif (isset($_POST['preview2public'])) {
    preview_to_public();
} elseif (isset($_POST['public2preview'])) {
    public_to_public();
} elseif (isset($_POST['restore-state'])) {
    select_state();
    restore_state();
    $_GET['action'] = 'select-state';
}

switch ($_GET['action']) {
    case 'save-state':
        save_state();
        break;
    case 'select-state':
        select_state();
        break;

}

function preview_to_public () {
        // Cheating for now
        $command = SiteRoot . '../preview_to_public';
        $last = system($command, $result);

}

function public_to_preview () {

        $command = SiteRoot . '../public_to_preview';
        $last = system($command, $result);

}

function save_state () {
        $base = 'JWSF-SS-';
        $date = time();
        $locationSql = SiteRoot . '/../_public_html/';
        $locationFile = SiteRoot . '/../dumps/';
        $fileSql = $base . $date . '.sql';
        $fileFiles = $base . $date . '.tar.gz';
        $targets = SiteRoot
                    . '/../_public_html'
                    . ' '
                    . SiteRoot
                    . '/../public_html'; // Gotta do it this goofy way
        $command_dump = '/usr/bin/mysqldump --user='
                        . dbUser
                        . ' --password='
                        . dbPass
                        . ' '
                        . dbName
                        . ' > '
                        . $locationSql
                        . $fileSql;
        $command_compressFiles = 'tar -czf '
                        . $locationFile
                        . $fileFiles
                        . ' '
                        . $targets;
        $command_removeSql = 'rm ' . $locationSql . $fileSql;

        $last_dump = system($command_dump, $result_dump);
        $last_compressFiles = system($command_compressFiles,
                                        $result_compressFiles);
        $last_rm = system($command_removeSql, $result_rm);

        $GLOBALS['messagesNotice'][] = 'Your request is processing in the'
                                        . ' background and may take a moment to'
                                        . ' appear below';
        $GLOBALS['messagesSuccess'][] = 'Current state has been saved!';
}

function delete_state () {
        $location = SiteRoot . '/../dumps/';
        $id = $_POST['delete-state'];
        $file = $GLOBALS['jwsfPage']->sessionBackupFiles[$id];
        $command_remove = 'rm ' . $location . $file;
        $last_remove = system($command_remove, $result_remove);
        $GLOBALS['messagesSuccess'][] = 'Session data '
                                        . $file
                                        . ' removed!';
}

function restore_state () {
        $id = $_POST['restore-state'];
        $fileFiles = $GLOBALS['jwsfPage']->sessionBackupFiles[$id];
        $fileSql = substr($fileFiles, 0, -7) . '.sql';
        $locationSql = PathIncAbs . '/';
        $locationFile = SiteRoot . '/../dumps/';
        $command_removeCurrent = 'rm -rf '
                                . SiteRoot
                                . '/* '
                                . PathIncAbs; // SiteRoot needs to stay
        $command_decompressFiles = 'tar -xzf '
                                . $locationFile
                                . $fileFiles
                                . ' --directory '
                                . SiteRoot
                                . '/../';
        $command_restoreSql = 'mysql --user='
                        . dbUser
                        . ' --password='
                        . dbPass
                        . ' '
                        . dbName
                        . ' < '
                        . $locationSql
                        . $fileSql;
        $command_removeSql = 'rm ' . $locationSql . $fileSql;
        $last_removeCurrent = system($command_removeCurrent,
                                        $result_removeCurrent);
        $last_decompress = system($command_decompressFiles, $result_decompress);
        $last_restore = system($command_restoreSql, $result_restore);
        $last_removeSql = system($command_removeSql, $result_removeSql);
        $GLOBALS['messagesSuccess'][] = 'Session '
                                        . $fileFiles
                                        . ' restored!';
}

function select_state () {
    $valid = array();
    // Read available files into array
    $location = SiteRoot . '/../dumps/';
    $base = 'JWSF-SS-';
    $ext = '.tar.gz';
    $files = dir_as_file_array($location);
    // Eliminate superfluous entries
    foreach ($files as $file) {
        if ((strpos($file, $base) === 0)
                && strpos($file, $ext)
                ) {
            $valid[] = $file;
        }
    }
    // Register array with smarty
    $GLOBALS['jwsfPage']->sessionBackupFiles = array_reverse($valid);
}


?>
