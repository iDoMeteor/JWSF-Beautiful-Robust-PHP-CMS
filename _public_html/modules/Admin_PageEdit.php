<?php

/*
 *  $Revision: 1.1.1.1 $
 */

/* ************* Page Editor ***************************************************
 *  If a page ID has not been set, this should just
 *  display the page selector drop box (add an orphan
 *  section based on OPTGROUPs).  If a page ID is set,
 *  load that page's meta data and content for editing.
 *
 *  TODO:
 *      2   Sort the page selector
 *      3   Add OPTGROUPS for Active, Inactive, & Orphan
 ******************************************************************************/


if (isset($_POST['saveEdit']) && ($_POST['saveEdit'] == true)) {
    $GLOBALS['jwsfPage']->update();
}

if (isset($_POST['editID'])) {
    $GLOBALS['jwsfPage']->populate(); // Ensures updates are applied
    $GLOBALS['jwsfPage']->editContent();
    $GLOBALS['smarty']->register_object('page', $GLOBALS['jwsfPage']);
    $GLOBALS['smarty']->assign('data', $GLOBALS['jwsfPage']->content);
    $GLOBALS['jwsfPage']->load_content_templates();
    $GLOBALS['smarty']->assign('content_templates',
                               $GLOBALS['jwsfPage']->contentTemplates);
    $so = array();
    $so = generate_numbers_array (1, 255);
    $GLOBALS['smarty']->assign('so_array', $so);
    // Get available sitemap frequency/id pairs
    $GLOBALS['jwsfPage']->load_sitemap_frequency_array ();
    $GLOBALS['smarty']->assign('frequency_array',
                               $GLOBALS['jwsfPage']->frequencies);
    // Generate values for sitemap priority
    $priorities = array();
    $priorities = generate_numbers_array (0, 10);
    $GLOBALS['smarty']->assign('priority_array', $priorities);
}


$GLOBALS['jwsfPage']->getActives();
$GLOBALS['smarty']->assign('pagesActive', $GLOBALS['jwsfPage']->actives);
$GLOBALS['jwsfPage']->getInactives();
$GLOBALS['smarty']->assign('pagesInactive', $GLOBALS['jwsfPage']->inactives);
$GLOBALS['jwsfPage']->getOrphans();
$GLOBALS['smarty']->assign('pagesOrphan', $GLOBALS['jwsfPage']->orphans);


?>
