<?php

/***** JWSF Module Administration **********************************************
 *
 *  $Revision: 1.1.1.1 $
 *
 *  This module allows you to assign multiple modules to multiple pages and set
 *  their sort orders and access levels.  This is an extremely powerful and
 *  flexible way to use the system as modules can function independently from
 *  the system, or interact with it on any level and have immediate results.
 *
 *  Display table of currently assigned/active modules with editable parameters
 *      and delete button sorted by titleNav(pid), so, module
 *  Display table of assigned/inactive modules with editable parameters
 *      and delete button sorted by titleNav(pid), so, module
 *  Display table of assigned/orphan modules with editable parameters
 *      and delete button sorted by pid, so, module
 *  Display new module assignment row
 *
 *    id        tinyint
 *    pid       tinyint
 *    so        so_array
 *    active    bool
 *    acsLow    tinyint
 *    acsHigh   tinyint
 *    module    tinytext
 *    stamp     timestamp
 *
 ******************************************************************************/

// Process full submission
if (isset($_POST['updateModules'])) {
    foreach ($_POST['record'] as $key => $record) {
        if (substr($key, -3) == 'new') {
            /* Insert new module assignment
                    Check for non-defaults */
            if (($record['pid'] != ModuleDefaultPid)
                    && ($record['module'] != ModuleDefaultModule)) {
                $sql = 'INSERT INTO `' . TableModuleAssignments
                        . '` (pid, location, so, active, acsLow, acsHigh, module)'
                        . ' VALUES ("'
                        . $record['pid'] . '", "'
                        . $record['location'] . '", "'
                        . $record['so'] . '", "'
                        . $record['active'] . '", "'
                        . $record['acsLow'] . '", "'
                        . $record['acsHigh'] . '", "'
                        . $record['module'] . '");';
                $GLOBALS['db']->query($sql, __FILE__, __LINE__);
            }
        } elseif (isset($record['deleteMod'])) {
            $sql = "DELETE FROM `" . TableModuleAssignments
                    . '` WHERE id="' . $record['id']
                    . '" LIMIT 1;';
            $GLOBALS['db']->query($sql, __FILE__, __LINE__);
        } else {
            // Update existing assignment
            $sql = 'UPDATE `' . TableModuleAssignments
                    . '` set pid="' . $record['pid']
                    . '", location="' . $record['location']
                    . '", so="' . $record['so']
                    . '", active="' . $record['active']
                    . '", acsLow="' . $record['acsLow']
                    . '", acsHigh="' . $record['acsHigh']
                    . '", module="' . $record['module']
                    . '" WHERE id="' . $record['id']
                    . '" LIMIT 1;';
            $GLOBALS['db']->query($sql, __FILE__, __LINE__);

        }
    }
}

$modules = new jwsfModuleObject;
$modules->buildModuleTable();


?>
