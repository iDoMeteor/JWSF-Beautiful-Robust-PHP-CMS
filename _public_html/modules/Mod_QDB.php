<?php

/*******************************************************************************
 ******************************************************************************/


// These should remain hardcoded for safety
DEFINE ('Supy_QuoteDB', '/home/lookinto/subdomains/dev/public_html/uploads/text/blinkenbot-quotedb.txt');
DEFINE ('Supy_UserDB', '/home/lookinto/subdomains/dev/public_html/uploads/text/blinkenbot-userdb.txt');

// Instantiate
$SupyQDB = new SupyQDB;


class SupyQDB {

    function SupyQDB () {

        $this->admin =
            ($GLOBALS['userObject']->acslvl >=
                $GLOBALS['jwsfConfig']->AdminLevelLow)
            ? true : false;

        $this->readUsers();
        $this->readQuotes();
        $this->total = count($this->quotes);

        if (!isset($_GET['action'])) {
            $_GET['action'] = '';
        }
        
        switch ($_GET['action']) {
            case "all":
                break;
            case "recent":
                $this->selectRecent();
                break;
            case "browse":
                $this->browse();
                break;
            case "random":
                $this->selectRandom();
                break;
            case "top_rated":
                $this->selectTop();
                break;
            case "worst_rated":
                $this->selectWorst();
                break;
            case "stats":
                $this->generateStats();
                break;
            case "search":
                $this->search();
                break;
            case "single":
                $this->selectSingle();
                break;
            default:
                $this->selectRandomSingle();
                break;
        }

        if (isset($this->quotes))
            $GLOBALS['smarty']->assign('SupyQuotes', $this->quotes);
        if (isset($this->total))
            $GLOBALS['smarty']->assign('SupyTotal', $this->total);
        if (isset($this->high))
            $GLOBALS['smarty']->assign('SupyHigh', $this->high);


    // End instantiation
    }

    function browse () {

        $this->selectRange();
        $this->generatePager();

    }

    function deactivate () {



    }

    function generatePager () {

        $array = array();
        $last = array();
        $i = 0;
        $qty = floor($this->total/20);
        while ($i < $qty) {
            $start = $i*20;
            $array[] = array('start' => $start,
                            'low' => $i*20+1,
                            'high' => $i*20+20);
        	$i++;
        }
        $last[] = array('start' => $i*20,
                        'low' => $i*20+1,
                        'high' => $i*20+($this->total%20));
        $GLOBALS['smarty']->assign('SupyRangeArray', $array);
        $GLOBALS['smarty']->assign('SupyRangeLast', $last);

    }

    function generateStats () {



    }

    function rate () {



    }

    function readQuotes () {

        $this->quotes = array();

        // Read file into an array of strings
        $quoteArray = file (Supy_QuoteDB);
        foreach ($quoteArray as $record) {

            if (strlen($record) > 30) {
                $values = array();

                /* Fucking goofy file format, gotta do this weird */
                
                // Search for first seperator
                $colon = strpos($record, ':');
                $values['number'] = intval(substr($record, 0, $colon));
                $period = strpos($record,'.');
                $values['stamp'] = substr($record, $colon+1, $period-$colon);
                $comma1 = strpos($record, ',');
                $values['micro'] = substr($record, $period+1, $comma1-$period);
                $comma2 = strpos($record, ',', $comma1+1);
                $values['userId'] =
                    intval(substr($record, $comma1+1, $comma2-$comma1));
                $values['quote'] = substr($record, $comma2+1);
                if (substr($values['quote'], 0, 3) == '"""') {
                    $values['quote'] = substr($values['quote'], 3, -4);
                } elseif (substr($values['quote'], 0, 2) == '"\'') {
                    $values['quote'] = substr($values['quote'], 2, -3);
                } else {
                    $values['quote'] = substr($values['quote'], 1, -2);
                }
                $values['quote'] = htmlentities($values['quote']);
                $id = $values['userId'];
                $values['userNick'] = isset($this->users[$id])
                                    ? $this->users[$id]
                                    : 'anonymous';
                $this->quotes[] = $values;
            }
        }
        $this->high = $values['number'];

    }

    function readUsers () {

        $userNo = null;
        $userNick = null;
        $this->users = array();

        // Read file into an array of strings
        $userArray = file (Supy_UserDB);
        foreach ($userArray as $record) {

            $record = trim($record);
            $prefix = substr($record, 0, 4);
            if ($prefix == 'user') {
                $userNo = substr($record, 5);
            } elseif ($prefix == 'name') {
                $userNick = substr($record, 5);
                $this->users[$userNo] = $userNick;
                $userNo = null;
                $userNick = null;
            }
        }

    }

    function search () {



    }

    function selectRandom () {

        shuffle($this->quotes);
        $this->selectRange(0, 10);

    }

    function selectRandomSingle () {

        shuffle($this->quotes);
        $this->selectRange(0, 1);

    }

    function selectRange ($start = 0, $qty = 20) {

        if ((isset($_GET['start']))
                && ($_GET['start'] < $this->total)) {
            $start = intval($_GET['start']);
        }
        $this->quotes = array_slice($this->quotes, $start, $qty);

    }

    function selectRecent () {

        $this->quotes = array_reverse(array_slice($this->quotes, -10));

    }

    function selectSingle () {

        if (!isset($_POST['SupyQDB_Num'])
                || !is_numeric($_POST['SupyQDB_Num'])
                ) {
            $this->selectRandomSingle();
            return;
        }

        foreach($this->quotes as $v) {
            if($v['number'] == $_POST['SupyQDB_Num']) {
                $this->quotes = array($v);
            }
        }

        if(count($this->quotes) != 1) {
            $GLOBALS['messagesNotice'][] = 'Quote #'
                    . $_POST['SupyQDB_Num']
                    . ' not found, have a random single instead!';
            $this->selectRandomSingle();
        }
        
    }

    function selectTop () {



    }

    function selectWorst () {



    }

// End class
}


?>