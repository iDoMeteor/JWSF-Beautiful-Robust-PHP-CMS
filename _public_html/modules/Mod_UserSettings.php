<?php

/*******************************************************************************
 *  JWSF Slim Frame User Sign-up Module
 *
 *  $Revision: 1.1.1.1 $
 *
 *  This should be kept and moved to the admin area
 *
 *  First run through we simply display the form
 *
 *  Second pass performs validation
 *      Upon successful
 *          Log to database
 *          Send welcome email
 *          Assign Smarty values
 *          Display receipt
 *      Upon failure
 *          Assign Smarty values
 *          Back to step one with user entered data
 *
 *  TODO:
 *      Keep in mind this will be ported to a 'user control panel'!!
 *
 ******************************************************************************/

/* Set method of operation */
$type = 'update';
$update = false;
$nochange = false;

if (isset($_POST['editId'])) {
    $id = $_POST['editId'];
    $update = true;
} else
    $id = $GLOBALS['userObject']->id;

/* These should be moved to the language table.. not the strings table.. */
$arrayLabels = array();
$arrayLabels['formId'] = 'Use this form to edit your account information';
$arrayLabels['formTitle'] = 'Use this form to edit your account information';
$arrayLabels['nameFirst'] = '*First name:';
$arrayLabels['nameMiddle'] = 'Middle name:';
$arrayLabels['nameLast'] = '*Last name:';
$arrayLabels['nameCompany'] = 'Company Name:';
$arrayLabels['email'] = '*Primary email:';
$arrayLabels['emailv'] = '*Verify primary:';
$arrayLabels['email2'] = 'Secondary email:';
$arrayLabels['telephone'] = 'Primary telephone';
$arrayLabels['telephone2'] = 'Secondary telephone:';
$arrayLabels['fax'] = 'Fax:';
$arrayLabels['address'] = '*Address 1:';
$arrayLabels['address2'] = 'Address  2:';
$arrayLabels['city'] = '*City:';
$arrayLabels['state'] = '*State:';
$arrayLabels['zip'] = '*Zip:';
$arrayLabels['country'] ='*Country:';
$arrayLabels['language'] = 'Language:';
$arrayLabels['language2'] = 'Language 2:';
$arrayLabels['password'] = 'Change Password:';
$arrayLabels['passwordv'] = '*Verify password:';
$arrayLabels['secretQuestion'] = 'Secret question:';
$arrayLabels['secretAnswer'] = 'Secret answer:';
$arrayLabels['secretAnswerv'] = 'Verify answer:';
$arrayLabels['welcomeEmail'] = 'Send welcome email:';
$arrayLabels['specialOffers'] = 'Special offers:';
$arrayLabels['stampCreated'] = 'Creation Date:';
$arrayLabels['stampLogin'] = 'Last Login:';
$arrayLabels['stampModified'] = 'Last Page View:';
$arrayLabels['loginsFailed'] = 'Recent Failed Logins:';
$arrayLabels['loginsFailedTotal'] = 'Lifetime Failed Logins:';
$arrayLabels['loginsSuccessTotal'] = 'Lifetime Successful Logins:';
$arrayLabels['submit'] = 'Update';


/***** Begin Program Flow *****************************************************/
$errors = array();

// Request to update user data
if ($id > 0) {
    // Validate edit id against valid user ids
    if ($GLOBALS['userObject']->validateId ($id) == true) {
        // ID is valid, create user object
        $tempUserObject = new userObject ();
        // Load user
        $tempUserObject->edit($id);
        // Make sure user doesn't go over their head,
        // but can still edit themselves
        if ($tempUserObject->id == $GLOBALS['userObject']->id){
            // Process form submission
            if ($update == true) {
                // Validate form information
                $tempUserObject->validateFormUserData($type);
                if (empty($tempUserObject->errors)) {
                    // Update user
                    if ($tempUserObject->update($id) == true) {
                        // Load modified information
                        $tempUserObject->edit($id);
                        // Let user know we succedded
                        $GLOBALS['smarty']->assign ('success', true);
                    } else {
                        $nochange = true;
                        $errors[] = 'Your information was *not* updated';
                    }
                } else
                    $errors = array_merge($errors, $tempUserObject->errors);
            }
            // Assign form labels
            $tempUserObject->userFormLabels = $arrayLabels;
            // Construct form
            $tempUserObject->constructUserForm ($type);
            $GLOBALS['smarty']->assign ('template', 'userDataForm');
        } else {
            $nochange = true;
            $errors[] = 'You may only edit yourself from this page';
        }
    } else {
        // Invalid id, set error
        $nochange = true;
        $errors[] = 'Invalid user id';
    }
} else {
    $nochange = true;
    $errors[] = 'You must be logged in to edit user settings';
}


/***** Register Errors ********************************************************/
if (!empty($errors)) {

    if ($nochange == false) {
        // Give warning about reset fields
        $errors[] = 'Your submission contained errors, please try again';
    }
    // Make errors available externally
    $GLOBALS['messagesError'] = array_merge($GLOBALS['messagesError'], $errors);
}



?>
