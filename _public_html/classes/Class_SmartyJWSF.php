<?php

/*******************************************************************************
 *  Jason White's Slim Frame Site-Wide Configuration Object
 *
 *  $Revision: 1.1.1.1 $
 *
 *  This is the site-wide configuration object which provides various parameters
 *  that affect the global behavior of the Jason White Slim Frame system.  These
 *  parameters are controlled via the administration area and include things
 *  like default theme, whether or not to allow themes, css and javascript, and
 *  more.
 *
 *	20080304 - Move most of index.php(s) to here (finally)
 *				PathIncAbs is defined in both index.php(s) and is imperative
 *				that it is a valid path.
 *
 *	*Smarty Note*
 *		Extending Smarty class to allow per-theme smarty templates
 *		For now, this little gem will lie buried at the bottom of this sea
 *
 ******************************************************************************/

DEFINE ('PathSmarty', PathIncAbs . '/' . 'smarty/');
require_once (PathSmarty . 'Smarty.class.php');


/***** Instantiate Smarty Object ***********************************************
 *  This fires up the Smarty engine and configures
 *  necessary file locations.  All editable files
 *  are stored in the templates directory, but
 *  should only need to be modified if data structures
 *  are altered in some way on the back end.  Additional
 *  content templates for page by page styling may be
 *  added by naming the file contentXX.tpl where XX is
 *  the page id from the database.  If any additional
 *  markup tags are required for styling, they should
 *  be added via the theme specific Javascript.  This
 *  will ensure continuity of data regardless of the
 *  theme chosen.
 *
 *  5/29
 *      Moved this way up top so that we can assign
 *      smarty data from anywhere.
 *
 *	Smarty Caching
 *
 *		This loads here so that it can access configuration switch
 *
 *		Setting caching to 2 allows us to use per-template cache timeouts, which
 *		is good for things like the authentication area, who's online, etc..
 *
 *		Note: Any authentication changes should probably flush all caches
 *
 *
 *		***BIG NOTE***
 *		Using this with such a dynamic system is probably totally insane.  I keep
 *		my system with caching off, but on a stable system with little changes this
 *		would be fantastic.  I considered giving everything custom caching times,
 *		but on a multi-user system that is completely impractical.
 *
 *  TODO:
 *      Verify per-theme templates work properly with user-selected themes
 *
 ******************************************************************************/

class MySmarty extends Smarty {

	// Instantiate as usual
	function MySmarty () {

		$this->smarty();

		// This is taken care of in config
		// $this->template_dir = PathSmarty . 'templates';
		$this->compile_dir = PathSmarty . 'templates_c';
		$this->config_dir = PathSmarty . 'configs';
		$this->cache_dir = PathSmarty . 'cache';
		$this->template_dir =  PathSmarty . 'templates';
		// This is our XHTML output buffer
		$this->output = null;

	}


/***** Add Template Output to Output Buffer ************************************
 *
 *
 *
 ******************************************************************************/

    function addToBuffer ($resource_name) {

		$resource_name = $this->getTemplate($resource_name);
        $this->output .= $this->fetch ($resource_name);

    }


/***** Modified Display Method *************************************************
 *
 *
 *
 ******************************************************************************/

	function display ($resource_name, $cache_id = null, $compile_id = null) {

		$resource_name = $this->getTemplate($resource_name);
		parent::display($resource_name, $cache_id, $compile_id);

	}


/***** Get Template Location ***************************************************
 *
 *
 *
 ******************************************************************************/

	function getTemplate($resource_name) {

		if (is_array($GLOBALS['jwsfConfig']->templatesTheme)
				&& ($GLOBALS['jwsfConfig']->ThemeLoad == 'true')) {
			$array = $GLOBALS['jwsfConfig']->templatesTheme;
			if (in_array($resource_name, $array)) {
				return ('file:'
						. PathThemesAbs
						. $GLOBALS['jwsfConfig']->ThemeActive
						. '/smarty/'
						. $resource_name);
			}
		}

		if (is_array($GLOBALS['jwsfConfig']->templatesAdvanced)
				&& ($GLOBALS['jwsfConfig']->SmartyFallbackAdvanced == 'true')) {
			$array = $GLOBALS['jwsfConfig']->templatesAdvanced;
			if (in_array($resource_name, $array)) {
				return ('file:'
						. $this->template_dir
						. '/advanced/'
						. $resource_name);
			}
		}

		if (is_array($GLOBALS['jwsfConfig']->templatesStandard)) {
			$array = $GLOBALS['jwsfConfig']->templatesStandard;
			if (in_array($resource_name, $array)) {
				return ($resource_name);
			}
		}
		return false;

	}


/***** Return Output as String *************************************************
 *
 *
 *
 ******************************************************************************/

    function returnString ($resource_name) {

		$resource_name = $this->getTemplate($resource_name);
        return $this->fetch($resource_name);

    }


/*
	function fetch ($resource_name, $cache_id = null, $compile_id = null) {

		if (!isset($GLOBALS['smarty']->output)) {
			$GLOBALS['smarty']->output = null;
		}

		$GLOBALS['smarty']->output .= parent::fetch($resource_name, $cache_id, $compile_id);

	}
*/
// End class
}


?>
