<?php

/*******************************************************************************
 *  Jason White's Slim Frame Navigational Object
 *
 *  $Revision: 1.3 $
 *
 *  This API contains all navigational features.  Breadcrumb/related content
 *  requires that the global page object be available and thus should be created
 *  first.
 *
 *  The SQL here massively affects how the navigational structure for the
 *  site will be displayed, courtesy of 'function.jwsfNestedUListFromObject'!
 *
 *  One may make the rather obvious observation that this class does not modify
 *  the database in any way, it simply performs reads and returns arrays of
 *  navigational items.
 *
 *  This class generates all levels of navigation, so for anyone interested
 *  in replacing the navigation in some way should start here.  For instance,
 *  someone interested in using a flash based navigational interface would want
 *  to hack around in here and the Smarty templates.
 *
 *  All of the features in this class and the pages class rely on each other
 *  pretty heavily which makes it difficult to just say 'shut off navigation'
 *  or 'shut off pages' with a site-wide configuration switch (I don't know why
 *  you would want to anyway :>).  As such, one might wonder why these aren't
 *  all just methods of the pages class?  Well, there are a lot of good reasons
 *  for that and not real good reasons to join them.  They operate on different
 *  sub-sets of the same information in completely different ways and seperating
 *  them makes that clear, as well as saving a pages of scrolling in the page
 *  class file.  woot.
 *
 *  A Tale of Three Navigational Levels
 *
 *      Long ago, in a galaxy far, far away..... I built this simple little PHP
 *      template system.  It had one level of navigation which I skillfully
 *      manipulated into many extremely different interfaces.  As I started
 *      using it as the base for small business web (and other) web sites, I
 *      realized (after a long time actually) that I should add an extra level
 *      of navigation because all sites should have a footer, yes?  For a while
 *      I left it static but then decided I should make it more dynamic and thus
 *      I added in the secondary navigation to this class as well as the ability
 *      to choose in which location the item would appear.  Well I soon realized
 *      that I should allow the same page to appear in both locations, or even
 *      be hidden (for receipts and such).  Thus we had a location setting for
 *      each page that could have the following values/meanings:
 *          * 0 == do not output in any navigation list
 *          * 1 == appear only in primary navigation (hierarchical by default)
 *          * 2 == appear only in secondary navigation (footer by default)
 *          * 3 == appear in both (which i thought was very ingenious)
 *
 *      This has been great and worked much more smoothly than anticipated from
 *      the start.  If you are an old fart like me, you may notice that this
 *      logic process very closely resembles the evolution of most web sites
 *      on the Internet.
 *
 *      Well, the web continues to evolve, sites get larger, and more navigation
 *      is needed to provide clarity and speed in user interface design.  There-
 *      fore, as of today, the JWSF will add a third (or tertiary) level of
 *      navigation which will, by default, be the header navigationl.  Strange?
 *      Perhaps to those who are newer to the seen, but that is just how it is
 *      going to be, for two historical reasons.  Of course, once I add the
 *      ability to reassign block locations, it will become a non-issue.
 *
 *      So, following good UNIX principles, here are the possible assignment
 *      values and their meanings for the 'location' property of each page.
 *          * 0 == hidden
 *          * 1 == primary only
 *          * 2 == secondary only
 *          * 3 == primary & secondary  (1+2 == 3)
 *          * 4 == tertiary only
 *          * 5 == primary & tertiary   (1+4 == 5)
 *          * 6 == secondary & tertiary (2+4 == 6)
 *          * 7 == all three locations  (1+2+4 == 7)
 *
 *      Worthy of note in this story is that only the primary navigational unit
 *      allows for hierarchical navigation, everything else will be output in
 *      alphabetical order (except a page with titleNav == Home should always
 *      come first!).  There is a *very* good reason for this.  Multiple
 *      hiearchical navigation areas would be a usability nightmare!  It would
 *      be easy to hack it in if you want to try for yourself.. ick!
 *
 *      Confusing?  Perhaps for beginners.  Powerful?  Flexible?  *Extremely*
 *
 *  TODO:
 *      Create a page-by-page 'override' setting for related content
 *      Ya know, at this point those sql statements are getting pretty ugly,
 *          perhaps it is time to just make one big draw and parse it all out
 *          with php...popping inaccessibles out of the array for safety.
 *      Fully integrate SessionsUse everywhere appropriate (like themer)
 *          Themer will still work, just don't output user selector regardless
 *          of ThemeAllowUserSelect
 *      Integrate 'AllowUsers' check
 *      Condense SQL statements
 *
 ******************************************************************************/

/***** Instantiate Navigation Object *******************************************
 *
 *  Instantiating this object generates all navigational units as multi-
 *  dimensional array properties.  The multi-dimensional arrays consist only
 *  of data pertinent to navigation.  ie:
 *
 *  $this->semanticName[#]['id']
 *  $this->semanticName[#]['pid']
 *  $this->semanticName[#]['acsLow']
 *  $this->semanticName[#]['acsHigh']
 *  $this->semanticName[#]['so']
 *  $this->semanticName[#]['active']
 *  $this->semanticName[#]['location']
 *  $this->semanticName[#]['titleNav']
 *  $this->semanticName[#]['titleFull']
 *  $this->semanticName[#]['semanticURI']
 *
 *  Note that each object property relies on configuration switches in the
 *  admin area.  All methods return a different subset from the same data and
 *  may be accessed using identical methods outside of this class.
 *
 ******************************************************************************/

class navigationObject {

    function navigationObject () {

        $this->populate ();

        // Navigation Blocks
        $this->loadPrimary ();
        $this->loadSecondary ();
        $this->loadTertiary ();

        // Bread Crumb
        $this->getParents();

        // Related Content
        $this->getSiblings();
        $this->getChildren();

    }


/***** Populate Navigation Object **********************************************
 *
 *  Populate an array of pages that the user has access to, sorted for our
 *  primary navigation array (smarty plugin).
 *
 ******************************************************************************/

    function populate () {

        $row = array();
        $this->navItem = array();
        $this->navArray = array();

        $fields = array('id',
                        'pid',
                        'acsLow',
                        'acsHigh',
                        'so',
                        'active',
                        'location',
                        'titleNav',
                        'titleFull',
                        'semanticURI',
                        'stamp'
                        );
        $clause = 'WHERE location!=0 ORDER BY pid, so, titleNav';
        $GLOBALS['db']->select($fields,
                                TablePageMetaData,
                                $clause,
                                __FILE__,
                                __LINE__);

        while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
            foreach ($row as $property => $value) {
                $this->navItem[$property] = $value;
            }

            // Format navigational href value (semantic/external/id/etc)
            $this->formatHref();

            // Check access up the chain
            if ($this->checkAccess($this->navItem['active'],
                                    $this->navItem['acsLow'],
                                    $this->navItem['acsHigh'],
                                    $this->navItem['pid'])
                                    == true)
                $this->navArray[] = $this->navItem;

        }

    // End instantiation method
    }


/***** Populate Primary Navigation Object **************************************
 *  Load navigation
 *  What we should be expecting back here is a
 *  multi-dimensional area of relavent navigation properties
 *  $this->[#]['id']
 *  $this->[#]['pid']
 *  $this->[#]['acsLow']
 *  $this->[#]['acsHigh']
 *  $this->[#]['so']
 *  $this->[#]['active']
 *  $this->[#]['location']
 *  $this->[#]['titleNav']
 *  $this->[#]['titleFull']
 *  $this->[#]['semanticURI']
 *
 *  This is set to the semantic uri if possible, otherwise a GET string with
 *  the page id set.
 *      $this->[#]['href']
 *
 *  What we want to build here is a multi-dimensional array of valid
 *  page IDs for use by our Smarty plugin.  Page data should only be
 *  added to the array if the page and all of its parents are active
 *  and the user has qualified for access to the page and all of its
 *  parents.
 *
 *  The checkAccess function weeds out those pages which snuck by
 *  the SQL select requirements but should not actually be displayed
 *  because they are sub-pages of pages that the user does not have
 *  access to.  Theoretically, every page SHOULD be set correctly,
 *  but in reality this may not occur so this is here to catch those
 *  oversights.
 *
 *  This sets the navItems property to an array that will be passed to the
 *  Smarty plugin which builds the XHTML nested navigational list
 *  (jwsfNestedUListFromObject).
 ******************************************************************************/

    function loadPrimary () {

        // This should still be set properly for parent check
        $this->navPrimary = array();
        foreach ($this->navArray as $k => $v) {
            switch ($v['location']) {
                case 1:
                case 3:
                case 5:
                case 7:
                    $this->navPrimary[] = $v;
            }
        }

        if ($GLOBALS['jwsfConfig']->NavUsePrimary == 'true') {
		    $GLOBALS['smarty']->assign('navPrimary', $this->navPrimary);
        }

    // End instantiation method
    }


/***** Build Secondary Navigation **********************************************
 *
 *  This essentially does the same thing as the primary navigation builder,
 *  but with pages where location = 2 and will be utilized directly by the
 *  smarty template because the secondary navigation will not be nested.
 *
 ******************************************************************************/

     function loadSecondary () {

        if ($GLOBALS['jwsfConfig']->NavUseSecondary == 'true') {

            $this->navSecondary = array();
            foreach ($this->navArray as $k => $v) {
                switch ($v['location']) {
                    case 2:
                    case 3:
                    case 6:
                    case 7:
                        $this->navSecondary[] = $v;
                }
            }

            $this->navSecondary
                = array_sort_by_any_key($this->navSecondary, 'titleNav');

		    $GLOBALS['smarty']->assign('navSecondary', $this->navSecondary);

        }

    // End buildSecondary
    }


/***** Build Tertiary Navigation ***********************************************
 *
 *  This is identical to the secondary navigation builder except it picks
 *  the tertiary elements.
 *
 ******************************************************************************/

     function loadTertiary () {

        if ($GLOBALS['jwsfConfig']->NavUseTertiary == 'true') {

            $this->navTertiary = array();
            foreach ($this->navArray as $k => $v) {
                switch ($v['location']) {
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                        $this->navTertiary[] = $v;
                }
            }

            $this->navTertiary
                = array_sort_by_any_key($this->navTertiary, 'titleNav');

		    $GLOBALS['smarty']->assign('navTertiary', $this->navTertiary);

        }
    // End buildSecondary
    }


/***** Format Href *************************************************************
 *
 *  This section formats the navigational elements based on the contents (or
 *  lack thereof) of the semanticURI field.  If the item id is equal to the
 *  PageDefaultId in configuration numerics, then it will be output with:
 *      <a href="/">
 *  regardless of semanticURI value.
 *
 *  If the item has :// anywhere in it or starts with a /, then it will be
 *  output as:
 *      <a href="VALUE">
 *  if you would like it to launch in a new tab/window you must use javascript
 *  because that is not a supported feature in XHTML 1.1).
 *
 *  If the value does not start with a leading / or contain :// then it will
 *  have a leading / prepended to it (making the link relative to site root)
 *  like so:
 *      <a href="/VALUE">
 *
 *  If the semanticURI field is empty one of two things will happen dependant
 *  upon the NavAllowNonSemanticHrefs settings in configuration switches.  If set
 *  to true, the link will look like:
 *      <a href="/?id=XX">
 *  where XX is the page id number.  If NavAllowNonSemanticHrefs is set to false,
 *  then items without a semanticURI value will not be clickable.
 *
 *  Faking directory depth should not be an issue, so you could set a
 *  semanticURIvalue to something like:
 *      'top_level/second_level/date-time/article_name'
 *  and it should work fine... depending on your web server setup.
 *
 ******************************************************************************/

    function formatHref () {

            // pretty things up
            $id = $this->navItem['id'];
            $semantic = $this->navItem['semanticURI'];
            // Item is non-clickable by default
            $href = null;

            // Do not use ?id= for default (home/root) page
            if ($id == $GLOBALS['jwsfConfig']->PageDefaultId) {
                // Set root href
                $href = '/';
            // Check for semantic/external link
            } elseif (($semantic != NULL)
                    && ($GLOBALS['jwsfConfig']->NavForceNonSemantic == 'false')
                    ){
                // Internal or external?
                if (stripos($semantic, '://') == true) {
                    // Set external href
                    $href = $semantic;
                } else {
                    // Set internal href, check for leading /
                    $href = (substr($semantic, 0, 1) == '/')
                            ? $href = $semantic
                            : $href = "/" . $semantic;
                }
            // Allow non-semantic links?
            } elseif ($GLOBALS['jwsfConfig']->NavAllowNonSemanticHrefs == 'true') {
                $href = "/?id=" . $id;
            }

            $GLOBALS['jwsfPage']->href = $href;
            $this->navItem['href'] = $href;

    // End formatHref
    }


/***** Check Page Access Requirements ******************************************
 *
 *  Return true if page and parents are active and user has required
 *  privileges for this page and its parents.
 *
 *  TODO:
 *      Clean this up
 *
 ******************************************************************************/

    // Passing parameters in like this makes recursion neater
    function checkAccess ($active, $low, $high, $parent) {

        if ($active == 1) {
            // Page is active, test levels
            if (
                    ($GLOBALS['jwsfConfig']->UsersAllow == 'false')
                    || (($GLOBALS['userObject']->acslvl >= $low)
                        && ($GLOBALS['userObject']->acslvl <= $high))
                    ) {
                if ($parent == 0) {
                    // End of the road, all clear
                    return true;
                } elseif (isset($this->navArray)
                        && !empty($this->navArray)
                        ) {
                    // good so far, check parents

                    // get props from main nav array
                    foreach ($this->navArray as $k => $v) {
                        if ($v['id'] == $parent) {
                            $pinfo = array();
                            $pinfo = $v;
                        }
                    }
                    if (isset($pinfo)
                            && ($this->checkAccess($pinfo['active'],
                                            $pinfo['acsLow'],
                                            $pinfo['acsHigh'],
                                            $pinfo['pid'])
                                            == true
                                            )
                        )
                        // All ancestors are accessible and active
                        return true;
                    else
                        // Something up the chain returned false
                        return false;
                }
            } else
                // User does not have permission
                return false;
        } else
            // Page is not active
            return false;

    // End checkAccess
    }


/***** Get Parents *************************************************************
 *
 *  This method will generate an array of page ids, titles and hrefs  from the
 *  selected page id upwards until it reaches the top level.  This is used by
 *  the breadcrumb feature and draws its operational data from the primary
 *  navigation array.  Using the primary navigational array allows us to avoid
 *  having to go to the database several times and also ensures that any pages
 *  which the user should not have access to are also weeded out.
 *
 *  The breadcrumb feature will depend on:
 *      configuration->switches ShowBreadCrumb true
 *
 ******************************************************************************/

    function getParents ($id = null) {

        if ($GLOBALS['jwsfConfig']->NavShowBreadCrumb == 'true') {
            // Sanity checks
            if (!isset($this->parents)) {
                $this->parents = array();
            }
            if ($id == null) {
                $id = $GLOBALS['jwsfPage']->id;
            }

            // This right here is gold Jerry
            foreach ($this->navPrimary as $key => $value) {
                if ($value['id'] == $id) {
                    array_unshift ($this->parents, $value);
                    // Recurse
                    $this->getParents ($value['pid']);
                }
            }

            // Limit size of bread crumb
            $max = $GLOBALS['jwsfConfig']->NavMaxDepthBreadCrumb;
            if (count($this->parents) > $max) {
                $this->parents = array_slice($this->parents, 0-$max);
            }

		    $GLOBALS['smarty']->assign('navParents', $this->parents);

        }
    // End getParents
    }


/***** Get Siblings ************************************************************
 *
 *  This method will generate an array of page ids, titles and hrefs which all
 *  have the same parent id as the calling page, obviously with that page
 *  excluded.  This is used by the related content features.
 *
 *  Ah, another benefit of using the primary navigation array... already sorted!
 *
 *  The related content feature will depend on:
 *      configuration->switches ShowSiblings true
 *
 ******************************************************************************/

    function getSiblings () {

        if (
                isset($GLOBALS['jwsfConfig'])
                && isset($GLOBALS['jwsfPage'])
                && ($GLOBALS['jwsfConfig']->NavShowRelated == 'true')
                && ($GLOBALS['jwsfConfig']->NavShowSiblings == 'true')
                && ($GLOBALS['jwsfPage']->pid != 0)
                ) {

            $this->siblings = array();
            $id = $GLOBALS['jwsfPage']->id;
            $pid = $GLOBALS['jwsfPage']->pid;

            foreach ($this->navPrimary as $key => $value) {
                if (
                        ($value['pid'] == $pid)
                        && ($value['id'] != $id)
                        ) {
                    $this->siblings[] = $value;
                }
            }

		    $GLOBALS['smarty']->assign('navSiblings', $this->siblings);

        }
    // End getSiblings
    }


/***** Get Children ************************************************************
 *
 *  This method will generate an array of page ids, titles and hrefs which all
 *  have the calling page id as their parent id.  This is used by the related
 *  content features.
 *
 *
 *  The related sub-content feature will depend on:
 *      configuration->switches ShowChildren true
 *
 ******************************************************************************/

    function getChildren () {

        if (
                isset($GLOBALS['jwsfConfig'])
                && isset($GLOBALS['jwsfPage'])
                && ($GLOBALS['jwsfConfig']->NavShowRelated == 'true')
                && ($GLOBALS['jwsfConfig']->NavShowChildren == 'true')
                ) {

            $this->children = array();
            $id = $GLOBALS['jwsfPage']->id;

            foreach ($this->navPrimary as $key => $value) {
              if ($value['pid'] == $id) {
                    $this->children[] = $value;
                }
            }

            $GLOBALS['smarty']->assign('navChildren', $this->children);

        }
    // End getChildren
    }


// End class
}

?>
