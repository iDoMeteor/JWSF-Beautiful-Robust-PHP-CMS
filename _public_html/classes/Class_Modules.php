<?php

/*******************************************************************************
 *  Jason White's Slim Frame Module Object Class
 *
 *  $Revision: 1.1.1.1 $
 *
 *  TODO:
 *      Move Default Admin Module to Database
 ******************************************************************************/

/* Module assignment table */
DEFINE ('TableModuleAssignments', 'JWSF_PageModules');
DEFINE ('PathModules', PathIncAbs . '/' . 'modules/');
DEFINE ('PathModuleExt', '.php');
DEFINE ('PathModulesAdminPre', 'Admin_');
DEFINE ('PathModulesTemplatePre', 'Mod_');
DEFINE ('PathModulesTemplateExt', '.tpl');
DEFINE ('AdminModuleDefault', 'PageEdit');
DEFINE ('ModuleDefaultId', 'new');
DEFINE ('ModuleDefaultPid', 0);
DEFINE ('ModuleDefaultLocation', 1);
DEFINE ('ModuleDefaultSo', 0);
DEFINE ('ModuleDefaultActive', 1);
DEFINE ('ModuleDefaultAcsLow', 0);
DEFINE ('ModuleDefaultAcsHigh', 255);
DEFINE ('ModuleDefaultModule', 'Choose Module');


/***** Load Page Modules ***************************************************
 *
 *  This method loads all active modules associated with a page if the user
 *  has appropriate permissions.
 *
 **************************************************************************/

class jwsfModuleObject {

    function loadPageModules () {
        $this->modules = array();

        // Read modules assigned to this page id
        $fields = array('location', 'module');
        $clause = 'WHERE pid="'
                . $GLOBALS['jwsfPage']->id
                . '" AND acsLow<="'
                . $GLOBALS['jwsfPage']->acslvl
                . '" AND acsHigh>="'
                . $GLOBALS['jwsfPage']->acslvl
                . '" AND active="1" ORDER BY location ASC, so ASC';
        $GLOBALS['db']->select($fields,
                                TableModuleAssignments,
                                $clause,
                                __FILE__,
                                __LINE__);

        // Fill the modules array and load the modules
        // 20090107 added hack to fix error trying to create existing user
        if ($GLOBALS['db']->numRows() > 1) {
            $this->modules = array();
            while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
                $this->modules[] = array($row['location'], $row['module']);
                if (file_exists(PathModules
                        . PathModulesTemplatePre
                        . $row['module']
                        . PathModuleExt)
                        ) {
                    include (PathModules
                        . PathModulesTemplatePre
                        . $row['module']
                        . PathModuleExt);
                }
            }
        } elseif ($GLOBALS['db']->numRows() == 1) {
            $row = $GLOBALS['db']->fetch(__FILE__, __LINE__);
            $this->modules[] = array($row['location'], $row['module']);
            if (file_exists(PathModules
                    . PathModulesTemplatePre
                    . $row['module']
                    . PathModuleExt)
                    ) {
                include (PathModules
                    . PathModulesTemplatePre
                    . $row['module']
                    . PathModuleExt);
            }
        }
    // End loadPageModules
    }


/***** Load Selected Administrative Module *************************************
 *
 *  This method loads the module passed in the URL if the filename is valid
 *  and the file exists in the modules directory with the name:
 *      'Admin_' . PassedValue . '.tpl'
 *  otherwise, it will load the default admin module (which better exist!).
 *
 ******************************************************************************/

    function loadAdminModule () {

        if (isset($_REQUEST['mod'])
                && (eregi('[a-z0-9_.-]{1,250}', $_REQUEST['mod']) == true)
                && (is_file(PathModules
                        . PathModulesAdminPre
                        . $_REQUEST['mod']
                        . PathModuleExt))
                ) {
            // If module has a valid filename and exists, load it
            include (PathModules
                . PathModulesAdminPre
                . $_REQUEST['mod']
                . PathModuleExt);
        } elseif (is_file(PathModules
                . PathModulesAdminPre
                . AdminModuleDefault
                . PathModuleExt)
                ) {
            // If module passed in the URL is not valid, load default
            include (PathModules
                . PathModulesAdminPre
                . AdminModuleDefault
                . PathModuleExt);

        }
    }


/***** Load Selected Administrative Module *************************************
 *
 *  This method loads the module passed in the URL if the filename is valid
 *  and the file exists in the modules directory with the name:
 *      'Admin_' . PassedValue . '.tpl'
 *  otherwise, it will load the default admin module (which better exist!).
 *
 ******************************************************************************/

    function loadModule ($module = null) {
        if (($module != null)
                && (eregi('[a-z0-9_.-]{1,250}', $module) == true)
                && (is_file(PathModules
                    . PathModulesTemplatePre
                    . $module
                    . PathModuleExt))
                ) {
            // If module has a valid filename and exists, load it
            include (PathModules
                . PathModulesTemplatePre
                . $module
                . PathModuleExt);
        }

    }


/***** Assign Modules To A Page ************************************************
 *
 *  This method adds an assignment to the module table.  This includes a
 *  page ID to associate with, sort order, active flag, access levels, and
 *  module name.
 *
 ******************************************************************************/

    function assign () {

    }


/***** Get All Assigments ******************************************************
 *
 *  This method lists all assignments and their properties.
 *
 ******************************************************************************/

    function getAssigned () {

        $this->temp = array();
        $this->assigned = array();

        $fields = array('id',
                        'pid',
                        'location',
                        'so',
                        'active',
                        'acsLow',
                        'acsHigh',
                        'module',
                        'stamp');
        $clause = 'WHERE pid IN (SELECT id FROM `'
                        . TablePageMetaData
                        . '`) ORDER BY pid, location, so, module';
        $GLOBALS['db']->select ($fields,
                                TableModuleAssignments,
                                $clause,
                                __FILE__,
                                __LINE__);
        while ($row = $GLOBALS['db']->fetch (__FILE__, __LINE__)) {
            foreach ($row as $key => $value) {
                $this->temp[$key] = $value;
            }
            $this->assigned[] = $this->temp;
            unset ($this->temp);
        }

    // End list
    }


/***** Remove A Module From A Page *********************************************
 *
 *  This method removes an assignment from the module table, all that is
 *  required is the ID from the module table.
 *
 ******************************************************************************/

    function remove () {

    }


/***** Update Module Assignment Table ******************************************
 *
 *  This method updates the entire module database according to the
 *  submitted post data.
 *
 *  TODO: This is not currently incorporated
 *
 ******************************************************************************/

    function update () {

    }


/***** Get Orphan Module Information *******************************************
 *
 *  This method returns all information on modules which do not have a valid
 *  page ID assignment.
 *
 ******************************************************************************/

    function getOrphans () {

        $this->temp = array();
        $this->orphans = array();

        $fields = array('id',
                        'pid',
                        'location',
                        'so',
                        'active',
                        'acsLow',
                        'acsHigh',
                        'module',
                        'stamp');
        $clause = 'WHERE pid NOT IN (SELECT id FROM `'
                        . TablePageMetaData
                        . '`) ORDER BY pid';
        $GLOBALS['db']->select ($fields,
                                TableModuleAssignments,
                                $clause,
                                __FILE__,
                                __LINE__);
        while ($row = $GLOBALS['db']->fetch (__FILE__, __LINE__)) {
            foreach ($row as $key => $value) {
                $this->temp[$key] = $value;
            }
            $this->orphans[] = $this->temp;
            unset ($this->temp);
        }

    // End getOrphans
    }


/***** Get Names Of Modules Which Exist ****************************************
 *
 *  This method acquires a list of all the non-admin modules in the module
 *  directory.  It also relieves the remaining file names of their prefix
 *  and extension and returns an associative array where key and value are
 *  equal (for passing to the form generator).
 *
 ******************************************************************************/

    function getAvailable () {

        $this->available = array();
        $mods = dir_as_file_array (PathModules);
        // Remove admin modules
        $l = strlen(PathModulesAdminPre);
        foreach ($mods as $key => $value){
            if (substr($value, 0, $l) != PathModulesAdminPre) {
                // Trim the fat and dub the array
                $l2 = strlen(PathModulesTemplatePre);
                $l3 = strlen(PathModulesTemplateExt);
                $l3 = $l3 - ($l3*2);
                $modName = substr ($value, $l2, $l3);
                $this->available[$modName] = $modName;
            }
        }
    // End getAvailable
    }


/***** Build Table Tags ********************************************************
 *
 *  Assemble all the tags for tabular display and assign to Smarty.  Assign
 *  open and close tags, submit button, valid assigned modules, orphan
 *  modules, and new assignment tags all as seperate values for manipulation
 *  in Smarty.
 *
 *  Open form tag and fieldset/legend
 *  Generate assigned module data and caption
 *  Generate orphan module data and caption
 *  Generate new assignment tag and caption
 *  Close fieldset/form
 *  Assign all generated values to smarty
 *
 ******************************************************************************/

    function buildModuleTable () {

        // Set destination variables
        $formOpen = array();
        $assigned = array();
        $orphans = array();
        $new = array();
        $formClose = array();
        // Prep object values
        $this->getAssigned();
        $this->getAvailable();
        $this->available['Choose Module'] = 'Choose Module';
        $this->getOrphans();
        // Get parent page title array
        $temp = new pageObject;
        $temp->getAll();
        $parents = $temp->all;
        $parents[0] = 'Choose A Page';
        unset ($temp);
        // Open form and fieldset
        $formOpen[] = jwsfGenerateForm (
                                        'moduleForm',
                                        'Module Assignment Form'
                                        );

        $formOpen[] = jwsfGenerateFieldsetWithLegend (
                                        'Module Assignment Tables'
                                        );
        // Generate assigned data
        foreach ($this->assigned as $record) {
            $assigned[] = $this->generateRowTags ($parents, $record);
        }
        // Generate orphan data
        foreach ($this->orphans as $record) {
            $orphans[] = $this->generateRowTags ($parents, $record);
        }
        // Generate insertion data
        $new[] = $this->generateRowTags ($parents);
        // Generate submit button
        $formClose[] = jwsfGenerateInputButton (
                                                'updateModules',
                                                'Update'
                                                );
        // Close form/fieldset
        $formClose[] = jwsfCloseFieldset ();
        $formClose[] = jwsfCloseForm ();
        // Assign to Smarty
        $GLOBALS['smarty']->assign ('formOpen', $formOpen);
        $GLOBALS['smarty']->assign ('assigned', $assigned);
        $GLOBALS['smarty']->assign ('orphans', $orphans);
        $GLOBALS['smarty']->assign ('new', $new);
        $GLOBALS['smarty']->assign ('formClose', $formClose);

    }


/***** Generate Assignment Table Row Array *************************************
 *
 *  This method operates on an associative array of parameters from the module
 *  assignment table and returns a full set of data to be looped into a display
 *  table via Smarty.
 *
 *  If the array parameter is null, it returns tags for an insertion row.
 *
 *  Requires:
 *      An array of id/titleNavs
 *
 ******************************************************************************/

    function generateRowTags ($pages, $array = null) {

        $rowTags = array();

        if ($array == null) {
            // Set defaults for new module
            $id = ModuleDefaultId;
            $pid = ModuleDefaultPid;
            $location = ModuleDefaultLocation;
            $so = ModuleDefaultSo;
            $active = ModuleDefaultActive;
            $acsLow = ModuleDefaultAcsLow;
            $acsHigh = ModuleDefaultAcsHigh;
            $module = ModuleDefaultModule;
        } else {
            // Turn passed module array into variables
            extract ($array);
            $rowTags[] = jwsfGenerateInputBox('record[' . $id . '][id]',
                                                'hidden',
                                                '',
                                                '',
                                                $id
                                                );
        }
        $rowTags[] = jwsfGenerateSelectFromArray ($pages,
                                                    'record[' . $id . '][pid]',
                                                    $pid
                                                    );
        $rowTags[] = jwsfGenerateSelectRange('record[' . $id . '][location]',
                                                    $location, 0, 2);
        $rowTags[] = jwsfGenerateSelectRange('record[' . $id . '][so]', $so);
        $rowTags[] = jwsfGenerateSelectBool('record[' . $id . '][active]',
                                                    $active);
        $rowTags[] = jwsfGenerateSelectRange('record[' . $id . '][acsLow]',
                                                    $acsLow);
        $rowTags[] = jwsfGenerateSelectRange('record[' . $id . '][acsHigh]',
                                                    $acsHigh);
        $rowTags[] = jwsfGenerateSelectFromArray($this->available,
                                                    'record[' . $id
                                                    . '][module]',
                                                    $module
                                                    );
        if ($array != null) {
            $rowTags[] = jwsfGenerateCheckRadio (
                                                    'record[' . $id
                                                        . '][deleteMod]',
                                                    '1',
                                                    'checkbox',
                                                    'Remove this assignment'
                                                    );
        }
        return $rowTags;

    // End generateRowTags
    }



// End class
}

?>
