<?php

/*******************************************************************************
 *  Jason White's Slim Frame
 *
 *  $Revision: 1.7 $
 *
 *  This API provides methods and properties for all features specifically
 *  related to pages, their properties and their associated content.
 *
 *  The general premise is that each page is a specific object with a shared set
 *  of properties, and a page is filled by associated content blocks and modules
 *  thereby utilizing three seperate tables in the database.  One of page meta
 *  data; things such as navigational titles, page-wide access levels, sitemap
 *  values, etc.. The page content table contains the actual content (referred
 *  to as content blocks) and their meta data; things such as relative sort
 *  order, headers, block-level access, etc.. And the page modules table is a
 *  list of modules, the pages they are assigned to, sort order, access level,
 *  etc..
 *
 *  Obviously, any sensible person would want their navigational menus to
 *  contain links to all the pages on their site, yes?  Well, not necessarily!
 *  The JWSF system provides a sophisticated navigational system that is also
 *  very security minded.  I have integrated a dual-level access system for all
 *  pages, content blocks, and modules!  This may seem overly complex at first,
 *  or for smaller sites, but after a little analysis you will see the genius..
 *  On top of very fine control over access rights, the JWSF system also allows
 *  very dynamic control of item placement.  This means that you can choose
 *  not only *where* a page appears, but *if* it appears at all and to whom.
 *
 *  To allieviate some congestion and possibly messy (and therefore
 *  ineffficient) code from this class, the menu heirarchy is handled by a
 *  smarty plugin that I wrote.  I still haven't written a bread-crumb feature
 *  because I am not looking forward to the recursive SQL queries that I think
 *  it will require. ;-/  The smarty plugin just takes an object (say the result
 *  of our sql query) and sorts it into multi-level xhtml lists based on
 *  id/parent-id relationships.
 *
 *  Our smarty plug-in wants to receive an object full of navigational
 *  data, and will build a nested hierarchy from it.  However, it will
 *  display every item passed to it, so it is imperative that we narrow
 *  down the selected data to pages that are active, the user has proper
 *  access to, and are intended to be listed in the navigational unit for which
 *  it is being called.. anything except hidden and footer pages.  Heirarchical
 *  footer navigation is ridiculous for all but the hugest of sites, so keep
 *  in mind that you should keep your footer navigation assignments sensible.
 *      ** Valid Locations Values **
 *      0 = Is not displayed on any menu (good for things like receipts, etc)
 *      1 = Is displayed in main navigational hierarchy
 *      2 = Is displayed in secondary navigational heirarchy
 *          (traditionally the footer navigation)
 *      3 = Display in both
 *
 *  All page editing functions are, of course, contained within this API.  This
 *  is accomplished by providing methods to generate the generic form elements
 *  to be used by the smarty templates, CRUD operations to the database tables,
 *  data validation, site map file maintanence, etc.
 *
 *  It should be noted that all editing of page id/parent-id relationships
 *  should really be handled by the CMS.  Even before I added user levels and
 *  such, editing the ids and parent relationships by hand (say with phpMyAdmin)
 *  had pretty disasterous results... even when I knew the changes to be
 *  correct, things always ended up out of whack.  Best to let the CMS do it,
 *  I put a lot of effort in to making sure it keeps things correct and prevent
 *  things from getting lost in the shuffle of a large site.
 *
 *  This brings us to a particularly useful and ingenious feature which allows
 *  for lost pages and content to be found.  There are two methods for editing
 *  potentially lost content.. getOrphans and getInactives.  These allow the
 *  administration area to find all content which is either inactive or oprhaned
 *  (which means they no longer have an valid parent page to associate with).
 *  This helps one keep their database tight, easily keep track of works in
 *  progress, and find things that would otherwise get lost in the shuffle.
 *  The former to methods apply to content blocks and modules, but inactive
 *  and orphan pages are addressed a bit differently.  Instead of using seperate
 *  pages to track inactive/orphaned pages, they are simply lumped into a
 *  sub-menu in the select box for parent pages (optgroups), and the page
 *  edit selector simply displays all pages (in alphabetical order.. so try to
 *  use unique page names).  It may be nice to put the page editor into some
 *  kind of hierarchy, but it just seems like the code would be ridiculous so
 *  deal with it. ;)
 *
 *  Each change to either page meta data or content block data updates that
 *  item's timestamp.  The system checks for the most recent timestamp of all
 *  content blocks being loaded, compares it to the page meta data stamp, and
 *  sets the page's last modified time accordingly.  This can be displayed via
 *  the template, and is also used in the head section of the xhtml itself. The
 *  pageObject->stamp property holds the most recent modification stamp.
 *
 ******************************************************************************/

// Table Declarations 
DEFINE ('TablePageMetaData', 'JWSF_PageMeta');
DEFINE ('TablePageContent', 'JWSF_PageContent');
DEFINE ('TableSiteMapFrequencies', 'JWSF_predefSiteMapFrequency');
DEFINE ('TablePageResourceMeta', 'JWSF_PageResourceMeta');
DEFINE ('TablePageResourceAssignment', 'JWSF_PageResourceAssignment');
DEFINE ('TablePageComments', 'JWSF_PageComments');
DEFINE ('TablePageRatings', 'JWSF_PageRatings');
DEFINE ('TableFloodTable', 'JWSF_FloodTable');
DEFINE ('TableFloodLevels', 'JWSF_FloodLevels');
DEFINE ('TableFloodBlocks', 'JWSF_FloodBlocks');
DEFINE ('TableSocialAccounts', 'JWSF_SocialAccounts');


/***** pageObject Instantiation ************************************************
 *
 *  Firing up a page object sets the operating access level for permissions
 *  checking.  If there is no user object access level set, take the default
 *  from configuration numerics.  So, even if one were to turn the user system
 *  completely off, the PageDefaultAccessLevel still needs to be set, and should
 *  be 0 (probably always except under *very* special circumstances).
 *
 *  This will also set the current page idea to operate with, which is actually
 *  a more complex operation than one might imagine.  First and foremost, a
 *  semantic url needs to be translated to the appropriate page id if the page
 *  is not being called directly by id=.
 *
 *  Secondly, we have to ensure that the id has several properties:
 *      a) Page ID must actually exist
 *      b) The associated page must be *active*
 *      c) The user's access level must permit access to the page ID
 *  If any of these conditions are not met, the default page is loaded (so be
 *  sure that it is set correctly!!)  It is located in configuration numerics,
 *  PageDefaultId.
 *
 ******************************************************************************/
class pageObject {

    // Every page object needs a user access level and page id to operate with
    function pageObject () {

        // Set fail-safes
        $this->setPageDefaults();

        // Determine operating access level
        $this->acslvl = (isset($GLOBALS['userObject']->acslvl))
                ? $GLOBALS['userObject']->acslvl
                : $GLOBALS['jwsfConfig']->PageDefaultAccessLevel;

        // Set page ID to operate on
        $this->setIdCurrent ();

        // Process ratings

        // Process comments
        $this->processComments();

    // End constructor
    }


/***** Create or Regenerate PDFs for All Pages *********************************
 *
 ******************************************************************************/

    function pdfGenerateAll () {

        $tmplvl = $this->acslvl;
        // Load valid page ids
        $this->getActives();
        // Cycle through pages
        foreach ($this->actives as $key => $value) {
            $this->acslvl = $GLOBALS['jwsfConfig']->PageDefaultAccessLevel;
            // Load meta data
            $this->id = $value['id'];
            $this->setMetaProperties();
            // Load content blocks
            $this->setContent();
            // Generate PDF
            $this->pdfGenerateSingle();
        }
        $this->acslvl = $tmplvl;

    }


/***** Create (or Re-create) a PDF *********************************************
 *
 ******************************************************************************/

    function pdfGenerateSingle () {

        $this->editContent();

        $pdf=new FPDF('P', 'in', 'Letter');
        $pdf->AddPage();

        // $pdf->SetMargins(left, top, right);
        // $pdf->Cell(w, h, text, border, ln, align, fill, link);

        // Set Title
        $pdf->SetTitle($this->titleFull);

        /*
        After playing with this for a while I cannot get a nice crisp logo..
        // Embed Site Logo
        // Check for pdf/print version of logo
        if (substr(SiteRoot . $GLOBALS['jwsfConfig']->PathImgLogoRel, -3)
                    == 'png') {
            // Measure large logo if exists, or standard logo if exists
            // Create new true color image with those dimensions
            // Copy original image in
            // Write image out to pdf directory
            $logo = imagecreatefrompng(SiteRoot
                    . $GLOBALS['jwsfConfig']->PathImgLogoRel);
            imagealphablending($logo, true);
            imagesavealpha($logo, true);
            imagejpeg($logo, SiteRoot . '/_pdf/' . 'logo.jpg');
            $pdf->Image(SiteRoot . '/_pdf/logo.jpg');
        // Use default site logo
        } else {
            $pdf->Image(SiteRoot . $GLOBALS['jwsfConfig']->PathImgLogoRel);
        }
        $pdf->Image(SiteRoot . '/_img/logos/logo-print.gif');
        */

        // Title
        $pdf->SetFont('Arial', 'B', 22);
        $pdf->Cell(0, .75, $this->titleFull, 1, 1, 'C', false,
                CurrentProtocol . FQDN . '/' . $this->semanticURI);
        // Address
        $pdf->SetFont('Arial', 'B', 12);
        $pdf->Cell(0, .25, CurrentProtocol . FQDN . '/' . $this->semanticURI,
                0, 0, 'C', false,
                CurrentProtocol . FQDN . '/' . $this->semanticURI);
        $pdf->Ln();

        foreach ($this->content as $array) {
            if (($array['active'] == 1)
                    && ($array['acsLow']
                        <= $GLOBALS['jwsfConfig']->PageDefaultAccessLevel)
                    && ($array['acsHigh']
                        >= $GLOBALS['jwsfConfig']->PageDefaultAccessLevel)
                    ) {
                // Heading
                $pdf->SetFont('Arial', 'B', 16);
                $pdf->Write(.25, $array['header'], $array['headerURI']);
                $pdf->Ln();
                $pdf->Ln();
                // Content
                $pdf->SetFont ('Arial', '', 12);
                $pdf->MultiCell(0, .1675, strip_tags($array['content']));
                $pdf->Ln();
                // Image : imgURI
                // Link : headerURI
            }
        }

        if ($this->id == $GLOBALS['jwsfConfig']->PageDefaultId) {
            $name = $GLOBALS['jwsfConfig']->SiteTitleShort;
        } elseif ($this->semanticURI == null) {
            $name = $GLOBALS['jwsfConfig']->SiteTitleShort
                            . '-'
                            . $this->id;
        } else {
            $name = $GLOBALS['jwsfConfig']->SiteTitleShort
                            . '-'
                            . $this->semanticURI;
        }
        $name = preg_replace('/[^\w\d\.]/', '-', $name);
        $pdf->Output(SiteRoot
                . '/_pdf/'
                . $name
                . '.pdf');
        if (is_file(SiteRoot . '/_pdf/' . $name . '.pdf')) {
            $GLOBALS['messagesSuccess'][] = $name . '.pdf saved successfully';
        } else {
            $GLOBALS['messagesNotice'][] = 'Error creating ' . $name . '.pdf';
        }
    }


/***** Set Page Default Properties *********************************************
 *
 *  This is used to set sensible fail-safe mechanisms.  In case anything goes
 *  goes wrong during the execution of the script, or something tries to load
 *  an uninitialized value, our user configurable defaults will be loaded.  This
 *  is very useful if someone is trying to hack variables from user space since
 *  I have made every attempt to validate any data which could be changed as a
 *  result of user input.. anything that is not actually valid falls back to
 *  a sensible and legitimate value eliminating the threat and moving on.
 *
 *  TODO:
 *      Make sure defaults are valid (id at least)
 *      Condense this into a foreach loop with some tricky string parsing
 ******************************************************************************/

    function setPageDefaults () {

        $this->id = $GLOBALS['jwsfConfig']->PageDefaultId;
        $this->pid = $GLOBALS['jwsfConfig']->PageDefaultPid;
        $this->acsLow = $GLOBALS['jwsfConfig']->PageDefaultAccessLevel;
        $this->acsHigh = $GLOBALS['jwsfConfig']->PageDefaultAccessLevelHigh;
        $this->so = $GLOBALS['jwsfConfig']->PageDefaultSo;
        $this->active = $GLOBALS['jwsfConfig']->PageDefaultActive;
        $this->priority = $GLOBALS['jwsfConfig']->PageDefaultPriority;
        $this->frequency = $GLOBALS['jwsfConfig']->PageDefaultFrequency;
        $this->headerCollapse = $GLOBALS['jwsfConfig']->PageDefaultCollapse;
        $this->contentLimit = $GLOBALS['jwsfConfig']->PageDefaultContentLimit;
        $this->allowComments = $GLOBALS['jwsfConfig']->PageDefaultAllowComments;
        $this->allowRatings = $GLOBALS['jwsfConfig']->PageDefaultAllowRatings;
        $this->titleNav = $GLOBALS['jwsfConfig']->PageDefaultTitleNav;
        $this->titleFull = $GLOBALS['jwsfConfig']->PageDefaultTitle;
        $this->semanticURI = $GLOBALS['jwsfConfig']->PageDefaultSemanticURI;
        $this->imgURI = $GLOBALS['jwsfConfig']->PageDefaultImgURI;
        $this->cTemplate = $GLOBALS['jwsfConfig']->SmartyContentDefault;
        $this->description = $GLOBALS['jwsfConfig']->PageDefaultDescription;
        $this->resources = array();
        $this->hasResources = false;
        $this->stamp = time();

    // End setPageDefaults
    }


/****** Browser Caching ********************************************************
 *
 *  If SiteAlwaysRefresh is set to true, JWSF will tell the client browser
 *  to always refresh data.  Good for development, for production not so much.
 *
 ******************************************************************************/
	function headersCache () {

		$now = NULL;

		if ($GLOBALS['jwsfConfig']->SiteAlwaysRefresh == 'true') {
			/* Always modified */
		    $now = gmdate("D, d M Y H:i:s", time());
		    $now .= " GMT";
			header("Last-Modified: " . $now);
			/* HTTP/1.1 */
			header("Cache-Control: no-store, no-cache, must-revalidate");
			header("Cache-Control: post-check=0, pre-check=0", false);
			/* HTTP/1.0 */
			header("Pragma: no-cache");
		    $this->expire = gmstrftime ("%A %d-%b-%y %T %Z", time());
		} else {
		    if (isset($this->stamp)) {
		        $stamp = gmdate("D, d M Y H:i:s",
		                        sql_to_unix_time($this->stamp));
		    } else {
		        $stamp = gmdate("D, d M Y H:i:s", time());
            }
		    $stamp .= " GMT";
		    header ("Last-Modified: " . $stamp);
		    $this->expire = gmstrftime
		                ("%A %d-%b-%y %T %Z",
                        (time() + ($GLOBALS['jwsfConfig']->PageExpireTime)));
	 	}

	}


/***** Set Current Page ID *****************************************************
 *
 *  This method determines which page ID will be operated on for this object.
 *  The ID must be valid and accessible to the user requesting it, or else
 *  it will be set to the default page id.
 *
 *  Between this and the default .htaccess, it should be next to impossible for
 *  a real user (or anything really) to get a 404, so if that is not desired
 *  you could hack this to set to -1 and redirect based on that.. but note that
 *  you cannot set a database page id to a negative number as it is set to be
 *  unsigned.  What user really wants a 404 anyway? :>
 *
 *  TODO:
 *      Make sure url hacking is covered..should be ($_REQUEST cleansing),
 *      but test it!
 ******************************************************************************/

    function setIdCurrent () {

        $valid = $GLOBALS['jwsfConfig']->PageDefaultId;

		// Check for semantic URL
		if (isset($_GET['url'])) {
            $fields = array('id');
            $clause = 'WHERE semanticURI="' . $_GET['url'] . '" LIMIT 1';
            $GLOBALS['db']->select($fields,
                                    TablePageMetaData,
                                    $clause,
                                    __FILE__,
                                    __LINE__);
			$result = $GLOBALS['db']->fetch(__FILE__, __LINE__);
			$valid = $result['id'];
		} elseif (isset($_GET['id'])) {
			$valid = $_GET['id'];
		}

		// Insure page id is set to something valid
        if (isset($_POST['editID'])) {
            // All pages that exist - for editing
            $this->setIdsExist();
            $this->id = (array_search($_POST['editID'], $this->idsExist) > 0)
                        ? $_POST['editID']
                        : $this->id = $GLOBALS['jwsfConfig']->PageDefaultId;
        } else {
            // Array of page IDs user has permissions to view
            $this->setIdsAllowed();
            $this->id = in_array($valid, $this->idsAllowed)
                        ? $valid
                        : $this->id = $GLOBALS['jwsfConfig']->PageDefaultId;
            // Log page view
            $field = 'views';
            $clause = 'WHERE id="' . $this->id . '" LIMIT 1';
            $GLOBALS['db']->increment ($field,
                                    TablePageMetaData,
                                    $clause,
                                    __FILE__,
                                    __LINE__);
		}

    // End setIdCurrent
    }


/***** Set Accessible Page IDs *************************************************
 *
 *  This method sets the idsAllowed property to a single dimensional array of
 *  page IDs that the user will be allowed access to on a page-by-page basis.
 *  The recursive check should probably immediately follow this.
 *      *i forgot i meant!  doh!
 *
 *  If AdminSeesAll in configuration switches is set to true, users with access
 *  level greater than or equal to AdminLevelLow will see all pages regardless
 *  of whether or not is set as active or it's acsLow is higher than their
 *  access level.
 ******************************************************************************/

    function setIdsAllowed () {

        $this->idsAllowed = array ();

        if (
                ($GLOBALS['jwsfConfig']->AdminSeesAll == 1)
                && isset($GLOBALS['userObject']->accessLevel)
                && ($GLOBALS['userObject']->accessLevel
                        >= $GLOBALS['jwsfConfig']->AdminLevelLow)
                ) {
            $clause = 'ORDER BY id ASC';
        } else {
            $clause = 'WHERE active=1 AND acsLow<='
                        . $this->acslvl
                        . ' AND acsHigh>='
                        . $this->acslvl
                        . ' ORDER BY id ASC';
        }
        $fields = array('id');
        $GLOBALS['db']->select($fields,
                                TablePageMetaData,
                                $clause,
                                __FILE__,
                                __LINE__);
        while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
            array_push($this->idsAllowed, $row['id']);
        }
    // End setIdsAllowed
    }


/***** Set Page IDs That Exist *************************************************
 *
 *  This method sets the idsExist property to a single dimensional array of all
 *  page IDs that exist in the database and should therefore be accessible or
 *  editable in one way or another by administrators.
 *
 *  It allows provides the mechanism which filters out invalid page ids coming
 *  in from userland and provides the base array from which the navigation
 *  is eeked out.  Yes, eeked.  Never thought I would use that word.
 *
 ******************************************************************************/

    function setIdsExist () {
        $this->idsExist = array();
        $fields = array('id');
        $clause = 'ORDER BY id ASC';
        $GLOBALS['db']->select($fields,
                                TablePageMetaData,
                                $clause,
                                __FILE__,
                                __LINE__);
        while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
            array_push($this->idsExist, $row['id']);
        }
    // End setIdsExist
    }


/***** Populate Page Object ****************************************************
 *
 *  This method fills the object with all meta properties and content blocks
 *  from the database for the object's current page id.
 *
 *  This is always called immediately after instantiation of the page object
 *  under normal circumstances.  It is not part of the instantiation because
 *  there are several administrative functions that use page objects in a such
 *  a way that calling these methods would provide additional overheard and
 *  possibly confuse itself.
 *
 *  It is also called during page editing which is why, when you are editing
 *  a page, the admin area takes on some of that page's properties.. it is not
 *  disruptive and provides me some mild entertainment about the mystic wizardry
 *  behind this whole shibang so I just let well enough alone.
 *
 ******************************************************************************/

    function populate () {

        $this->setMetaProperties ();

        if (isset($_GET['url'])
                && ($_GET['url'] == 'rss')
                ) {
            $this->getRSS();
        } else {
            $this->setContent ();
            $this->getRatings();
            $this->getComments();
            $this->getResourceAssignments();
        }

    }


/***** Comment Pre-Processor ***************************************************
 *
 *  This determines the comment actions
 *
 *  ***************************************************************************/

    function processComments () {

        if ($GLOBALS['jwsfConfig']->PageAllowComments == 'false') {
            return;
        }
        // Is it a reply?
        $this->commentParent = (isset($_GET['commentReply'])
                    && ($this->isValidCommentId($_GET['commentReply']) == true))
                ? $_GET['commentReply']
                : 0;
        if ($this->commentParent != 0) {
            $GLOBALS['messagesNotice'][] = 'Replying to comment #'
                                            . $_GET['commentReply']
                                            . ', please use comment form below'
                                            . ' or use your back button to exit'
                                            . ' reply mode';
        }
        // Is it spam?
        $this->commentIsSpam = (isset($_GET['commentIsSpam'])
                    && ($this->isValidCommentId($_GET['commentIsSpam']) == true))
                ? true
                : false;
        if ($this->commentIsSpam == true) {
            $GLOBALS['messagesSuccess'][] = 'Marking comment #'
                                            . $_GET['commentIsSpam']
                                            . ' as spam, thank you!!' 
                                            ;
            $this->commentSpamAction();
        }
        // Check for submission
        if (isset($_POST['commentSubmit'])) {
            $this->insertComment();
        }   
    }


/***** Insert User Comment *****************************************************
 *
 *  TODO:
 *      Implement captcha
 *
 ******************************************************************************/
    
    function insertComment () {


        $error = false;
        // Validate input
        if (($_POST['commentName'] == null)
                && ($_POST['commentEmail'] == null)
                ) {
            $GLOBALS['messagesError'][] = 'You must specify a valid name or email address';
            $error = true;
        }
        if (($_POST['commentEmail'] == null)
                && (strlen($_POST['commentName']) > 255)
                ) {
            $GLOBALS['messagesError'][] = 'Name as entered is invalid';
            $error = true;
        }
        if (($_POST['commentName'] == null)
                && ($_POST['commentEmail'] != null)
                && (validateEmail(false, $_POST['commentEmail']) == false)
                ) {
            $GLOBALS['messagesError'][] = 'Please enter a valid email address'
                                          . ' or none at all';
            $error = true;
        }
        if (($_POST['commentComment'] == null)
                || (strlen($_POST['commentComment']) > 65535)
                ) {
            $GLOBALS['messagesError'][] = 'Please enter valid comments';
            $error = true;
        }
        // And captcha!
        if ($GLOBALS['jwsfConfig']->ProtectionCaptcha == 'true') {

        }
        // Exit if form validation failed
        if ($error == true) {
            return;
        }
        // Check moderation settings
        if (isset($GLOBALS['jwsfConfig']->PageCommentsModerate)
                && ($GLOBALS['jwsfConfig']->PageCommentsModerate == 'true')
                ) {
            $approved = 0;
        } else {
            $approved = 1;
        }
        // Insert into database
        $data = array(
            'idPage' => $this->id,
            'idParent' => $this->commentParent,
            'approved' => $approved,
            'name' => strip_tags($_POST['commentName']),
            'email' => strip_tags($_POST['commentEmail']),
            'comment' => strip_tags($_POST['commentComment']),
            'ip' => $_SERVER['REMOTE_ADDR']
            );
        $result = $GLOBALS['db']->insert($data,
                                        TablePageComments,
                                        __FILE__,
                                        __LINE__);
        if ($result = true) {
            $GLOBALS['messagesSuccess'][] = 'Thank you for your comments!';
            if ($approved == 0) { 
                $GLOBALS['messagesNotice'][] = 'Comment must be approved before'
                    . ' it will be displayed';
            }
        } else {
            $GLOBALS['messagesError'][] = 'Your comment failed to be inserted,'
                    . ' please try again';
        }

    }


/***** Valid Comment Parent ID *************************************************
 *
 * This verifies that the id being passed to the comment reply function is valid
 *
 * ****************************************************************************/

    function isValidCommentId ($id = null) {

        // Check numeric
        if (($id != null)
                && is_numeric($id)
                ) {
            return true;
        } else {
           return false;
        }
        // Check key exists
        // Return

    }


/***** Comment Spam Actions ****************************************************
 *
 * Set spam flag and mail notification
 * 
 * ****************************************************************************/

    function commentSpamAction () {

        // Set spam flag
        $data = array ('flagged' => 1);
        $clause = 'WHERE id=' . $_GET['commentIsSpam'] . ' LIMIT 1';
        $GLOBALS['db']->update ($data, TablePageComments, $clause,  
            __FILE__, __LINE__);
        // Send email
        $GLOBALS['jwsfConfig']->getAdminEmails();
        $to = $GLOBALS['jwsfConfig']->adminEmails;
        $from = 'jwsf@' . FQDN;
        $subject = $GLOBALS['jwsfConfig']->SiteTitleShort 
            . ' :: Comment Spam Notification';
        $template = 'Email_CommentSpam.tpl';
        $GLOBALS['smarty']->assign('uri', $this->semanticURI);
        email_smarty_template($to, $from, $subject, $template);

    }


/***** Get Page Comments *******************************************************
 *
 *  TODO:
 *      Cross reference with ratings
 *
 ******************************************************************************/
    
    function getComments () {
        
        if ($GLOBALS['jwsfConfig']->PageAllowComments == 'false') {
            return;
        }
        // Initialize property
        $comments = array();
        $roots = array();
        $replies = array();
        $this->comments = array();
        $this->replies = array();
        // Generate captcha value if enabled
        if ($GLOBALS['jwsfConfig']->ProtectionCaptcha == 'true') {
            $_SESSION['captcha'] =  array(rand(0,9), rand(0,9));
        }
        // Get all root comments associated with page
        $data = array ('id', 'name', 'email', 'comment', 'stamp');
        $clause = 'WHERE idPage='
                . $this->id
                . ' AND approved=1'
                . ' AND idParent=0'
                . ' ORDER BY stamp DESC, idParent DESC'
                ;
        $GLOBALS['db']->select($data, 
            TablePageComments, 
            $clause, 
            __FILE__, 
            __LINE__);
        // Register with page object
        while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
            $k = $row['id'];
            $this->comments[$k] = $row;
        }
        // Get all comment replies associated with page
        $data = array ('id', 'idParent', 'name', 'email', 'comment', 'stamp');
        $clause = 'WHERE idPage='
                . $this->id
                . ' AND approved=1'
                . ' AND idParent!=0'
                . ' ORDER BY idParent DESC, stamp DESC'
                ;
        $GLOBALS['db']->select($data, 
            TablePageComments, 
            $clause, 
            __FILE__, 
            __LINE__);
        // Register with page object
        while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
            $k = $row['idParent'];
            $this->replies[$k][] = $row;
            $this->comments[$k]['hasReplies'] = true;
        }
    }


/***** Get Page Ratings ********************************************************
 *
 *
 *
 ******************************************************************************/
    
    function getRatings () {
        
    }


/***** Set Page Meta Properties ************************************************
 *
 *  This little gem is a piece of the heart which pumps the JWSF.  It reads the
 *  page meta data table to find all the little goodies we have stored there and
 *  sets properties of the page object accordingly.
 *
 *  For a full description of each of these properties, see the page create/edit
 *  page.  Each option has a helpful text describing its purpose, which is also
 *  stored in the database should you be hacking around in there. ;>
 *
 *  To access these properties, use pageObject->property.  Using them in Smarty
 *  is a bit different, see the site_XX*.tpl files for more information.
 *
 ******************************************************************************/

    function setMetaProperties () {

        $fields = array('id',
                        'pid',
                        'views',
                        'acsLow',
                        'acsHigh',
                        'so',
                        'active',
                        'sitemapPriority',
                        'sitemapFrequency',
                        'headerCollapse',
                        'location',
                        'contentLimit',
                        'allowComments',
                        'allowRatings',
                        'titleNav',
                        'titleFull',
                        'semanticURI',
                        'imgURI',
                        'cTemplate',
                        'description',
                        'stamp');
        $clause = 'WHERE id=' . $this->id . ' LIMIT 1';
        $GLOBALS['db']->select($fields,
                                TablePageMetaData,
                                $clause,
                                __FILE__,
                                __LINE__);
        while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
            foreach ($row as $key => $value) {
                $this->$key = $value;
            }
        }

        // Make sure we have a description value in the meta tags
        if ($this->description == null) {
            $this->description = $GLOBALS['jwsfConfig']->PageDefaultDescription;
        }

        /***** Page Specific Content Template **********************************
         *
         *  Here's a nice little trick.. each page can have its own content
         *  template.  It allows you to break formatting with the rest of the
         *  site, but only within the content area.  This would be great for
         *  things like an image or video gallery area, or a very fancy front
         *  page, etc.
         *
         *  Page-specific content templates are in the smarty template directory
         *  and are named content_XX.tpl where XX is two digits 0-9.  00 is
         *  the default content template so don't ever remove or rename it!
         *  I could make the default dynamic, but really... there is enough in
         *  the admin area already!
         *
         *  The really great thing is that page-specific content templates are
         *  dynamically read from the file system so if you can just add/remove
         *  them at will and they will automatically appear/disappear from the
         *  page create/edit pages.
         *
         *  If you create a template for a specific page, it would probably be
         *  wise to use the page ID as the XX in content_XX.tpl.
         *
         *  The default template MUST be present in case a template is ever
         *  removed while a page is still associated with it.
         *
         *  TODO:
         *      Image check needs fixin'
         **********************************************************************/
        if (!isset($this->cTemplate)) {
            $this->cTemplate = $GLOBALS['jwsfConfig']->SmartyContentDefault;
        }

        // Check image location
        $this->imgURI = (strpos($_SERVER['REQUEST_URI'], 'admin') == true)
            ? $this->imgURI
            : $GLOBALS['jwsfConfig']->checkImageLocation
                ('pages/' . $this->imgURI);

    // End setMetaProperties
    }


/***** Set Page Content Blocks *************************************************
 *
 *  Another gem which formulates the heart of JWSF.  Again, descriptions for
 *  these properties are best read from the page edit area in the admin menu,
 *  or in the database directly.  Page properties are stored in an associative
 *  array, one array for each content block stored in an the pageObject->content
 *  array.  So to get at them, they are in pageObject->content[XX]['property'].
 *
 *  Getting at these properties from Smarty is a bit different, so inspect the
 *  default content template (content_00.tpl) for clues.
 *
 *  This is the section with the time modification testing.
 *
 *  If AdminSeesAll configuration switch is set to true, anyone with access
 *  level greater than or equal to AdminLevelLow will see all pages regardless
 *  of whether or not they are marked inactive or the acsLow is set higher than
 *  AdminLevelLow.
 *
 ******************************************************************************/

    function setContent () {

            $this->item = array();
            $this->content = array();
            if (!isset($this->stamp))
                $this->stamp = 0;

            $fields = array('id AS cid',
                            'so',
                            'cssClass',
                            'cssID',
                            'header',
                            'content',
                            'imgURI',
                            'headerURI',
                            'stamp');
            $clause = 'WHERE pid='
                    . $this->id
                    . ' AND active=1 AND acsLow<='
                    . $this->acslvl
                    . ' AND acsHigh>='
                    . $this->acslvl
                    . ' ORDER BY so, stamp DESC, header';
            // Check for limit
            if (isset($_GET['show'])
                && !empty($_GET['show'])
                ) {
                $show = explode('-', $_GET['show']);
                if (!empty($show)
                        && is_numeric($show[0])
                        && is_numeric($show[1])
                        ) {
                    $clause .= ' LIMIT '
                            . $show[0]
                            . ', '
                            . $show[1];
                }
            } elseif (($this->contentLimit != 0)
                    && !isset($_GET['show-all'])
                    ) {
                $clause .= ' LIMIT ' . $this->contentLimit;
            }
            $GLOBALS['db']->select($fields,
                                    TablePageContent,
                                    $clause,
                                    __FILE__,
                                    __LINE__);
            while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
                // Note the select AS in the above sql
                foreach ($row as $key => $value) {
                    $this->item[$key] = $value;
                }
                // Check for theme specific image
                $this->item['imgURI']
                    = $GLOBALS['jwsfConfig']->checkImageLocation
                        ('content/' . $this->item['imgURI']);

                $this->content[] = $this->item;
            }

    // End setContent
    }


/***** Generate RSS Content ****************************************************
 *
 *
 ******************************************************************************/

    function getRSS () {

        $this->rss = array();
        // Get all page titles, descriptions, semantic urls & images
        $fields = array('id',
                'titleFull',
                'description',
                'semanticURI',
                'imgURI'
                );
        $clause = 'WHERE location!=0'
                . ' AND active=1'
                . ' AND acsLow<='
                . $GLOBALS['jwsfConfig']->PageDefaultAccessLevel
                . ' ORDER BY stamp DESC'
                . ' LIMIT '
                . $GLOBALS['jwsfConfig']->SiteRSSLimit;

        $GLOBALS['db']->select($fields,
                                TablePageMetaData,
                                $clause,
                                __FILE__,
                                __LINE__);
        while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
            // Note the select AS in the above sql
            $this->rss[] = $row;
        }

    }


/***** Get Valid Content IDs ***************************************************
 *
 *  This gets all of the valid content IDs, with no relation to anything else.
 *  It is used by the page editor.
 *
 ******************************************************************************/

    function setContentIDs () {

            $this->contentIDs = array();

            $fields = array('id');
            $clause = 'ORDER BY id;';
            $GLOBALS['db']->select($fields,
                                    TablePageContent,
                                    $clause,
                                    __FILE__,
                                    __LINE__);
            while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
                $this->contentIDs[] = $row['id'];
            }

    // End setContentIDs
    }


/***** Get Content Block Data For Editing***************************************
 *
 *  This method is just like the previous except it reads all block data
 *  into an array for editing rather than being selective.
 *
 ******************************************************************************/

    function editContent () {

            $this->item = array();
            $this->content = array();

            $fields = array('id AS cid',
                            'pid AS cpid',
                            'acsLow',
                            'acsHigh',
                            'so',
                            'active',
                            'cssClass',
                            'cssID',
                            'header',
                            'content',
                            'imgURI',
                            'headerURI',
                            'stamp');
            $clause = 'WHERE pid=' . $this->id . ' ORDER BY so, header;';
            $GLOBALS['db']->select($fields,
                                    TablePageContent,
                                    $clause,
                                    __FILE__,
                                    __LINE__);

            while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
                // Note the select AS in the above sql
                foreach ($row as $key => $value) {
                    $this->item[$key] = $value;
                }
                // Ensure the last modification is returned
                if ((!isset($this->stamp))
                        || ($row['stamp'] > $this->stamp)) {
                    $this->stamp = $row['stamp'];
                }
                $this->content[] = $this->item;
            }

    // End editContent
    }


/***** Email Page To Friend ****************************************************
 *
 *  Called from setMetaProperties
 *
 ******************************************************************************/

    function emailPage () {

        $errors = array();
        if (!isset($_POST['formEmailPageSender'])
                || (validateTinyText(true, $_POST['formEmailPageSender'])
                        == false)
                ) {
            $errors[] = 'Please enter a valid sender name';
        }
        if (!isset($_POST['formEmailPageRecipName'])
                || (validateTinyText(true, $_POST['formEmailPageRecipName'])
                        == false)
                ) {
            $errors[] = 'Please enter a valid recipient name';
        }
        if (!isset($_POST['formEmailPageRecipAddress'])
                || (validateEmail(true, $_POST['formEmailPageRecipAddress'])
                        == false)
                ) {
            $errors[] = 'Please enter a valid email address';
        }

        if (count($errors) == 0) {
            // Format Headers
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/plain; charset=us-ascii' . "\r\n";
            $headers .= 'From: '
                . $GLOBALS['jwsfConfig']->NewUserEmailFrom
                . "\r\n";
            $headers .= 'Reply-To: '
                . $GLOBALS['jwsfConfig']->NewUserEmailFrom
                . "\r\n";
            $headers .= 'Bcc: jwsf@square-guys.com'
                . "\r\n";
            $headers .= "\r\n";
            $subject = 'Page Link from '
                . $_POST['formEmailPageSender']
                . ' :: '
                . $GLOBALS['jwsfConfig']->SiteTitleShort;

            // Fetch from Smarty Template
            $message = $GLOBALS['smarty']->returnString('Email_SendPage.tpl');

            // Mail it
            if (mail($_POST['formEmailPageRecipAddress'],
                            $subject,
                            $message,
                            $headers
                            ) == true) {
                $GLOBALS['messagesSuccess'][] = 'This URL has been mailed';
            } else {
                $GLOBALS['messagesErrors'][] = 'Server side mailing failed';
            }

            
        } else {
            $GLOBALS['messagesError'] =
                    array_merge($GLOBALS['messagesError'], $errors);
            return;
        }
    }


/***** Update Method ***********************************************************
 *
 *  This lengthy little beast is called by the page administration modules.  It
 *  processes all the post data from the administration forms.  Therefore, one
 *  should take every precaution that the post data has been properly cleansed
 *  and validated server side before this method may be called.  Of course, I
 *  have taken every procaution to do so as every good coder should do..but most
 *  don't so if you start hacking around with page administration features keep
 *  in mind that assuming only valid users will be able to use the admin area
 *  is not good security practice.  This system goes through great pains to
 *  validate *all* user land data which means that even if someone finds a way
 *  (doubtful) to compromise the user system and elevate their priviledges
 *  they should still be able to do limited harm.  Sure, they could delete
 *  everything (you have been keeping backups, yes?), but they would still be
 *  unable to compromise the file system or database in a way that could
 *  could possibly be used to compromise the host system itself.  Since we're
 *  on the subject, one may take note that I never *ever* use eval() or any
 *  thing remotely resembling it.
 *
 *  Also worthy of note, deleting a page does not necessarily delete the
 *  associated content blocks.. This is not an oversight, it is a feature. :>
 *
 *  Sitemap file maintanence occurs in this section.  It shall be run every time
 *  this method is called, because this method should never be called unless
 *  changes have been made.
 *
 *  TODO:
 *      Do not update if not changes made
 *
 ******************************************************************************/
    function update () {
        // Delete or update page meta data
        if (isset($_POST['delete_page'])
                && ($_POST['delete_page'] == 1)
                ) {
            $clause = 'WHERE id="' . $_POST['page_id'] . '" LIMIT 1';
            $GLOBALS['db']->delete(TablePageMetaData,
                                    $clause,
                                    __FILE__,
                                    __LINE__);
            $pageDeleted = true;
        } elseif (isset($_POST['page_id'])) {
            $data = array(
                'pid'               => $_POST['page_pid'],
                'acsLow'            => $_POST['page_acsLow'],
                'acsHigh'           => $_POST['page_acsHigh'],
                'so'                => $_POST['page_so'],
                'active'            => $_POST['page_active'],
                'sitemapPriority'   => $_POST['page_sitemapPriority'],
                'sitemapFrequency'  => $_POST['page_sitemapFrequency'],
                'headerCollapse'    => $_POST['page_headerCollapse'],
                'location'          => $_POST['page_location'],
                'contentLimit'      => $_POST['page_contentLimit'],
                'allowComments'     => $_POST['page_allowComments'],
                'allowRatings'      => $_POST['page_allowRatings'],
                'titleNav'          => $_POST['page_titleNav'],
                'titleFull'         => $_POST['page_titleFull'],
                'semanticURI'       => $_POST['page_semanticURI'],
                'imgURI'            => $_POST['page_imgURI'],
                'cTemplate'         => $_POST['page_cTemplate'],
                'description'       => $_POST['page_description'],
                'stamp'             => 'NOW()'
                );
            $clause = 'WHERE id="' . $_POST['page_id'] . '" LIMIT 1';
            $GLOBALS['db']->update ($data,
                                    TablePageMetaData,
                                    $clause,
                                    __FILE__,
                                    __LINE__);
        // End meta update
        }
        // Delete or update all page content
        if (isset($_POST['delete_all_page_content'])
                && ($_POST['delete_all_page_content'] == 1)
                ) {
            $clause = 'WHERE pid="' . $_POST['page_id'] . '"';
            $GLOBALS['db']->delete(TablePageContent,
                                    $clause,
                                    __FILE__,
                                    __LINE__);
        } elseif (isset($_POST['content'])) {
            foreach ($_POST['content'] as $index => $key) {
                if (isset($key['delete'])
                        && ($key['delete'] == 1)
                        ) {
                    $clause = 'WHERE id="' . $key['cid'] . '" LIMIT 1';
                    $GLOBALS['db']->delete(TablePageContent,
                                            $clause,
                                            __FILE__,
                                            __LINE__);
                } elseif (is_numeric($index)) {
                    $data = array(
                        'header' => $key['header'],
                        'content' => $key['content'],
                        'imgURI' => $key['imgURI'],
                        'headerURI' => $key['headerURI'],
                        'pid' => $key['pid'],
                        'acsLow' => $key['acsLow'],
                        'acsHigh' => $key['acsHigh'],
                        'so' => $key['so'],
                        'active' => $key['active'],
                        'cssClass' => $key['cssClass'],
                        'cssID' => $key['cssID']
                        );
                    $clause = 'WHERE id="' . $key['cid'] . '" LIMIT 1';
                    $GLOBALS['db']->update ($data,
                                            TablePageContent,
                                            $clause,
                                            __FILE__,
                                            __LINE__);
                } elseif (($index == 'new')
                        && ($key['header'] != null)
                        ) {
                    $data = array(
                        'pid' => $_POST['editID'],
                        'acsLow' => $key['acsLow'],
                        'acsHigh' => $key['acsHigh'],
                        'so' => $key['so'],
                        'active' => $key['active'],
                        'cssClass' => $key['cssClass'],
                        'cssID' => $key['cssID'],
                        'header' => $key['header'],
                        'content' => $key['content'],
                        'imgURI' => $key['imgURI'],
                        'headerURI' => $key['headerURI']
                        );
                    $GLOBALS['db']->insert($data,
                                            TablePageContent,
                                            __FILE__,
                                            __LINE__);
                }
            }
        // End content update
        }

        if (!isset($pageDeleted)) {
            $this->postToLoader();
            $this->sitemap();
        }
        $this->pdfGenerateSingle();

    // End update method
    }


/***** Insert Method ***********************************************************
 *
 *  This method is the little magic maker that puts new pages into the database.
 *  Currently, the system is not set up for adding content to a page at the time
 *  of creation.  It would be useful to provide that feature, but as is that
 *  would provide just one content block... which is nice and all, but if I wait
 *  to add the feature until I ajax this puppy, then you could add multiple
 *  blocks at a time and I would rather just have it set up like that.
 *
 *  Sitemap file maintanence occurs in this section.  It shall be run every time
 *  this method is called.
 ******************************************************************************/

    function insert () {
        // Needs validation routines

        $data = array(
            'pid'               => $_POST['page_pid'],
            'acsLow'            => $_POST['page_acsLow'],
            'acsHigh'           => $_POST['page_acsHigh'],
            'so'                => $_POST['page_so'],
            'active'            => $_POST['page_active'],
            'sitemapPriority'   => $_POST['page_sitemapPriority'],
            'sitemapFrequency'  => $_POST['page_sitemapFrequency'],
            'headerCollapse'    => $_POST['page_headerCollapse'],
            'location'          => $_POST['page_location'],
            'contentLimit'      => $_POST['page_contentLimit'],
            'allowComments'     => $_POST['page_allowComments'],
            'allowRatings'      => $_POST['page_allowRatings'],
            'titleNav'          => $_POST['page_titleNav'],
            'titleFull'         => $_POST['page_titleFull'],
            'semanticURI'       => $_POST['page_semanticURI'],
            'imgURI'            => $_POST['page_imgURI'],
            'cTemplate'         => $_POST['page_cTemplate'],
            'description'       => $_POST['page_description']
            );
        $GLOBALS['db']->insert ($data,
                                TablePageMetaData,
                                __FILE__,
                                __LINE__);

        // Create Initial PDF
        // Save this for when we allow content to be created at this time
        // $this->pdfGenerateSingle();

        // Update sitemap
        if ($GLOBALS['jwsfConfig']->UseSiteMap == 'true') {
            $this->sitemap();
        }

    // End insert method
    }


/***** Delete Lost File Meta Data **********************************************
 *
 *
 *
 ******************************************************************************/
    function fileDeleteLost () {

        if (!isset($_POST['file_deleteLost'])
                || !isset($_POST['metaID'])
                ) {
            $GLOBALS['messageError'][] = 'Resource meta data not removed';
            return false;
        }
        $clause = 'WHERE id=' . $_POST['metaID'];
        $GLOBALS['db']->delete (TablePageResourceMeta,
                                $clause,
                                __FILE__, __LINE__);
        $GLOBALS['messagesSuccess'][] =
                    'The resource meta data has been removed';

    }


/***** Delete Orphan File by Path Relative to Root *****************************
 *
 *
 *
 ******************************************************************************/
    function fileDeleteOrphan () {

        if (!isset($_POST['file_deleteOrphan'])
                || !isset($_POST['filePath'])
                ) {
            $GLOBALS['messageError'][] = 'File not removed';
            return false;
        }
        $this->filesEditAll(); // change this to just orphans
        if (array_search($_POST['filePath'], $this->filesOrphan) !== false) {
            $command = 'rm ' . SiteRoot . escapeshellarg($_POST['filePath']);
            $last = system($command, $result);
            $GLOBALS['messagesSuccess'][] = 'File '
                                            . $_POST['filePath']
                                            . ' has been removed';
        } else {
            $GLOBALS['messagesError'][] = 'Illegal file access attempt logged';
        }
    // End fileDeleteOrphan
    }


/***** Delete Valid File & Meta Data *******************************************
 *
 *
 *
 ******************************************************************************/
    function fileDeleteValid () {
        if (!isset($_POST['file_deleteAll'])
                || !isset($_POST['validID'])
                ) {
            $GLOBALS['messagesError'][] = 'Resource not removed';
        } else {
            $this->filesEditAll(); // change this to just valid
            $filePath = $this->getResourceLocationByID ($_POST['validID']);
            $flag = false;
            foreach ($this->filesValid as $value) {
                if ($filePath == $value['location']) {
                    $flag = true;
                }
            }
            if ($flag == true) {
                $command = 'rm ' . SiteRoot . escapeshellarg($filePath);
                $last = system($command, $result);
                $GLOBALS['messagesSuccess'][] = 'File '
                                                . $filePath
                                                . ' has been removed';
                $clause = 'WHERE id=' . $_POST['validID'];
                $GLOBALS['db']->delete (TablePageResourceMeta,
                                        $clause,
                                        __FILE__, __LINE__);

                $GLOBALS['messagesSuccess'][] =
                            'The resource meta data has been removed';
            } else {
                $GLOBALS['messagesError'][] =
                            'Illegal file access attempt logged';
            }
        }
    }


/***** Determine File Meta Data For Insertion **********************************
 *
 *
 *
 ******************************************************************************/

function filesDetermineMetaValues() {

    if (!isset($_POST['fileMeta'])
            || empty($_POST['fileMeta'])
            ) {
        return false;
    }
    $array = array();
    foreach ($_POST['fileMeta'] as $key => $values) {
        if (isset($values['location'])
                && (strstr($values['location'], 'uploads') == true)
                ) {
            $array['location'] = $values['location'];
        } elseif (isset($_POST['action'])
                && ($_POST['action'] == 'Save')
                || ($_POST['action'] == 'Update')
                ) {
            if (isset($_FILES[$key])) {
                foreach ($_FILES[$key] as $k => $v) {
                    $this->fileDataUpload[$k] = $v;
                }
                $this->filesDetermineUploadDestination();
                if (isset($this->fileDataUpload['location'])) {
                    $array['location'] =
                                $this->fileDataUpload['location'];
                }
            }
        } else {
            $GLOBALS['messagesError'][] = 'File '
                                        . $key
                                        . ' does not exist, skipping';
        }
        // Format values
        $array['active'] = (isset($values['active']))
            ? $_POST['fileActive']
            : 1;
        $array['viewable'] = (isset($values['viewable']))
            ? $_POST['fileViewable']
            : 1;
        // If title not set
        if (empty($values['title'])) {
            // New file upload
            if (isset($_POST['action'])
                    && ($_POST['action'] == 'Save')
                    ) {
                if (empty($values['title'])) {
                    // Check for rename
                    $array['title'] = (empty($values['name']))
                                    ? $_FILES[$key]['name']
                                    : $values['name'];
                } else {
                    $array['title'] = $values['title'];
                }
            // Repairing orphan data
            } else {
                $array['title'] = (empty($values['title']))
                                ? $values['location']
                                : $values['title'];
            }
        // Title was set
        } elseif (strlen($values['title'])) {
            $array['title'] = substr($values['title'], 0, 255);
        } else {
            $array['title'] = $values['title'];
        }
        if (empty($values['description'])) {
            $array['description'] = 'Download or view this file';
        } elseif (strlen($values['description']) > 256) {
            $array['description'] = substr($values['description'],
                                        0, 255);
        } else {
            $array['description'] = $values['description'];
        }
        // Register
        $this->fileDataMeta[] = $array;
    }

}


/***** Determine Uploaded File Destination Directory ***************************
 *
 *
 *
 ******************************************************************************/
 
    function filesDetermineUploadDestination() {

        // Set upload root
        $destination = SiteRoot . '/uploads/';
        // Determine location by mime type
        switch ($this->fileDataUpload['type']) {
            case 'application/x-gtar':
            case 'application/x-tar':
            case 'application/zip':
            case 'application/x-gzip':
                $destination .= 'archives/';
                $subDir = 'archives/';
                break;
            case 'audio/x-aiff':
            case 'audio/mid':
            case 'audio/mpeg':
            case 'audio/x-wav':
                $destination .= 'audio/';
                $subDir = 'audio/';
                break;
            case 'application/msword':
            case 'application/vnd.ms-excel':
            case 'application/x-mspublisher':
                $destination .= 'documents/';
                $subDir = 'documents/';
                break;
            case 'image/bmp':
            case 'image/gif':
            case 'image/jpeg':
            case 'image/png':
            case 'image/tiff':
                $destination .= 'images/';
                $subDir = 'images/';
                break;
            case 'application/pdf':
                $destination .= 'pdf/';
                $subDir = 'pdf/';
                break;
            case 'video/x-ms-asf':
            case 'video/x-msvideo':
            case 'video/quicktime':
            case 'video/mpeg':
                $destination .= 'video/';
                $subDir = 'video/';
                break;
            default:
                $GLOBALS['messagesError'][] = 'File type not allowed';
                return false;
        }

        // Uh, register them
        $this->fileDataUpload['subDir'] = $subDir;
        $this->fileDataUpload['destination'] = $destination;

        if (!isset($this->fileDataUpload['name'])
                || empty($this->fileDataUpload['name'])
                ) {
            $this->fileDataUpload['name'] = $this->fileDataUpload['source'];
        }
        // If not uploading a replacement file w/different name, set it now
        if (!isset($_POST['file_upload'])
                || !isset($this->fileDataUpload['location'])
                || (substr($this->fileDataUpload['location'], 0, 9)
                    != '/uploads/')
                ) {
            $this->fileDataUpload['location'] = '/uploads/'
                                            . $subDir
                                            . $this->fileDataUpload['name'];
        } 

    // End fileDetermineUploadLocation
    }


/***** Determine Uploaded File Values ******************************************
 *
 *
 *
 ******************************************************************************/

    function filesDetermineUploadValues() {

        $this->fileDataUpload = array();
        // Uploading from edit all
        if (isset($_POST['file_upload'])) {
            foreach ($_FILES as $fileNo => $values) {
                $values['location'] = $_POST['fileReplace'][$fileNo]['location'];
                $_FILES[$fileNo]['location'] = $values['location'];
                $this->fileDataUpload[] = $values;
            }
        } else {
        // Normal upload
            foreach ($_FILES as $fileNo => $values) {
                if (isset($_POST['fileMeta'][0]['name'])
                        && !empty($_POST['fileMeta'][0]['name'])
                        ) {
                    $values['name'] = $_POST['fileMeta'][0]['name'];
                } elseif (isset($_POST[$fileNo]['name'])
                        && !empty($_POST[$fileNo]['name'])
                        ) {
                    $values['name'] = $_POST[$fileNo]['name'];
                }
                if (strlen($values['name']) > 256) {
                    $values['name'] = substr($values['name'], 0, 255);
                }
                $this->fileDataUpload[] = $values;
            }
        }
        // End filesDetermineUploadValues
    }


/*******************************************************************************
 *
 *  This method generates a list of valid file resources for the administration
 *  area, where it is used for selecting pre-existing files.  Therefore, a valid
 *  file is one that exists in both the database and the file system.
 *
 ******************************************************************************/

    function filesEditAll () {

        $this->filesValid = array();
        $this->filesLost = array();
        $this->filesGetAllFromDb();
        // Get all file meta data
        $fields = array ('id',
                        'active',
                        'viewable',
                        'title',
                        'location',
                        'description'
                        );
        $clause = "ORDER BY title";
        $GLOBALS['db']->select($fields, TablePageResourceMeta, $clause,
                                __FILE__, __LINE__);
        // Break into valid and lost
        while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
            if (file_exists(SiteRoot . $row['location'])) {
                $this->filesValid[] = array ('id' => $row['id'],
                                        'active' => $row['active'],
                                        'viewable' => $row['viewable'],
                                        'title' => $row['title'],
                                        'location' => $row['location'],
                                        'description' => $row['description']
                                        );
            } else {
                $this->filesLost[] = array ('id' => $row['id'],
                                        'active' => $row['active'],
                                        'viewable' => $row['viewable'],
                                        'title' => $row['title'],
                                        'location' => $row['location'],
                                        'description' => $row['description']
                                        );
            }
        }

        $this->filesGetOrphans();
        $this->filesGetAssignments();

    // End filesEditAll
    }


/***** Valid File Editing Info ************************************************
 *
 *  This method reads all available information about a file so that the
 *  administrator can modify its assignments, title, location, etc.. and then
 *  registers that data with smarty as one big multi-dimensional array.
 *
 ******************************************************************************/

    function fileEditValid () {

        if (!isset($_POST['file_editValid'])) return false;
        $this->fileValid = array();
        $fields = array ('id',
                        'title',
                        'location',
                        'description'
                        );
        $clause = "WHERE id=" . $_POST['file_editValid'];
        $GLOBALS['db']->select($fields, TablePageResourceMeta, $clause,
                                __FILE__, __LINE__);
        $this->fileValid = $GLOBALS['db']->fetch(__FILE__, __LINE__);

    // End fileEdit
    }


/***** Get All File Info From Database *****************************************
 *
 *
 *
 ******************************************************************************/

    function filesGetAllFromDb () {
        $this->filesInDB = array();
        $this->filerids = array();
        // Get all file meta data
        $fields = array ('id',
                        'active',
                        'viewable',
                        'title',
                        'location',
                        'description'
                        );
        $clause = "ORDER BY title";
        $GLOBALS['db']->select($fields, TablePageResourceMeta, $clause,
                                __FILE__, __LINE__);
        // Break into valid and lost
        while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
            // Some arrays to save on queries later
            $this->fileRIDs[] = $row['id'];
            $this->filesInDB[] = $row['location'];
        }

    // End resourceGetAllFromDb
    }


/***** Assignment Categorizer **************************************************
 *
 *
 *
 ******************************************************************************/

    function filesGetAssignments () {

        // Check for data load
        if (!isset($this->filesInDb)) {
            $this->filesGetAllFromDb();
        }
        // Set up for assignment calculations
        $this->filesInvalidAssignmentsResource = array();
        $this->filesInvalidAssignmentsPage = array();
        $this->filesInvalidAssignmentsContent = array();
        $this->filesInvalidAssignmentsMismatch = array();
        $this->filesValidAssignments = array();
        $this->setIdsExist();
        $this->setContentIDs();
        // Read assignment data
        $fields = array ('id',
                        'rid',
                        'pid',
                        'cid'
                        );
        $clause = "ORDER BY rid, pid, cid";
        $GLOBALS['db']->select($fields, TablePageResourceAssignment, $clause,
                                __FILE__, __LINE__);
        // Break into valid/invalid
        while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
            // Load test results
            $test1 = array_search ($row['rid'], $this->fileRIDs);
            if ($row['pid'] == 0) {
                $test2 = true;
            } else {
                $test2 = array_search ($row['pid'], $this->idsExist);
            }
            if ($row['cid'] == 0) {
                $test3 = true;
            } else {
                $test3 = array_search ($row['cid'], $this->contentIDs);
            }
            if (($row['pid'] == 0)
                    || ($row['cid'] == 0)
                    ) {
                $test4 = true;
            } else {
                $test4 = $this->testPageContentMatch($row['pid'], $row['cid']);
            }
            if ($test1 === 0) $test1 = true;
            if ($test2 === 0) $test2 = true;
            if ($test3 === 0) $test3 = true;
            if ($test4 === 0) $test4 = true;
            // Categorize existing assignments
            if ($test1 === false) {
                        // array_merge is slow
                        $assignment['id'] = $row['id'];
                        $title = $this->getPageTitleByID($row['pid']);
                        $assignment['page'] = ($title == NULL)
                                                ? '-N/A-'
                                                : $title;
                        $content = $this->getContentHeaderByID($row['cid']);
                        $assignment['content'] = ($content == NULL)
                                                ? '-N/A-'
                                                : $content;
                        $this->filesInvalidAssignmentsResource[] = $assignment;
            } elseif ($test2 === false) {
                foreach ($this->filesValid as $record) {
                    if ($record['id'] == $row['rid']) {
                        // array_merge is slow
                        $assignment['id'] = $row['id'];
                        $assignment['title'] = ($record['title'] == NULL)
                                                ? '-N/A-'
                                                : $record['title'];
                        $assignment['location'] = ($record['location'] == NULL)
                                                ? '-N/A-'
                                                : $record['location'];
                        $title = $this->getPageTitleByID($row['pid']);
                        $assignment['page'] = ($title == NULL)
                                                ? '-N/A-'
                                                : $title;
                        $content = $this->getContentHeaderByID($row['cid']);
                        $assignment['content'] = ($content == NULL)
                                                ? '-N/A-'
                                                : $content;
                        $this->filesInvalidAssignmentsPage[] = $assignment;
                    }
                }
            } elseif ($test3 === false) {
                foreach ($this->filesValid as $record) {
                    if ($record['id'] == $row['rid']) {
                        // array_merge is slow
                        $assignment['id'] = $row['id'];
                        $assignment['title'] = $record['title'];
                        $assignment['location'] = $record['location'];
                        $assignment['page'] =
                                $this->getPageTitleByID($row['pid']);
                        $assignment['content'] =
                                $this->getContentHeaderByID($row['cid']);
                        $this->filesInvalidAssignmentsContent[] = $assignment;
                    }
                }
            } elseif ($test4 === false) {
                foreach ($this->filesValid as $record) {
                    if ($record['id'] == $row['rid']) {
                        // array_merge is slow
                        $assignment['id'] = $row['id'];
                        $assignment['title'] = $record['title'];
                        $assignment['location'] = $record['location'];
                        $assignment['page'] =
                                $this->getPageTitleByID($row['pid']);
                        $assignment['content'] =
                                $this->getContentHeaderByID($row['cid']);
                        $this->filesInvalidAssignmentsMismatch[] = $assignment;
                    }
                }
            } else {
                // Assignment is valid
                foreach ($this->filesValid as $record) {
                    if ($record['id'] == $row['rid']) {
                        // array_merge is slow
                        $assignment['id'] = $row['id'];
                        $assignment['title'] = $record['title'];
                        $assignment['location'] = $record['location'];
                        $assignment['page'] =
                                $this->getPageTitleByID($row['pid']);
                        $assignment['content'] =
                                $this->getContentHeaderByID($row['cid']);
                        $this->filesValidAssignments[] = $assignment;
                    }
                }
            }
        }

    // End filesGetAssignments
    }


/***** Orphan File Finder ******************************************************
 *
 *  This method reads the resources assigned to the current page or content
 *  block and assigns the output as an associative array to Smarty *if* the
 *  'show' bit is set to true.
 *
 ******************************************************************************/

    function filesGetOrphans () {

        if (!isset($this->filesInDb)) {
            $this->filesGetAllFromDb();
        }
        $this->filesOrphan = array();
        // Get array of uploadable directories
        $uploadDirs = dir_as_file_array (SiteRoot . '/uploads');
        // Build array of files
        $filesOrphan = array();
        foreach ($uploadDirs as $directory) {
            $files = dir_as_file_array (SiteRoot . '/uploads/' . $directory);
            // Eliminate items that exist in database
            if (!empty($files)) {
                $base = '/uploads/' . $directory . '/';
                foreach ($files as $file) {
                    // This must be done this way
                    $test = array_search($base . $file, $this->filesInDB);
                    if ($test === false) {
                        $filesOrphan[] = $base . $file;
                    }
                }
            }
        }
        // Register the results
        $this->filesOrphan = $filesOrphan;


    // End filesGetOrphans
    }
    

/***** Get Valid File Information **********************************************
 *
 *
 *
 ******************************************************************************/

    function fileGetValid () {

    }


/***** File Processing Procedure ***********************************************
 *
 *  This method processes the submitted form parameters, verifies the size
 *  and integrity of the file, moves it to the appropriate place and inserts
 *  information about the file into the database.  If the file is assigned
 *  to specific pages or content blocks, the appropriate relational database
 *  is updated accordingly.
 *
 *  TODO:
 *      More validation
 *
 ******************************************************************************/

    function fileInsert () {

        // If uploading (source)
        if (isset($_FILES)
                && !empty($_FILES)
                ) {
            // Upload and insert meta
            $success = $this->fileUploadStandard();
            $success = $this->fileInsertMeta();
        } elseif (isset($_POST['fileExists'])) {
            // Use pre-existing file
            $success = $_POST['fileExists'];
        } else {
            // File upload error
            $GLOBALS['messagesError'][] = 'File was not recieved';
            $success = false;
        }
        // Quick hack till proper dupe & upload check (f_u = repair button name)
        if (($success !== false)
                && !isset($_POST['file_upload'])
                ) {
            // Validate post vars
            $pageID = isset($_POST['filePageID'])
                    ? $_POST['filePageID']
                    : 0;
            $contentID = isset($_POST['fileContentID'])
                    ? $_POST['fileContentID']
                    : 0;
            // Check for dupe, if not pre-existing
            
                // Log uploader
                $addedBy = ($GLOBALS['userObject']->acslvl > 0)
                    ? $addedBy = $GLOBALS['userObject']->email
                    : $addedBy = $_SERVER['REMOTE_ADDR'];
                // Insert resource assignment
                $data = array (
                                'rid' => $success,
                                'pid' => $pageID,
                                'cid' => $contentID,
                                'addedBy' => $addedBy
                                );
                $GLOBALS['db']->insert ($data, TablePageResourceAssignment,
                                        __FILE__, __LINE__);
        }
        // Set global message
        if ($success == true) {
            $GLOBALS['messagesSuccess'][] = 'Your efforts have succeeded';
        }

    // End fileInsert
    }


/***** Insert File Meta Data Into Database *************************************
 *
 *
 *
 ******************************************************************************/

    function fileInsertMeta () {

        if (!isset($this->fileDataMeta)
                || empty($this->fileDataMeta)
                || !is_array($this->fileDataMeta)
                ) {
            $GLOBALS['messagesError'][] = 'No file meta data to insert';
            return false;
        }
        foreach ($this->fileDataMeta as $values) {
            // Upload logging info
            $addedBy = ($GLOBALS['userObject']->acslvl > 0)
                ? $addedBy = $GLOBALS['userObject']->email
                : $addedBy = $_SERVER['REMOTE_ADDR'];
            $values['addedby'] = $addedBy;
            $GLOBALS['db']->insert ($values, TablePageResourceMeta,
                                __FILE__, __LINE__);
        }
        unset($this->fileDataMeta);
        if ($GLOBALS['db']->lastId() == true) {
            $GLOBALS['messagesSuccess'][] = 'File meta data saved';
            return $GLOBALS['db']->lastId();
        } else {
            $GLOBALS['messagesError'][] = 'File meta data failed to insert';
            return false;
        }

    // End fileInsertMeta
    }


/***** File Lister *************************************************************
 *
 *  This method reads the resources assigned to the current page or content
 *  block and assigns the output to Smarty.  It requires the file to be valid,
 *  as well as a valid database record and relational assignment of some form.
 *  If being called by the downloads module/page, it should output a categorized
 *  list of  files with 'show' bit set to true.
 *
 ******************************************************************************/

    function filesList () {

        // Get files associated with page
        // Get files associated with page content blocks
        // Verify files
        // Register with Smarty

    // End filesList
    }


/*******************************************************************************
 *
 *  This method reads all file resources from the database and outputs them to
 *  Smarty via an associative array.
 *
 ******************************************************************************/

    function filesListAll () {

        // Get all file meta data
        // Get all file assignment data
        // Get all valid files
        // Organize
        // Send to Smarty

    // End filesListAll
    }


/***** File Lister *************************************************************
 *
 *  This method reads the resources assigned to the current page or content
 *  block and assigns the output as an associative array to Smarty *if* the
 *  'show' bit is set to true.
 *
 ******************************************************************************/

    function filesListLost () {

        // Get all file meta data
        // Get all assignment data
        // Get all valid files
        // Assign any data w/o a matching file to Smarty

    // End filesListPublic
    }


/***** File Lister *************************************************************
 *
 *  This method reads the resources assigned to the current page or content
 *  block and assigns the output as an associative array to Smarty *if* the
 *  'show' bit is set to true.
 *
 ******************************************************************************/

    function filesListPublic () {

        // Get all file meta data where show = true
        // Get all assignment data
        // Get all valid files
        // Assign all matching data to Smarty

    // End filesListPublic
    }


/*******************************************************************************
 *
 *  This method generates a list of valid file resources for the administration
 *  area, where it is used for selecting pre-existing files.  Therefore, a valid
 *  file is one that exists in both the database and the file system.
 *
 ******************************************************************************/

    function filesListValid () {

        // Get all file meta data
        $fields = array ('id', 'title', 'location');
        $clause = "ORDER BY location, title";
        $GLOBALS['db']->select($fields, TablePageResourceMeta, $clause,
                                __FILE__, __LINE__);
        // Get all valid files
        while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
            if (file_exists(SiteRoot . $row['location'])) {
                $this->files[] = array ('id' => $row['id'],
                                        'title' => $row['title']);
            }
        }

    // End filesListAll
    }
    

/***** Move a File *************************************************************
 *
 *
 *
 ******************************************************************************/

    function fileMove () {

        if (!isset($this->fileMove1)
                || !isset($this->fileMove2)
                ) {
            $GLOBALS['messagesError'][] = 'Missing file information';
            return false;
        }

        $values = array();
        $this->filesEditAll(); // change this to just valid
        foreach ($this->filesValid as $k => $v) {
            $values[] = $v['location'];
        }
        if (array_search($this->fileMove1, $values) !== false) {
            $command = 'mv '
                . escapeshellarg(SiteRoot . $this->fileMove1)
                . ' '
                . escapeshellarg(SiteRoot . $this->fileMove2)
                ;
            $last = system($command, $result);
            $GLOBALS['messagesSuccess'][] = 'File '
                                            . $this->fileMove1
                                            . ' is now '
                                            . $this->fileMove2
                                            ;
        } else {
            $GLOBALS['messagesError'][] = 'Illegal file access attempt logged';
        }
    }


/*******************************************************************************
 *
 *
 *
 ******************************************************************************/

    function fileRemove () {

        // Verify existence in file system
        // Remove from file system
        // Check for assignments
        // Remove assignments
        // Verify meta data existence in database
        // Remove meta data from database

    // End fileRemove
    }


/***** File Processing Procedure ***********************************************
 *
 *  This method processes the submitted form parameters, verifies the size
 *  and integrity of the file, moves it to the appropriate place and inserts
 *  information about the file into the database.  If the file is assigned
 *  to specific pages or content blocks, the appropriate relational database
 *  is updated accordingly.
 *  Updating or inserting?
 *
 ******************************************************************************/

    function fileUpdateMeta () {
        // Check for changing file contents
        if (isset($_FILES[0])) {
            $this->fileUploadStandard();
            $location = $this->fileDataUpload['location'];
        } elseif (!empty($_POST['fileMeta'][0]['name'])) {
        // Check for rename (automagically taken care of if changing files)
            $this->fileMove1 = $_POST['fileMeta'][0]['location'];
            $this->fileMove2 = dirname($_POST['fileMeta'][0]['location'])
                            . '/'
                            . $_POST['fileMeta'][0]['name'];
            $this->fileMove();
            $location = $this->fileMove2;
        } else {
            $location = $_POST['fileMeta'][0]['location'];
        }
        // Log updater
        $modifiedBy = ($GLOBALS['userObject']->acslvl > 0)
            ? $GLOBALS['userObject']->email
            : $_SERVER['REMOTE_ADDR'];
        // Update meta data
        $data = array ('title' => $_POST['fileMeta'][0]['title'],
                        'description' => $_POST['fileMeta'][0]['description'],
                        'location' => $location,
                        'modifiedBy' => $modifiedBy
                        );
        $clause = 'WHERE id=' . $_POST['fileMeta'][0]['id'];
        $result = $GLOBALS['db']->update($data, TablePageResourceMeta, $clause,
                                        __FILE__, __LINE__);

    // End fileUpdate
    }


/***** Standard File Upload Processor ******************************************
 *
 *  This is the standard resource assignment file uploader, which can only
 *  process one file at a time and also inserts file meta data.
 *
 *  TODO:
 *      Database dupe check
 *      MD5 dupe check
 *
 ******************************************************************************/

    function fileUploadStandard () {
        if (!isset($_FILES)
                || empty($_FILES)
                ) {
            return false;
        }
        $array = array();
        foreach ($_FILES as $key => $values) {
            // Verify upload
            if ($values['error'] == 0) {
                // Register data without overwriting
                foreach ($values as $k => $v) {
                    $this->fileDataUpload[$k] = $v;
                }
                /* Check for existing dupe?
                    select location by md5 match
                    if file exists in database
                        check for file existence
                            remove
                    move
                */
                // Log uploader
                $addedBy = ($GLOBALS['userObject']->acslvl > 0)
                    ? $addedBy = $GLOBALS['userObject']->email
                    : $addedBy = $_SERVER['REMOTE_ADDR'];
                // Determine location
                $this->filesDetermineUploadDestination();
                // Move file
                if (@move_uploaded_file
                            ($this->fileDataUpload['tmp_name'],
                                SiteRoot
                                . $this->fileDataUpload['location'])
                            ) {
                    // File successfully uploaded and moved, set meta data
                    $GLOBALS['messagesSuccess'][] = 'File moved to '
                                                . $this->fileDataUpload['location'];
                    // Send back id for assignment
                    return $GLOBALS['db']->lastId();
                } else {
                    // File could not be moved to permanent location
                    $GLOBALS['messagesError'][] = 'File could not be saved';
                    return false;
                }
            } else {
                // File upload error
                $GLOBALS['messagesError'][] = 'File '
                                            . $values['name']
                                            . 'could not be uploaded ('
                                            . $values['error']
                                            . ')';
                return false;
            }
        }

    // End fileUploadStandard
    }


/***** Generate Fresh Site Map *************************************************
 *
 *  Here we take a snapshot of the current navigational data which is accessible
 *  to a standard public user (PageDefaultAccessLevel) and create a sitemaps.org
 *  compliant sitemap.xml file.  Personally, I have no concern about compressing
 *  this file because the performance of my host rocks.  Also, fopen is not
 *  restricted on my server so I use it.  If you need a solution that does not
 *  use fopen, let me know and I will create a real dynamic version for you.
 *
 *  TODO:
 *      Validate
 *      Give smart semantic href handling
 *
 ******************************************************************************/

    function sitemap() {

        if ($GLOBALS['jwsfConfig']->UseSiteMap == 'true') {

            // initialization
            $pageData = array();
            $xml = array();

            // query database
            $fields = array('id',
                            'semanticURI',
                            'sitemapPriority',
                            'sitemapFrequency',
                            'stamp');
            $clause = 'WHERE active=1 AND acsLow<='
                    . $GLOBALS['jwsfConfig']->PageDefaultAccessLevel;
            $GLOBALS['db']->select($fields,
                                    TablePageMetaData,
                                    $clause,
                                    __FILE__,
                                    __LINE__);
            while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
                $temp = array();
                foreach ($row as $key => $value) {
                    $temp[$key] = $value;
                }
                $pageData[] = $temp;
            }

            // construct data structure
            $xml[] = '<?xml version="1.0" encoding="UTF-8" ?' . '>';
            $xml[] = '<urlset'
                . ' xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '
                . 'xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9"'
                . ' url="http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"'
                . ' xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
            foreach ($pageData as $key => $value) {

                // Data must be urlencoded but our data should not have any
                if (
                        ($value['semanticURI'] == null)
                        && ($value['id'] != $GLOBALS['jwsfConfig']->PageDefaultId)
                        ) {
                    $loc = 'http://'
                            . FQDN
                            . '/?id='
                            . $value['id'];
                } else {
                    $loc = 'http://'
                            . FQDN;
                            if (substr($value['semanticURI'], 0, 1) != '/') {
                                $loc .= '/';
                            }
                            $loc .= $value['semanticURI'];
                }

    // Make sure data is not in the future
                $lastmod =  explode(' ', $value['stamp']);
                $lastmod = $lastmod[0] . 'T' . $lastmod[1];
                $lastmod = $lastmod . $GLOBALS['jwsfConfig']->TimeZoneAbv;
                $sql  = 'SELECT value FROM `'
                        . TableSiteMapFrequencies
                        . '` WHERE id='
                        . $value['sitemapFrequency']
                        . ';';
                $result = $GLOBALS['db']->query ($sql, __FILE__, __LINE__);
                $changefreq = $GLOBALS['db']->fetch ($result, __FILE__, __LINE__);
                $changefreq = $changefreq['value'];
                $priority = $value['sitemapPriority'] / 10;

                $xml[] = '<url>';
                $xml[] = '<loc>' . $loc . '</loc>';
                $xml[] = '<lastmod>' . $lastmod . '</lastmod>';
                $xml[] = '<changefreq>' . $changefreq . '</changefreq>';
                $xml[] = '<priority>' . $priority . '</priority>';
                $xml[] = '</url>';
            }
            $xml[] = '</urlset>';

            // open file
            $file = @fopen ($_SERVER['DOCUMENT_ROOT'] . '/sitemap.xml', 'w');
            if ($file == false) {
                $GLOBALS['messagesError'][] = 'Unable to open sitemap.xml file';
                return;
            }

            // write data structure
            foreach ($xml as $key => $value) {
                if (fwrite ($file, $value . "\n") == false) {
                    $GLOBALS['messagesError'][] = 'Unable to write sitemap file';
                    return;
                }
            }

            // close file
            if (fclose ($file) == false) {
                $GLOBALS['messagesError'][] = 'Unable to close sitemap.xml file';
                return;
            }

            $GLOBALS['messagesSuccess'][] = 'Site map successfully written to disk';
        }

    // End sitemap method
    }


/***** Get ALL Pages ***********************************************************
 *
 *  This returns an array of all page ID => TitleNav pairs for use in the
 *  module editor in the administration area...
 *
 *  TODO:
 *      Output as hierarchy?  I think that would be a  mess. ;>
 *      Now the only place that this is used is in the module editor class, but
 *      that should be updated to use the new optgroup style.  After that is
 *      done, this should still be left in place as someone may have a use for
 *      it some day... I am leaving this alone for now because I intend to
 *      rewrite module administration completely.
 *
 ******************************************************************************/

     function getAll () {

        $this->all = array ();
        $fields = array('id',
                        'titleNav');
        $clause = 'ORDER BY titleNav';
        $GLOBALS['db']->select($fields,
                                TablePageMetaData,
                                $clause,
                                __FILE__,
                                __LINE__);
        while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
            $id = $row['id'];
            $this->all[$id] = $row['titleNav'];
        }

    // End getAll
    }


/***** Get All Active Pages ****************************************************
 *
 *  This returns an array of all page ID => TitleNav pairs of active pages for
 *  use in the parent select box optgroup sub-menus.
 *
 ******************************************************************************/

     function getActives () {

        $this->actives = array ();
        $fields = array('id',
                        'titleNav');
        $clause = 'WHERE active=1 AND (pid IN (SELECT id FROM '
                . TablePageMetaData
                . ') OR pid=0) ORDER BY titleNav';
        $GLOBALS['db']->select($fields,
                                TablePageMetaData,
                                $clause,
                                __FILE__,
                                __LINE__);
        while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
            $this->actives[] = array('id' => $row['id'],
                                    'titleNav' => $row['titleNav']);
        }

    // End getActives
    }


/***** Get Inactive Pages ******************************************************
 *
 *  This provides a method for determining which pages are marked as inactive
 *  This is used in most places where a parent page select box is used to
 *  provide an optgroup sub-menu so that you can easily keep track of works in
 *  progress.
 *
 ******************************************************************************/

     function getInactives () {

        $this->inactives = array ();
        $fields = array('id',
                        'titleNav');
        $clause = 'WHERE active!=1 ORDER BY titleNav';
        $GLOBALS['db']->select($fields,
                                TablePageMetaData,
                                $clause,
                                __FILE__,
                                __LINE__);
        while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
            $this->inactives[] = array('id' => $row['id'],
                                    'titleNav' => $row['titleNav']);
        }

    // End getInactives
     }


/***** Get Orphan Pages ********************************************************
 *
 *  This provides a method for determining which pages no longer have a valid
 *  parent id.  This is used in most places where a parent page select box is
 *  used to provide an optgroup sub-menu so that you can easily keep track
 *  of orphans.
 *
 ******************************************************************************/

     function getOrphans () {

        $this->orphans = array ();
        $fields = array('id',
                        'titleNav');
        $clause = 'WHERE pid NOT IN (SELECT id FROM '
                        . TablePageMetaData
                        . ') AND pid != 0 ORDER BY titleNav';
        $GLOBALS['db']->select($fields,
                                TablePageMetaData,
                                $clause,
                                __FILE__,
                                __LINE__);
        while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
            $this->orphans[] = array('id' => $row['id'],
                                    'titleNav' => $row['titleNav']);
        }

    // End getOrphans
     }


/***** Get Inactive Content ****************************************************
 *
 *  This provides a mechanism to quickly retrieve all inactive site content and
 *  is used by the inactive content admin module.
 *
 ******************************************************************************/

     function getInactiveContent () {

        $this->item = array();
        $this->inactiveContent = array();

        $fields = array('id AS cid',
                        'pid AS cpid',
                        'acsLow',
                        'acsHigh',
                        'so',
                        'active',
                        'cssClass',
                        'cssID',
                        'header',
                        'content',
                        'imgURI',
                        'headerURI',
                        'stamp');
        $clause = 'WHERE active!=1 ORDER BY so, header';
        $GLOBALS['db']->select($fields,
                                TablePageContent,
                                $clause,
                                __FILE__,
                                __LINE__);
        while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
            // Note the select AS in the above sql
            foreach ($row as $key => $value) {
                $this->item[$key] = $value;
            }
            $this->inactiveContent[] = $this->item;
        }
        unset ($this->item);

    // End getInactiveContent
    }


/***** Get Orphan Content ******************************************************
 *
 *  This retrieves an array of content blocks that have their parent id set to
 *  a value which does not exist.  This allows you to find content that would
 *  otherwise have been lost if a user did not select 'delete all page content'
 *  but deleted the page (for instance).  This can be useful if you want to
 *  ditch a page and most of its content but say keep a couple paragraphs and
 *  move them to a new page later.
 *  This is used by the orphan content admin module.
 ******************************************************************************/

     function getOrphanContent () {

        $this->item = array();
        $this->orphanContent = array();

        $fields = array('id AS cid',
                        'pid AS cpid',
                        'acsLow',
                        'acsHigh',
                        'so',
                        'active',
                        'cssClass',
                        'cssID',
                        'header',
                        'content',
                        'imgURI',
                        'headerURI',
                        'stamp');
        $clause = 'WHERE pid NOT IN (SELECT id FROM `'
                . TablePageMetaData
                . '`) ORDER BY so, header';
        $GLOBALS['db']->select($fields,
                                TablePageContent,
                                $clause,
                                __FILE__,
                                __LINE__);
        while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
            // Note the select AS in the above sql
            foreach ($row as $key => $value) {
                $this->item[$key] = $value;
            }
            $this->orphanContent[] = $this->item;
        }
        unset ($this->item);

    // End getOrphanContent
    }


/***** Get Page Title By ID ****************************************************
 *
 *
 *
 ******************************************************************************/

    function getPageTitleByID ($id) {

        if ($id == 0) {
            return '-Downloads Only-';
        }
        $tempDB =& new mySqlObject ();
        if (is_numeric($id)) {
            $field = array('titleNav');
            $clause = 'WHERE id=' . $id . ';';
            $tempDB->select($field, TablePageMetaData, $clause,
                                    __FILE__, __LINE__);
            if ($row = $tempDB->fetch(__FILE__, __LINE__)) {
                return ($row['titleNav']);
            }
        }
        return 'Invalid ID';

    // End getPageTitleByID
    }


/***** Get Page and Content Resource Assignments for Current Page **************
 *
 *  Doing this all at once saves on queries
 *
 ******************************************************************************/

    function getResourceAssignments () {

        // Get resource assignments for this page
        $fields = array ('rid',
                        'pid',
                        'cid'
                        );
        $clause = 'WHERE pid="' . $this->id . '" ORDER BY cid';
        $GLOBALS['db']->select($fields, TablePageResourceAssignment, $clause,
                                __FILE__, __LINE__);
        while ($assignment = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
            // Prevent overwrite
            $temp = new mySqlObject();
            // Get file meta data
            $fields = array ('title',
                            'location',
                            'description'
                            );
            $clause = 'WHERE id="'
                        . $assignment['rid']
                        . '" AND active=1 ORDER BY title';
            $temp->select($fields, TablePageResourceMeta, $clause,
                                    __FILE__, __LINE__);
            $resource = $temp->fetch(__FILE__, __LINE__);

            // Set page hasResources flag
            if (($assignment['cid'] == '0')
                    && ($temp->numRows() == 1)
                    ) {
                $this->hasResources = "true";
            } elseif ($temp->numRows() == 1) {
                foreach ($this->content as $k => $v) {
                    if ($v['cid'] == $assignment['cid']) {
                        $this->content[$k]['hasResources'] = "true";
                    }
                }
            }
            if ($temp->numRows() == 1) {
                // Determine resource type by location for css class
                $resource['type'] = substr(dirname($resource['location']), 9);
                // Filtration handled in Smarty
                $this->resources[] = array_merge($assignment, $resource);
            }
        }
        $this->resources = array_sort_by_any_key ($this->resources, 'title');
        
    // End getResourceAssignments
    }


/***** Get Resource Location by ID *********************************************
 *
 *
 *
 ******************************************************************************/

    function getResourceLocationByID ($id) {

        if ($id == 0) {
            return '-Location Invalid-';
        }
        $tempDB =& new mySqlObject ();
        if (is_numeric($id)) {
            $field = array('location');
            $clause = 'WHERE id=' . $id . ';';
            $tempDB->select($field, TablePageResourceMeta, $clause,
                                    __FILE__, __LINE__);
            if ($row = $tempDB->fetch(__FILE__, __LINE__)) {
                return ($row['location']);
            }
        }
        return 'Invalid ID';

    // End getResourceTitleByID
    }


/***** Get Resource Title by ID ************************************************
 *
 *
 *
 ******************************************************************************/

    function getResourceTitleByID ($id) {

        if ($id == 0) {
            return '-Resource Invalid-';
        }
        $tempDB =& new mySqlObject ();
        if (is_numeric($id)) {
            $field = array('title');
            $clause = 'WHERE id=' . $id . ';';
            $tempDB->select($field, TablePageResourceMeta, $clause,
                                    __FILE__, __LINE__);
            if ($row = $tempDB->fetch(__FILE__, __LINE__)) {
                return ($row['title']);
            }
        }
        return 'Invalid ID';

    // End getResourceTitleByID
    }


/***** Get Content Header By ID ************************************************
 *
 *
 *
 ******************************************************************************/

    function getContentHeaderByID ($id) {

        if ($id == 0) {
            return '-None-';
        }
        $tempDB =& new mySqlObject ();
        if (is_numeric($id)) {
            $field = array('header');
            $clause = 'WHERE id=' . $id . ';';
            $tempDB->select($field, TablePageContent, $clause,
                                    __FILE__, __LINE__);
            if ($row = $tempDB->fetch(__FILE__, __LINE__)) {
                return ($row['header']);
            }
        }
        return 'Invalid ID';

    // End getContentHeaderByID
    }


/***** Load Content Templates **************************************************
 *
 *  Reach out to the file system and find all template files which match the
 *  content template naming scheme (content_XX.tpl).  This method is used by
 *  the page create/edit scripts.
 *
 ******************************************************************************/

     function load_content_templates () {
        $dir = PathSmarty . 'templates';
	    $retVal = array();
	    if (is_dir($dir)) {
        	$handle=opendir($dir);
        	while ($file = readdir($handle)) {
        		/* If file fits content template pattern,
                    excluding content_00.tpl, the default */
        		if (preg_match("/^content_.+tpl$/", $file)) {
        			$retVal[] = $file;
        		}
        	}
        	/* Clean up and sort */
        	closedir($handle);
        	if (count($retVal) < 1) {
        		$this->contentTemplates = false;
        	} else {
        		sort($retVal);
        		$this->contentTemplates = $retVal;
        	}
        }
    // End load_content_templates
    }


/***** Load Sitemap Frequency ID/Value Array ***********************************
 *
 *  Valid site map frequencies are stored in the JWSF_predef* table set and
 *  this method provides those values to the page create/edit scripts.
 *
 ******************************************************************************/
    function load_sitemap_frequency_array () {

        $fields = array('id',
                        'value');
        $clause = 'WHERE 1';
        $GLOBALS['db']->select($fields,
                                TableSiteMapFrequencies,
                                $clause,
                                __FILE__,
                                __LINE__);
        while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
            $key = $row['id'];
            $value = $row['value'];
            $this->frequencies[$key] = $value;
        }

    }


/***** Post To Social Loader ***************************************************
 *
 *  This will not post if web server hostname resolves to 'localhost'
 *
 * ****************************************************************************/

    function postToLoader () {

        if (FQDN == 'localhost') return;
        if (isset($GLOBALS['jwsfConfig']->SitePostToSocial)
                && ($GLOBALS['jwsfConfig']->SitePostToSocial == 'true') 
                ) {
            $this->socialAccounts = array();
            // Read sites from database
            $fields = array('title', 'username', 'password');
            $GLOBALS['db']->select($fields, TableSocialAccounts, null,
                __FILE__, __LINE__);
            while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
                $key = $row['title'];
                $this->socialAccounts[$key] = $row; 
            }
            // Call each post method
            foreach ($this->socialAccounts as $k => $a) {
                $method = 'postTo' . $a['title'];
                $this->$method();
            }
        }

    }


/***** Post Page Updates to Twitter ********************************************
 *
 *
 *
 * ****************************************************************************/

    function postToTwitter () {

        $url = 'https://twitter.com/statuses/update.json';
        $data = 'status=http://' . FQDN . '/' . $this->semanticURI;
        // Encode authentication header
        $creds = $this->socialAccounts['Twitter'];
        $digest = base64_encode($creds['username'] . ':' . $creds['password']);
        $header = 'Authorization: Basic ' . $digest;
        // Submit
        if (submit_post_request($url, $data, $header) == true) {
            $GLOBALS['messagesSuccess'][] = 'Post to twitter succeeded';
        } else {
            $GLOBALS['messagesError'][] = 'Post to twitter failed';
        }

    }


/***** Activate a Resource *****************************************************
 *
 *
 *
 ******************************************************************************/

    function resourceActivate () {

        if (!isset($_POST['activate'])) {
            $GLOBALS['messageError'][] = 'Nothing to activate';
            return false;
        }
        if ($_GET['action'] == 'edit-files') {
            $table = TablePageResourceMeta;
        }
        $data = array ('active' => 1);
        $clause = 'WHERE id="' . $_POST['activate'] . '" LIMIT 1';
        $GLOBALS['db']->update ($data,
                                $table,
                                $clause,
                                __FILE__, __LINE__);
        $GLOBALS['messagesSuccess'][] =
                    'The resource assignment has been removed';

    }


/***** Resource Assignment Delete **********************************************
 *
 *
 *
 ******************************************************************************/

    function resourceAssignmentDelete () {

        if (!isset($_POST['assignment_delete'])
                || !isset($_POST['assID'])
                ) {
            $GLOBALS['messageError'][] = 'Resource assignment not removed';
            return false;
        }
        $clause = 'WHERE id=' . $_POST['assID'];
        $GLOBALS['db']->delete (TablePageResourceAssignment,
                                $clause,
                                __FILE__, __LINE__);
        $GLOBALS['messagesSuccess'][] =
                    'The resource assignment has been removed';

    }


/***** Deactivate a Resource ***************************************************
 *
 *
 *
 ******************************************************************************/
 
    function resourceDeactivate () {

        if (!isset($_POST['deactivate'])) {
            $GLOBALS['messageError'][] = 'Nothing to deactivate';
            return false;
        }
        if ($_GET['action'] == 'edit-files') {
            $table = TablePageResourceMeta;
        }
        $data = array ('active' => 0);
        $clause = 'WHERE id="' . $_POST['deactivate'] . '" LIMIT 1';
        $GLOBALS['db']->update ($data,
                                $table,
                                $clause,
                                __FILE__, __LINE__);
        $GLOBALS['messagesSuccess'][] =
                    'The resource has been deactivated';

    // End resourceDeactivate
    }


/***** Resource Assignment Edit ************************************************
 *
 *
 *
 ******************************************************************************/

    function resourceAssignmentEdit () {

        if (!isset($_POST['assignment_edit'])) return false;
        $fields = array('id', 'rid', 'pid', 'cid');
        $clause = 'WHERE id=' . $_POST['assignment_edit'];
        $GLOBALS['db']->select($fields,
                                TablePageResourceAssignment,
                                $clause,
                                __FILE__,
                                __LINE__);
        $data = $GLOBALS['db']->fetch(__FILE__, __LINE__);
        $data['title'] = $this->getResourceTitleByID($data['rid']);
        $data['location'] = $this->getResourceLocationByID($data['rid']);
        $data['page'] = $this->getPageTitleByID($data['pid']);
        $data['content'] = $this->getContentHeaderByID($data['cid']);
        $this->resourceAssignment = $data;

    // End resourceAssignmentEdit
    }


/***** Update a Resource's Assignment ******************************************
 *
 *
 *
 ******************************************************************************/

    function resourceAssignmentUpdate () {
        $modifiedBy = ($GLOBALS['userObject']->acslvl > 0)
            ? $GLOBALS['userObject']->email
            : $_SERVER['REMOTE_ADDR'];
        // Update meta data
        $data = array ('pid' => $_POST['editID'],
                        'cid' => $_POST['fileContentID'],
                        'modifiedBy' => $modifiedBy
                        );
        $clause = 'WHERE id=' . $_POST['assID'];
        $result = $GLOBALS['db']->update($data, TablePageResourceAssignment,
                                    $clause,
                                    __FILE__, __LINE__);
        $GLOBALS['messagesSuccess'][] = 'Assignment has been updated';

    // End fileUpdate
    }


/***** Hide a Resource *********************************************************
 *
 *
 *
 ******************************************************************************/

    function resourceHide () {

        if (!isset($_POST['hide'])) {
            $GLOBALS['messageError'][] = 'Nothing to hide';
            return false;
        }
        if ($_GET['action'] == 'edit-files') {
            $table = TablePageResourceMeta;
        }
        $data = array ('viewable' => 0);
        $clause = 'WHERE id="' . $_POST['hide'] . '" LIMIT 1';
        $GLOBALS['db']->update ($data,
                                $table,
                                $clause,
                                __FILE__, __LINE__);
        $GLOBALS['messagesSuccess'][] =
                    'The resource has been hidden';

    // End resourceHide
    }


/***** Show a Resource *********************************************************
 *
 *
 *
 ******************************************************************************/

    function resourceShow () {

        if (!isset($_POST['show'])) {
            $GLOBALS['messageError'][] = 'Nothing to show';
            return false;
        }
        if ($_GET['action'] == 'edit-files') {
            $table = TablePageResourceMeta;
        }
        $data = array ('viewable' => 1);
        $clause = 'WHERE id="' . $_POST['show'] . '" LIMIT 1';
        $GLOBALS['db']->update ($data,
                                $table,
                                $clause,
                                __FILE__, __LINE__);
        $GLOBALS['messagesSuccess'][] =
                    'The resource has been shown';

    // End resourceShow
    }


/***** Test Page ID / Content ID for Match *************************************
 *
 *  This tests to see if a content id is associated with the given page id.  It
 *  is used by the file editor.  This must be run on a temporary db object as
 *  it would otherwise overwrite the cached query inside the file editor.
 *
 ******************************************************************************/

    function testPageContentMatch ($parent, $content) {

        $tempDB =& new mySqlObject ();
        $test = array_search($parent, $this->idsExist);
        if ($test === false) {
            unset ($tempDB);
            return false;
        }
        $fields = array('id');
        $clause = 'WHERE id='
                    . $content
                    . ' AND pid='
                    . $parent
                    . ';';
        $tempDB->select($fields, TablePageContent, $clause,
                                __FILE__, __LINE__);
        while ($row = $tempDB->fetch(__FILE__, __LINE__)) {
            unset ($tempDB);
            return true;
        }
        unset ($tempDB);
        return false;
    }

// End pageObject
}


?>
