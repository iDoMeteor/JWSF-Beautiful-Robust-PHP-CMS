<?php

/*******************************************************************************
 *  Jason White's Slim Frame Site-Wide Configuration Object
 *
 *  $Revision: 1.10 $
 *
 *  This is the site-wide configuration object which provides various parameters
 *  that affect the global behavior of the Jason White Slim Frame system.  These
 *  parameters are controlled via the administration area and include things
 *  like default theme, whether or not to allow themes, css and javascript, and
 *  more.
 *
 *	20080304 - Move most of index.php(s) to here (finally)
 *				PathIncAbs is defined in both index.php(s) and is imperative
 *				that it is a valid path.
 *
 *	*Smarty Note*
 *		Extending Smarty class to allow per-theme smarty templates
 *		For now, this little gem will lie buried at the bottom of this sea
 *
 ******************************************************************************/

/***** Class Instantiation Function ********************************************
 *
 *  Upon creation of a new object, draw all the values from the database
 *  in alphabetical order setting an object property => value set
 *  for each configuration option.
 *
 *  It also currently sets a property for the favicon location as
 *  defined in the path constants file, if the file exists.  This could
 *  possibly be moved out or given an administrative interface.
 *
 *  20090107 - Moved to multiple tables
 *
 *	TODO:
 *		Inspect placement of sanitize's effect on sessionstart
 *		Update comments
 *
 ******************************************************************************/

class configObject {

    function configObject () {

		$this->sanitizeEnvironment();

        $this->defineCriticalValues();

        $this->setGlobalEnvironment();

        $this->getConfig();

		$this->setTimezone();

		$this->floodCheck();

		$this->sessionStart();

		$this->setMimeType();

		$this->detectUserAgent();

        $this->setThemeEnvironment();

        $this->setImageEnvironment();

        $this->smartyStart();

    }


/***** Sanitize Incoming Data **************************************************
 *  This function formats the $_REQUEST array of
 *  incoming data by trimming whitespace, making
 *  sure buffer overflow attacks are discovered,
 *  and cleaning up anything that might get passed
 *  to the database is passed as data and not SQL
 *  instructions.
 *
 *	$_REQUEST is just an associative array of $_GET, $_POST, and $_COOKIE, so
 *	those are all taken care of there.  Just make sure you sanitize
 *	appropriately if you use $_FILES anywhere!
 *
 *	sanitize_incoming() is a function from Func_Required and is also used to
 *	sanitize queries in the MySQL class.
 *
 ******************************************************************************/

	function sanitizeEnvironment() {

		if (isset($_REQUEST)) {
			$_REQUEST = sanitize_incoming($_REQUEST);
		}
		if (isset($_GET)) {
			$_GET = sanitize_incoming($_GET);
		}
        /*
		 *if (isset($_POST)) {
		 *    $_POST = sanitize_incoming($_POST);
		 *}
         */
		if (isset($_SESSION)) {
			$_SESSION = sanitize_incoming($_SESSION);
		}

	// End sanitizeEnvironment
    }


/***** Define Critical Values **************************************************
 *
 *	This just sort of landed here recently and is bound to be improved
 *
 ******************************************************************************/

	function defineCriticalValues () {

		/***** Configuration tables *****/
		DEFINE ('TableConfigNumeric', 'JWSF_ConfigNumeric');
		DEFINE ('TableConfigStrings', 'JWSF_ConfigStrings');
		DEFINE ('TableConfigSwitches', 'JWSF_ConfigSwitches');
		DEFINE ('TableEmailBccs', 'JWSF_EmailBccs');
		DEFINE ('TableEmailAdmins', 'JWSF_EmailAdmins');


		/***** Default Session Name & Location *****/
		DEFINE ('SessionDefaultName', 'JWSF');
		DEFINE ('SessionStorage', PathIncAbs . '/' . 'sessions');

		/***** Default Site-Wide Templates *****/
		DEFINE ('SiteTemplate', 'Site.tpl');

		// Directory name of theme root
		DEFINE ('ThemeRootDir', 'themes');
		DEFINE ('ThemeDefault', 'Default-White');

		// Path to root theme folder
		DEFINE ('PathThemesAbs', SiteRoot . '/' . ThemeRootDir . '/');
		DEFINE ('PathThemesRel', CurrentProtocol . FQDN . '/' . ThemeRootDir . '/');

		// File extensions for CSS, JavaScript, and Smarty files
		DEFINE ('ExtCSS', 'css');
		DEFINE ('ExtJS', 'js');
		DEFINE ('ExtSmarty', 'tpl');

		// CSS file name units used to specify CSS media
		// CSS file names MUST be in format 'semanticname.mediatype.css'!
		// example: 'nagivation.screen.css'
		// note:  periods (.) MUST be the seperator!
		DEFINE ('CSSMediaAll', 'all');
		DEFINE ('CSSMediaAural', 'aural');
		DEFINE ('CSSMediaBraille', 'braille');
		DEFINE ('CSSMediaEmbossed', 'emboss');
		DEFINE ('CSSMediaHandHeld', 'hh');
		DEFINE ('CSSMediaPrint', 'print');
		DEFINE ('CSSMediaProjection', 'projection');
		DEFINE ('CSSMediaScreen', 'screen');
		DEFINE ('CSSMediaTTY', 'tty');
		DEFINE ('CSSMediaTV', 'tv');

	// End defineCriticalValues
	}


/***** Set Global Environment **************************************************
 *
 *
 ******************************************************************************/

    function setGlobalEnvironment () {

        $GLOBALS['numQueries'] = 0;
        $GLOBALS['tabindex'] = 1;

        $GLOBALS['messagesError'] = array();
        $GLOBALS['messagesNotice'] = array();
        $GLOBALS['messagesSuccess'] = array();

    }


/***** Get Admin Email Addresses ***********************************************
 *
 *  This should read all admin level users unless user system is disabled,
 *  in which case it should read from admin email table.
 *
 * ****************************************************************************/

    function getAdminEmails () {

        $this->adminEmails = array();
        if ($GLOBALS['jwsfConfig']->UsersAllow == 'false') {
            // Read users from email table
            $fields = array('item', 'value');
            $GLOBALS['db']->select($fields, TableEmailAdmins,
                null, __FILE__, __LINE__);
            while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
                if (validateEmail(true, $row['value']) == true) {
                    $this->adminEmails[] = $row['value'];
                }
            }
        } else {
            // Read admin users from user table
            $GLOBALS['userObject']->getAdminEmails();
            $this->adminEmails = $GLOBALS['userObject']->adminEmails;
        }

    }


/***** Get BCC List ************************************************************
 *
 *  This list will be emailed a copy of every email sent from JWSF
 *  TODO:
 *      Switch contact bcc table to site-wide name
 *
 * ****************************************************************************/

    function getBccList () {

        $this->bccList = array();
        $fields = array('item', 'value');
        $GLOBALS['db']->select($fields, TableEmailBccs, 
            null, __FILE__, __LINE__);
        while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
            if (validateEmail(true, $row['value']) == true) {
                $this->adminEmails[] = $row['value'];
            }
        }

    }


/***** Get Configuration Values ************************************************
 *
 *  Used to populate the configuration table in the admin section.
 *
 ******************************************************************************/

    function getConfig () {

            // Database query
            $sql = 'SELECT name, value FROM '
                    . TableConfigNumeric
            		. ' UNION SELECT name, value FROM '
                    . TableConfigStrings
            		. ' UNION SELECT name, value FROM '
                    . TableConfigSwitches
                    . ' ORDER BY name';
            $result = $GLOBALS['db']->query($sql, __FILE__, __LINE__);
            while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
                $this->$row['name'] = $row['value'];
            }

    }


    /***** Get Configuration Descriptions **************************************
     *
     *  Used to populate the configuration table in the admin section.
     *
     **************************************************************************/

    function setTimezone () {

		if (isset($this->TimeZone)) {
			date_default_timezone_set ($this->TimeZone);
		}

	}


    /***** Get Configuration Descriptions **************************************
     *
     *  Used to populate the configuration table in the admin section.
     *
     **************************************************************************/

    function read ($table) {

        $this->editData = array();

		$fields = array('name', 'value', 'description');
		$clause = 'ORDER BY name';
        $GLOBALS['db']->select($fields, $table, $clause, __FILE__, __LINE__);
        while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
            $this->editData[] =
                    array (
                        $row['name'],
                        $row['value'],
                        $row['description']
                    );
        }

    }


    /***** Update Configuration Parameters *************************************
     *
     *  Upon submission of the configuration parameters form, this function
     *  updates the database with any altered values.  No real validation is
     *  done, since this is coming from the admin area.  However, when this is
     *  re-written into the proper module form, we should add validation for
     *  continuity at least.
     *
     *  For now, since these are stored in the db as strings rather than bool,
     *  all true/false values must be evaluated as strings!
     **************************************************************************/

    function update ($table) {

		$fields = array('name', 'value');
		$clause = 'ORDER BY name';
        $GLOBALS['db']->select($fields, $table, $clause, __FILE__, __LINE__);
	    // Must create new object or result will be overwritten
	    $tempObject = new mySqlObject();
        while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
            $name = $row['name'];
            $value = $row['value'];
            // Only update if value is different (reduces number of queries)
            if ($_POST[$name] != $value) {
        	$data = array ('value' => $_POST[$name]);
        	$clause = 'WHERE name="' . $name . '" LIMIT 1';
            $tempObject->update ($data, $table, $clause, __FILE__,__LINE__);
            }
        }
        // Read new configuration values
		$this->getConfig();

    // End update
    }


/***** Session Management ******************************************************
 *  Start the session, set a non-world readable location to store session
 *  information in, to reduce the ease with which sessions may be hijacked due
 *  to someone else running an insecure script on this server.
 *
 *	TODO:
 *		Further enhance this to only start a session when necessary
 *
 ******************************************************************************/

	function sessionStart () {

		if ($this->SessionsUse == 'true') {
			session_name (SessionDefaultName);
			session_save_path (SessionStorage);
			session_start ();
		}

	// End sessionStart
	}


/***** Instantiate Smarty Object ***********************************************
 *  This fires up the Smarty engine and configures
 *  necessary file locations.  All editable files
 *  are stored in the templates directory, but
 *  should only need to be modified if data structures
 *  are altered in some way on the back end.  Additional
 *  content templates for page by page styling may be
 *  added by naming the file contentXX.tpl where XX is
 *  the page id from the database.  If any additional
 *  markup tags are required for styling, they should
 *  be added via the theme specific Javascript.  This
 *  will ensure continuity of data regardless of the
 *  theme chosen.
 *
 *  5/29
 *      Moved this way up top so that we can assign
 *      smarty data from anywhere.
 *
 *	Smarty Caching
 *
 *		This loads here so that it can access configuration switch
 *
 *		Setting caching to 2 allows us to use per-template cache timeouts, which
 *		is good for things like the authentication area, who's online, etc..
 *
 *		Note: Any authentication changes should probably flush all caches
 *
 *
 *		***BIG NOTE***
 *		Using this with such a dynamic system is probably totally insane.  I keep
 *		my system with caching off, but on a stable system with little changes this
 *		would be fantastic.  I considered giving everything custom caching times,
 *		but on a multi-user system that is completely impractical.
 *
 *	TODO
 *		Fix so smarty checks advanced like a theme dir and then
 *		advanced only has to contain those with diffs
 ******************************************************************************/

	function smartyStart () {

		$GLOBALS['smarty'] = new MySmarty;

        $GLOBALS['smarty']->register_object('config', $this);

        // Smarty could just read from config assignment.. maybe later
        $GLOBALS['smarty']->assign('themes', $this->availableThemes);

		$this->bufferTemplates();

	// End smartyStart
	}


/***** JWSF Feature Check ******************************************************
 *
 *
 *
 * ****************************************************************************/

    function featureCheck() {



    }


/***** JWSF Feature Installer *************************************************
 *
 *
 *
 * ****************************************************************************/

    function featureInstall() {



    }


/***** Flood Check *************************************************************
 *
 *  TODO:
 *      Strings and settings to database w/admin
 *      Check string prefixes in template files
 *      Move checks into some kind of array? Should be easy to extend!
 *      Send email to admin on ban
 *
 ******************************************************************************/
    function floodCheck() {

        // These could go to database
        $this->floodFrequency = 60;    // seconds between actions
        $this->floodDuration = 10;     // minutes of ban duration

        // Do not alter this order unless you really know what you're doin!
        $this->floodCheckInstall();
        $this->floodLoadLevels();
        $this->floodPrune();
        $this->floodCheckBan();
        $this->floodInsert();
        $this->floodInterpolate();
        $this->floodRatioGenerate();
        $this->floodRatioCheck();
    }


/***** Check Installation ******************************************************
 *
 *
 *
 * ****************************************************************************/

    function floodCheckInstall() {
return;
        if (!isset($this->ProtectionFlood)) {

            // Create tables
            $sql = "
                CREATE TABLE IF NOT EXISTS JWSF_FloodBlocks (
                    id int(10) unsigned NOT NULL auto_increment,
                    ip varchar(15) NOT NULL,
                    host tinytext NOT NULL,
                    `level` int(1) unsigned NOT NULL,
                    stamp timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
                    PRIMARY KEY  (id)
                ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=26 ;
                CREATE TABLE IF NOT EXISTS JWSF_FloodLevels (
                    id int(1) unsigned NOT NULL auto_increment,
                    `level` int(1) unsigned NOT NULL,
                    max int(10) unsigned NOT NULL,
                    description tinytext NOT NULL,
                    stamp timestamp NOT NULL default CURRENT_TIMESTAMP,
                    PRIMARY KEY  (id)
                ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

                CREATE TABLE IF NOT EXISTS JWSF_FloodTable (
                    id int(10) unsigned NOT NULL auto_increment,
                    `level` int(1) unsigned NOT NULL,
                    ip varchar(15) NOT NULL,
                    stamp timestamp NOT NULL default CURRENT_TIMESTAMP,
                    PRIMARY KEY  (id)
                ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=188 ;
                ";
            $GLOBALS['db']->query($sql, __FILE__, __LINE__);

            // Populate levels table
            $sql = "
                INSERT INTO `JWSF_FloodLevels` (`id`, `level`, `max`, `description`, `stamp`) VALUES
                (1, 1, 100, 'Level 1 actions include general page requests and get requests.  More than max # of times during frequency number of seconds result in a ban of duration minutes.', '2009-12-04 00:33:22'),
                (2, 2, 100, 'Unused at this time.', '2009-12-04 00:33:22'),
                (3, 3, 60, 'Level 3 actions include generic post requests.', '2009-12-04 00:33:44'),
                (4, 4, 30, 'Level 4 actions include ratings, comments and contact form submissions.', '2009-12-04 00:33:44'),
                (5, 5, 10, 'Level 5 actions include authentication requests, sql injection attempts, and other obvious script kiddie hacker robots.', '2009-12-04 00:33:58'),
                (6, 6, 100, 'Level 6 actions are applied to blocked users and will prevent them from receiving any output whatsoever.  Other banned users will receive a very minimal message describing there ban reason and duration.', '2009-12-04 00:59:00');
            ";

            // Insert config switch
            $data = array('SiteProtectionFlood', 'true', 'Site wide flood protection');
            $GLOBALS['db']->insert($data, TableConfigSwitches, 
            __FILE__, __LINE__);

        }

    }


/***** Flood Load Levels *******************************************************
 *
 *
 *
 * ****************************************************************************/

    function floodLoadLevels() {

        $this->floodLevels = array();
        $fields = array('id', 'level', 'max', 'description');
        $clause = 'ORDER BY id ASC';
        $GLOBALS['db']->select($fields, TableFloodLevels, 
            $clause, __FILE__, __LINE__);
        while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
            $this->floodLevels[] = $row;
        }

    }


/***** Flood Pruning ***********************************************************
 *
 *
 *
 * ****************************************************************************/

    function floodPrune() {

        // Set ban age limit
        $age = time() - $this->floodDuration*60;
        $clause = 'WHERE UNIX_TIMESTAMP(`stamp`)<' . $age;
        // Prune ban table
        $GLOBALS['db']->delete(TableFloodBlocks, $clause, __FILE__, __LINE__);
        // Prune flood table
        $GLOBALS['db']->delete(TableFloodTable, $clause, __FILE__, __LINE__);

    }


/***** Flood Table Insert ******************************************************
 *
 *
 *
 * ****************************************************************************/

    function floodCheckBan() {
        $this->flooders = array();
        $fields = array('id', 'level', 'ip', 'UNIX_TIMESTAMP(`stamp`)');
        $clause = 'WHERE ip=\'' . $_SERVER['REMOTE_ADDR'] . '\'';
        $GLOBALS['db']->select($fields, TableFloodBlocks, 
            null, __FILE__, __LINE__);
        $bP = array(0,0,0,0,0);
        while ($this->flooders = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
            // User is banned
            $banLevel = $this->flooders['level'] - 1;
            // Smarty is not initialized yet, so here it is
            $banMessage = 'Your access to our site is currently'
                . ' restricted due to suspicious activity.  If you feel'
                . ' that you have received this message in error,'
                . ' please send an email to webmaster at '
                . FQDN
                . ' and we will correct the issue.  Otherwise, please'
                . ' try to modify your browser, computer or scripts to'
                . ' prevent any further errors.  The details of your'
                . ' ban may be seen below.'
                . "<br />"
                . "<br />"
                . 'Ban Duration: '
                . $this->floodDuration
                . " minutes."
                . "<br />"
                . 'Ban Reason: '
                . $this->floodLevels[$banLevel]['description']
                . ' You are only allowed '
                . $this->floodLevels[$banLevel]['max']
                . ' requests of this type per '
                . $this->floodFrequency
                . ' seconds!';
            switch ($this->flooders['level']) {
            case 1:
                if ($bP[0] == 1) break;
                $bP[0] = 1;
                exit ($banMessage); 
            case 2:
                // Currently unused
                if ($bP[1] == 1) break;
                $bP[1] = 1;
                break;
            case 3:
                if ($bP[2] == 1) break;
                unset($_POST);
                $bP[2] = 1;
                $GLOBALS['messagesNotice'][] = $banMessage;
                break;
            case 4:
                if ($bP[3] == 1) break;
                unset($_POST);
                $bP[3] = 1;
                $GLOBALS['messagesError'][] = $banMessage;
                break;
            case 5:
                if ($bP[4] == 1) break;
                unset($_POST);
                $bP[4] = 1;
                $GLOBALS['messagesError'][] = $banMessage;
                break;
            case 6:
                // Bad robot!
                exit (); 
            }
        }

    }


/***** Flood Table Insert ******************************************************
 *
 *
 *
 * ****************************************************************************/

    function floodInsert() {

        // Everything counts for level 1
        $data = array(
            'level' => 1,
            'ip' => $_SERVER['REMOTE_ADDR']
        );
        $GLOBALS['db']->insert($data, TableFloodTable, __FILE__, __LINE__);
        if (isset($_POST)
            && !empty($_POST)
        ) {
            // Generic post requests
            $data = array(
                'level' => 3,
                'ip' => $_SERVER['REMOTE_ADDR']
            );
            $GLOBALS['db']->insert($data, TableFloodTable, __FILE__, __LINE__);
            foreach ($_POST as $k => $v) {
                $prefix = substr($k, 0, 5);
                switch ($prefix) {
                case 'comme':
                case 'ratin':
                case 'conta':
                    $data = array(
                        'level' => 4,
                        'ip' => $_SERVER['REMOTE_ADDR']
                    );
                    $GLOBALS['db']->insert($data, TableFloodTable, __FILE__, __LINE__);
                    break;
                case 'authe':
                case 'admin':
                    $data = array(
                        'level' => 5,
                        'ip' => $_SERVER['REMOTE_ADDR']
                    );
                    $GLOBALS['db']->insert($data, TableFloodTable, __FILE__, __LINE__);
                    break;
                }
            }
        }
    }


/***** Generate Flood Ratio ****************************************************
 *
 *
 *
 * ****************************************************************************/

    function floodInterpolate () {

        $hits = array();
        $fields = array('level', 'ip', 'stamp');
        $clause = 'WHERE ip=\'' . $_SERVER['REMOTE_ADDR'] . '\'';
        $GLOBALS['db']->select($fields, TableFloodTable, $clause,
            __FILE__, __LINE__);
        while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
            $hits[] = $row;
        }
        $this->floodTable = array(0,0,0,0,0);
        foreach ($hits as $k => $v) {
            switch ($v['level']) {
            case 1:
                $this->floodTable[0]++;
                break;
            case 2:
                $this->floodTable[1]++;
                break;
            case 3:
                $this->floodTable[2]++;
                break;
            case 4:
                $this->floodTable[3]++;
                break;
            case 5:
                $this->floodTable[4]++;
                break;
            }
        }
    }


/***** Generate Flood Ratio ****************************************************
 *
 *
 *
 * ****************************************************************************/

    function floodRatioGenerate () {

        $this->floodRatio = array(0,0,0,0,0);
        foreach ($this->floodTable as $k => $v) {
            switch ($k) {
            case 0:
                $this->floodRatio[0] = $v / $this->floodLevels[0]['max'];
                break;
            case 1:
                $this->floodRatio[1] = $v / $this->floodLevels[1]['max'];
                break;
            case 2:
                $this->floodRatio[2] = $v / $this->floodLevels[2]['max'];
                break;
            case 3:
                $this->floodRatio[3] = $v / $this->floodLevels[3]['max'];
                break;
            case 4:
                $this->floodRatio[4] = $v / $this->floodLevels[4]['max'];
                break;
            }
        }

    }


/***** Check Flood Ratio *******************************************************
 *
 *
 *
 * ****************************************************************************/

    function floodRatioCheck () {

        foreach ($this->floodRatio as $k => $v) {
            // Block
            if ($v >= 1) {
                $data = array(
                    'ip' => $_SERVER['REMOTE_ADDR'],
                    'host' => gethostbyaddr($_SERVER['REMOTE_ADDR']),
                    'level' => $k + 1
                );
                $GLOBALS['db']->insert($data, TableFloodBlocks,
                    __FILE__, __LINE__);
            // Warn
            } elseif ($v > .85) {
                $GLOBALS['messagesNotice'][] = 'Slow down there chief, before'
                    . ' you force us to take action!';
            }
        }
    }


/***** Buffer Available Smarty Templates ***************************************
 *
 *
 *
 ******************************************************************************/

	function bufferTemplates () {

		$this->templatesTheme = array();
		$this->templatesAdvanced = array();
		$this->templatesStandard = array();

		if (isset($this->ThemeActive)
				&& is_dir(PathThemesAbs
					. $this->ThemeActive
					. '/smarty')
				) {
			// Load available theme templates
			$this->templatesTheme = dir_as_file_array
									(PathThemesAbs
									. $this->ThemeActive
									. '/smarty');
		}

		if (($this->SmartyFallbackAdvanced == 'true')
				&& is_dir(PathSmarty . 'templates/advanced')
				) {
			// Load available theme templates
			$this->templatesAdvanced
				= dir_as_file_array(PathSmarty . 'templates/advanced');
		}

		if (is_dir(PathSmarty . 'templates')) {
			// Load available theme templates
			$this->templatesStandard
					= dir_as_file_array(PathSmarty . 'templates');
		}
	// End bufferTemplates
	}


/****** Set MIME Type **********************************************************
 ******************************************************************************/

	function setMimeType () {

    	$this->mimeType = null;

        if (isset($_GET['url'])
                && ($_GET['url'] == 'rss')
                ) {
            header("Content-Type: application/xml; charset=ISO-8859-1");
            $this->mimeType = "Content-Type: application/xml;"
                            . " charset=ISO-8859-1";
        } elseif (isset($_SERVER['HTTP_ACCEPT']) &&
                stristr($_SERVER['HTTP_ACCEPT'], $this->MIMEPreferred)) {
            header('Content-Type: ' . $this->MIMEPreferred);
            $this->mimeType = $this->MIMEPreferred . '; charset=utf-8';
        } else {
            header('Content-Type: ' . $this->MIMEFallback);
            $this->mimeType = $this->MIMEFallback;
        }

	}


/****** Detect User Agent ******************************************************
 *
 *	This is useful for debugging, but I never refer to it so it is scheduled
 *	for removal. 20090312
 *
 ******************************************************************************/
    function detectUserAgent () {

        $agent = $_SERVER['HTTP_USER_AGENT'];
        $this->userAgent = 'other';

        if (strpos($agent, "Opera") != 0) {
        	$this->userAgent = "Opera";
        } else if (strpos($agent, "MSIE") != 0) {
        	$this->userAgent = "MSIE";
        } else if (strpos($agent, "Firefox") != 0) {
        	$this->userAgent = "Firefox";
        } else if (strpos($agent, "Netscape") != 0) {
        	$this->userAgent = "Netscape";
        } else if (strpos($agent, "Mozilla") != 0) {
        	$this->userAgent = "Mozilla";
        } else if (strpos($agent, "Safari") != 0) {
        	$this->userAgent = "Safari";
        }

    }


/***** Theme Preperattion ******************************************************
 *  Read the folder names from the theme directory (set
 *  in critical values area above) into an array to
 *  be passed on to Smarty.  Also, check the incoming
 *  data for a request to set a new theme.  Available themes
 *	are assigned to Smart in smartyStart() in this file.
 *
 *  We also determine which theme to load at this time. If theme
 *  loading and user theme selection is true, we verify the
 *  user's selected theme or fall back to the same process on site
 *  default theme, and finally upon failure of that we fall back to
 *  the system default theme, which is generally 'skeleton' and the
 *  if that is not there, all theme loading features are turned off.
 *
 ******************************************************************************/

    function setThemeEnvironment () {

		$themes = array();
		$themes = dir_as_file_array(PathThemesAbs);

		if (isset($_POST['theme'])
                && (in_array($_POST['theme'], $themes) == true)
                && ($this->ThemeAllowUserSelect == 'true')
				) {
		   $this->ThemeActive = $_POST['theme'];
		   $_SESSION['ThemeActive'] = $this->ThemeActive;
		} elseif (isset($_SESSION['ThemeActive'])
                && (in_array($_SESSION['ThemeActive'], $themes) == true)
                && ($this->ThemeAllowUserSelect == 'true')
                ) {
            $this->ThemeActive = $_SESSION['ThemeActive'];
		} elseif (isset($this->ThemeActive)
                && (in_array($this->ThemeActive, $themes) == true)
                ) {
            $this->ThemeActive = $this->ThemeActive;
        } elseif (in_array(ThemeDefault, $themes) == true) {
            $this->ThemeActive = ThemeDefault;
		} else {
			// No valid theme directories found
			$this->ThemeActive = null;
			$this->ThemeLoad = false;
			$this->ThemeLoadCss = false;
			$this->ThemeLoadJs = false;
		}

		// This finds theme specific image directory if it exists
		if (is_dir(PathThemesAbs . $this->ThemeActive . '/images')) {
				$this->PathThemeImagesRel = PathThemesRel
											. $this->ThemeActive
											. '/images/';
				$this->PathThemeImagesAbs = PathThemesAbs
											. $this->ThemeActive
											. '/images/';
		} else {
			$this->PathThemeImagesAbs = null;
			$this->PathThemeImagesRel = null;
		}

		$this->availableThemes = $themes;

	// End getThemes
	}


/***** Set Image Environment ***************************************************
 *
 *	This method defines possible image locations.  The default
 *	images should be in /_img (may change to /images soon) or
 *	a sub-directory within there.  Any image file which exists in
 *	/themes/Theme-Name/images with the same name as a default image
 *	will be replaced by the theme specific image.
 *
 *	$this->favIcon is set to null regardless to keep meta template simple
 *
 ******************************************************************************/

	function setImageEnvironment () {

		$this->favIcon = null;

		// Move these to db
		$imageDir = '_img';
		$imageNameLogo = 'logo';
		$imageNameFavIcon = 'favicon.ico';
		$w3perfect = 'jwsf/w3perfect.png';
		$w3perfectOver = 'jwsf/w3perfect-over.png';

		$this->imageTypes = array('png', 'jpg', 'gif', 'jpeg',
							'PNG', 'JPG', 'GIF', 'JPEG', 'ico');

		// This is the path, relative to site root, where images are stored
		if (is_dir(SiteRoot . '/' . $imageDir . '/')) {
			$this->PathImgRel = '/' . $imageDir . '/';
			$this->PathImgAbs = SiteRoot . '/' . $imageDir . '/';
		}

		// Don't load images if there is not a valid image folder
		if (isset($this->PathImgRel) || isset($this->PathThemeImagesRel)) {

            // Build array of theme images to reduce file system calls
            $this->buildThemeImageArray();


			// Path to site logo
			$this->PathImgLogoRel = $this->checkImageLocation($imageNameLogo);


            /*********** Set Favicon Location **********************************
             *  This seems to have a difficult time finding a home, but this is
             *  as good a place as any.
             * ****************************************************************/
			if ($this->SiteUseFavIcon == 'true') {
				$this->favIcon = $this->checkImageLocation($imageNameFavIcon);
			}

			/***** W3C Perfect Code Image Location ****************************************/
			$this->w3perfect = $this->checkImageLocation($w3perfect);
			$this->w3perfectOver = $this->checkImageLocation($w3perfectOver);

			/***** Credit Card Images *****************************************************/
			if ($this->SiteShowPaymentLogo == 'true') {
				// These could be genericized and moved to db
				$this->ImageCardsAccepted
					= $this->checkImageLocation('/card_logos/cardlogos.gif');
				$this->ImageCardsAmex
					= $this->checkImageLocation('/card_logos/amex_43x43.gif');
				$this->ImageCardsDiscover
					= $this->checkImageLocation('/card_logos/discover_67x42.gif');
				$this->ImageCardsMasterCard
					= $this->checkImageLocation('/card_logos/mc_65x40.gif');
				$this->ImageCardsVisa
					= $this->checkImageLocation('/card_logos/visa_67x42.gif');
			}

		}

	// End set image environment
	}


/***** Build Theme Image Array *************************************************
 *
 *	This method builds an array of images available in the currently selected
 *	theme's /image directory, if it exists.  This is used for checking every
 *	output image against for a matching file name in the theme image directory.
 *
 ******************************************************************************/
    function buildThemeImageArray() {

		$this->ThemeImagesArray = dir_as_file_array($this->PathThemeImagesAbs);

	}


/***** Check Image Location ****************************************************
 *
 *	This method takes an image name (with or without extension) and checks for
 *	its existence in first the ActiveTheme/images directory, followed the site
 *	/_img directory and/or subdirectories.  Basically, this will really try to
 *	find an appropriate image with a matching name.
 *
 *	If there is a pathname in the passed parameter, then it will be stripped for
 *	checking in the theme specific directory.  When no matching image is found
 *	there it will check the specified path, the specified path relative to
 *	/_img, and /_img with the path once again stripped.  If there is an
 *	extension on the file name, it will be stripped and the first matching
 *	filename + extension from $this->imageTypes will be used.
 *
 *	If the parameter begins with any combination of of up to six letters
 *	followed by a colon and two /s, then it will not be checked for existence,
 *	it will simply return itself unaltered.  (ie: http://imagelocation.xx)
 *
 *	This method will return false if no valid image is found, or the full
 *	url relative to SiteRoot.  Also, it returns a result as soon as possible,
 *	thus terminating any remaining logic.
 *
 ******************************************************************************/
    function checkImageLocation($param) {

		if (strpos(substr($param, 0, 9), '://')) {
			return $param;
		}

		$file = (strpos($param, '/') == true)
			 	? basename($param)
			 	: $param;

		$image = (strpos($file, '.') == true)
				? substr($file, 0, strpos($file, '.'))
				: $file;

		// Check theme directory
		if (isset($this->PathThemeImagesAbs)
				&& is_array($this->ThemeImagesArray)) {
			foreach ($this->imageTypes as $v) {
				if (in_array($image . '.' . $v, $this->ThemeImagesArray)) {
					return ($this->PathThemeImagesRel . $image . '.' . $v);
				}
			}
		}

		// Check specified location relative to site image root
		$dir = (strpos($param, '/') == true)
			 	? dirname($param) . '/'
			 	: null;
		if (strpos($param, '/') === 0) substr($dir, 1);
		if (($dir != null) && isset($this->PathImgAbs)) {
			foreach ($this->imageTypes as $v) {
				if (is_file($this->PathImgAbs . $dir . $image . '.' . $v)) {
					return ($this->PathImgRel . $dir . $image . '.' . $v);
				}
			}
		}

		// Check site image root
		if (isset($this->PathImgAbs)) {
			foreach ($this->imageTypes as $v) {
				if (is_file($this->PathImgAbs . $image . '.' . $v)) {
					return ($this->PathImgRel . $image . '.' . $v);
				}
			}
		}

		if (is_file(SiteRoot . '/' . $image . '.ico')) {
			return ('/' . $image . '.ico');
		}

		// No valid image exists
		return null;

	// End checkImageLocation
	}


/***** DHTML Loader ***********************************************************/

    function loadDhtml() {

        /******************* Theme Loader **************************************
         * This section determines which CSS and Javascript theme
         * files to load.  The first option attempted is the user
         * selected option, tracked via a session cookie.  If this
         * fails, we load the theme selected via the administration
         * area from the configuration database.  If this theme
         * does not exist, we fallback to the default theme as
         * set in the paths constant file and should really never
         * be changed from 'skeleton', nor should the skeleton
         * theme ever be modified!
         * ********************************************************************/
        if (isset($this->ThemeLoad)
                && ($this->ThemeLoad == 'true')
                ) {

            /*********** Load Theme CSS Templates ******************************
             * Page specific files MAY be used by placing them in the
             * CSS theme folder following the naming convention:
             * xx.css where xx is = to the page id number
             * files must have a non-numeric name to load with each page!
             * ****************************************************************/

            if (isset($this->ThemeLoadCss)
                    && ($this->ThemeLoadCss == 'true'))
                $this->loadThemeCss();

            /*********** Load Theme Javascript Templates ***********************
             * Page specific files MAY be used by placing them in the
             * Javascript theme folder following the naming convention:
             * xx.js where xx is = to the page id number
             * files must have a non-numeric name to load with each page!
             * ****************************************************************/
            if (isset($this->ThemeLoadJs)
                    && ($this->ThemeLoadJs == 'true'))
                $this->loadThemeJavascript();

        }

    // End loadDhtml
    }


    /***** CSS Loader **********************************************************
     *
     *  This ingenious little baby works the miracles of context sensitive CSS.
     *  What this means is that the script will automatically load any CSS files
     *  available in the CSS folder of the currently selected them.  In addition
     *  to that, based on their file name, it will automagically set the media
     *  type for that specific file.  For instance, considering the following:
     *
     *      StandardTags.screen.css
     *          This file will be set to display for 'screen' media types
     *      StandardTags.print.css
     *          This style sheet will be used when a user prints the page
     *      StandardTags.aural.css
     *          This style sheet would be applied to an aural web browser
     *          such as the type used by visually challenged users
     *
     *  Not only that, but you can even have page specific style sheets load
     *  only on the applicable page by naming them with the page id as the
     *  first part of the file name, then the media, and top it off with the
     *  css extension like so:
     *
     *      00.screen.css   # Loads only for page with id=00
     *      00.print.css    # Loads only for page with id=00 for printing
     *      69.screen.css   # Loads only for page with id=69
     *
     *  Support for friendly url naming scheme is not supported and there are
     *  no plans to be implemented.
     *
     *  TODO:
     *      Implement the friendly naming scheme, gravy mang!
     *
     **************************************************************************/

    function loadThemeCss () {

        $this->css = array();
        $files = array();

        $files = dir_as_file_array (PathThemesAbs
                                        . $this->ThemeActive
                                        . '/css/');
        if ($files != false) {

            $modules = array();
            foreach ($GLOBALS['jwsfPageModules']->modules as $v) {
                $modules[] = $v[1];
            }
			foreach ($files as $file) {
		        $flag = false;
				list ($semantic, $media, $extension) = explode('.', $file );

				if ($extension == 'css') {
		            if (substr($semantic, 0, 4) == 'site') {
		                // Site-Wide CSS File
		                $flag = true;
		            } elseif (substr($semantic, 0, 5) == 'admin') {
		                // Admin-Wide CSS File
                        if (isset($_SERVER['REQUEST_URI'])) {
                            if (substr($_SERVER['REQUEST_URI'], 0, 6)  == '/admin') {
                               $flag = true;
                            }
                        } elseif (isset($_SERVER['REQUEST_URI'])) {
                            if (substr($_SERVER['REQUEST_URI'], 0, 6)  == '/admin') {
                               $flag = true;
                            }
                       }
                    } elseif (($semantic == $GLOBALS['jwsfPage']->id)
                            && (strpos(RelativeURL, 'admin') === false)
		                    ) {
		                // Page ID CSS File
		                $flag = true;
		            } elseif (isset($GLOBALS['jwsfPageModules']->modules)
		                    && in_array($semantic, $modules)
		                    ) {
		                // Page Module CSS File
		                $flag = true;
		            } elseif (isset($_REQUEST['mod'])
		                    && ($semantic == $_REQUEST['mod'])) {
		                // Admin Module Specific CSS File
		                $flag = true;
		            }

		            // Load file
		            if ($flag == true) {
		        		$this->css[] = PathThemesRel
		                                    . $this->ThemeActive
		                                    . '/css/'
		                                    . $file;
		            }
		        }
			}
    	}

		// This loads late so does its own assignment
        $GLOBALS['smarty']->assign('css', $this->css);

    // End loadThemeCss
    }


    /***** Javascript Loader ***************************************************
     *
     *  This works pretty much the same as the CSS loader, without the media
     *  detection feature since that does not apply to Javascript.  Basically,
     *  any Javascript that does load, should not excute anything at all until
     *  it has verified that all of its features are supported by the client.
     *
     **************************************************************************/

    function loadThemeJavascript () {

        $this->js = array();
        $files = array();

        $files = dir_as_file_array (PathThemesAbs
                                        . $this->ThemeActive
                                        . '/js/');
        if ($files != false)
        	foreach ($files as $file) {
        		list ($semantic, $extension) = explode('.', $file );
    			if (($extension == ExtJS)
                        && ((!is_numeric($semantic))
                        || ($semantic == $GLOBALS['pageId'])))
               		$this->js[] = PathThemesRel
                                    . $this->ThemeActive
                                    . '/js/'
                                    . $file;
            }

		// This loads late so does its own assignment
        $GLOBALS['smarty']->assign('js', $this->js);

    // End loadThemeJavascript
    }

// End class
}


?>
