<?php

/***** JWSF Database Object ****************************************************
 *
 *  This class manages all interaction with the MySQL server.  The object is
 *  intended to be registered in the global array so that only one connection
 *  to the database will be made during script execution.
 *
 *  $Revision: 1.3 $
 *
 ******************************************************************************/

// SQL Tracking table
DEFINE ('TableSQLTracking', 'JWSF_TrackSQL');


class mySqlObject {


    function mySqlObject () {

        $this->connect();

    }


/***** Establish connection to database server ********************************/

    function connect () {

        // Connect to server
        if (!$this->dbConn = mysql_connect(
                                    dbHost,
                                    dbUser,
                                    dbPass)
                                    ) {
            trigger_error('SQL server credentials are invalid!');
            $this->dbError = true;
        // Select database
        } elseif (!mysql_select_db(dbName, $this->dbConn)) {
            trigger_error('Database not found!');
            $this->dbError = true;
        }
     }


/***** Enhanced Select Interface ***********************************************
 *
 *  This is a select wrapper, it builds a statement to pass to the query wrapper
 *
 *  @param  array   field names
 *  @param  string  table
 *  @param  string  clause
 *  @param  string  calling file name
 *  @param  int     calling code line
 *
 ******************************************************************************/


    function select ($fields, $table, $clause, $file, $line) {

        $sql = 'SELECT '
                . implode(',', $fields)
                . ' FROM `'
                . $table
                . '`';
        if ($clause != null) {
            $sql .= ' ' . $clause;
        }
        $sql .= ';';

        $this->query($sql, $file, $line);

    }

/***** Enhanced Insert Interface ***********************************************
 *
 *  $data is an associative area of field names and new values
 *
 ******************************************************************************/


    function insert ($data, $table, $file, $line) {

        $sql = 'INSERT INTO `'
                . $table
                . '` SET ';

        foreach ($data as $k => $v) {
                $sql .= $k . '="' . trim($v) . '",';
        }
        // Trim last comma
        $sql = substr($sql, 0, -1);

        $sql .= ';';

        return $this->query($sql, $file, $line);

    }

/***** Enhanced Update Interface ***********************************************
 *
 *  $data is an associative area of field names and new values
 *
 ******************************************************************************/


    function update ($data, $table, $clause, $file, $line) {

        $sql = 'UPDATE `'
                . $table
                . '` SET ';

        foreach ($data as $k => $v) {
                $sql .= $k . '="' . trim($v) . '",';
        }
        // Trim last comma
        $sql = substr($sql, 0, -1);
        if ($clause != null) {
            $sql .= ' ' . $clause;
        }
        $sql .= ';';

        $this->query($sql, $file, $line);

    }


/***** Increment A Field ******************************************************/


    function increment ($field, $table, $clause, $file, $line) {

        $sql = 'UPDATE `'
                . $table
                . '` SET '
                . $field
                . '='
                . $field
                . '+1';
        if ($clause != null) {
            $sql .= ' ' . $clause;
        }
        $sql .= ';';

        $this->query($sql, $file, $line);

    }


/***** Enhanced Delete Interface **********************************************/

    function delete ($table, $clause, $file, $line) {

        $sql = 'DELETE FROM `'
                . $table
                . '` '
                . $clause
                . ';';

        $this->query($sql, $file, $line);

    }


/***** The Infamous Query *****************************************************/

    function query($query, $page, $line) {

        /*** I think I should modify the data washer ***/
    	$safeQuery = sanitize_incoming($query);

        // Every query is logged to be studied later
        if (isset($GLOBALS['jwsfConfig']->UseTrackSQL)
                && ($GLOBALS['jwsfConfig']->UseTrackSQL == 'true')
                ) {
        	$track = "INSERT INTO `"
        	    . TableSQLTracking
        	    . "` VALUES ('', '"
        	    . $safeQuery
        	    . "', '"
        	    . $_SERVER['REMOTE_ADDR']
        	    . "', NOW());";
        	if (!$result = mysql_query($track, $this->dbConn)) {
        	    trigger_error ("Error!<br />\n"
        	        . mysql_error($this->dbConn)
        	        . "SQL: $track :: Page: $page :: Line: $line<br />\n"
        	        . mysql_errno()
        	        . ": "
        	        . mysql_error())
        	        . "<br />\n";
                return false;
            } else {
                $GLOBALS['numQueries']++;
            }
        }
        if ($result = mysql_query($query, $this->dbConn)) {
            $this->result = $result;
            $GLOBALS['numQueries']++;
        } else {
    	    trigger_error ("Error!<br />\n"
    	        . mysql_error($this->dbConn)
    	        . "SQL: $query :: Page: $page :: Line: $line<br />\n"
    	        . mysql_errno()
    	        . ": "
    	        . mysql_error())
    	        . "<br />\n";
            return false;
        }

        return true;
    }


    function fetch($page, $line) {
        if ($this->result != false) return mysql_fetch_assoc($this->result);
    }


    function numRows() {
        if ($this->result != false) return mysql_num_rows($this->result);
    }


    function rowsAffected() {
        if ($this->result != false) return mysql_affected_rows();
    }


    function lastId() {
        $retVal = ($this->result == true)
            ? mysql_insert_id()
            : false;
        return $retVal;
    }


    function safeSql ($value) {
        return sanitize_incoming ($value);
    }

    
    function close() {
      // closes connection to the database
      return mysql_close($this->dbConn);
    }


    function &isError() {
        if ($this->dbError) {
            return true;
        }
        $error = mysql_error($this->dbConn);
        if (empty($error)) {
            return false;
        } else {
            return true;
        }
    }
// End Class
}
?>
