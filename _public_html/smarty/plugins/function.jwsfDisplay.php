<?php

/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 *
 *  $Revision: 1.1.1.1 $
 *
 */


/**
 * Smarty {jwsfDisplay} function plugin
 *
 * Type:     function
 * Name:     jwsfDisplay
 * Input:
 *           - string   Template name
 * Purpose:  To get little chunks
 *
 * @author Jason White <j a white @ nmu dot edu>
 * @param array (from object)
 * @param Smarty
 * @return string
 *
 * v1.5 parsed out theme template and fallback for itself
 *
 */
function smarty_function_jwsfDisplay ($this, &$smarty) {

    $GLOBALS['smarty']->display($this['resource_name']);

}
?>
