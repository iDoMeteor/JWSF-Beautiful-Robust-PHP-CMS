<?php

/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 *
 *  $Revision: 1.1.1.1 $
 *
 */


/**
 * Smarty {jwsfNestedUListFromObject} function plugin
 *
 * Type:     function<br>
 * Name:     jwsfNestedUListFromObject<br>
 * Input:
 *           - array   This should be a nested object passed to smarty by php
 * Purpose:  We are expecting to recieve an array sorted by Parent ID AND Sort
 *           Order rather than by ID or Level.
 *           Returns a nested <ul><li><a href= title=>label</a></li>[..]</ul>
 *           with a UL level for each level of parents.. unless the href
 *           value is set to null, in which case there will be no <a href..>
 *
 * @author Jason White <j a white @ nmu dot edu>
 * @param array (from object)
 * @param Smarty
 * @return string
 *
 *  TODO:
 *      Change plugin name to jwsfNestedUlListFromArray
 *
 */
function smarty_function_jwsfNestedUListFromObject ($this, &$smarty) {

    $retVal = NULL;
    $result = NULL;
    $navlist = $this['navList'];

    $retVal = "<ul class=\"c_navLevel_00\">";
    while (count($navlist) > 0) {
        $children = array();
        if ($result = list_builder ($navlist, 0, 0)) {
            /* We pass the whole array of pages, plus a single
                array of just the first (should be home) page
                since we are obviously on level 0.  We stop
                passing parameters here because the home
                page should have no parents OR children     */
            $retVal .= $result;
        } else {
            return false;
        }
    }
    $retVal .= "</ul>";
    return $retVal;
}

/* Build nested list */
function list_builder
    (&$orig_array, $cur_key, $this_level) {

    /*
    Required -      Reference to original array
    On Recursion -  Calling array
                    Calling level
                    Reference to sorted child array
                    Calling array's parent id
    (orig_array, calling_array, calling_level, calling_parent, calling_children)
    */
    $retVal = NULL;
    $level = fix_single_digit ($this_level);
    $this_array = $orig_array[$cur_key];
    $listed = array_splice($orig_array, $cur_key, 1);

    /*
    Create <li><a href title></a></li> &
    remove current element from original array
    IF user has appropriate access level
    AND page is active */

    // Construct list item for this menu entry
    $retVal = "<li class=\"c_navItem_$level\">";

    if (
            ($this_array['href'] != null)
            && ($this_array['id'] != $GLOBALS['jwsfPage']->id)
            ) {
        $retVal .= "<a href=\""
                . $this_array['href']
                . "\" title=\""
                . $this_array['titleFull'] . "\">";
    }
    $retVal .= $this_array['titleNav'];

    if (
            ($this_array['href'] != null)
            && ($this_array['id'] != $GLOBALS['jwsfPage']->id)
            ) {
        $retVal .= "</a>";
    }

    /* If this element has children (returns array of child ids or false)
        AND is child of current level (or parent)
        then create a new unordered list and repeat */
    $kiddies = array();
    $kiddies = get_children($this_array['id'], $orig_array);
    if (count($kiddies) > 0) {
        $this_level++;
        if ($this_level < $GLOBALS['jwsfConfig']->NavMaxDepthPrimary) {
            $retVal .= '<ul class="c_navLevel_'
                    . fix_single_digit($this_level)
                    . '">';
        }
        $times = 0;
        foreach ($kiddies as $key) {
            $key = $key - $times;
            $times++;
            /*need to pass next array values */
            $retVal .= list_builder ($orig_array, $key, $this_level);
        }
        if ($this_level < $GLOBALS['jwsfConfig']->NavMaxDepthPrimary) {
        $retVal .= "</ul>";
        }
        $this_level--;
    }
    $retVal .= "</li>";

    if ($this_level < $GLOBALS['jwsfConfig']->NavMaxDepthPrimary) {
        return $retVal;
    }

}


function get_children ($parent_id, $root_array) {

    $child_array = array();
    foreach ($root_array as $group => $properties) {
        $potential_child_pid = $properties['pid'];
        if ($potential_child_pid == $parent_id) {
            $child_array[] = $group;
        }
    }
    return $child_array;
}


?>
