{strip}

    {* /* $Revision: 1.2 $ */ *}

    {* Begin Module DIV *}
    <div class="c_ModuleAdmin">
        <h2>Page Creator</h2>
        {* Output Page Creator *}
        <div id="id_containerFormPageCreate">
            <form action="{$smarty.const.PathCurrentRel|escape}" method="post" enctype="application/x-www-form-urlencoded" id="id_formPageCreate" title="Page Creator">
                <div id="id_containerPageCreateCritical">
                    <fieldset>
                        <legend>Critical Information</legend>
                        <dl>
                            {* Start DD Counter *}
                            {assign var=counter value=0}
                            <dt>
                                <label for="id_newPage_pid">
                                    Parent Page</label>
                                <select name="page_pid" id="id_newPage_pid" title="Select a Parent Page">
                                    {page->pid assign='pid'}
                                    <option value="0"{if $pid == 0} selected="selected"{/if}> --Top Level-- </option>
                                    {if !empty($pagesActive)}
                                        <optgroup label="Active Pages">
                                        {section name=pdata loop=$pagesActive}
                                            <option value="{$pagesActive[pdata].id}"{if $pid == $pagesActive[pdata].id} selected="selected"{/if}>{$pagesActive[pdata].titleNav}</option>
                                        {/section}
                                        </optgroup>
                                    {/if}
                                    {if !empty($pagesInactive)}
                                        <optgroup label="Inactive Pages">
                                        {section name=pdata loop=$pagesInactive}
                                            <option value="{$pagesInactive[pdata].id}"{if $pid == $pagesInactive[pdata].pid} selected="selected"{/if}>{$pagesInactive[pdata].titleNav}</option>
                                        {/section}
                                        </optgroup>
                                    {/if}
                                    {if !empty($pagesOrphan)}
                                        <optgroup label="Orphan Pages">
                                        {section name=pdata loop=$pagesOrphan}
                                            <option value="{$pagesOrphan[pdata].id}"{if $pid == $pagesOrphan[pdata].pid} selected="selected"{/if}>{$pagesOrphan[pdata].titleNav}</option>
                                        {/section}
                                        </optgroup>
                                    {/if}
                                </select>
                            </dt>
                            <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                            {assign var="counter" value="`$counter+1`"}
                                This setting defines the hierarchical relationship of the primary navigational menu.  Children are listed as sub-menus of their parents.  A 'Top Level' item will not be sub-menued.  If you select any other option, the page you are creating or editing will be listed as a sub-menu of that page.
                            </dd>
                            <dt>
                                <label for="id_newPage_location">Location</label>
                                <select name="page_location" id="id_newPage_location" title="Menu Location">
                                    <option value="0">Hidden</option>
                                    <option value="1" selected="selected">Primary</option>
                                    <option value="2">Secondary</option>
                                    <option value="3">Primary &amp; Secondary</option>
                                    <option value="4">Tertiary</option>
                                    <option value="5">Primary &amp; Tertiary</option>
                                    <option value="6">Secondary &amp; Tertiary</option>
                                    <option value="7">All Three</option>
                                </select>
                            </dt>
                            <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                            {assign var="counter" value="`$counter+1`"}
                                This defines where the page will appear on the page. Hidden would be good for receipts or secrets, primary will place it the most prominent menu location, secondary will put it in into what is typically the 'footer' area, and both will place the item in both of the navigational menus.
                            </dd>
                            <dt>
                                <label for="id_newPage_titleNav">Navigation Title</label>
                                <input name="page_titleNav" id="id_newPage_titleNav" value="{config->PageDefaultTitleNav}" />
                            </dt>
                            <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                            {assign var="counter" value="`$counter+1`"}
                                This is the label the page will have in the nagivational menus.
                            </dd>
                            <dt>
                                <label for="id_newPage_titleFull">Full Title</label>
                                <input name="page_titleFull" id="id_newPage_titleFull" value="{config->PageDefaultTitle}" />
                            </dt>
                            <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                            {assign var="counter" value="`$counter+1`"}
                                This title will be displayed in the title bar of the browser, to the left of the site title.  If 'PageShowTItle' is set to true in the Switches area, then the page title will be displayed as an H1 element in the top of the content area, which is generally a good idea (semantically speakiing).
                            </dd>
                            <dt>
                                <label for="id_newPage_semanticURI">Friendly URL</label>
                                <input name="page_semanticURI" id="id_newPage_semanticURI" />
                            </dt>
                            <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                            {assign var="counter" value="`$counter+1`"}
                                This will be displayed after the last / in 'http://yoursite.com/'.  Do not use any funny characters except for dashes or underscores.  If it is not set, the url will show 'http://yoursite.com/?id=x'.  If it begins with 'anything://', then it will expect a full, valid external url and will link to it appropriately (that part doesn't work yet!).
                            </dd>
                            <dt>
                                <label for="id_imgURI">Page Image</label>
                                <input type="text" name="page_imgURI" id="id_imgURI" value="{page->imgURI}" />
                            </dt>
                            <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                            {assign var="counter" value="`$counter+1`"}
                                This is currently quite experimental.  You may attempt to enter the location of a image to be loaded with this page and see what happens. :)
                            </dd>
                            <dt>
                                <label for="id_newPage_description">Search Engine Description</label>
                                <input name="page_description" id="id_newPage_description">{config->PageDefaultDescription}</input>
                            </dt>
                            <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                            {assign var="counter" value="`$counter+1`"}
                                This will be the page description used by search engines.  Be concise, you have 255 characters.
                            </dd>
                        </dl>
                    </fieldset>
                {* End Critical Div *}
                </div>
                <div id="id_containerPageCreateOptions">
                    <fieldset>
                        <legend>Optional/Advanced Data</legend>
                        <dl>
                            <dt>
                                <label for="id_newPage_acsLow">Access Level Low</label>
                                <select name="page_acsLow" id="id_newPage_acsLow" title="Access Level Low">
                                    <option value="0"> Public </option>
                                    {section name=so loop=$so_array}
                                        <option value="{$so_array[so]}">{$so_array[so]}</option>
                                    {/section}
                                </select>
                            </dt>
                            <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                            {assign var="counter" value="`$counter+1`"}
                                A user must be at least this level to access the page.  Public is 0, anything else will generally require a user to be logged in.
                            </dd>
                            <dt>
                                <label for="id_newPage_acsHigh">Access Level High</label>
                                <select name="page_acsHigh" id="id_newPage_acsHigh" title="Access Level High">
                                    <option value="0"> Public Only </option>
                                    {section name=so loop=$so_array}
                                    {* Default is all access, 0-255 *}
                                        <option value="{$so_array[so]}" {if ($so_array[so] == 255)} selected="selected"{/if}>{$so_array[so]}</option>
                                    {/section}
                                </select>
                            </dt>
                            <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                            {assign var="counter" value="`$counter+1`"}
                                A user higher than this level will not see this page.  255 is the maximum value.
                            </dd>
                            <dt>
                                <label for="id_newPage_active">Active</label>
                                <select name="page_active" id="id_newPage_active" title="Active">
                                    <option value="0">False</option>
                                    <option value="1">True</option>
                                </select>
                            </dt>
                            <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                            {assign var="counter" value="`$counter+1`"}
                                Whether or not this page is active determines whether or not it will be accessible by anyone or no one.
                            </dd>
                            <dt>
                                <label for="id_newPage_allowComments">Allow Comments</label>
                                <select name="page_allowComments" id="id_newPage_allowComments" title="Allow Comments">
                                    <option value="1">True</option>
                                    <option value="0">False</option>
                                </select>
                            </dt>
                            <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                            {assign var="counter" value="`$counter+1`"}
                                Whether or not to allow users to comment on pages.  This is not yet implemented, but will basically function as a 'default module'.
                            </dd>
                            <dt>
                                <label for="id_newPage_allowRatings">Allow Ratings</label>
                                <select name="page_allowRatings" id="id_newPage_allowRatings" title="Allow Ratings">
                                    <option value="1">True</option>
                                    <option value="0">False</option>
                                </select>
                            </dt>
                            <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                            {assign var="counter" value="`$counter+1`"}
                                Whether or not to allow users to rate pages.  This is not yet implemented, but will basically function as a 'default module'.
                            </dd>
                            <dt>
                                <label for="id_newPage_contentLimit">Content Limit</label>
                                <select name="page_contentLimit" id="id_newPage_contentLimit" title="Content Limit">
                                  <option value="0"> Unlimited </option>
                                    {* This currently uses the SO array since it is currently has the same limitations *}
                                    {section name=so loop=$so_array}
                                        <option value="{$so_array[so]}">{$so_array[so]}</option>
                                    {/section}
                                </select>
                            </dt>
                            <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                            {assign var="counter" value="`$counter+1`"}
                                This determines the number of content blocks that will show on a page.  An example of a page you may want to limit would be a 'Recent News' type of page where you would allow the content to be sorted reverse chronologically and limit it to say the five most recently entered 'articles' (content blocks).
                            </dd>
                            <dt>
                                <label for="id_newPage_cTemplate">Content Template</label>
                                <select name="page_cTemplate" id="id_newPage_cTemplate" title="Select a Content Template">
                                    {foreach from=$content_templates item=template}
                                        <option value="{$template}">{$template}</option>
                                    {/foreach}
                                </select>
                            </dt>
                            <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                            {assign var="counter" value="`$counter+1`"}
                                If there are multiple content templates, then you may choose from them here.
                            </dd>
                            <dt>
                                <label for="id_newPage_headerCollapse">Collapse Headers</label>
                                <select name="page_headerCollapse" id="id_newPage_headerCollapse" title="Collapse Headers">
                                    <option value="0">False</option>
                                    <option value="1">True</option>
                                </select>
                            </dt>
                            <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                            {assign var="counter" value="`$counter+1`"}
                                If this is set to true, and the theme supports it, all of the headers on the page will be collapsed when a user first loads it and they will have to click the expansion link to see the content.  This should be settable per block/module as well!
                            </dd>
                            <dt>
                                <label for="id_newPage_sitemapFrequency">Sitemap Frequency</label>
                                <select name="page_sitemapFrequency" id="id_newPage_sitemapFrequency" title="Select a Sitemap Frequency">
                                    {config->PageDefaultFrequency assign='dF'}
                                    {foreach from=$frequency_array item=k name=foo}
                                        <option value="{$smarty.foreach.foo.iteration}"{if $dF eq $smarty.foreach.foo.iteration} selected="selected"{/if}>{$k}</option>
                                    {/foreach}
                                </select>
                            </dt>
                            <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                            {assign var="counter" value="`$counter+1`"}
                                This defines how often you expect the content on this page to change.  Be honest Google hates liars!
                            </dd>
                            <dt>
                                <label for="id_newPage_sitemapPriority">Sitemap Priority</label>
                                <select name="page_sitemapPriority" id="id_newPage_sitemapPriority" title="Sitemap Priority">
                                    {* This currently uses the SO array since it is currently has the same limitations *}
                                    {config->PageDefaultPriority assign='dP'}
                                    {section name=p loop=$priority_array}
                                        <option value="{$priority_array[p]}"{if $priority_array[p] == $dP} selected=" selected"{/if}>{$priority_array[p]}</option>
                                    {/section}
                                </select>
                            </dt>
                            <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                            {assign var="counter" value="`$counter+1`"}
                                This defines the relative importance of this page in relation to other pages on your site.  Default is 5 in a range from 0 to 10.  You are only competing against yourself here, so do so wisely.
                            </dd>
                            <dt>
                                <label for="id_newPage_so">Sort Order</label>
                                <select name="page_so" id="id_newPage_so" title="Sort Order">
                                    <option value="0"> Alphabetical </option>
                                    {section name=so loop=$so_array}
                                        <option value="{$so_array[so]}">{$so_array[so]}</option>
                                    {/section}
                                </select>
                            </dt>
                            <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                            {assign var="counter" value="`$counter+1`"}
                                This is the sort order in the primary navigational menu (secondary is alphabetical).  Any pages with competing sort orders will be displayed alphabetically.  The default is settable in the Numerics area.  I like to use 10 as the default, and go up from there so that if I decide to bump a couple pages upward, I don't have to go through and change every page!
                            </dd>
                        </dl>
                    </fieldset>
                {* End Options Div *}
                </div>
                {* Submit Div *}
                <div id="id_containerPageCreateSubmit">
                    <button name="pageCreate" value="true" type="submit">Save!</button>
                </div>
            </form>
        {* End Form Div *}
        </div>
    {* End Content Div *}
    </div>
{/strip}
