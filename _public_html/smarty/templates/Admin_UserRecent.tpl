{strip}

    {* /* $Revision: 1.1.1.1 $ */ *}

    <div class="c_ModuleAdmin">
        <h2>Recent User List</h2>
        <p>
                Click a user's name to edit that user, click a user's email address
                to open a new email to that user, or right click and hit 'Copy Email
                Address' and paste it into wherever you like.
        </p>
        <table class="c_userList">
            <caption>
                Last 25 Users
            </caption>
            <tr>
                <th>
                    Name
                </th>
                <th>
                    Email
                </th>
            </tr>
            <tr>
                <th>
                    IP Address
                </th>
                <th>
                    Last Seen
                </th>
            </tr>
            {* Repeat for each user in this level range *}
            {foreach from=$users item=user}
                <tr class="{cycle values="c_userEven, c_userOdd"}">
                    <td class="c_userColumnLeft">
                        <a href="/admin/?mod=UserEditor&amp;UserId={$user.id}" title="Click to edit {$user.nameFirst} {$user.nameLast}">{$user.nameLast}, {$user.nameFirst}</a>
                    </td>
                    <td class="c_userColumnRight">
                        <a href="mailto:{$user.email}" title="Click to email this user">{$user.email}</a>
                    </td>
                </tr>
                <tr class="{cycle values="c_userEven, c_userOdd"}">
                    <td class="c_userColumnLeft">
                        {$user.stampModified}
                    </td>
                    <td class="c_userColumnRight">
                        {$user.ip}
                    </td>
                </tr>
            {/foreach}
        </table>
    </div>
{/strip}
