{strip}

    {* Page Level Errors *}
        {if !empty($messagesError)}
            <div id="id_containerMessagesError">
                <img src="/_img/icons/messagesError.png" alt="Error!" title="Your request has failed!" />
                <h1>
                    The following errors have occurred with your request:
                </h1>
                <ul>
                    {section loop=$messagesError name=list}
                        <li>{$messagesError[list]}</li>
                    {/section}
                </ul>
            </div>
        {/if}

    {* Page Level Warnings/Notices *}
        {if !empty($messagesNotice)}
            <div id="id_containerMessagesNotice">
                <img src="/_img/icons/messagesNotice.png" alt="Notice" title="Your request has raised some concerns" />
                <h1>
                    Please take note:
                </h1>
                <ul>
                    {section loop=$messagesNotice name=list}
                        <li>{$messagesNotice[list]}</li>
                    {/section}
                </ul>
            </div>
        {/if}

    {* Page Level Success *}
        {if !empty($messagesSuccess)}
            <div id="id_containerMessagesSuccess">
                <img src="/_img/icons/messagesSuccess.png" alt="Success!" title="Your request was successful!" />
                <h1>
                    Your request was successful!
                </h1>
                <ul>
                    {section loop=$messagesSuccess name=list}
                        <li>{$messagesSuccess[list]}</li>
                    {/section}
                </ul>
            </div>
        {/if}

{/strip}
