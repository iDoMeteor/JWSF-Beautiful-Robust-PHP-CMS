{strip}

    {* /* $Revision: 1.1.1.1 $ */ *}

    {* Display page level errors *}
    <div class="c_ModuleAdmin">
        {if $template == 'userDataForm'}
            {foreach from=$userFormTags item=tag}
                {$tag}
            {/foreach}
            <h6>* Denotes required information</h6>
            <p>
                Please note, users created from the administration area
                will be activated automatically, and will thusly only receive
                the welcome email and *not* the activation email!
            </p>
        {/if}
    </div>
{/strip}
