{strip}

    {* /* $Revision: 1.5 $ */ *}

    <div class="c_Module" id="jwsf_SupyQDB">

        {* Menu *}
        <div id="jwsf_SupyQDB_Menu">
            <ul>
                <li>
                    <a href="/{page->semanticURI}?action=recent">
                        Fresh
                    </a>
                    &nbsp;|&nbsp;
                </li>
                <li>
                    <a href="/{page->semanticURI}?action=browse">
                        Shoplift
                    </a>
                    &nbsp;|&nbsp;
                </li>
                <li>
                    <a href="/{page->semanticURI}?action=random">
                        Shuffle
                    </a>
                    &nbsp;|&nbsp;
                </li>
                <li>
                    <a href="/{page->semanticURI}?action=top_rated">
                        Schweet
                    </a>
                    &nbsp;|&nbsp;
                </li>
                <li>
                    <a href="/{page->semanticURI}?action=worst_rated">
                        Suck
                    </a>
                    &nbsp;|&nbsp;
                </li>
                <li>
                    <a href="/{page->semanticURI}?action=stats">
                        Props
                    </a>
                    &nbsp;|&nbsp;
                </li>
                <li>
                    <a href="/{page->semanticURI}?action=search">
                        Hunt
                    </a>
                    &nbsp;|&nbsp;
                </li>
                <li>
                    <a href="/{page->semanticURI}?action=all">
                        *
                    </a>
                    &nbsp;|&nbsp;
                </li>
                <li>
                    #
                    <form method="post" action="{$smarty.const.RootURL}/{page->semanticURI}?action=single">
                        <input name="SupyQDB_Num" size="3" />
                    </form>
                    &nbsp;|&nbsp;
                </li>
                <li>
                    <a href="/{page->semanticURI}?action=about">
                        ?
                    </a>
                </li>
            </ul>
        </div>
        <h5>
            Total Quotes: {$SupyTotal} | Highest #: {$SupyHigh}
        </h5>

        {if $smarty.get.action eq 'browse'}
            <ul>
                {section loop=$SupyRangeArray name=range}
                    <li>
                        <a href="/{page->semanticURI}?action=browse&amp;start={$SupyRangeArray[range].start}">
                            {$SupyRangeArray[range].low} - {$SupyRangeArray[range].high}
                        </a>
                        &nbsp;|&nbsp;
                    </li>
                {/section}
                {section loop=$SupyRangeLast name=rangeLast}
                    <li>
                        <a href="/{page->semanticURI}?action=browse&amp;start={$SupyRangeLast[rangeLast].start}">
                            {$SupyRangeLast[rangeLast].low} - {$SupyRangeLast[rangeLast].high}
                       </a>
                    </li>
                {/section}
            </ul>
        {/if}

        {if $smarty.get.action eq 'about'}
            <h2>
                Supybot Quote Database (QDB)<br />
                PHP Plugin for the JWSF CMS
            </h2>
            <p>
                This script has been written by the owner and maintainer of JWSF Content Management System for #blinkenshell.  Quotes can be added by any user that has registered and identified with blinkenbot.
            </p>
        {/if}

        {if !empty($SupyQuotes)}
            {section loop=$SupyQuotes name=quote}

                <div class="jwsf_SupyQDB_Quote" id="jwsf_SupyQDB_{$smarty.section.quote.index}">

                    <div class="jwsf_SupyQDB_QuoteText">
                        <h3>
                            Quote #{$SupyQuotes[quote].number}:
                        </h3>
                        <code>
                            {$SupyQuotes[quote].quote}
                        </code>
                        <h4>
                                (added by {$SupyQuotes[quote].userNick} at {$SupyQuotes[quote].stamp|date_format:"%R"} on {$SupyQuotes[quote].stamp|date_format:"%D"})
                        </h4>
                    </div>
                    <div class="jwsf_SupyQDB_Ratings">
                        <h4>This Makes You:</h4>
                        <a href="/{page->semanticURI}?x&#61;{$SupyQuotes[quote].number}&amp;y&#61;{$SupyQuotes[quote].userId}&amp;z&#61;10">
                            <img src="/_img/icons/happy_panda.png" alt="Schweet" title="" />
                        </a>
                        <a href="/{page->semanticURI}?x&#61;{$SupyQuotes[quote].number}&amp;y&#61;{$SupyQuotes[quote].userId}&amp;z&#61;0">
                            <img src="/_img/icons/sad_panda.png" alt="Suck" title="" />
                        </a>
                    </div>

                </div>

            {/section}
        {/if}

    </div>
{/strip}
