{strip}

    {* /* $Revision: 1.1.1.1 $ */ *}

    {* Display page level errors *}
    {if $template != 'none'}
        <div class="c_ModuleAdmin">
            {if $success == true}
                <h2>The information has been updated</h2>
            {/if}
            {if $template == 'userDataForm'}
                {foreach from=$userFormTags item=tag}
                    {$tag}
                {/foreach}
                <h6>* Denotes required information</h6>
        {/if}
        <div id="id_userSearch">
            <h2>User Search</h2>
            {foreach from=$searchFormTags item=tag}
                {$tag}
            {/foreach}
            <h6>
                Use * as a wildcard for non-complete names: ie; lastna* will
                return lastname, lastnames, lasttime, etc..
            </h6>
        </div>
        {if $template == 'admins'}
            <div id="id_adminList">
                <h2>Administrator List</h2>
                <p>
                        Click a user's name to edit that user, click a user's email address
                        to open a new email to that user, or right click and hit 'Copy Email
                        Address' and paste it into wherever you like.
                </p>
                {* Repeat table for each access level *}
                {foreach from=$admins key=level item=levelUsers}
                    <table class="c_userList">
                        <caption>
                            Users with Access Level :: {$level}
                        </caption>
                        <tr>
                            <th>
                                Name
                            </th>
                            <th>
                                Email
                            </th>
                        </tr>
                        <tr>
                            <th>
                                Company
                            </th>
                            <th>
                                Telephone
                            </th>
                        </tr>
                        {* Repeat for each user in this level range *}
                        {foreach from=$levelUsers item=user}
                            <tr class="{cycle values="c_userEven, c_userOdd"}">
                                <td class="c_userColumnLeft">
                                    <a href="/admin/?mod=UserEditor&amp;UserId={$user.id}" title="Click to edit {$user.nameFirst} {$user.nameLast}">{$user.nameLast}, {$user.nameFirst}</a>
                                </td>
                                <td class="c_userColumnRight">
                                    <a href="mailto:{$user.email}" title="Click to email this user">{$user.email}</a>
                                </td>
                            </tr>
                            <tr class="{cycle values="c_userEven, c_userOdd"}">
                                <td class="c_userColumnLeft">
                                    {$user.nameCompany}
                                </td>
                                <td class="c_userColumnRight">
                                    {$user.telephone}
                                </td>
                            </tr>
                        {/foreach}
                </table>
            {* End table loop *}
            {/foreach}
            </div>
        {/if}
        {if $template == 'searchResults'}
            <div id="id_adminList">
                <h2>Your Search Results</h2>
                <p>
                        Click a user's name to edit that user, click a user's email address
                        to open a new email to that user, or right click and hit 'Copy Email
                        Address' and paste it into wherever you like.
                </p>
                {* Repeat table for each access level *}
                {foreach from=$results key=level item=levelUsers}
                    <table class="c_userList">
                        <caption>
                            Users with Access Level :: {$level}
                        </caption>
                        <tr>
                            <th>
                                Name
                            </th>
                            <th>
                                Email
                            </th>
                        </tr>
                        <tr>
                            <th>
                                Company
                            </th>
                            <th>
                                Telephone
                            </th>
                        </tr>
                        {* Repeat for each user in this level range *}
                        {foreach from=$levelUsers item=user}
                            <tr class="{cycle values="c_userEven, c_userOdd"}">
                                <td class="c_userColumnLeft">
                                    <a href="/admin/?mod=UserEditor&amp;UserId={$user.id}" title="Click to edit {$user.nameFirst} {$user.nameLast}">{$user.nameLast}, {$user.nameFirst}</a>
                                </td>
                                <td class="c_userColumnRight">
                                    <a href="mailto:{$user.email}" title="Click to email this user">{$user.email}</a>
                                </td>
                            </tr>
                            <tr class="{cycle values="c_userEven, c_userOdd"}">
                                <td class="c_userColumnLeft">
                                    {$user.nameCompany}
                                </td>
                                <td class="c_userColumnRight">
                                    {$user.telephone}
                                </td>
                            </tr>
                        {/foreach}
                </table>
            {* End table loop *}
            {/foreach}
            </div>
        {/if}
        </div>
    {/if}
{/strip}
