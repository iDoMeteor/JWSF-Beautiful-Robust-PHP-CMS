{strip}

    {* /* $Revision: 1.1.1.1 $ */ *}

        <div class="c_ModuleAdmin" id="id_containerFileDelete">
            <h2>Delete a File</h2>
            <div id="id_containerFormFileConfirmDelete">
                <form method="post" action="{$smarty.const.PathCurrentRel|escape}">
                    <input type="hidden" name="filePath" value="{$smarty.post.orphan_deleteConfirmation}" />
                    <fieldset>
                        <legend>Confirm Removal from File System</legend>
                        <input type="submit" name="file_deleteOrphan" id="id_deleteResource" value="Terminate" />
                    </fieldset>
                </form>
            </div>
        </div>
{/strip}