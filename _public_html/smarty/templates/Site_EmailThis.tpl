{strip}
    <head>
        <title>{config->SiteTitle} :: {page->titleFull}</title>
        <meta http-equiv="Author" content="Jason E. White - Marquette, MI" />
        <meta http-equiv="Content-Language" content="EN" />
        <meta http-equiv="Content-Type" content="{config->mimeType}" />
        <meta http-equiv="Copyright" content="The author retains all copyrights to all code, script or graphics which are directly related to the JWSF Content Management System.  {config->SiteTitle} retains all copyrights to any intellectual property, logos, or graphics created exclusively for {config->SiteTitle}. Copyright 2009." />
        <meta http-equiv="Description" content="{page->description}" />
        <meta http-equiv="Distribution" content="Global" />
        <meta http-equiv="Expires" content="{page->expire}" />
        <meta http-equiv="Keywords" content="{config->PageDefaultKeywords}" />
        <meta http-equiv="Last-Modified" content="{page->stamp}" />
        <meta http-equiv="Revisit-After" content="7 Days" />
        <meta http-equiv="Robots" content="index,follow" />
		{config->favIcon assign=favicon}
        {if $favicon != null}
            <link rel="icon" href="{$favicon}" />
            <link rel="shortcut icon" href="{$favicon}" />
        {/if}
        {foreach from=$css item=file}
            <link rel="stylesheet" href="{$file}" type="text/css" media="screen" />
        {/foreach}
        {if (count($js) > 0)}
            <meta http-equiv="Content-Script-Type" content="text/javascript" />
        {/if}
        {foreach from=$js item=file}
            <script src="{$file}" type="text/javascript"></script>
        {/foreach}
    </head>
{/strip}
