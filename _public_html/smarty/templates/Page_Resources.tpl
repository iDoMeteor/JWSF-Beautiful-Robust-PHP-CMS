{strip}

                {* Page specific resources *}
                {page->resources assign='resources'}
                {page->hasResources assign='pageHasResources'}
                {if $pageHasResources == 'true'}
                    <div id="id_pageResources">
                        <h4>Page Related Resources</h4>
                        <ul>
                            {foreach from=$resources item=resource}
                                {if $resource.cid == 0}
                                    <li class="c_{$resource.type}-small">
                                        <a href="{$resource.location}" title="{$resource.description}">
                                            {$resource.title}
                                        </a>
                                    </li>
                                {/if}
                            {/foreach}
                        </ul>
                    </div>
                {/if}

{/strip}
