{strip}

    {* /* $Revision: 1.1.1.1 $ */ *}

                {* Actual Content *}
                    {section loop=$data name=info}
                        <div class="c_contentBlock" id="id_containerContentBlock_{$smarty.section.info.index|string_format:"%.2d"}">

                            {*
                                Anything you wish to style in this section
                                should be done by addressing either the content
                                block class then tag, or the content id then
                                tag.  Example:
                                    .c_contentBlock h2 {color: white}
                                    #id_containerContent_00 h2 {color: white}
                            *}
                            <h2 class="c_contentHeader" id="id_containerHeader_{$smarty.section.info.index|string_format:"%.2d"}">
                                {if $data[info].headerURI != null}
                                    <a href="{$data[info].headerURI}">
                                {/if}
                                {$data[info].header|strip}
                                {if $data[info].headerURI != null}
                                    </a>
                                {/if}
                            </h2>

                            <div class="c_contentBody" id="id_containerContentBody_{$smarty.section.info.index|string_format:"%.2d"}">
                                {if $data[info].imgURI != null}
                                    <span class="c_contentImage" id="id_containerContentImage_{$smarty.section.info.index|string_format:"%.2d"}">
					                    <img src="{$data[info].imgURI}" alt="{$data[info].header|strip}" title="{$data[info].header|strip}" />
                                    </span>
			                    {/if}
                                {$data[info].content|strip}
                                {* Block specific resources *}
                                {page->resources assign='resources'}
                                {if $data[info].hasResources eq 'true'}
                                    <div class="c_contentResources">
                                        <h4>Associated Resources</h4>
                                        <ul>
                                            {foreach from=$resources item=resource}
                                                {if $resource.cid == $data[info].cid}
                                                    <li class="c_{$resource.type}">
                                                        <a href="{$resource.location}" title="{$resource.description}">
                                                            {$resource.title}
                                                        </a>
                                                    </li>
                                                {/if}
                                            {/foreach}
                                        </ul>
                                    </div>
                                {/if}
                            </div>

                        </div>
                    {/section}
                {* End Actual Content *}

                {* Show Template Number *}
                    {config->SiteShowTemplateNo assign=showTemp}
                    {if $showTemp == 'true'}
                        <h5>Content Template 00 (default)</h5>
                    {/if}

{/strip}
