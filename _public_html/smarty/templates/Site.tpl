{* Check for RSS *}
{if $smarty.get.url != 'rss'}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

<!--
    Jason White's Slim Frame - A W3C Compliant Content Management System
    This site driven by: Apache 2, PHP 5, MySQL 5, XHTML 1.1,
                         EMCAScript-262 3rd edition, CSS 2.1
// -->

    {strip}

    {* /* $Revision: 1.2 $ */ *}


        {jwsfDisplay resource_name='Site_Meta.tpl'}

            {jwsfDisplay resource_name='Site_Body.tpl'}

                {* Outermost Container 1 - Presentation Frame *}
                <div id="id_container1">

            		{* Major Section 1 - Header Containment *}
                    <div id="id_container1-Row1">

                        {jwsfDisplay resource_name='Nav_SkipTo.tpl'}

                        {jwsfDisplay resource_name='User_Greeting.tpl'}

                        {jwsfDisplay resource_name='User_Authentication.tpl'}

                        {jwsfDisplay resource_name='Site_Logo.tpl'}

                        {jwsfDisplay resource_name='Page_Image.tpl'}

                    </div>
            		{* End Major Section 1 - Header *}

                    {* Major Section 2 - Quickies (secondary header) *}
                    <div id="id_container1-Row2">

                        {jwsfDisplay resource_name='Nav_Tertiary.tpl'}

                        {jwsfDisplay resource_name='Nav_Parents.tpl'}

                    </div>
            		{* End Major Section 2 *}


                    {* Major Section 3 - Column Containment *}
                    <div id="id_container1-Row3">

            		    {* Column 1 - System Messages and Content *}
                        <div id="id_container1-Row3-Col1">


        		            {jwsfDisplay resource_name='Site_Messages.tpl'}

                            {page->adminArea assign=adminArea}
                            {if $adminArea == true}

            		            {jwsfDisplay resource_name='Admin_Menu.tpl'}

                                {page->adminModule assign=adminModule}
                                {if $adminModule neq null }
                                    {jwsfDisplay resource_name=$adminModule}
                                {/if}

                            {else}

            		            {jwsfDisplay resource_name='Page_Title.tpl'}

                                {config->SiteEngageSocial assign=engageSocial}
                                {if $engageSocial eq 'true'}
                                    {jwsfDisplay resource_name='Site_SocialBar.tpl'}
                                {/if}

                                {if $smarty.get.email_this eq true}
                                    {jwsfDisplay resource_name='Site_EmailThis.tpl'}
                                {/if}

            		            {jwsfDisplay resource_name='Page_Resources.tpl'}

            		            {jwsfDisplay resource_name='Nav_Related.tpl'}

                                {* Loop pre-content modules  *}
                                {if !empty($modulesPre)}
                                    {foreach from=$modulesPre item=module}
                                        {jwsfDisplay resource_name=$module}
                                    {/foreach}
                                {/if}

                				{* Display page content when not in admin area *}
            					{if (strpos($smarty.server.PHP_SELF, '/admin') === false)}
            						{page->cTemplate assign=cTemplate}
            				        {jwsfDisplay resource_name=$cTemplate}
            					{/if}

                                {* Loop post-content modules  *}
                                {if !empty($modulesPost)}
                                    {foreach from=$modulesPost item=module}
                                        {jwsfDisplay resource_name=$module}
                                    {/foreach}
                                {/if}

                                {* Page ratings *}
                                {page->allowRatings assign=allowRatings}
                                {if $allowRatings == 1}
                                    {jwsfDisplay resource_name='Page_Ratings.tpl'}
                                {/if}

                                {* Page comments *}
                                {page->allowComments assign=allowComments}
                                {if $allowComments == 1}
                                    {jwsfDisplay resource_name='Page_Comments.tpl'}
                                {/if}

                            {/if}

                        </div>
                        {* End Column 1 *}

            		    {* Column 2 - Primary Navigation *}
                        <div id="id_container1-Row3-Col2">

                            {jwsfDisplay resource_name='Nav_Primary.tpl'}

                        </div>
                        {* End Column 2 *}

            		    {* Column 3 *}
                        {if $adminArea == false}

                            <div id="id_container1-Row3-Col3">

                                {jwsfDisplay resource_name='Mod_PagesRecentlyModified.tpl'}

                                {jwsfDisplay resource_name='Mod_FollowMe.tpl'}

                                {jwsfDisplay resource_name='Mod_WhosOnline.tpl'}

                            </div>

                        {/if}
                        {* End Column 3 *}

                    </div>
                    {* End Major Section 3 - Columns *}

            		{* Major Section 4 - Footer Nav *}
                    <div id="id_container1-Row4">

                        {jwsfDisplay resource_name='Nav_Secondary.tpl'}

                        {jwsfDisplay resource_name='Site_ThemeSelect.tpl'}

                    </div>
                    {* End Major Section 4 - Footer Nav *}

                </div>
                {* End Container 1 *}

            	{* Outermost Container 2 - Disclaimers *}
                <div id="id_container2">

                    {jwsfDisplay resource_name='Site_PaymentsAccepted.tpl'}

                    {jwsfDisplay resource_name='Site_PerfectCode.tpl'}

                    {jwsfDisplay resource_name='Site_LastModified.tpl'}

                    {jwsfDisplay resource_name='Site_LoadTimer.tpl'}

                </div>
            	{* End Container 2 *}

            </body>
        </html>
    {/strip}
{else}
{* Show RSS *}
{jwsfDisplay resource_name='Site_RSS.tpl'}
{/if}
