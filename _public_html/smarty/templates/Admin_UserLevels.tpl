{strip}

    {* /* $Revision: 1.1.1.1 $ */ *}

    <div class="c_ModuleAdmin">
        <h2>Complete System User List</h2>
        <p>
                Click a user's name to edit that user, click a user's email address
                to open a new email to that user, or right click and hit 'Copy Email
                Address' and paste it into wherever you like.
        </p>
        {* Repeat table for each access level *}
        {foreach from=$users key=level item=levelUsers}
            <table class="c_userList">
                <caption>
                    Users with Access Level :: {$level}
                </caption>
                <tr>
                    <th>
                        Name
                    </th>
                    <th>
                        Email
                    </th>
                </tr>
                <tr>
                    <th>
                        Company
                    </th>
                    <th>
                        Telephone
                    </th>
                </tr>
                {* Repeat for each user in this level range *}
                {foreach from=$levelUsers item=user}
                    <tr class="{cycle values="c_userEven, c_userOdd"}">
                        <td class="c_userColumnLeft">
                            <a href="/admin/?mod=UserEdit&amp;UserId={$user.id}" title="Click to edit {$user.nameFirst} {$user.nameLast}">{$user.nameLast}, {$user.nameFirst}</a>
                        </td>
                        <td class="c_userColumnRight">
                            <a href="mailto:{$user.email}" title="Click to email this user">{$user.email}</a>
                        </td>
                    </tr>
                    <tr class="{cycle values="c_userEven, c_userOdd"}">
                        <td class="c_userColumnLeft">
                            {$user.nameCompany}
                        </td>
                        <td class="c_userColumnRight">
                            {$user.telephone}
                        </td>
                    </tr>
                {/foreach}
            </table>
        {* End table loop *}
        {/foreach}

    </div>
{/strip}
