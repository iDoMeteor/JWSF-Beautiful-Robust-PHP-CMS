{strip}

    {* /* $Revision: 1.1.1.1 $ */ *}

        <div class="c_ModuleAdmin" id="id_containerResourceDeletion">
            <h2>Delete File Assignment</h2>
            <div id="id_containerFormFilesEdit">
                <form method="post" action="{$smarty.const.PathCurrentRel|escape}">
                    <input type="hidden" name="assID" value="{$smarty.post.assignment_deleteConfirmation}" />
                    <fieldset>
                        <legend>Confirm Removal of Assignment</legend>
                        <input type="submit" name="assignment_delete" id="id_deleteResource" value="Eradicate" />
                    </fieldset>
                </form>
            </div>
        </div>
{/strip}