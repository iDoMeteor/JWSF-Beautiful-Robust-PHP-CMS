{strip}

    {* Last Modified / Site Copy Right *}
        <h6>
            Page Last Modified: {page->stamp|date_format:"%A, %B %e, %Y @ %I:%M %p"} | Views: {page->views}
        </h6>
        <h6>
            &copy; {$smarty.now|date_format:"%Y"} {config->SiteTitle}
        </h6>
        <h6>A <a href="http://MarquetteWebSites.com">Marquette Web Site</a> and <a href="http://JasonEdwardWhite.com">J.E.W. Production</a>.</h6>

{/strip}
