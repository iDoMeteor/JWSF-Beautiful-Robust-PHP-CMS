{strip}

    {* Navigation *}
		{config->NavUsePrimary assign=NavUsePrimary}
		{if $NavUsePrimary == 'true'}
            <div id="id_containerNavigationPrimary">
                {jwsfNestedUListFromObject navList=$navPrimary}
            </div>

        {/if}

{/strip}
