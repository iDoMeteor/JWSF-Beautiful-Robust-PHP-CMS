{strip}

    {* Page Title Display *}
        {config->PageShowTItle assign=showTitle}
        {if $showTitle == 'true'}
            <div id="id_containerPageTitle">
                <h1>{page->titleFull}</h1>
            </div>
        {/if}

{/strip}
