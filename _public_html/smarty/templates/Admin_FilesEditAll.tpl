{strip}

    {* /* $Revision: 1.1.1.1 $ */ *}

        <div class="c_ModuleAdmin" id="id_containerEditFiles1">
            <h2>Edit Files</h2>
            <div id="id_containerFormFilesEdit">
                <form method="post" enctype="multipart/form-data" action="{$smarty.const.PathCurrentRel|escape}">
                    {page->filesValid assign='filesV'}
                    {if count($filesV) > 0}
                        <fieldset>
                            <legend>Valid Files</legend>
                            <table>
                                <tr class="c_resourceTitle">
                                    <th colspan="5">
                                        Title
                                    </th>
                                </tr>
                                <tr class="c_resourceLocation">
                                    <th colspan="5">
                                        Location
                                    </th>
                                </tr>
                                <tr class="c_resourceDetails">
                                    <th class="c_resourceActive">
                                        Active
                                    </th>
                                    <th class="c_resourceViewable">
                                        Viewable
                                    </th>
                                    <th class="c_resourceDescription">
                                        Description
                                    </th>
                                    <th class="c_resourceEdit">
                                        Edit
                                    </th>
                                    <th class="c_resourceDelete">
                                        Delete
                                    </th>
                                </tr>
                                {section loop=$filesV name=info}
                                    <tr class="c_resourceTitle">
                                        <td colspan="5">
                                            {$filesV[info].title}
                                        </td>
                                    </tr>
                                    <tr class="c_resourceLocation">
                                        <td colspan="5">
                                            <!-- img by type -->
                                            {$filesV[info].location}
                                        </td>
                                    </tr>
                                    <tr class="c_resourceDetails">
                                        <td>
                                            {if $filesV[info].active eq true}
                                                <input alt="Active" title="Click to deactivate" type="image" src="/_img/icons/positive.png" name="deactivate" id="id_resourceActive" value="{$filesV[info].id}" />
                                            {else}
                                                <input alt="Inactive" title="Click to Activate" type="image" src="/_img/icons/negative.png" name="activate" id="id_resourceActive" value="{$filesV[info].id}" />
                                            {/if}
                                        </td>
                                        <td>
                                            {if $filesV[info].viewable eq true}
                                                <input alt="Viewable" title="Click to hide this file from the downloads page" type="image" src="/_img/icons/positive.png" name="hide" id="id_resourceViewable" value="{$filesV[info].id}" />
                                            {else}
                                                <input alt="Unviewable" title="Click to show this file on the downloads page" type="image" src="/_img/icons/negative.png" name="show" id="id_resourceViewable" value="{$filesV[info].id}" />
                                            {/if}
                                        </td>
                                        <td>
                                            {$filesV[info].description}
                                        </td>
                                        <td>
                                            <input alt="Edit" title="Click to edit this file" type="image" src="/_img/icons/edit.png" name="file_editValid" id="id_resourceEdit" value="{$filesV[info].id}" />
                                        </td>
                                        <td>
                                            <input alt="Delete" title="Click to delete this file" type="image" src="/_img/icons/delete.png" name="file_deleteAllConfirmation" id="id_resourceDelete" value="{$filesV[info].id}" />
                                        </td>
                                    </tr>
                                {/section}
                            </table>
                        </fieldset>
                        <br /><br />
                    {/if}
                    {page->filesLost assign='filesL'}
                    {if count($filesL) > 0}
                        <fieldset>
                            <legend>Lost Files</legend>
                            <table>
                                <tr class="c_resourceTitle">
                                    <th colspan="3">
                                        Title
                                    </th>
                                </tr>
                                <tr class="c_resourceLocation">
                                    <th colspan="3">
                                        Location
                                    </th>
                                </tr>
                                <tr class="c_resourceDetails">
                                    <th class="c_resourceDescription">
                                        Description
                                    </th>
                                    <th class="c_resourceEdit">
                                        Upload
                                    </th>
                                    <th class="c_resourceDelete">
                                        Delete
                                    </th>
                                </tr>
                                {section loop=$filesL name=info}
                                    <tr class="c_resourceTitle">
                                        <td colspan="3">
                                            {$filesL[info].title}
                                        </td>
                                    </tr>
                                    <tr class="c_resourceLocation">
                                        <td colspan="3">
                                            <!-- img by type -->
                                            <label for="{$smarty.section.info.index}">{$smarty.section.info.index}:{$filesL[info].location}</label>
                                            <input type="file" name="{$smarty.section.info.index}" id="id_fileSrc" />
                                            <input type="hidden" name="fileReplace[{$smarty.section.info.index}][location]" value="{$filesL[info].location}" />
                                        </td>
                                    </tr>
                                    <tr class="c_resourceDetails">
                                        <td>
                                            {$filesL[info].description}
                                        </td>
                                        <td>
                                            <input alt="Upload" title="Click to upload a new file" type="image" src="/_img/icons/upload.png" name="file_upload" id="id_resourceEdit" value="1" />
                                        </td>
                                        <td>
                                            <input alt="Delete" title="Click to delete this record" type="image" src="/_img/icons/delete.png" name="meta_deleteConfirmation" id="id_resourceDelete" value="{$filesL[info].id}" />
                                        </td>
                                    </tr>
                                {/section}
                            </table>
                        </fieldset>
                        <br /><br />
                    {/if}
                    {page->filesOrphan assign='filesO'}
                    {if count($filesO) > 0}
                        <fieldset>
                            <legend>Orphan Files</legend>
                            <table>
                                <tr class="c_resourceTitle">
                                    <th colspan="3">
                                        Title
                                    </th>
                                </tr>
                                <tr class="c_resourceLocation">
                                    <th colspan="3">
                                        Location
                                    </th>
                                </tr>
                                <tr class="c_resourceDetails">
                                    <th class="c_resourceDescription">
                                        Description
                                    </th>
                                    <th class="c_resourceDelete">
                                        Delete
                                    </th>
                                </tr>
                                {foreach from=$filesO item=file name=row}
                                    <tr class="c_resourceTitle">
                                        <td colspan="3">
                                            <input type="text" name="fileMeta[{$smarty.foreach.row.index}][title]" />
                                        </td>
                                    </tr>
                                    <tr class="c_resourceLocation">
                                        <td colspan="3">
                                            <!-- img by type -->
                                            <input type="hidden" name="fileMeta[{$smarty.foreach.row.index}][location]" value="{$file}" />
                                            {$smarty.foreach.row.index}:{$file}
                                        </td>
                                    </tr>
                                    <tr class="c_resourceDetails">
                                        <td>
                                            <textarea name="fileMeta[{$smarty.foreach.row.index}][description]"></textarea>
                                        </td>
                                        <td>
                                            <input type="image" src="/_img/icons/delete.png" alt="Delete" name="orphan_deleteConfirmation" id="id_resourceDelete" value="{$file}" />
                                        </td>
                                    </tr>
                                {/foreach}
                            </table>
                            <input type="image" src="/_img/icons/repair.png" alt="Repair" name="meta_repair" id="id_resourceEdit" value="{$file}" />
                        </fieldset>
                        <br /><br />
                    {/if}
                    {page->filesInvalidAssignmentsResource assign='filesIRA'}
                    {if count($filesIRA) > 0}
                        <fieldset>
                            <legend>Invalid Resource Assignments</legend>
                            <table>
                                <tr class="c_resourceDetails">
                                    <th class="c_resourceActive">
                                        Page
                                    </th>
                                    <th class="c_resourceDescription">
                                        Content Block
                                    </th>
                                    <th class="c_resourceEdit">
                                        Edit
                                    </th>
                                    <th class="c_resourceDelete">
                                        Delete
                                    </th>
                                </tr>
                                {section loop=$filesIRA name=info}
                                    <tr class="c_resourceDetails">
                                        <td>
                                            {if $filesIRA[info].page eq NULL}
                                                -N/A-
                                            {else}
                                                {$filesIRA[info].page}
                                            {/if}
                                        </td>
                                        <td>
                                            {if $filesIRA[info].content eq NULL}
                                                -N/A-
                                            {else}
                                                {$filesIRA[info].content}
                                            {/if}
                                        </td>
                                        <td>
                                            <input type="image" src="/_img/icons/edit.png" alt="Edit" name="assignment_edit" id="id_resourceEdit" value="{$filesIRA[info].id}" />
                                        </td>
                                        <td>
                                            <input type="image" src="/_img/icons/delete.png" alt="Delete" name="assignment_deleteConfirmation" id="id_resourceDelete" value="{$filesIRA[info].id}" />
                                        </td>
                                    </tr>
                                {/section}
                            </table>
                        </fieldset>
                        <br /><br />
                    {/if}
                    {page->filesInvalidAssignmentsPage assign='filesIPA'}
                    {if count($filesIPA) > 0}
                        <fieldset>
                            <legend>Invalid Page Assignments</legend>
                            <table>
                                <tr class="c_resourceTitle">
                                    <th colspan="4">
                                        Title
                                    </th>
                                </tr>
                                <tr class="c_resourceLocation">
                                    <th colspan="4">
                                        Location
                                    </th>
                                </tr>
                                <tr class="c_resourceDetails">
                                    <th class="c_resourceActive">
                                        Page
                                    </th>
                                    <th class="c_resourceDescription">
                                        Content Block
                                    </th>
                                    <th class="c_resourceEdit">
                                        Edit
                                    </th>
                                    <th class="c_resourceDelete">
                                        Delete
                                    </th>
                                </tr>
                                {section loop=$filesIPA name=info}
                                    <tr class="c_resourceTitle">
                                        <td colspan="4">
                                            {if $filesIPA[info].title eq NULL}
                                                -N/A-
                                            {else}
                                                {$filesIPA[info].title}
                                            {/if}
                                        </td>
                                    </tr>
                                    <tr class="c_resourceLocation">
                                        <td colspan="4">
                                            {if $filesIPA[info].location eq NULL}
                                                -N/A-
                                            {else}
                                                <!-- img by type -->
                                                {$filesIPA[info].location}
                                            {/if}
                                        </td>
                                    </tr>
                                    <tr class="c_resourceDetails">
                                        <td>
                                            {if $filesIPA[info].page eq NULL}
                                                -N/A-
                                            {else}
                                                {$filesIPA[info].page}
                                            {/if}
                                        </td>
                                        <td>
                                            {if $filesIPA[info].content eq NULL}
                                                -N/A-
                                            {else}
                                                {$filesIPA[info].content}
                                            {/if}
                                        </td>
                                        <td>
                                            <input type="image" src="/_img/icons/edit.png" alt="Edit" name="assignment_edit" id="id_resourceEdit" value="{$filesIPA[info].id}" />
                                        </td>
                                        <td>
                                            <input type="image" src="/_img/icons/delete.png" alt="Delete" name="assignment_deleteConfirmation" id="id_resourceDelete" value="{$filesIPA[info].id}" />
                                        </td>
                                    </tr>
                                {/section}
                            </table>
                        </fieldset>
                        <br /><br />
                    {/if}
                    {page->filesInvalidAssignmentsContent assign='filesICA'}
                    {if count($filesICA) > 0}
                        <fieldset>
                            <legend>Invalid Content Assignments</legend>
                            <table>
                                <tr class="c_resourceTitle">
                                    <th colspan="4">
                                        Title
                                    </th>
                                </tr>
                                <tr class="c_resourceLocation">
                                    <th colspan="4">
                                        Location
                                    </th>
                                </tr>
                                <tr class="c_resourceDetails">
                                    <th class="c_resourceActive">
                                        Page
                                    </th>
                                    <th class="c_resourceDescription">
                                        Content Block
                                    </th>
                                    <th class="c_resourceEdit">
                                        Edit
                                    </th>
                                    <th class="c_resourceDelete">
                                        Delete
                                    </th>
                                </tr>
                                {section loop=$filesICA name=info}
                                    <tr class="c_resourceTitle">
                                        <td colspan="4">
                                            {if $filesICA[info].title eq NULL}
                                                -N/A-
                                            {else}
                                                {$filesICA[info].title}
                                            {/if}
                                        </td>
                                    </tr>
                                    <tr class="c_resourceLocation">
                                        <td colspan="4">
                                            {if $filesICA[info].location eq NULL}
                                                -N/A-
                                            {else}
                                                <!-- img by type -->
                                                {$filesICA[info].location}
                                            {/if}
                                        </td>
                                    </tr>
                                    <tr class="c_resourceDetails">
                                        <td>
                                            {if $filesICA[info].page eq NULL}
                                                -N/A-
                                            {else}
                                                {$filesICA[info].page}
                                            {/if}
                                        </td>
                                        <td>
                                            {if $filesICA[info].content eq NULL}
                                                -N/A-
                                            {else}
                                                {$filesICA[info].content}
                                            {/if}
                                        </td>
                                        <td>
                                            <input type="image" src="/_img/icons/edit.png" alt="Edit" name="assignment_edit" id="id_resourceEdit" value="{$filesICA[info].id}" />
                                        </td>
                                        <td>
                                            <input type="image" src="/_img/icons/delete.png" alt="Delete" name="assignment_deleteConfirmation" id="id_resourceDelete" value="{$filesICA[info].id}" />
                                        </td>
                                    </tr>
                                {/section}
                            </table>
                        </fieldset>
                        <br /><br />
                    {/if}
                    {page->filesInvalidAssignmentsMismatch assign='filesMMA'}
                    {if count($filesMMA) > 0}
                        <fieldset>
                            <legend>Mis-Matched Page/Content Assignments</legend>
                            <table>
                                <tr class="c_resourceTitle">
                                    <th colspan="5">
                                        Title
                                    </th>
                                </tr>
                                <tr class="c_resourceLocation">
                                    <th colspan="4">
                                        Location
                                    </th>
                                </tr>
                                <tr class="c_resourceDetails">
                                    <th class="c_resourceActive">
                                        Page
                                    </th>
                                    <th class="c_resourceViewable">
                                        ContentBlock
                                    </th>
                                    <th class="c_resourceEdit">
                                        Edit
                                    </th>
                                    <th class="c_resourceDelete">
                                        Delete
                                    </th>
                                </tr>
                                {section loop=$filesMMA name=info}
                                    <tr class="c_resourceTitle">
                                        <td colspan="4">
                                            {if $filesMMA[info].title eq NULL}
                                                -N/A-
                                            {else}
                                                {$filesMMA[info].title}
                                            {/if}
                                        </td>
                                    </tr>
                                    <tr class="c_resourceLocation">
                                        <td colspan="4">
                                            {if $filesMMA[info].location eq NULL}
                                                -N/A-
                                            {else}
                                                <!-- img by type -->
                                                {$filesMMA[info].location}
                                            {/if}
                                        </td>
                                    </tr>
                                    <tr class="c_resourceDetails">
                                        <td>
                                            {if $filesMMA[info].page eq NULL}
                                                -N/A-
                                            {else}
                                                {$filesMMA[info].page}
                                            {/if}
                                        </td>
                                        <td>
                                            {if $filesMMA[info].content eq NULL}
                                                -N/A-
                                            {else}
                                                {$filesMMA[info].content}
                                            {/if}
                                        </td>
                                        <td>
                                            <input type="image" src="/_img/icons/edit.png" alt="Edit" name="assignment_edit" id="id_resourceEdit" value="{$filesMMA[info].id}" />
                                        </td>
                                        <td>
                                            <input type="image" src="/_img/icons/delete.png" alt="Delete" name="assignment_deleteConfirmation" id="id_resourceDelete" value="{$filesMMA[info].id}" />
                                        </td>
                                    </tr>
                                {/section}
                            </table>
                        </fieldset>
                        <br /><br />
                    {/if}
                    {page->filesValidAssignments assign='filesVRA'}
                    {if count($filesVRA) > 0}
                        <fieldset>
                            <legend>Valid Assignments</legend>
                            <table>
                                <tr class="c_resourceTitle">
                                    <th colspan="4">
                                        Title
                                    </th>
                                </tr>
                                <tr class="c_resourceLocation">
                                    <th colspan="4">
                                        Location
                                    </th>
                                </tr>
                                <tr class="c_resourceDetails">
                                    <th class="c_resourceViewable">
                                        Page
                                    </th>
                                    <th class="c_resourceDescription">
                                        Block
                                    </th>
                                    <th class="c_resourceEdit">
                                        Edit
                                    </th>
                                    <th class="c_resourceDelete">
                                        Remove
                                    </th>
                                </tr>
                                {section loop=$filesVRA name=info}
                                    <tr class="c_resourceTitle">
                                        <td colspan="4">
                                            {if $filesVRA[info].title eq NULL}
                                                -N/A-
                                            {else}
                                                {$filesVRA[info].title}
                                            {/if}
                                        </td>
                                    </tr>
                                    <tr class="c_resourceLocation">
                                        <td colspan="4">
                                            {if $filesVRA[info].location eq NULL}
                                                -N/A-
                                            {else}
                                                <!-- img by type -->
                                                {$filesVRA[info].location}
                                            {/if}
                                        </td>
                                    </tr>
                                    <tr class="c_resourceDetails">
                                        <td>
                                            {if $filesVRA[info].page eq NULL}
                                                -N/A-
                                            {else}
                                                {$filesVRA[info].page}
                                            {/if}
                                        </td>
                                        <td>
                                            {if $filesVRA[info].content eq NULL}
                                                -N/A-
                                            {else}
                                                {$filesVRA[info].content}
                                            {/if}
                                        </td>
                                        <td>
                                            <input type="image" src="/_img/icons/edit.png" alt="Edit" name="assignment_edit" id="id_resourceEdit" value="{$filesVRA[info].id}" />
                                        </td>
                                        <td>
                                            <input type="image" src="/_img/icons/delete.png" alt="Delete" name="assignment_deleteConfirmation" id="id_resourceDelete" value="{$filesVRA[info].id}" />
                                        </td>
                                    </tr>
                                {/section}
                            </table>
                        </fieldset>
                        <br /><br />
                    {/if}
                </form>
            </div>
        </div>
{/strip}