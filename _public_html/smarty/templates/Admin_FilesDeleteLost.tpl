{strip}

    {* /* $Revision: 1.1.1.1 $ */ *}

        <div class="c_ModuleAdmin" id="id_containerFileMetaDelete">
            <h2>Remove File Meta-Data</h2>
            <div id="id_containerFormFileConfirmMetaDelete">
                <form method="post" action="{$smarty.const.PathCurrentRel|escape}">
                    <input type="hidden" name="metaID" value="{$smarty.post.meta_deleteConfirmation}" />
                    <fieldset>
                        <legend>Confirm Removal from Database</legend>
                        <input type="submit" name="file_deleteLost" id="id_deleteResource" value="Total Erasure" />
                    </fieldset>
                </form>
            </div>
        </div>
{/strip}