{strip}

    {* Related Content *}
        {config->NavShowRelated assign=showRelated}
        {if (($showRelated == 'true')
                && (!empty($navSiblings)
                    || !empty($navChildren))
                )}
            <div id="id_containerNavigationRelated">
                <h3>Simliar Items</h3>

            {* Children - Directly Related Content *}
                {config->NavShowChildren assign=showChildren}
                {if (($showChildren == 'true') && !empty($navChildren))}
                    <div id="id_containerNavigationChildren">
                        <h4>Directly Related</h4>
                        <ul>
                            {foreach from=$navChildren key=key item=value}
                                <li>
                                    <a class="c_button" href="{$value.href}">
                                        {$value.titleNav}
                                    </a>
                                </li>
                            {/foreach}
                        </ul>
                    </div>
                {/if}

            {* Siblings - Indirectly Related Content *}
                {config->NavShowSiblings assign=showSiblings}
                {if (($showSiblings == 'true') && !empty($navSiblings))}
                    <div id="id_containerNavigationSiblings">
                        <h4>Indirectly Related</h4>
                        <ul>
                            {foreach from=$navSiblings key=key item=value}
                                <li>
                                    <a class="c_button" href="{$value.href}">
                                        {$value.titleNav}
                                    </a>
                                </li>
                            {/foreach}
                        </ul>
                    </div>
                {/if}

            </div>
        {/if}
    {* End Related *}

{/strip}
