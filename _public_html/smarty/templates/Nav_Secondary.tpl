{strip}

    {* Secondary Navigation *}
        <div id="id_containerNavigationSecondary">
            <ul>
                {foreach from=$navSecondary key=key item=value}
                    <li>
						{page->id assign=idCur}
						{if $value.id != $idCur}
                            <a class="c_button" href="{$value.href}">
						{/if}
						{if $value.href eq '/rss'}
                            <img src="/_img/icons/rss.gif" alt="RSS Icon" title="Click to subscribe to our RSS feed!" />
						{/if}
                        {$value.titleNav}
						{if $value.id != $idCur}
                            </a>
						{/if}
                    </li>
                {/foreach}
            </ul>
        </div>

{/strip}
