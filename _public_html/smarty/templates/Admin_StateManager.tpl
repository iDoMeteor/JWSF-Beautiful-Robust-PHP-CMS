{strip}

    {* /* $Revision: 1.1.1.1 $ */ *}

<div class="c_ModuleAdmin">
    {if $smarty.get.action eq 'preview2public'}
        <div class="c_ModuleAdmin" id="id_containerConfirmTransfer">
            <h2>Transfer Preview Site to Public Domain</h2>
            <div id="id_containerFormConfirmTransfer">
                <form method="post" action="{$smarty.const.PathCurrentRel|escape}">
                    <fieldset>
                        <legend>Confirm Complete Site Migration</legend>
                        <input type="submit" name="preview2public" value="Migrate" />
                    </fieldset>
                </form>
            </div>
        </div>
    {elseif $smarty.get.action eq 'public2preview'}
        <div class="c_ModuleAdmin" id="id_containerConfirmTransfer">
            <h2>Transfer Public Domain to Preview Site</h2>
            <div id="id_containerFormConfirmTransfer">
                <form method="post" action="{$smarty.const.PathCurrentRel|escape}">
                    <fieldset>
                        <legend>Confirm Complete Site Migration</legend>
                        <input type="submit" name="public2preview" value="Migrate" />
                    </fieldset>
                </form>
            </div>
        </div>
    {elseif $smarty.get.action eq 'select-state'}
        <form method="post" action="{$smarty.const.PathCurrentRel|escape}">
            <fieldset>
                <legend>
                    Select a data set to delete or restore
                </legend>
                {page->sessionBackupFiles assign='files'}
                <table>
                    <tr class="c_odd">
                        <th colspan="4">File Name</th>
                    </tr>
                    <tr class="c_even">
                        <th>Restore</th>
                        <th>Time</th>
                        <th>Date</th>
                        <th>Delete</th>
                    </tr>
                    {foreach key=id item=file from=$files}
                        <tr class="c_odd">
                            <td colspan="4">{$file}</td>
                        </tr>
                        {assign var='file' value=$file|replace:'JWSF-SS-':''}
                        {assign var='file' value=$file|replace:'.tar.gz':''}
                        <tr class="c_even">
                            <td>
                                <input type="image" name="restore-state" src="" title="" value="{$id}" />
                            </td>
                            <td>
                                {$file|date_format:"%H:%M:%S"}
                            </td>
                            <td>
                                {$file|date_format:"%A, %B %e, %Y"}
                            </td>
                            <td>
                                <input type="image" name="delete-state" src="" title="" value="{$id}" />
                            </td>
                        </tr>
                    {foreachelse}
                        <tr>
                            <td colspan="3">
                                No session backups found!
                            </td>
                        </tr>
                    {/foreach}
                </table>
            </fieldset>
        </form>
    {/if}
</div>
{/strip}
