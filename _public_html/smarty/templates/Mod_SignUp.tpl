{strip}

    {* /* $Revision: 1.1.1.1 $ */ *}

    <div class="c_Module">
        <div id="id_containerUserCreate">
            {if $templ == 'activationSuccess'}
                <h2>Thank you for activating your account!</h2>
                <p>Your activation was successful.  You may now login using your full email address as the user name by using the authentication boxes in the upper right hand corner of every page.</p>
            {elseif $templ == 'activationFailure'}
                <h2>You have failed to activate an account!</h2>
            {elseif $templ == 'creationSuccess'}
                <h2>You have successfully created your account!</h2>
                <p>Please check your email for instructions on how to activate your new account!</p>
            {else $template == 'userDataForm'}
                {foreach from=$userFormTags item=tag}
                    {$tag}
                {/foreach}
                <h6>* Denotes required information</h6>
            {/if}
        </div>
    </div>
{/strip}
