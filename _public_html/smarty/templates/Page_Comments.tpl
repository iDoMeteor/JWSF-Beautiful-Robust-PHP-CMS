{strip}

    {* $Revision: 1.2 $ *}

    <div id="id_containerPageComments">
        <h3 id="UserComments">User Comments</h3>
        <div id="id_containerPageCommentsNew">
            {if isset($smarty.get.commentReply)}
                <h4>Replying to Comment #{$smarty.get.commentReply}</h4>
                <p>Use your browser's back button to exit reply mode and leave a standard comment</p>
            {/if}
            <form method="post" action="{$smarty.const.PathCurrentRel|escape}">
                <label for="commentName">
                    Name:
                </label>
                <input name="commentName" id="commentName" type="text" />
                <label for="commentEmail">
                    Email:
                </label>
                <input name="commentEmail" id="commentEmail" type="text" />
                <label for="commentComment">
                    Comments:
                </label>
                <textarea name="commentComment" id="commentEmail"></textarea>
                {* captcha check *}
                {config->ProtectionCaptcha  assign=captcha}
                {if $captcha eq 'true'}
                    <label for="commentCaptcha">
                        If you are human, answer the following:
                    </label>
                    <img src="/_img/php/captcha.php" title="Image verification" alt="Image verification" />
                    <input name="commentCaptcha" id="commentCaptcha" type="text" size="4" />
                {/if}
                <br />
                <input type="submit" name="commentSubmit" value="Do it" id="CommentSubmit" />
            </form>

        </div>
        <div id="id_containerPageCommentsPrevious">
            {* loop *}
            {page->comments assign=comments}
            {foreach from=$comments key=ck item=ci}
                <div class="c_containerPageComment" id="comment_id-{$ci.id}">
                    <h4>:{$ci.name}:{$ci.id}</h4>
                    <p>:{$ci.comment}:</p>
                    <h5>@{$ci.stamp}</h5>
                    <ul class="inline">
                        <li>
                            <a href="{page->semanticURI}?commentReply={$ci.id}#UserComments" title="Reply to this comment thread">
                                :reply:
                            </a>
                        </li>
                        <li>
                            <a href="{page->semanticURI}#comment_id-{$ci.id}" title="Link to this comment">
                                :link:
                            </a>
                        </li>
                        <li>
                            <a href="{page->semanticURI}?commentIsSpam={$ci.id}" title="Mark this comment as spam">
                                :spam:
                            </a>
                        </li>
                    </ul>
                </div>
                {if $ci.hasReplies == true}
                    <div class="c_containerPageCommentReplies">
                        {page->replies assign=replies}
                        {foreach from=$replies[$ck] key=rk item=ri}
                            {assign var=parent value=$ri.idParent}
                            {if $ck == $parent}
                                <div class="c_containerPageCommentReply">
                                    <h4>:{$ri.name}:</h4>
                                    <p>:{$ri.comment}:</p>
                                    <h5>@{$ri.stamp}</h5>
                                    <ul class="inline">
                                        <li>
                                            <a href="{page->semanticURI}?commentReply={$parent}" title="Reply to this comment thread">
                                                :reply:
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{page->semanticURI}#comment_id-{$parent}" title="Link to this comment thread">
                                                :link:
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{page->semanticURI}?commentIsSpam={$ri.id}" title="Mark this comment as spam">
                                                :spam:
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            {/if}
                        {/foreach}
                    </div>
                {/if}
            {/foreach}
        </div>
    </div>
{/strip}
