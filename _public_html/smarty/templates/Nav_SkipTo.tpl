{strip}

{* Skip to Navigation *}
    {config->NavShowSkipTo assign=showSkip}
    {if ($showSkip == 'true')}
        <div id="id_containerSkipToNav">
            <a href="#id_containerNavigation">Skip to navigation</a>
        </div>
    {/if}

{/strip}
