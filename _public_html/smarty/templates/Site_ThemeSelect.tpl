{strip}

    {* Theme Selector*}
        {config->ThemeAllowUserSelect assign=userSel}
        {if ($userSel == 'true')}
            <div id="id_containerThemeChooser">
                <form action="{$smarty.const.PathCurrentRel|escape}" method="post" enctype="application/x-www-form-urlencoded" id="id_formThemeChooser" title="JWSF Theme Chooser">
                    <fieldset>
                        <select name="theme" id="id_selectTheme" title="Theme Chooser">
                            <option value="0"> --Choose a Theme-- </option>
                            {foreach from=$themes item=theme}
                                <option value="{$theme}">{$theme}</option>
                            {/foreach}
                        </select>
                        <button name="selectTheme" value="new" type="submit">Go!</button>
                    </fieldset>
                </form>
            </div>
        {/if}

{/strip}
