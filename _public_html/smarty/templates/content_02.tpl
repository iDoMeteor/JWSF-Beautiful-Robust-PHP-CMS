{strip}

    {* /* $Revision: 1.1.1.1 $ */ *}

    {config->PageShowTItle assign=showTitle}
    {if $showTitle == 'true'}
        <h1>{page->titleFull}</h1>
    {/if}
    <hr />
    {section loop=$data name=info}
        <h2>{$data[info].header}</h2>
        <p>{$data[info].content}</p>
    {/section}
    <hr />
    {config->SiteShowTemplateNo assign=showTemp}
    {if $showTemp == 'true'}
        <h5>Content Template 02</h5>
    {/if}
{/strip}
