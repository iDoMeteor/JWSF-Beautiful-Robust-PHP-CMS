{strip}

    {* /* $Revision: 1.1.1.1 $ */ *}

    <div class="c_Module">
        <div id="id_formUserEdit">
            {if $success == true}
                <h2>Your information has been updated</h2>
            {/if}
            {if $template == 'userDataForm'}
                {foreach from=$userFormTags item=tag}
                    {$tag}
                {/foreach}
                <h6>* Denotes required information</h6>
            {/if}
        </div>
    </div>
{/strip}
