{strip}

	{* Page Image *}
        {config->UsePageImages assign=showPageImage}
        {if $showPageImage == 'true'}
            {config->PageDefaultImgUri assign=imgDefault}
            {page->imgURI assign=img}
            {if $img != null}
                <div id="id_containerPageImage">
				    <img src="{$img}" alt="{page->titleFull}" title="{page->titleFull}" />
                </div>
		    {elseif $imgDefault != null}
                <div id="id_containerPageImage">
				    <img src="{$imgDefault}" alt="{page->titleFull}" title="{page->titleFull}" />
                </div>
		    {/if}
        {/if}

{/strip}
