<?xml version="1.0" encoding="ISO-8859-1" ?>
{* strip *}

    {* /* $Revision: 1.1.1.1 $ */ *}

	<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
		<channel>
			<title>
                    {config->SiteTitle}
            </title>
			<link>
                {$smarty.const.CurrentProtocol}{$smarty.const.FQDN}
            </link>
            <atom:link
                    href="{$smarty.const.CurrentProtocol}{$smarty.const.FQDN}/{page->semanticURI}"
                    rel="self" type="application/rss+xml" />
            <description>
                {page->description}
            </description>
			<language>
                {config->SiteLanguage}
            </language>
			<image>
				<title>
                    {config->SiteTitle}
                </title>
				<url>
                    {$smarty.const.CurrentProtocol}{$smarty.const.FQDN}{config->PathImgLogoRel}
                </url>
				<link>
                    {$smarty.const.CurrentProtocol}{$smarty.const.FQDN}
                </link>
			</image>

            {section loop=$rss name=info}

                <item>
                	<title>
                        {$rss[info].titleFull}
                    </title>
                	<link>
                        {$smarty.const.CurrentProtocol}{$smarty.const.FQDN}/{$rss[info].semanticURI}
                    </link>
                    <guid>
                        {$smarty.const.CurrentProtocol}{$smarty.const.FQDN}/?id={$rss[info].id}
                    </guid>
                	<description>
                        {$rss[info].description}
                    </description>
                    {if $rss[info].imgURI neq null}
            			<image>
            				<title>
                                {$rss[info].titleFull}
                            </title>
            				<url>
                                {$smarty.const.CurrentProtocol}{$smarty.const.FQDN}{$rss[info].imgURI}
                            </url>
            				<link>
                                {$smarty.const.CurrentProtocol}{$smarty.const.FQDN}/{$rss[info].semanticURI}
                            </link>
            			</image>
                    {/if}
                </item>
            {/section}

        </channel>
	</rss>
{* /strip *}