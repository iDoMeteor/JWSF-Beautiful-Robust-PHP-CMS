{strip}

    {* /* $Revision: 1.1.1.1 $ */ *}

    {if $smarty.post.assignment_edit eq true}
        {page->resourceAssignment assign='file'}
        <div class="c_ModuleAdmin" id="id_containerFileAssignmentEdit">
            <h2>Edit File Assignment</h2>
            <div id="id_containerFormFileAssignmentEdit">
                <form method="post" action="{$smarty.const.PathCurrentRel|escape}">
                    <input type="hidden" name="assID" value="{$file.id}" />
                    <fieldset>
                        <legend>Verify Parent Page</legend>
                        <table>
                            <tr class="c_resourceTitle">
                                <th colspan="3">
                                    Title
                                </th>
                            </tr>
                            <tr class="c_resourceLocation">
                                <th colspan="3">
                                    Location
                                </th>
                            </tr>
                            <tr class="c_resourceDetails">
                                <th class="c_resourceViewable">
                                    Page
                                </th>
                                <th class="c_resourceDelete">
                                    Remove
                                </th>
                            </tr>
                            <tr class="c_resourceTitle">
                                <td colspan="3">
                                    {$file.title}
                                </td>
                            </tr>
                            <tr class="c_resourceLocation">
                                <td colspan="3">
                                    <!-- img by type -->
                                    {$file.location}
                                </td>
                            </tr>
                            <tr class="c_resourceDetails">
                                <td>
                                    <select name="editID" id="id_editID">
                                        <option value="0"> --Only Download Page-- </option>
                                        {if !empty($pagesActive)}
                                            <optgroup label="Active Pages">
                                            {section name=pdata loop=$pagesActive}
                                                <option value="{$pagesActive[pdata].id}"{if $file.pid == $pagesActive[pdata].id} selected="selected"{/if}>{$pagesActive[pdata].titleNav}</option>
                                            {/section}
                                            </optgroup>
                                        {/if}
                                        {if !empty($pagesInactive)}
                                            <optgroup label="Inactive Pages">
                                            {section name=pdata loop=$pagesInactive}
                                                <option value="{$pagesInactive[pdata].id}"{if $file.pid == $pagesInactive[pdata].id} selected="selected"{/if}>{$pagesInactive[pdata].titleNav}</option>
                                            {/section}
                                            </optgroup>
                                        {/if}
                                        {if !empty($pagesOrphan)}
                                            <optgroup label="Orphan Pages">
                                            {section name=pdata loop=$pagesOrphan}
                                                <option value="{$pagesOrphan[pdata].id}"{if $file.pid == $pagesOrphan[pdata].id} selected="selected"{/if}>{$pagesOrphan[pdata].titleNav}</option>
                                            {/section}
                                            </optgroup>
                                        {/if}
                                    </select>
                                </td>
                                <td>
                                    <input type="image" src="/_img/icons/delete.png" alt="Delete" name="assignment_deleteConfirmation" id="id_resourceDelete" value="{$file.id}" />
                                </td>
                            </tr>
                        </table>
                        <input type="submit" name="action" id="id_assignmentSave" value="Next" />
                    </fieldset>
                </form>
            </div>
        </div>
    {elseif $smarty.post.action eq 'Next'}
        {page->resourceAssignment assign='file'}
        <div class="c_ModuleAdmin" id="id_containerFileAssignmentEdit">
            <h2>Edit File Assignment</h2>
            <div id="id_containerFormFileAssignmentEdit">
                <form method="post" action="{$smarty.const.PathCurrentRel|escape}">
                    <input type="hidden" name="assID" value="{$smarty.post.assID}" />
                    <input type="hidden" name="editID" value="{$smarty.post.editID}" />
                    <fieldset>
                        <legend>Verify Content Block</legend>
                        <table>
                            <tr class="c_resourceTitle">
                                <th colspan="3">
                                    Title
                                </th>
                            </tr>
                            <tr class="c_resourceLocation">
                                <th colspan="3">
                                    Location
                                </th>
                            </tr>
                            <tr class="c_resourceDetails">
                                <th class="c_resourceViewable">
                                    Block
                                </th>
                            </tr>
                            <tr class="c_resourceTitle">
                                <td colspan="3">
                                    {$file.title}
                                </td>
                            </tr>
                            <tr class="c_resourceLocation">
                                <td colspan="3">
                                    <!-- img by type -->
                                    {$file.location}
                                </td>
                            </tr>
                            <tr class="c_resourceDetails">
                                <td>
                                    <select name="fileContentID" id="id_fileContentID">
                                        <option value="0"> --Page Only-- </option>
                                        {if count($data) > 0}
                                            {section loop=$data name=info}
                                                <option value="{$data[info].cid}"{if $file.cid == $data[info].cid} selected="selected"{/if}>{$data[info].header}</option>
                                            {/section}
                                        {/if}
                                    </select>
                                </td>
                            </tr>
                        </table>
                        <input type="submit" name="action" id="id_assignmentSave" value="Update" />
                    </fieldset>
                </form>
            </div>
        </div>

    {/if}
{/strip}