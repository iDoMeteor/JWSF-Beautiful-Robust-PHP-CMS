{strip}

    {* /* $Revision: 1.1.1.1 $ */ *}

        <div class="c_ModuleAdmin" id="id_containerResourceDeletion">
            <h2>Delete File and Remove Meta Data</h2>
            <div id="id_containerFormFilesEdit">
                <form method="post" action="{$smarty.const.PathCurrentRel|escape}">
                    <input type="hidden" name="validID" value="{$smarty.post.file_deleteAllConfirmation}" />
                    <fieldset>
                        <legend>Confirm Complete Removal of Resource</legend>
                        <input type="submit" name="file_deleteAll" id="id_deleteResource" value="Eliminate" />
                    </fieldset>
                </form>
            </div>
        </div>
{/strip}