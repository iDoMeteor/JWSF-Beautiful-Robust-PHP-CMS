{strip}

    {* /* $Revision: 1.1.1.1 $ */ *}

    {* Begin Content DIV *}
    <div class="c_ModuleAdmin" id="id_containerConfigSwitches">
        <h2>Site-Wide Configuration Editor</h2>
        <div id="id_containerConfiguration">
            <form method="post" action="{$smarty.const.PathCurrentRel|escape}">
                <fieldset>
                    <legend>Configuration Switches</legend>
                    <dl>
                        {assign var="counter" value="0"}
                        {foreach from=$config key=label item=value}
                            <dt>
                                <select name="{$value[0]}" id="id_{$value[0]}" title="{$value[0]}">
                                    {if $value[0] == 'ThemeActive'}
                                        {foreach from=$themes item=theme}
                                            <option value="{$theme}"{if $theme == $value[1]} selected="selected"{/if}>{$theme}</option>
                                        {/foreach}
                                    {else}
                                        <option value="true"{if $value[1] == 'true'} selected="selected"{/if}>True</option>
                                        <option value="false"{if $value[1] == 'false'} selected="selected"{/if}>False</option>
                                    {/if}
                                </select>
                                <label id="id_{$value[0]}" for="id_{$value[0]}">{$value[0]}</label>
                            </dt>
                            <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                            {assign var="counter" value="`$counter+1`"}
                                {$value[2]}
                            </dd>
                        {/foreach}
                    </dl>
                    <button name="submitConfig" value="true" type="submit">Update!</button>
                </fieldset>
            </form>
        </div>
    {* End Content DIV *}
    </div>
{/strip}
