{$smarty.post.formEmailPageRecipName},

{$smarty.post.formEmailPageSender} requested that we send you a link to one
of the pages on our web site!  Depending on your setup, you may either
click on the link below, or copy and paste it directly in to the *url* bar
of your favorite browser.

{$smarty.const.CurrentProtocol}{$smarty.const.FQDN}/{page->semanticURI}

Our content management system works hard to provide users a quality experience
free of dangerous scripts, spam and other negative aspects of the Internet.
We hope you enjoy our web site.

Sincerely,
{config->SiteTitle}