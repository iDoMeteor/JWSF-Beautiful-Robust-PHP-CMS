{strip}

    {* /* $Revision: 1.1.1.1 $ */ *}

    {php}
        $GLOBALS['jwsfPageModules']->loadModule('WhosOnline');
    {/php}

    {if !empty($online)}
        <div id="id_containerWhosOnline">
            <h3>Currently Online</h3>
            <ul>
                {* Repeat table for each access level *}
                {foreach from=$online key=level item=users}
                    {* Repeat for each user in this level range *}
                    {foreach from=$users item=user}
                        <li>
                            {$user.nameFirst} {$user.nameLast}
                        </li>
                    {/foreach}
                {* End table loop *}
                {/foreach}
            </ul>
        </div>
    {/if}

{/strip}
