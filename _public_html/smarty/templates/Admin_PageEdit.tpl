{strip}

    {* /* $Revision: 1.2 $ */ *}

    {* Begin Module DIV *}
    <div class="c_ModuleAdmin">
        <h2>Page Editor</h2>
        {* Output page selector *}
        <div id="#id_formPageEditSelect">
            <form action="{$smarty.const.PathCurrentRel|escape}" method="post" enctype="application/x-www-form-urlencoded" id="id_pageSelect" title="Page Selector">
                <fieldset>
                    <legend>Page Edit Selector</legend>
                    <label for="id_editID">Page to Edit</label>
                    <select name="editID" id="id_editID" title="Select a page to edit">
                        <option value="0"> --Choose a Page-- </option>
                        {if !empty($pagesActive)}
                            <optgroup label="Active Pages">
                            {section name=pdata loop=$pagesActive}
                                <option value="{$pagesActive[pdata].id}"{if $smarty.post.editID == $pagesActive[pdata].id} selected="selected"{/if}>{$pagesActive[pdata].titleNav}</option>
                            {/section}
                            </optgroup>
                        {/if}
                        {if !empty($pagesInactive)}
                            <optgroup label="Inactive Pages">
                            {section name=pdata loop=$pagesInactive}
                                <option value="{$pagesInactive[pdata].id}"{if $smarty.post.editID == $pagesInactive[pdata].id} selected="selected"{/if}>{$pagesInactive[pdata].titleNav}</option>
                            {/section}
                            </optgroup>
                        {/if}
                        {if !empty($pagesOrphan)}
                            <optgroup label="Orphan Pages">
                            {section name=pdata loop=$pagesOrphan}
                                <option value="{$pagesOrphan[pdata].id}"{if $smarty.post.editID == $pagesOrphan[pdata].id} selected="selected"{/if}>{$pagesOrphan[pdata].titleNav}</option>
                            {/section}
                            </optgroup>
                        {/if}
                    </select>
                    <button name="selectPage" value="true" type="submit">Edit!</button>
                    {if isset($smarty.post.editID)}
                        <h4>
                            <a href="/?id={page->id}" title="Click here to view this page">
                                View :: {page->titleNav}
                            </a>
                        </h4>
                    {/if}
                </fieldset>
            </form>
        </div>
        {* Only display editing information if a page has been selected *}
        {if isset($smarty.post.editID)}
            {* Begin Form Div *}
            <div id="id_containerFormPageEdit">
                <form action="{$smarty.const.PathCurrentRel|escape}" method="post" enctype="application/x-www-form-urlencoded" id="id_formPageEdit" title="Page Editor">
                    {* Always display new content section if editing a valid page *}
                    {* Begin New Content Div *}
                    <div id="id_containerFormPageEditContentNew">
                        <h4>Post to Social</h4>
                        <ul>
                            <li>
                                check box list
                            </li>
                        </ul>
                        <h2>
                            Add New Content to Page: {page->titleNav}
                        </h2>
                        <div id="id_containerPageEditContentGroupingNew">
                            <div id="id_containerFormPageEditContentNewCritical">
                                <fieldset>
                                    <legend><strong>Critical Content</strong></legend>
                                    <dl>
                                        <dt>
                                            <label for="header_new">Section Heading</label>
                                            <input type="text" name="content[new][header]" id="header_new" value="" />
                                        </dt>
                                        <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                        {assign var="counter" value="`$counter+1`"}
                                            This will appear above your content block to give the speed reader a clue as to the content that will be presented in the following block.  On collapsable pages, this will be all that shows to let the user know they may want to read the content block associated with the heading.
                                        </dd>
                                        <dt>
                                            <label for="content_new">Section Content</label>
                                            <textarea name="content[new][content]" rows="21" cols="50" id="contents_new"></textarea>
                                        </dt>
                                        <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                        {assign var="counter" value="`$counter+1`"}
                                            This is where you enter the textual content for your site.  Beware, special characters make break your page.. I am working on this.  For now, stay away from ampersands!  You can create paragraph breaks easily by putting '&lt;br /&gt;&lt;br /&gt;' (without the quotes) on a blank line in between the areas of text and it will create a blank line on the site.  I am working on dressing this up and making it easier.  XHTML 1.1 tags are allowed, but you had better be pretty sure that your syntax is correct!
                                        </dd>
                                    </dl>
                                </fieldset>
                            </div>
                            <div id="id_containerFormPageEditContentNewOptions">
                                <fieldset>
                                <legend>
                                    Content Options
                                </legend>
                                    <dl>
                                        <dt>
                                            <label for="acsLow_new">Access Level Low</label>
                                            <select name="content[new][acsLow]" id="acsLow_new" title="Access Level Low">
                                                <option value="0" selected="selected"> Public </option>
                                                {section name=so loop=$so_array}
                                                    <option value="{$so_array[so]}">{$so_array[so]}</option>
                                                {/section}
                                            </select>
                                        </dt>
                                        <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                        {assign var="counter" value="`$counter+1`"}
                                            A user must be at least this level to access this particular content.  Public is 0, anything else will generally require a user to be logged in.
                                        </dd>
                                        <dt>
                                            <label for="acsHigh_new">Access Level High</label>
                                            <select name="content[new][acsHigh]" id="acsHigh_new" title="Access Level High">
                                                <option value="0"> Public Only </option>
                                                {section name=so loop=$so_array}
                                                {* Default is all access, 0-255 *}
                                                    <option value="{$so_array[so]}"{if ($so_array[so] == 255)} selected="selected"{/if}>{$so_array[so]}</option>
                                                {/section}
                                            </select>
                                        </dt>
                                        <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                        {assign var="counter" value="`$counter+1`"}
                                            A user higher than this level will not see this content.  255 is the maximum value.
                                        </dd>
                                        <dt>
                                            <label for="so_new">Sort Order</label>
                                            <select name="content[new][so]" id="so_new" title="Sort Order">
                                                <option value="0"> Reverse Chronological </option>
                                                {section name=so loop=$so_array}
                                                    <option value="{$so_array[so]}">{$so_array[so]}</option>
                                                {/section}
                                            </select>
                                        </dt>
                                        <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                        {assign var="counter" value="`$counter+1`"}
                                            This is the sort order on the page.  Any sections with competing sort orders will be displayed alphabetically.
                                        </dd>
                                        <dt>
                                            <label for="active_new">Active</label>
                                            <select name="content[new][active]" id="active_new" title="Active">
                                                <option value="1">True</option>
                                                <option value="0">False</option>
                                            </select>
                                        </dt>
                                        <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                        {assign var="counter" value="`$counter+1`"}
                                            This defines whether or not this section will load with the page or not.  Setting this to false will cause the block to disappear from the page but still be editable here.
                                        </dd>
                                        <dt>
                                            <label for="cssClass_new">CSS Class</label>
                                            <input type="text" name="content[new][cssClass]" id="cssClass_new" />
                                        </dt>
                                        <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                        {assign var="counter" value="`$counter+1`"}
                                            If you would like to define a custom CSS Class for this section, you may do so here.
                                        </dd>
                                        <dt>
                                            <label for="cssID_new">CSS ID</label>
                                            <input type="text" name="content[new][cssID]" id="cssID_new" />
                                        </dt>
                                        <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                        {assign var="counter" value="`$counter+1`"}
                                            If you would like to define a custom CSS ID for this section, you may do so here.
                                        </dd>
                                        <dt>
                                            <label for="image_new">Section Image</label>
                                            <input type="text" name="content[new][imgURI]" id="image_new" value="" />
                                        </dt>
                                        <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                        {assign var="counter" value="`$counter+1`"}
                                            This is currently quite experimental.  You may attempt to enter the location of a image to be loaded with this content and see what happens. :)
                                        </dd>
                                        <dt>
                                            <label for="headerURI_new">Section Link</label>
                                            <input type="text" name="content[new][headerURI]" id="headerURI_new" value="" />
                                        </dt>
                                        <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                        {assign var="counter" value="`$counter+1`"}
                                            This is currently quite experimental.  You may attempt to enter the location of a url and see what happens. :)
                                        </dd>
                    			    </dl>
                                </fieldset>
                            {* End New Content Options Div *}
                            </div>
                        {* End Content Grouping New *}
                        </div>
                    </div>
                    <h2>
                        Edit '{page->titleNav}' Content
                    </h2>
                    {page->id assign='cpid'}
                    {if count($data) > 0}
                        <div id="id_containerPageEditContent">
                            {section loop=$data name=info}
                                {* Begin Existing Content Div *}
                                <div class="c_containerPageEditContentBlock">
                                    <input name="content[{$smarty.section.info.index}][cid]" type="hidden" value="{$data[info].cid}" />
                                    <div class="c_containerPageEditContentHead">
                                        <h3>
                                            Edit ::&nbsp;
                                            <input type="text" name="content[{$smarty.section.info.index}][header]" id="header_{$smarty.section.info.index}" value="{$data[info].header}" />
                                        </h3>
                                        <p id="id_containerP_{$counter|string_format:"%.2d"}">
                                            This will appear above your content block to give the speed reader a clue as to the content that will be presented in the following block.  On collapsable pages, this will be all that shows to let the user know they may want to read the content block associated with the heading.
                                        </p>
                                    </div>
                                    <div id="id_containerPageEditContentGrouping_{$counter|string_format:"%.2d"}">
                                        {assign var="counter" value="`$counter+1`"}
                                        <div class="c_containerPageEditContentCritical">
                                            <fieldset>
                                                <legend>
                                                    <strong>Critical Data</strong> - CID: <strong>{$data[info].cid}</strong>
                                                </legend>
                                                <dl>
                                                    <dt>
                                                        <label for="content_{$smarty.section.info.index}">Section Content</label>
                                                        <textarea name="content[{$smarty.section.info.index}][content]" rows="38" cols="50" id="contents_{$smarty.section.info.index}">{$data[info].content|escape:'htmlall'}</textarea>
                                                    </dt>
                                                    <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                                    {assign var="counter" value="`$counter+1`"}
                                                        This is where you enter the textual content for your site.  Beware, special characters make break your page.. I am working on this.  For now, stay away from ampersands!  You can create paragraph breaks easily by putting '&lt;br /&gt;&lt;br /&gt;' (without the quotes) on a blank line in between the areas of text and it will create a blank line on the site.  I am working on dressing this up and making it easier.  XHTML 1.1 tags are allowed, but you had better be pretty sure that your syntax is correct!
                                                    </dd>
                                                </dl>
                                            </fieldset>
                                        </div>
                                        <div class="c_containerPageEditContentOptions">
                                            <fieldset>
                                            <legend>
                                                <strong>Options Area</strong> - CID: <strong>{$data[info].cid}</strong>
                                            </legend>
                                                <dl>
                                                    <dt>
                                                        <label for="acsLow_{$smarty.section.info.index}">Access Level Low</label>
                                                        <select name="content[{$smarty.section.info.index}][acsLow]" id="acsLow_{$smarty.section.info.index}" title="Access Level Low">
                                                            <option value="0"{if $data[info].acsLow == 0} selected="selected"{/if}> Public </option>
                                                            {section name=so loop=$so_array}
                                                                <option value="{$so_array[so]}"{if $data[info].acsLow == $so_array[so]} selected="selected"{/if}>{$so_array[so]}</option>
                                                            {/section}
                                                        </select>
                                                    </dt>
                                                    <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                                    {assign var="counter" value="`$counter+1`"}
                                                        A user must be at least this level to access this particular content.  Public is 0, anything else will generally require a user to be logged in.
                                                    </dd>
                                                    <dt>
                                                        <label for="acsHigh_{$smarty.section.info.index}">Access Level High</label>
                                                        <select name="content[{$smarty.section.info.index}][acsHigh]" id="acsHigh_{$smarty.section.info.index}" title="Access Level High">
                                                            <option value="0"{if $data[info].acsHigh == 0} selected="selected"{/if}> Public Only </option>
                                                            {section name=so loop=$so_array}
                                                                <option value="{$so_array[so]}"{if $data[info].acsHigh == $so_array[so]} selected="selected"{/if}>{$so_array[so]}</option>
                                                            {/section}
                                                        </select>
                                                    </dt>
                                                    <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                                    {assign var="counter" value="`$counter+1`"}
                                                        A user higher than this level will not see this content.  255 is the maximum value.
                                                    </dd>
                                                    <dt>
                                                        <label for="so_{$smarty.section.info.index}">Sort Order</label>
                                                        <select name="content[{$smarty.section.info.index}][so]" id="so_{$smarty.section.info.index}" title="Sort Order">
                                                            <option value="0"{if $data[info].so == 0} selected="selected"{/if}> Reverse Chronological </option>
                                                            {section name=so loop=$so_array}
                                                                <option value="{$so_array[so]}"{if $data[info].so == $so_array[so]} selected="selected"{/if}>{$so_array[so]}</option>
                                                            {/section}
                                                        </select>
                                                    </dt>
                                                    <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                                    {assign var="counter" value="`$counter+1`"}
                                                        This is the sort order on the page.  Any sections with competing sort orders will be displayed alphabetically.
                                                    </dd>
                                                    <dt>
                                                        <label for="con_parent_{$smarty.section.info.index}">Parent Page</label>
                                                        <select name="content[{$smarty.section.info.index}][pid]" id="con_{$smarty.section.info.index}" title="Parent Page">
                                                            {if !empty($pagesActive)}
                                                                <optgroup label="Active Pages">
                                                                {section name=pdata loop=$pagesActive}
                                                                    <option value="{$pagesActive[pdata].id}"{if $cpid == $pagesActive[pdata].id} selected="selected"{/if}>{$pagesActive[pdata].titleNav}</option>
                                                                {/section}
                                                                </optgroup>
                                                            {/if}
                                                            {if !empty($pagesInactive)}
                                                                <optgroup label="Inactive Pages">
                                                                {section name=pdata loop=$pagesInactive}
                                                                    <option value="{$pagesInactive[pdata].id}"{if $cpid == $pagesInactive[pdata].id} selected="selected"{/if}>{$pagesInactive[pdata].titleNav}</option>
                                                                {/section}
                                                                </optgroup>
                                                            {/if}
                                                            {if !empty($pagesOrphan)}
                                                                <optgroup label="Orphan Pages">
                                                                {section name=pdata loop=$pagesOrphan}
                                                                    <option value="{$pagesOrphan[pdata].id}"{if $cpid == $pagesOrphan[pdata].id} selected="selected"{/if}>{$pagesOrphan[pdata].titleNav}</option>
                                                                {/section}
                                                                </optgroup>
                                                            {/if}
                                                        </select>
                                                    </dt>
                                                    <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                                    {assign var="counter" value="`$counter+1`"}
                                                        If you change the parent page here, this section of content will move to that page immediately.
                                                    </dd>
                                                    <dt>
                                                        <label for="active_{$smarty.section.info.index}">Active</label>
                                                        <select name="content[{$smarty.section.info.index}][active]" id="active_{$smarty.section.info.index}" title="Active">
                                                            <option value="1"{if ($data[info].active == '1')} selected="selected"{/if}>True</option>
                                                            <option value="0"{if ($data[info].active == '0')} selected="selected"{/if}>False</option>
                                                        </select>
                                                    </dt>
                                                    <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                                    {assign var="counter" value="`$counter+1`"}
                                                        This defines whether or not this section will load with the page or not.  Setting this to false will cause the block to disappear from the page but still be editable here.
                                                    </dd>
                                                    <dt>
                                                        <label for="cssClass_{$smarty.section.info.index}">CSS Class</label>
                                                        <input type="text" name="content[{$smarty.section.info.index}][cssClass]" id="cssClass_{$smarty.section.info.index}" value="{$data[info].cssClass}" />
                                                    </dt>
                                                    <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                                    {assign var="counter" value="`$counter+1`"}
                                                        If you would like to define a custom CSS Class for this section, you may do so here.
                                                    </dd>
                                                    <dt>
                                                        <label for="cssID_{$smarty.section.info.index}">CSS ID</label>
                                                        <input type="text" name="content[{$smarty.section.info.index}][cssID]" id="cssID_{$smarty.section.info.index}" value="{$data[info].cssID}" />
                                                    </dt>
                                                    <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                                    {assign var="counter" value="`$counter+1`"}
                                                        If you would like to define a custom CSS ID for this section, you may do so here.
                                                    </dd>
                                                    <dt>
                                                        <label for="imgURI_{$smarty.section.info.index}">Section Image</label>
                                                        <input type="text" name="content[{$smarty.section.info.index}][imgURI]" id="imgURI_{$smarty.section.info.index}" value="{$data[info].imgURI}" />
                                                    </dt>
                                                    <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                                    {assign var="counter" value="`$counter+1`"}
                                                        This is currently quite experimental.  You may attempt to enter the location of a image to be loaded with this content and see what happens. :)
                                                    </dd>
                                                    <dt>
                                                        <label for="headerURI_{$smarty.section.info.index}">Section Link</label>
                                                        <input type="text" name="content[{$smarty.section.info.index}][headerURI]" id="headerURI_{$smarty.section.info.index}" value="{$data[info].headerURI}" />
                                                    </dt>
                                                    <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                                    {assign var="counter" value="`$counter+1`"}
                                                        This is currently quite experimental.  You may attempt to enter the location of a url and see what happens. :)
                                                    </dd>
                                                </dl>
                                                <h3>CAUTION! Deleted data CANNOT be retrieved!</h3>
                                                <p>Perhaps active=false until you are really sure?</p>
                                                <label for="delete_{$smarty.section.info.index}">Delete This Permanently</label>
                                                <input name="content[{$smarty.section.info.index}][delete]" id="delete_{$smarty.section.info.index}" type="checkbox" value="1" />
                                            </fieldset>
                                        {* End Existing Content Div *}
                                        </div>
                                        <div class="c_containerFormPageEditSubmit">
                                            <button name="saveEdit" value="true" type="submit">Save!</button>
                                        </div>
                                    {* End Content Grouping Block *}
                                    </div>
                                {* End Block Container*}
                                </div>
                            {/section}
                        {* End current content outer container *}
                        </div>
                    {/if}
                    <h2>
                            Edit '{page->titleNav}' Meta Data
                    </h2>
                    {* Begin Page Meta Data Div *}
                    <div id="id_containerFormPageEditMeta">
                        <div id="id_containerFormPageEditCritical">
                            <fieldset>
                                <legend>Critical Data</legend>
                                <input name="selectPage" type="hidden" value="true" />
                                <input name="editID" type="hidden" value="{page->id}" />
                                <input name="page_id" type="hidden" value="{page->id}" />
                                <dl>
                                    <dt>
                                        <label for="id_page_pid">Parent Page</label>
                                        <select name="page_pid" id="id_page_pid" title="Select a Parent Page">
                                            {page->pid assign='pid'}
                                            <option value="0"{if $pid == 0} selected="selected"{/if}> --Top Level-- </option>
                                            {if !empty($pagesActive)}
                                                <optgroup label="Active Pages">
                                                {section name=pdata loop=$pagesActive}
                                                    <option value="{$pagesActive[pdata].id}"{if $pid == $pagesActive[pdata].id} selected="selected"{/if}>{$pagesActive[pdata].titleNav}</option>
                                                {/section}
                                                </optgroup>
                                            {/if}
                                            {if !empty($pagesInactive)}
                                                <optgroup label="Inactive Pages">
                                                {section name=pdata loop=$pagesInactive}
                                                    <option value="{$pagesInactive[pdata].id}"{if $pid == $pagesInactive[pdata].pid} selected="selected"{/if}>{$pagesInactive[pdata].titleNav}</option>
                                                {/section}
                                                </optgroup>
                                            {/if}
                                            {if !empty($pagesOrphan)}
                                                <optgroup label="Orphan Pages">
                                                {section name=pdata loop=$pagesOrphan}
                                                    <option value="{$pagesOrphan[pdata].id}"{if $pid == $pagesOrphan[pdata].pid} selected="selected"{/if}>{$pagesOrphan[pdata].titleNav}</option>
                                                {/section}
                                                </optgroup>
                                            {/if}
                                        </select>
                                    </dt>
                                    <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                    {assign var="counter" value="`$counter+1`"}
                                        This setting defines the hierarchical relationship of the primary navigational menu.  Children are listed as sub-menus of their parents.  A 'Top Level' item will not be sub-menued.  If you select any other option, the page you are creating or editing will be listed as a sub-menu of that page.
                                    </dd>
                                    <dt>
                                        <label for="id_page_location">Location</label>
                                        <select name="page_location" id="id_page_location" title="Menu Location">
                                            {page->location assign='location'}
                                            <option value="0"{if $location == 0} selected="selected"{/if}>Hidden</option>
                                            <option value="1"{if $location == 1} selected="selected"{/if}>Primary</option>
                                            <option value="2"{if $location == 2} selected="selected"{/if}>Secondary</option>
                                            <option value="3"{if $location == 3} selected="selected"{/if}>Primary &amp; Secondary</option>
                                            <option value="4"{if $location == 4} selected="selected"{/if}>Tertiary</option>
                                            <option value="5"{if $location == 5} selected="selected"{/if}>Primary &amp; Tertiary</option>
                                            <option value="6"{if $location == 6} selected="selected"{/if}>Secondary &amp; Tertiary</option>
                                            <option value="7"{if $location == 7} selected="selected"{/if}>All Three</option>
                                        </select>
                                    </dt>
                                    <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                    {assign var="counter" value="`$counter+1`"}
                                        This defines where the page will appear on the page. Hidden would be good for receipts or secrets, primary will place it the most prominent menu location, secondary will put it in into what is typically the 'footer' area, and both will place the item in both of the navigational menus.
                                    </dd>
                                    <dt>
                                        <label for="id_page_titleNav">Navigation Title</label>
                                        <input name="page_titleNav" id="id_page_titleNav" value="{page->titleNav}" />
                                    </dt>
                                    <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                    {assign var="counter" value="`$counter+1`"}
                                        This is the label the page will have in the nagivational menus.
                                    </dd>
                                    <dt>
                                        <label for="id_page_titleFull">Full Title</label>
                                        <input name="page_titleFull" id="id_page_titleFull" value="{page->titleFull}" />
                                   </dt>
                                    <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                    {assign var="counter" value="`$counter+1`"}
                                        This title will be displayed in the title bar of the browser, to the left of the site title.  If 'PageShowTItle' is set to true in the Switches area, then the page title will be displayed as an H1 element in the top of the content area, which is generally a good idea (semantically speakiing).
                                    </dd>
                                    <dt>
                                        <label for="id_page_semanticURI">Friendly URL</label>
                                        <input name="page_semanticURI" id="id_page_semanticURI" value="{page->semanticURI}" />
                                    </dt>
                                    <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                    {assign var="counter" value="`$counter+1`"}
                                        This will be displayed after the last / in 'http://yoursite.com/'.  Do not use any funny characters except for dashes or underscores.  If it is not set, the url will show 'http://yoursite.com/?id=x'.  If it begins with 'anything://', then it will expect a full, valid external url and will link to it appropriately (that part doesn't work yet!).
                                    </dd>
                                    <dt>
                                        <label for="id_imgURI">Page Image</label>
                                        <input type="text" name="page_imgURI" id="id_imgURI" value="{page->imgURI}" />
                                    </dt>
                                    <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                    {assign var="counter" value="`$counter+1`"}
                                        This is currently quite experimental.  You may attempt to enter the location of a image to be loaded with this page and see what happens. :)
                                    </dd>
                                    <dt>
                                        <label for="id_page_description">Search Engine Description</label>
                                        <input name="page_description" id="id_page_description">{page->description}</input>
                                    </dt>
                                    <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                    {assign var="counter" value="`$counter+1`"}
                                        This will be the page description used by search engines.  Be concise, you have 255 characters.
                                    </dd>
                                </dl>
                            </fieldset>
                        </div>
                        <div id="id_containerFormPageEditOptions">
                            <fieldset>
                                <legend>Optional/Advanced Data</legend>
                                <dl>
                                    <dt>
                                        <label for="id_page_acsLow">Access Level Low</label>
                                        <select name="page_acsLow" id="id_page_acsLow" title="Access Level Low">
                                            {page->acsLow assign='acsLow'}
                                            <option value="0" {if $acsLow == 0} selected="selected"{/if}> Public </option>
                                            {section name=so loop=$so_array}
                                                <option value="{$so_array[so]}" {if $acsLow == $so_array[so]} selected="selected"{/if}>{$so_array[so]}</option>
                                            {/section}
                                        </select>
                                    </dt>
                                    <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                    {assign var="counter" value="`$counter+1`"}
                                        A user must be at least this level to access the page.  Public is 0, anything else will generally require a user to be logged in.
                                    </dd>
                                    <dt>
                                        <label for="id_page_acsHigh">Access Level High</label>
                                        <select name="page_acsHigh" id="id_page_acsHigh" title="Access Level High">
                                            {page->acsHigh assign='acsHigh'}
                                            <option value="0" {if $acsHigh == 0} selected="selected"{/if}> Public Only </option>
                                            {section name=so loop=$so_array}
                                                <option value="{$so_array[so]}" {if $acsHigh == $so_array[so]} selected="selected"{/if}>{$so_array[so]}</option>
                                            {/section}
                                        </select>
                                    </dt>
                                    <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                    {assign var="counter" value="`$counter+1`"}
                                        A user higher than this level will not see this page.  255 is the maximum value.
                                    </dd>
                                    <dt>
                                        <label for="id_page_active">Active</label>
                                        <select name="page_active" id="id_page_active" title="Active">
                                            {page->active assign='active'}
                                            <option value="1" {if $active == 1} selected="selected"{/if}>True</option>
                                            <option value="0" {if $active == 0} selected="selected"{/if}>False</option>
                                        </select>
                                    </dt>
                                    <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                    {assign var="counter" value="`$counter+1`"}
                                        Whether or not this page is active determines whether or not it will be accessible by anyone or no one.
                                    </dd>
                                    <dt>
                                        <label for="id_page_allowComments">Allow Comments</label>
                                        <select name="page_allowComments" id="id_page_allowComments" title="Allow Comments">
                                            {page->PageAllowComments assign='allowComments'}
                                            <option value="1" {if $allowComments == 1} selected="selected"{/if}>True</option>
                                            <option value="0" {if $allowComments == 0} selected="selected"{/if}>False</option>
                                        </select>
                                    </dt>
                                    <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                    {assign var="counter" value="`$counter+1`"}
                                        Whether or not to allow users to comment on pages.  This is not yet implemented, but will basically function as a 'default module'.
                                    </dd>
                                    <dt>
                                        <label for="id_page_allowRatings">Allow Ratings</label>
                                        <select name="page_allowRatings" id="id_page_allowRatings" title="Allow Ratings">
                                            {page->allowRatings assign='allowRatings'}
                                            <option value="1" {if $allowRatings == 1} selected="selected"{/if}>True</option>
                                            <option value="0" {if $allowRatings == 0} selected="selected"{/if}>False</option>
                                        </select>
                                    </dt>
                                    <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                    {assign var="counter" value="`$counter+1`"}
                                        Whether or not to allow users to rate pages.  This is not yet implemented, but will basically function as a 'default module'.
                                    </dd>
                                    <dt>
                                        <label for="id_page_contentLimit">Content Limit</label>
                                        <select name="page_contentLimit" id="id_page_contentLimit" title="Content Limit">
                                            {page->contentLimit assign='contentLimit'}
                                            <option value="0" {if $contentLimit == 0} selected="selected"{/if}> Unlimited </option>
                                            {* This currently uses the SO array since it is currently has the same limitations *}
                                            {section name=so loop=$so_array}
                                                <option value="{$so_array[so]}" {if $contentLimit == $so_array[so]} selected="selected"{/if}>{$so_array[so]}</option>
                                            {/section}
                                        </select>
                                    </dt>
                                    <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                    {assign var="counter" value="`$counter+1`"}
                                        This determines the number of content blocks that will show on a page.  An example of a page you may want to limit would be a 'Recent News' type of page where you would allow the content to be sorted reverse chronologically and limit it to say the five most recently entered 'articles' (content blocks).
                                    </dd>
                                    <dt>
                                        <label for="id_page_cTemplate">Content Template</label>
                                        <select name="page_cTemplate" id="id_page_cTemplate" title="Select a Content Template">
                                            {page->cTemplate assign='templateCurrent'}
                                            {foreach from=$content_templates item=template}
                                                <option value="{$template}" {if $templateCurrent == $template} selected="selected"{/if}>{$template}</option>
                                            {/foreach}
                                        </select>
                                    </dt>
                                    <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                    {assign var="counter" value="`$counter+1`"}
                                        If there are multiple content templates, then you may choose from them here.
                                    </dd>
                                    <dt>
                                        <label for="id_page_headerCollapse">Collapse Headers</label>
                                        <select name="page_headerCollapse" id="id_page_headerCollapse" title="Collapse Headers">
                                            {page->headerCollapse assign='headerCollapse'}
                                            <option value="0" {if $headerCollapse == 0} selected="selected"{/if}>False</option>
                                            <option value="1" {if $headerCollapse == 1} selected="selected"{/if}>True</option>
                                        </select>
                                    </dt>
                                    <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                    {assign var="counter" value="`$counter+1`"}
                                        If this is set to true, and the theme supports it, all of the headers on the page will be collapsed when a user first loads it and they will have to click the expansion link to see the content.  This should be settable per block/module as well!
                                    </dd>
                                    <dt>
                                        <label for="id_page_sitemapFrequency">Sitemap Frequency</label>
                                        <select name="page_sitemapFrequency" id="id_page_sitemapFrequency" title="Select a Sitemap Frequency">
                                            {page->sitemapFrequency assign='sitemapFrequency'}
                                            {foreach from=$frequency_array item=k name=foo}
                                                <option value="{$smarty.foreach.foo.iteration}"{if $sitemapFrequency eq $smarty.foreach.foo.iteration} selected="selected"{/if}>{$k}</option>
                                            {/foreach}
                                        </select>
                                    </dt>
                                    <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                    {assign var="counter" value="`$counter+1`"}
                                        This defines how often you expect the content on this page to change.  Be honest Google hates liars!
                                    </dd>
                                    <dt>
                                        <label for="id_page_sitemapPriority">Sitemap Priority</label>
                                        <select name="page_sitemapPriority" id="id_page_sitemapPriority" title="Sitemap Priority">
                                            {page->sitemapPriority assign='sitemapPriority'}
                                            {section name=p loop=$priority_array}
                                                <option value="{$priority_array[p]}"{if $sitemapPriority == $priority_array[p]} selected=" selected"{/if}>{$priority_array[p]}</option>
                                            {/section}
                                        </select>
                                    </dt>
                                    <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                    {assign var="counter" value="`$counter+1`"}
                                        This defines the relative importance of this page in relation to other pages on your site.  Default is 5 in a range from 0 to 10.  You are only competing against yourself here, so do so wisely.
                                    </dd>
                                    <dt>
                                        <label for="id_page_so">Sort Order</label>
                                        <select name="page_so" id="id_page_so" title="Sort Order">
                                            {page->so assign='so'}
                                            <option value="0" {if $so == 0} selected="selected"{/if}> Alphabetical </option>
                                            {section name=so loop=$so_array}
                                                <option value="{$so_array[so]}" {if $so == $so_array[so]} selected="selected"{/if}>{$so_array[so]}</option>
                                            {/section}
                                        </select>
                                    </dt>
                                    <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                    {assign var="counter" value="`$counter+1`"}
                                        This is the sort order in the primary navigational menu (secondary is alphabetical).  Any pages with competing sort orders will be displayed alphabetically.  The default is settable in the Numerics area.  I like to use 10 as the default, and go up from there so that if I decide to bump a couple pages upward, I don't have to go through and change every page!
                                    </dd>
                                </dl>
                            </fieldset>
                        </div>
                        <div id="id_containerPageDeletions">
                            <fieldset>
                                <legend>DELETION AREA</legend>
                                    <h3>CAUTION! Deleted data CANNOT be retrieved!</h3>
                                    <p>Perhaps active=false until you are really sure?</p>
                                    <dl>
                                        <dt>
                                            <label for="delete_page">Delete This Page Permanently
                                        </label>
                                            <input name="delete_page" id="delete_page" type="checkbox" value="1" />
                                        </dt>
                                        <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                        {assign var="counter" value="`$counter+1`"}
                                            Checking this box will delete this page from the system when the form is submitted, the content will be left behind unless you check one of the content deletion boxes.
                                        </dd>
                                        <dt>
                                            <label for="delete_all_page_content">Delete All Page Content Permanently</label>
                                            <input name="delete_all_page_content" id="delete_all_page_content" type="checkbox" value="1" />
                                        </dt>
                                        <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                        {assign var="counter" value="`$counter+1`"}
                                            Checking this box will delete ALL content associated with this page.  The page will remain in place, with any changes made, unless the page deletion box is checked.
                                        </dd>
                                    </dl>
                            </fieldset>
                        {* End deletions *}
                        </div>
                        <div class="c_clear-both"></div>
                    {* End Page Meta Data Div *}
                    </div>
                    <div class="c_containerFormPageEditSubmit">
                        <button name="saveEdit" value="true" type="submit">Save!</button>
                    </div>
                </form>
            {* End Form Div *}
            </div>
        {/if}
    {* End Module Div *}
    </div>
{/strip}
