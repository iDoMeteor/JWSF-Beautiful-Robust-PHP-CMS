if type eq boolean (1)
    output rating symbols (/_img/ratings/themePrefix-positive/negative)
    output percentage if votes != 0
if type eq rating (2)
    if no rating output empty star field  (/_img/ratings/themePrefix-#)
    if rating output appropriate stars
if type eq 0
    nada