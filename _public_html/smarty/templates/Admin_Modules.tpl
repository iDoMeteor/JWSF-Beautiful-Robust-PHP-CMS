{strip}

    {* /* $Revision: 1.1.1.1 $ */ *}

<div class="c_ModuleAdmin">
    {foreach from=$formOpen item=open_item}
        {$open_item}
    {/foreach}
    {if !empty($assigned)}
        <table border="1">
            <caption>Assigned Modules</caption>
            <tr>
                <th>
                    Page
                </th>
                <th>
                    Location
                </th>
                <th>
                    Sort
                </th>
                <th>
                    Active
                </th>
                <th>
                    Low
                </th>
                <th>
                    High
                </th>
                <th>
                    Module
                </th>
                <th>
                    Delete
                </th>
            </tr>
            {foreach from=$assigned item=assigned_item}
                <tr>
                    {foreach from=$assigned_item key=assigned_key item=assigned_property}
                        {* Hidden field hack *}
                        {if $assigned_key != 1}
                            <td>
                        {/if}
                                {$assigned_property}
                        {if $assigned_key != 0}
                            </td>
                        {/if}
                    {/foreach}
                </tr>
            {/foreach}
        </table>
    {/if}
    {if !empty($orphans)}
        <table border="1">
            <caption>Orphan Modules</caption>
            <tr>
                <th>
                    Page
                </th>
                <th>
                    Location
                </th>
                <th>
                    Sort
                </th>
                <th>
                    Active
                </th>
                <th>
                    Low
                </th>
                <th>
                    High
                </th>
                <th>
                    Module
                </th>
                <th>
                    Delete
                </th>
            </tr>
            {foreach from=$orphans item=orphan_item}
                <tr>
                    {foreach from=$orphan_item key=orphan_key item=orphan_property}
                        {* Hidden field hack *}
                        {if $orphan_key != 1}
                            <td>
                        {/if}
                                {$orphan_property}
                        {if $orphan_key != 0}
                            </td>
                        {/if}
                    {/foreach}
                </tr>
            {/foreach}
        </table>
    {/if}
    <table border="1">
        <tr>
            <th>
                Page
            </th>
            <th>
                Location
            </th>
            <th>
                Sort
            </th>
            <th>
                Active
            </th>
            <th>
                Low
            </th>
            <th>
                High
            </th>
            <th>
                Module
            </th>
        </tr>
        <caption>Assign New Module</caption>
        {foreach from=$new item=new_item}
            {* Lacks hidden field and delete button *}
            <tr>
                {foreach from=$new_item key=new_key item=new_property}
                    <td>
                        {$new_property}
                    </td>
                {/foreach}
            </tr>
        {/foreach}
    </table>
    {foreach from=$formClose item=closing_item}
        {$closing_item}
    {/foreach}
</div>
{/strip}
