{strip}

    {* /* $Revision: 1.1.1.1 $ */ *}

{*
flush smarty cache = /admin?action=flush - clear_all_cache()
*}

{user->acslvl assign=level}
{config->AdminRequiresLogin assign=strict}

{* Should make this dynamic *}
    <div id="id_containerAdminMenu">
        <h1>
            <a href="javascript:void(0);" onclick="JWSF_BlockToggle(this,'id_containerAdminMenuItems');return document.JWSF_RetVal">
                Administrative Menu
            </a>
        </h1>
        <h5 style="margin-top: 0px;">
            Click to expand
        </h5>
        <ul id="id_containerAdminMenuItems">
            {if (($level == 255) || ($strict != 'true'))}
                <li>
                    Archive Manager
                    <ul>
                        <li>
                            <a href="/admin?mod=ArchiveManager&amp;action=backup-data" title="Generate a Database Backup of Current State">
                                DB Backup
                            </a>
                        </li>
                        <li>
                            <a href="/admin?mod=ArchiveManager&amp;action=select-data" title="Restore the Database to a Previous Version">
                                DB Restore
                            </a>
                        </li>
                        <li>
                            <a href="/admin?mod=ArchiveManager&amp;action=backup-files" title="Generate a Backup of Current File System State">
                                File Backup
                            </a>
                        </li>
                        <li>
                            <a href="/admin?mod=ArchiveManager&amp;action=select-files" title="Restore the File System to a Previous Version">
                                File Restore
                            </a>
                        </li>
                    </ul>
                </li>
            {/if}
            {if (($level == 255) || ($strict != 'true'))}
            <li>
                Configuration
                <ul>
                    <li>
                        <a href="/admin/?mod=ConfigNumerics" title="">
                            Numerics
                        </a>
                    </li>
                    <li>
                        <a href="/admin/?mod=ConfigStrings" title="">
                            Strings
                        </a>
                    </li>
                    <li>
                        <a href="/admin/?mod=ConfigSwitches" title="">
                            Switches
                        </a>
                    </li>
                </ul>
            </li>
            {/if}
            <li>
                Pages
                <ul>
                    <li>
                        <a href="/admin/?mod=PageCreate" title="This will create a new
                        page, linked to any parent or none.">
                            Create
                        </a>
                    </li>
                    <li>
                        <a href="/admin/?mod=PageEdit" title="This allows detailed editing
                        of any page on the site,
                        including search engine keywords and description,
                        content template, the content itself, location in
                        the navigation tree, and more!  Pages are organized
                        alphabetically by status: Active, Inactive, and Orphan (
                        pages that lack a valid parent page).">
                            Edit
                        </a>
                    </li>
                    <li>
                        <a href="/admin/?mod=PageInactiveContent" title="Locate content
                        from any page that is not marked active.">
                            Inactive Content
                        </a>
                    </li>
                    <li>
                        <a href="/admin/?mod=PageOrphanContent" title="Locate content
                        associated with a page ID that does not exist">
                            Orphan Content
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                Page Resources
                <ul>
                    <li>
                        <a href="/admin/?mod=Files-Links&amp;action=add-files" title="Add Files">
                            Add Files
                        </a>
                    </li>
                    <li>
                        <a href="/admin/?mod=Files-Links&amp;action=add-links" title="Add Links">
                            Add Links
                        </a>
                    </li>
                    <li>
                        <a href="/admin/?mod=Files-Links&amp;action=edit-files" title="Edit Files">
                            Edit Files
                        </a>
                    </li>
                    <li>
                        <a href="/admin/?mod=Files-Links&amp;action=edit-links" title="Edit Links">
                            Edit Links
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                Media
                <ul>
                    <li>
{*                        <a href="#" title="Control Your Media"> *}
                            Coming Soon - Audio, Flash, Images &amp; Video
{*                        </a> *}
                    </li>
                </ul>
            </li>
            {if (($level == 255) || ($strict != 'true'))}
                <li>
                    Modules
                    <ul>
                        <li>
                                Administrative Interfaces
                        </li>
                        <li>
                            <a href="/admin/?mod=Modules" title="Module Assignments">
                                Assignment
                            </a>
                        </li>
                    </ul>
                </li>
            {/if}
            {if (($level == 255) || ($strict != 'true'))}
                <li>
                    State Management
                    <ul>
                        <li>
{*                            <a href="/admin?mod=StateManager&amp;action=preview2public" title="Migrate preview state to public site"> *}
                                Preview -> Public
{*                            </a> *}
                        </li>
                        <li>
{*                            <a href="/admin?mod=StateManager&amp;action=public2preview" title="Migrate public state to preview site"> *}
                                Public -> Preview
{*                            </a> *}
                        </li>
                        <li>
                            <a href="/admin?mod=StateManager&amp;action=select-state" title="Restore a previous database and file system state">
                                Restore
                            </a>
                        </li>
                        <li>
                            <a href="/admin?mod=StateManager&amp;action=save-state" title="Save complete file system and database state">
                                Save
                            </a>
                        </li>
                    </ul>
                </li>
            {/if}
            {if (($level == 255) || ($strict != 'true'))}
                <li>
                    System
                    <ul>
                        <li>
    {* Make this smarter *}
                            <a href="/admin?action=flush-smarty" title="This will remove all cached and compiled Smarty templates">
                                Flush Smarty
                            </a>
                        </li>
                        <li>
                            <a href="/admin?action=generate-pdfs" title="Generate (or re-create) PDFs for all site pages">
                                (Re)Generate PDFs
                            </a>
                        </li>
                        <li>
                                Update Check
                        </li>
                        <li>
                                SandBox
                        </li>
                    </ul>
                </li>
            {/if}
            <li>
                Users
                <ul>
                    <li>
                        <a href="/admin/?mod=UserCreate" title="Create registered system
                        users.">
                            Create
                        </a>
                    </li>
                    <li>
                        <a href="/admin/?mod=UserEditor" title="View registered system
                        users.">
                            Edit
                        </a>
                    </li>
                    <li>
{*                        <a href="#" title="Modify system access levels"> *}
                            Levels
{*                        </a> *}
                    </li>
                    <li>
                        <a href="/admin/?mod=UserList" title="List all users">
                            List
                        </a>
                    </li>
                    <li>
                        <a href="/admin/?mod=UserRecent" title="View recently active
                        users.">
                            Recent
                        </a>
                    </li>
                    <li>
{*                        <a href="#" title="Modify system access levels"> *}
                            Permissions
{*                        </a> *}
                    </li>
                </ul>
            </li>
            {if (($level == 255) || ($strict != 'true'))}
                <li>
                    :: External ::
                    <ul>
                        <li>
                            <a href="/cpanel" title="">
                                cPanel
                            </a>
                        </li>
                        <li>
                            <a href="https://{$smarty.server.HTTP_HOST}:2083/frontend/x3/sql/PhpMyAdmin.html" title="">
                                DB
                            </a>
                        </li>
                        <li>
                            <a href="https://{$smarty.server.HTTP_HOST}:2083/frontend/x3/mail/pops.html" title="">
                                Email
                            </a>
                        </li>
                        <li>
                            <a href="https://{$smarty.server.HTTP_HOST}:2083/frontend/x3/stats/awstats_landing.html" title="">
                                Stats
                            </a>
                        </li>
                        <li>
                            <a href="https://{$smarty.server.HTTP_HOST}:2083/frontend/x3/cpanelpro/support.html" title="">
                                Support Request
                            </a>
                        </li>
                    {/if}
                </ul>
            </li>
        </ul>
    </div>
{/strip}
