{strip}

    {* Perfect Code Container *}
        <div id="id_containerPerfectCode">
            <a href="javascript:void(0);" onclick="JWSF_BlockToggle(this,'id_containerJasonWhiteSlimFrameInfo');return document.JWSF_RetVal" onmouseout="document.getElementById('id_perfectCodeImage').src='{config->w3perfect}';" onmouseover="document.getElementById('id_perfectCodeImage').src='{config->w3perfectOver}';">
                <img src="{config->w3perfect}" id="id_perfectCodeImage" title="Validate our hand-written code against THE Internet Standards!" alt="Validate our hand-written code against THE Internet Standards!" />
            </a>
            <div id="id_containerJasonWhiteSlimFrameInfo">
                <h6>This site meticulously hand-crafted to strict <a href="http://www.w3.org/Consortium/" rel="external">web standards</a> by <a href="http://JasonEdwardWhite.com">Jason Edward White of Marquette, MI</a> and is powered by <a href="http://JWSFCMS.com">JWSFCMS</a> &copy;2001-{$smarty.now|date_format:"%Y"}</h6>
                <ul>
                    <li>
                        <a href="http://validator.w3.org/check?uri={$smarty.const.PathCurrentRel|escape:'htmlall'}" rel="external" title="This page is valid XHTML 1.1!" >
                            <img src="/_img/jwsf/w3c-xhtml-1.1-valid-cms.png" alt="Valid XHTML 1.1" title="JWSF Generates Valid XHTML 1.1" />
                        </a>
                    </li>
                    <li>
                        <a href="http://jigsaw.w3.org/css-validator/validator?uri={$smarty.const.PathCurrentRel|escape:'htmlall'}" rel="external" title="This page is valid CSS 2.1!">
                            <img src="/_img/jwsf/w3c-css-2.1-valid-cms.png" alt="Valid CSS 2.1" title="JWSF Generates Valid CSS 2.1" />
                        </a>
                    </li>
                    <li>
                        <img alt="Creative Commons License" style="border-width:0" src="/_img/jwsf/cc.png"/>
                    </li>
                    <li>
                        <a href="http://validator.w3.org/feed/check.cgi?url={$smarty.const.PathCurrentRel|escape:'htmlall'}" rel="external" title="This page is valid CSS 2.1!">
                            <img src="/_img/jwsf/w3c-valid-rss.png" alt="Valid RSS" title="JWSF Generates Valid RSS Feeds" />
                        </a>
                    </li>
                    <li>
                        <a href="http://www.w3.org/2005/MWI/" rel="external" title="This page is cell phone, PDA, and handheld friendly!">
                            <img src="/_img/jwsf/w3c-mobile-friendly-valid-cms.png" alt="Mobile OK" title="JWSF Generates Mobile Friendly Code" />
                        </a>
                    </li>
                </ul>
            </div>
        </div>

{/strip}
