
{* /* $Revision: 1.1.1.1 $ */ *}

    <div id="id_containerSocialBar">
        <ul>
            <li>
                <a href="javascript:void(0);" title="Press Control+D to Bookmark This Page! Click the link if you are using Internet Explorer." onclick="JWSF_AddBookmark('{$smarty.const.CurrentProtocol}{$smarty.const.FQDN}?id={page->id}', '{page->titleFull}');">
                    <img src="/_img/icons/bookmark.gif" alt="Bookmark Icon" title="Press Control+D to Bookmark This Page! Click the link if you are using Internet Explorer." />
                    Bookmark It
                </a>
            </li>
            <li>
                <a href="/{page->semanticURI}?EmailPage&#61;true" title="Click here to have us send this page location to someone!">
                    <img src="/_img/icons/email.gif" alt="Email Icon" title="Click here to have us send this page location to someone!" />
                    Email It
                </a>
            </li>
            <li>
                <a href="{$smarty.const.CurrentProtocol}{$smarty.const.FQDN}/?id={page->id}" title="Right click and select 'Copy Link Location' or click on this link and copy the URL from the address bar to paste the permanent location (in case we change the title!) of this page into another application.  This would also be the appropriate URL to bookmark.">
                    <img src="/_img/icons/link-internal.gif" alt="PermaLink Icon" title="Right click and select 'Copy Link Location' or click on this link and copy the URL from the address bar to paste the permanent location (in case we change the title!) of this page into another application.  This would also be the appropriate URL to bookmark." />
                    PermaLink
                </a>
            </li>
            <li>
                {config->SiteTitleShort assign=pdfFile}
                {page->semanticURI assign=semanticURI}
                {config->PageDefaultId assign=defaultId}
                {page->id assign=pageId}
                {if ($semanticURI neq null) && ($defaultId != $pageId)}
                    <a href="/_pdf/{config->SiteTitleShort|regex_replace:"/[^\w\d\.]/":"-"}-{page->semanticURI|regex_replace:"/[^\w\d\.]/":"-"}.pdf" title="Click here to download a PDF version of this page!">
                {elseif $defaultId == $pageId}
                    <a href="/_pdf/{config->SiteTitleShort|regex_replace:"/[^\w\d\.]/":"-"}.pdf" title="Click here to download a PDF version of this page!">
                {else}
                    <a href="/_pdf/{config->SiteTitleShort|regex_replace:"/[^\w\d\.]/":"-"}-{page->id}.pdf" title="Click here to download a PDF version of this page!">
                {/if}
                    <img src="/_img/icons/pdf-small.png" alt="PDF Icon" title="Click here to open a PDF version of this page!" />
                    Print Version
                </a>
            </li>
    {* Removing until I decide whether or not I want it
            <li>
                <a href="/ page->semanticURI ?PrintPage&#61;true" title="Click here to view a printer friendly version of this page!">
                    <img src="/_img/icons/print.jpg" alt="Printer Icon" title="Click here to view a printer friendly version of this page!" />
                    Print It
                </a>
            </li>
    *}
            <li>
    {* javascript version            <a href="javascript:void(0);" title="Click here to share this with others!" onclick="JWSF_BlockToggle(this,'id_containerSharePage');return document.JWSF_RetVal"> *}
                <a href="/{page->semanticURI}?SharePage&#61;true" title="Click here to share this with others!">
                    <img src="/_img/icons/share.gif" alt="Sharing Icon" title="Click here to share this with others!" />
                    Share It
                </a>
            </li>
        </ul>
    </div>

    {if ($smarty.get.EmailPage eq true)
            && ($smarty.post.formEmailPageSubmit neq 'Send!')}
        <div id="id_containerEmailPage">
            <form method="post" action="{$smarty.const.PathCurrentRel|escape}">
                <h3>
                    Email the Link to This Page
                </h3>
                <label for="formEmailPageSender">
                    Your Name:
                </label>
                <input name="formEmailPageSender" type="text"  />
                <label for="formEmailPageRecipName">
                    Recipient Name:
                </label>
                <input name="formEmailPageRecipName" type="text"  />
                <label for="formEmailPageRecipAddress">
                    Recipient address:
                </label>
                <input name="formEmailPageRecipAddress" type="text"  />
                {* Check for Captcha Setting *}

                <button type="submit" name="formEmailPageSubmit" value="Send!">
                    Send!
                </button>
            </form>
        </div>
    {elseif $smarty.get.SharePage eq true}
        <div id="id_containerSharePage">
            <h3>
                Submit This Page to Your Favorite Social Sharing Sites
            </h3>
            <ul>
                <li>
                    <a href="http://del.icio.us/post?v&#61;2&amp;url&#61;{$smarty.const.CurrentProtocol}{$smarty.const.FQDN}/{page->semanticURI|escape}&amp;title&#61;{page->titleFull}" title="Share on Delicious">
                        <img src="/_img/icons/delicious.ico" alt="Delicious Icon" title="Post to Delicious" />
                        Delicious
                    </a>
                </li>
                <li>
                    <a href="http://digg.com/submit?url&#61;{$smarty.const.CurrentProtocol}{$smarty.const.FQDN}/{page->semanticURI|escape}&amp;title&#61;{page->titleFull}" title="Digg It">
                        <img src="/_img/icons/digg.ico" alt="Digg Icon" title="Digg It" />
                        Digg It
                    </a>
                </li>
                <li>
                    <a href="http://www.facebook.com/share.php?u&#61;{$smarty.const.CurrentProtocol}{$smarty.const.FQDN}/{page->semanticURI|escape}" title="Share with Facebook">
                        <img src="/_img/icons/facebook.ico" alt="Facebook Icon" title="Share this page on Facebook" />
                        Facebook
                    </a>
                </li>
                <li>
                    <a href="#" title="HCP..Coming Soon">
                        <img src="/_img/icons/jwsf.ico" alt="HCP" title="Share on HCP...Coming Soon" />
                        HCP
                    </a>
                </li>
                <li>
                    <a href="http://reddit.com/submit?url&#61;{$smarty.const.CurrentProtocol}{$smarty.const.FQDN}/{page->semanticURI|escape}&amp;title&#61;{page->titleFull}" title="Share with ReddIt">
                        <img src="/_img/icons/reddit.ico" alt="ReddIt Icon" title="Share with ReddIt" />
                        ReddIt
                    </a>
                </li>
            </ul>
            <ul>
                <li>
                    <a href="http://www.myspace.com/index.cfm?fuseaction&#61;postto&amp;t&#61;{page->titleFull}&amp;u&#61;{$smarty.const.CurrentProtocol}{$smarty.const.FQDN}/{page->semanticURI|escape}&amp;l&#61;2" title="Post to Your MySpace">
                        <img src="/_img/icons/myspace.gif" alt="MySpace Icon" title="Post to Your MySpace Account" />
                        MySpace
                    </a>
                </li>
                <li>
                    <a href="http://slashdot.org/slashdot-it.pl?op&#61;basic&amp;url&#61;{$smarty.const.CurrentProtocol}{$smarty.const.FQDN}/{page->semanticURI|escape}&amp;title&#61;{page->titleFull}" title="SlashDot It">
                        <img src="/_img/icons/slashdot.ico" alt="SlashDot Icon" title="SlashDot It" />
                        SlashDot
                    </a>
                </li>
                <li>
                    <a href="http://stumbleupon.com/submit?url&#61;{$smarty.const.CurrentProtocol}{$smarty.const.FQDN}/{page->semanticURI|escape}&amp;title&#61;{page->titleFull}" title="Stumble On It">
                        <img src="/_img/icons/stumbleupon.ico" alt="StumbleUpon Icon" title="Stumble On It" />
                        StumbleUpon
                    </a>
                </li>
                <li>
                    <a href="http://twitter.com/home?source&#61;ichc&amp;status&#61;%22{page->titleFull}%22%20-%20{$smarty.const.CurrentProtocol}{$smarty.const.FQDN}/{page->semanticURI|escape}" title="Share with Twitter">
                        <img src="/_img/icons/twitter.ico" alt="Twitter Icon" title="Click here to share this link on Twitter" />
                        Twitter
                    </a>
                </li>
            </ul>
        </div>
    {/if}
