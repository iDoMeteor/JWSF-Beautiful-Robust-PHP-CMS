{strip}

    {* /* $Revision: 1.1.1.1 $ */ *}

    {* Begin Module DIV *}
    <div class="c_ModuleAdmin">
        <h2>Site-Wide Configuration Editor</h2>
        <div id="id_containerConfiguration">
            <form method="post" action="{$smarty.const.PathCurrentRel|escape}">
                <fieldset>
                    <legend>Numeric Configuration</legend>
                        <dl>
                        {assign var="counter" value="0"}
                        {foreach from=$config key=label item=value}
                            <dt>
                                <label id="id_{$value[0]}Label" for="id_{$value[0]}">
                                    {$value[0]} ::&nbsp;
                                    <a href="javascript:void(0);" onclick="JWSF_BlockToggle(this,'id_containerDD_{$counter|string_format:"%.2d"}');return document.JWSF_RetVal">
                                        ?
                                    </a>
                                </label>
                                <input type="text" name="{$value[0]}" maxlength="5" title="{$value[0]}" value="{$value[1]}" id="id_{$value[0]}" />
                            </dt>
                            <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                            {assign var="counter" value="`$counter+1`"}
                                {$value[2]}
                            </dd>
                        {/foreach}
                    </dl>
                    <button name="submitConfig" value="true" type="submit">Update!</button>
                </fieldset>
            </form>
        </div>
    {* End Module DIV *}
    </div>
{/strip}
