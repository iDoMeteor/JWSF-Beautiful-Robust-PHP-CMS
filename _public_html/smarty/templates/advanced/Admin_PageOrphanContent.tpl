{strip}

    {* /* $Revision: 1.1.1.1 $ */ *}

    {* Begin Module DIV *}
    <div class="c_ModuleAdmin">
        <h2>Inactive Content Editor</h2>
        {if count($data) > 0}
            <form action="{$smarty.const.PathCurrentRel|escape}" method="post" enctype="application/x-www-form-urlencoded" id="id_pageEdit" title="Page Editor">
                <div id="id_containerPageEditContent">
                    {section loop=$data name=info}
                        {* Begin Existing Content Div *}
                        <div class="c_containerPageEditContentBlock">
                            <div class="c_containerPageEditContentHead">
                                <h2>Edit Block Header ::&nbsp;
                                    <a href="javascript:void(0);" onclick="JWSF_BlockToggle(this,'id_containerP_{$counter|string_format:"%.2d"}');return document.JWSF_RetVal">
                                        ?
                                    </a>
                                </h2>
                                <input name="content[{$smarty.section.info.index}][cid]" type="hidden" value="{$data[info].cid}" />
                                <input type="text" name="content[{$smarty.section.info.index}][header]" id="header_{$smarty.section.info.index}" value="{$data[info].header}" />
                                <p id="id_containerP_{$counter|string_format:"%.2d"}">
                                {assign var="counter" value="`$counter+1`"}
                                    This will appear above your content block to give the speed reader a clue as to the content that will be presented in the following block.  On collapsable pages, this will be all that shows to let the user know they may want to read the content block associated with the heading.
                                </p>
                            </div>
                            <div class="c_containerPageEditContentCritical">
                                <fieldset>
                                    <legend>
                                        <strong>Critical Data</strong> - CID: <strong>{$data[info].cid}</strong>
                                    </legend>
                                    <dl>
                                        <dt>
                                            <label for="content_{$smarty.section.info.index}">Section Content ::&nbsp;
                                                <a href="javascript:void(0);" onclick="JWSF_BlockToggle(this,'id_containerDD_{$counter|string_format:"%.2d"}');return document.JWSF_RetVal">
                                                    ?
                                                </a>
                                            </label>
                                            <textarea name="content[{$smarty.section.info.index}][content]" rows="38" cols="50" id="contents_{$smarty.section.info.index}">{$data[info].content|escape:'htmlall'}</textarea>
                                        </dt>
                                        <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                        {assign var="counter" value="`$counter+1`"}
                                            This is where you enter the textual content for your site.  Beware, special characters make break your page.. I am working on this.  For now, stay away from ampersands!  You can create paragraph breaks easily by putting '&lt;br /&gt;&lt;br /&gt;' (without the quotes) on a blank line in between the areas of text and it will create a blank line on the site.  I am working on dressing this up and making it easier.  XHTML 1.1 tags are allowed, but you had better be pretty sure that your syntax is correct!
                                        </dd>
                                    </dl>
                                </fieldset>
                        </div>
                        <div class="c_containerPageEditContentOptions">
                                <fieldset>
                                <legend>
                                    <strong>Options Area</strong> - CID: <strong>{$data[info].cid}</strong>
                                </legend>
                                    <dl>
                                        <dt>
                                            <label for="acsLow_{$smarty.section.info.index}">Access Level Low ::&nbsp;
                                                <a href="javascript:void(0);" onclick="JWSF_BlockToggle(this,'id_containerDD_{$counter|string_format:"%.2d"}');return document.JWSF_RetVal">
                                                    ?
                                                </a>
                                            </label>
                                            <select name="content[{$smarty.section.info.index}][acsLow]" id="acsLow_{$smarty.section.info.index}" title="Access Level Low">
                                                <option value="0"{if $data[info].acsLow == 0} selected="selected"{/if}> Public </option>
                                                {section name=so loop=$so_array}
                                                    <option value="{$so_array[so]}"{if $data[info].acsLow == $so_array[so]} selected="selected"{/if}>{$so_array[so]}</option>
                                                {/section}
                                            </select>
                                        </dt>
                                        <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                        {assign var="counter" value="`$counter+1`"}
                                            A user must be at least this level to access this particular content.  Public is 0, anything else will generally require a user to be logged in.
                                        </dd>
                                        <dt>
                                            <label for="acsHigh_{$smarty.section.info.index}">Access Level High ::&nbsp;
                                                <a href="javascript:void(0);" onclick="JWSF_BlockToggle(this,'id_containerDD_{$counter|string_format:"%.2d"}');return document.JWSF_RetVal">
                                                    ?
                                                </a>
                                            </label>
                                            <select name="content[{$smarty.section.info.index}][acsHigh]" id="acsHigh_{$smarty.section.info.index}" title="Access Level High">
                                                <option value="0"{if $data[info].acsHigh == 0} selected="selected"{/if}> Public Only </option>
                                                {section name=so loop=$so_array}
                                                    <option value="{$so_array[so]}"{if $data[info].acsHigh == $so_array[so]} selected="selected"{/if}>{$so_array[so]}</option>
                                                {/section}
                                            </select>
                                        </dt>
                                        <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                        {assign var="counter" value="`$counter+1`"}
                                            A user higher than this level will not see this content.  255 is the maximum value.
                                        </dd>
                                        <dt>
                                            <label for="so_{$smarty.section.info.index}">Sort Order ::&nbsp;
                                                <a href="javascript:void(0);" onclick="JWSF_BlockToggle(this,'id_containerDD_{$counter|string_format:"%.2d"}');return document.JWSF_RetVal">
                                                    ?
                                                </a>
                                            </label>
                                            <select name="content[{$smarty.section.info.index}][so]" id="so_{$smarty.section.info.index}" title="Sort Order">
                                                <option value="0"{if $data[info].so == 0} selected="selected"{/if}> Reverse Chronological </option>
                                                {section name=so loop=$so_array}
                                                    <option value="{$so_array[so]}"{if $data[info].so == $so_array[so]} selected="selected"{/if}>{$so_array[so]}</option>
                                                {/section}
                                            </select>
                                        </dt>
                                        <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                        {assign var="counter" value="`$counter+1`"}
                                            This is the sort order on the page.  Any sections with competing sort orders will be displayed alphabetically.
                                        </dd>
                                        <dt>
                                            <label for="con_parent_{$smarty.section.info.index}">Parent Page ::&nbsp;
                                                <a href="javascript:void(0);" onclick="JWSF_BlockToggle(this,'id_containerDD_{$counter|string_format:"%.2d"}');return document.JWSF_RetVal">
                                                    ?
                                                </a>
                                            </label>
                                            <select name="content[{$smarty.section.info.index}][pid]" id="con_{$smarty.section.info.index}" title="Parent Page">
                                                {if !empty($pagesActive)}
                                                    <optgroup label="Active Pages">
                                                    {section name=pdata loop=$pagesActive}
                                                        <option value="{$pagesActive[pdata].id}"{if $cpid == $pagesActive[pdata].id} selected="selected"{/if}>{$pagesActive[pdata].titleNav}</option>
                                                    {/section}
                                                    </optgroup>
                                                {/if}
                                                {if !empty($pagesInactive)}
                                                    <optgroup label="Inactive Pages">
                                                    {section name=pdata loop=$pagesInactive}
                                                        <option value="{$pagesInactive[pdata].id}"{if $cpid == $pagesInactive[pdata].id} selected="selected"{/if}>{$pagesInactive[pdata].titleNav}</option>
                                                    {/section}
                                                    </optgroup>
                                                {/if}
                                                {if !empty($pagesOrphan)}
                                                    <optgroup label="Orphan Pages">
                                                    {section name=pdata loop=$pagesOrphan}
                                                        <option value="{$pagesOrphan[pdata].id}"{if $cpid == $pagesOrphan[pdata].id} selected="selected"{/if}>{$pagesOrphan[pdata].titleNav}</option>
                                                    {/section}
                                                    </optgroup>
                                                {/if}
                                            </select>
                                        </dt>
                                        <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                        {assign var="counter" value="`$counter+1`"}
                                            If you change the parent page here, this section of content will move to that page immediately.
                                        </dd>
                                        <dt>
                                            <label for="active_{$smarty.section.info.index}">Active ::&nbsp;
                                                <a href="javascript:void(0);" onclick="JWSF_BlockToggle(this,'id_containerDD_{$counter|string_format:"%.2d"}');return document.JWSF_RetVal">
                                                    ?
                                                </a>
                                            </label>
                                            <select name="content[{$smarty.section.info.index}][active]" id="active_{$smarty.section.info.index}" title="Active">
                                                <option value="1"{if ($data[info].active == '1')} selected="selected"{/if}>True</option>
                                                <option value="0"{if ($data[info].active == '0')} selected="selected"{/if}>False</option>
                                            </select>
                                        </dt>
                                        <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                        {assign var="counter" value="`$counter+1`"}
                                            This defines whether or not this section will load with the page or not.  Setting this to false will cause the block to disappear from the page but still be editable here.
                                        </dd>
                                        <dt>
                                            <label for="cssClass_{$smarty.section.info.index}">CSS Class ::&nbsp;
                                                <a href="javascript:void(0);" onclick="JWSF_BlockToggle(this,'id_containerDD_{$counter|string_format:"%.2d"}');return document.JWSF_RetVal">
                                                    ?
                                                </a>
                                            </label>
                                            <input type="text" name="content[{$smarty.section.info.index}][cssClass]" id="cssClass_{$smarty.section.info.index}" value="{$data[info].cssClass}" />
                                        </dt>
                                        <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                        {assign var="counter" value="`$counter+1`"}
                                            If you would like to define a custom CSS Class for this section, you may do so here.
                                        </dd>
                                        <dt>
                                            <label for="cssID_{$smarty.section.info.index}">CSS ID ::&nbsp;
                                                <a href="javascript:void(0);" onclick="JWSF_BlockToggle(this,'id_containerDD_{$counter|string_format:"%.2d"}');return document.JWSF_RetVal">
                                                    ?
                                                </a>
                                            </label>
                                            <input type="text" name="content[{$smarty.section.info.index}][cssID]" id="cssID_{$smarty.section.info.index}" value="{$data[info].cssID}" />
                                        </dt>
                                        <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                        {assign var="counter" value="`$counter+1`"}
                                            If you would like to define a custom CSS ID for this section, you may do so here.
                                        </dd>
                                        <dt>
                                            <label for="imgURI_{$smarty.section.info.index}">Section Image ::&nbsp;
                                                <a href="javascript:void(0);" onclick="JWSF_BlockToggle(this,'id_containerDD_{$counter|string_format:"%.2d"}');return document.JWSF_RetVal">
                                                    ?
                                                </a>
                                            </label>
                                            <input type="text" name="content[{$smarty.section.info.index}][imgURI]" id="imgURI_{$smarty.section.info.index}" value="{$data[info].imgURI}" />
                                        </dt>
                                        <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                        {assign var="counter" value="`$counter+1`"}
                                            This is currently quite experimental.  You may attempt to enter the location of a image to be loaded with this content and see what happens. :)
                                        </dd>
                                        <dt>
                                            <label for="headerURI_{$smarty.section.info.index}">Section Link ::&nbsp;
                                                <a href="javascript:void(0);" onclick="JWSF_BlockToggle(this,'id_containerDD_{$counter|string_format:"%.2d"}');return document.JWSF_RetVal">
                                                    ?
                                                </a>
                                            </label>
                                            <input type="text" name="content[{$smarty.section.info.index}][headerURI]" id="headerURI_{$smarty.section.info.index}" value="{$data[info].headerURI}" />
                                        </dt>
                                        <dd id="id_containerDD_{$counter|string_format:"%.2d"}">
                                        {assign var="counter" value="`$counter+1`"}
                                            This is currently quite experimental.  You may attempt to enter the location of a url and see what happens. :)
                                        </dd>
                                    </dl>
                                    <h3>CAUTION! Deleted data CANNOT be retrieved!</h3>
                                    <p>Perhaps active=false until you are really sure?</p>
                                    <label for="delete_{$smarty.section.info.index}">Delete This Permanently</label>
                                    <input name="content[{$smarty.section.info.index}][delete]" id="delete_{$smarty.section.info.index}" type="checkbox" value="1" />
                                </fieldset>
                            {* End Existing Content Div *}
                            </div>
                            <div class="c_containerFormPageEditSubmit">
                                <button name="saveEdit" value="true" type="submit">Save!</button>
                            </div>
                        {* End Block Container*}
                        </div>
                    {/section}
                {* End current content outer container *}
                </div>
            <button name="saveEdit" value="true" type="submit">Save!</button>
            </form>
        {else}
            <h2>There is no inactive content!</h2>
        {/if}
    </div>
{/strip}
