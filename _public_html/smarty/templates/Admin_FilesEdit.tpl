{strip}

    {* /* $Revision: 1.1.1.1 $ */ *}
    
    {* TODO: Dupes, MD5 *}

    {if isset($smarty.post.assignment_deleteConfirmation)}
        {jwsfDisplay resource_name='Admin_FilesDeleteAssignment.tpl'}
    {elseif $smarty.post.action ne 'Update'
            && isset($smarty.post.editID)
            || isset($smarty.post.assignment_edit)
            }
        {jwsfDisplay resource_name='Admin_FilesEditAssignment.tpl'}
    {elseif isset($smarty.post.orphan_deleteConfirmation)}
        {jwsfDisplay resource_name='Admin_FilesDeleteOrphan.tpl'}
    {elseif isset($smarty.post.file_deleteConfirmation)}
        {jwsfDisplay resource_name='Admin_FilesDeleteOrphan.tpl'}
    {elseif isset($smarty.post.meta_deleteConfirmation)}
        {jwsfDisplay resource_name='Admin_FilesDeleteMeta.tpl'}
    {elseif isset($smarty.post.file_editValid)}
        {jwsfDisplay resource_name='Admin_FilesEditValid.tpl'}
    {elseif isset($smarty.post.file_deleteAllConfirmation)}
        {jwsfDisplay resource_name='Admin_FilesDeleteAll.tpl'}
    {elseif $smarty.get.action eq "edit-files"}
        {jwsfDisplay resource_name='Admin_FilesEditAll.tpl'}
    {/if}

{/strip}