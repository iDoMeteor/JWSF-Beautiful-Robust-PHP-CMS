{strip}
    {if $template == 'activationSuccess'}
        <h2>Thank you for activating your account!</h2>
        <p>You are now welcome to login and begin using the system.</p>
    {elseif $template == 'activationFailure'}
        <h2>You have failed to activate an account!</h2>
    {elseif $template == 'creationSuccess'}
        <h2>You have successfully created your account!</h2>
        <p>Please check your email for instructions on how to activate yourself!</p>
    {elseif $template == 'form'}
        {foreach from=$form item=tag}
            {$tag}
        {/foreach}
        <h6>* Denotes required information</h6>
    {/if}
{/strip}
