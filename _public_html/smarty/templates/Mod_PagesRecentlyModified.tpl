{strip}

    {* /* $Revision: 1.1.1.1 $ */ *}

    {php}
        $GLOBALS['jwsfPageModules']->loadModule('PagesRecentlyModified');
    {/php}


    {if !empty($RecentlyModified)}
        <div id="id_containerPagesRecentlyModified">
            <h3>Recent Updates</h3>
            <ul>
                {section loop=$RecentlyModified name=pageR step=-1}
                    <li>
                        <a href="{$RecentlyModified[pageR].href}" title="{$RecentlyModified[pageR].titlefull}">
                            {$RecentlyModified[pageR].titleNav}
                        </a>
                    </li>
                {/section}
            </ul>
        </div>
    {/if}

{/strip}
