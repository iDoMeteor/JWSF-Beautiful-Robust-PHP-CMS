{strip}

	{* Welcome Message *}
		{config->UsersAllow assign=usersAllow}
		{user->acslvl assign=userLevel}
		{config->UserShowInfo assign=showInfo}

		{if (($usersAllow == 'true')
		        && ($userLevel > 0)
		        && ($showInfo == 'true')
		        ) }

			<div id="id_containerUserInfo">
				<p>
					Welcome, {user->nameFirst} {user->nameLast}
				</p>
		    </div>
		{/if}

{/strip}
