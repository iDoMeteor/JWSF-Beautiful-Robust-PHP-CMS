{strip}

    {* /* $Revision: 1.1.1.1 $ */ *}

    {config->PageShowTItle assign=showTitle}
    {if $showTitle == 'true'}
        <h1>{page->titleFull}</h1>
    {/if}
    {section loop=$data name=info}
        <div class="c_contentHeader">{$data[info].header}</div>
        {if $data[info].imgURI != null}
            <div class="c_contentImage">
				<img src="{$data[info].imgURI}" alt="{$data[info].header|strip}" title="{$data[info].header|strip}" />
            </div>
		{/if}
        <div class="c_contentBody">{$data[info].content}</div>
    {/section}
    {config->SiteShowTemplateNo assign=showTemp}
    {if $showTemp == 'true'}
        <h5>Content Template 01</h5>
    {/if}
{/strip}
