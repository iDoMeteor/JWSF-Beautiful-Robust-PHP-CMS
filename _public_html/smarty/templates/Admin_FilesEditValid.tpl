{strip}

    {* /* $Revision: 1.1.1.1 $ */ *}

        {page->fileValid assign='file'}

        <div class="c_ModuleAdmin" id="id_containerEditFiles2">
            <h2>Edit a File</h2>
            <div id="id_containerFormFilesEdit">
                <form method="post" enctype="multipart/form-data" action="{$smarty.const.PathCurrentRel|escape}">
                    <fieldset>
                        <legend>Edit Valid File Details</legend>
                        <input type="hidden" name="fileMeta[0][id]" value="{$file.id}">
                        <input type="hidden" name="fileMeta[0][location]" value="{$file.location}">
                        <h3>
                            {$file.location}
                        </h3>
                        <label for="0">Replace file</label>
                        <input type="file" name="0" id="id_fileSrc" />
                        <label for="fileMeta[0][name]">Rename File</label>
                        <input type="text" name="fileMeta[0][name]" id="id_fileName" />
                        <label for="fileMeta[0][title]">Change Title</label>
                        <input type="text" name="fileMeta[0][title]" id="id_fileTitle" value="{$file.title}" />
                        <label for="fileMeta[0][description]">Alter Description</label>
                        <textarea name="fileMeta[0][description]" id="id_fileDescription">{$file.description}</textarea>
                        <br /><br />
                        <input type="submit" name="action" id="id_saveResource" value="Update" />
                    </fieldset>
                </form>
            </div>
        </div>
{/strip}