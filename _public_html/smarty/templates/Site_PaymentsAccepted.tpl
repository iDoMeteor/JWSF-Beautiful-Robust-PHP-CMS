{strip}

    {* Payment Logos *}
	    {config->SiteShowPaymentLogo assign=showPaymentLogo}
	    {if ($showPaymentLogo == 'true')}
            <div id="id_containerPaymentLogo">
                    <img src="{config->ImageCardsAccepted}" title="We accept Visa, MasterCard and American Express!" alt="We accept Visa, MasterCard and American Express!" />
            </div>
	    {/if}

{/strip}
