{strip}

    {* /* $Revision: 1.1.1.1 $ */ *}

    {if $smarty.post.action eq 'Next'}
        <div class="c_ModuleAdmin" id="id_containerAddFiles2">
            <h2>Add Files</h2>
            <div id="id_containerFormFilesAdd">
                <form method="post" enctype="multipart/form-data" action="{$smarty.const.PathCurrentRel|escape}">
                    <input type="hidden" name="filePageID" value="{$smarty.post.editID}" />
                    <fieldset>
                        {if $smarty.post.preExistingResource == 0}
                            <legend>Manifest Details</legend>
                            <label for="0">Browse for your file</label>
                            <input type="file" name="0" id="id_fileSrc" />
                            <label for="fileMeta[0][name]">Desired File Name with Extension (or blank for no change)</label>
                            <input type="text" name="fileMeta[0][name]" id="id_fileName" />
                            <label for="fileMeta[0][title]">Desired Title (in plain English)</label>
                            <input type="text" name="fileMeta[0][title]" id="id_fileTitle" />
                            <label for="fileMeta[0][description]">Short description (for rollover tip)</label>
                            <input type="text" name="fileMeta[0][description]" id="id_fileDescription" />
                        {elseif $smarty.post.preExistingResource == 1}
                            <legend>Select Existing File</legend>
                            <select name="fileExists" id="id_fileExists">
                                {page->files assign='files'}
                                {if count($files) > 0}
                                    {section loop=$files name=info}
                                        <option value="{$files[info].id}">{$files[info].title}</option>
                                    {/section}
                                {/if}
                            </select>
                        {/if}
                        {if (($smarty.post.blockAssociation eq "1")
                                && ($smarty.post.editID != 0)
                                )}
                            <label for="id_fileContentID">Select Associated Content Block</label>
                            <select name="fileContentID" id="id_fileContentID">
                                <option value="0"> --Page Only-- </option>
                                {if count($data) > 0}
                                    {section loop=$data name=info}
                                        <option value="{$data[info].cid}">{$data[info].header}</option>
                                    {/section}
                                {/if}
                            </select>
                        {/if}
                        <br /><br />
                        <input type="submit" name="action" id="id_saveResource" value="Save" />
                    </fieldset>
                </form>
            </div>
        </div>
    {elseif $smarty.get.action eq "add-files"}
        <div class="c_ModuleAdmin" id="id_containerAddFiles1">
            <h2>Add Files</h2>
            <div id="id_containerFormFilesAdd">
                <form method="post" action="{$smarty.const.PathCurrentRel|escape}">
                    <fieldset>
                        <legend>File Association</legend>
                        <label for="id_editID">Select Associated Page</label>
                        <select name="editID" id="id_editID">
                            <option value="0"> --Only Download Page-- </option>
                            {if !empty($pagesActive)}
                                <optgroup label="Active Pages">
                                {section name=pdata loop=$pagesActive}
                                    <option value="{$pagesActive[pdata].id}"{if $smarty.post.editID == $pagesActive[pdata].id} selected="selected"{/if}>{$pagesActive[pdata].titleNav}</option>
                                {/section}
                                </optgroup>
                            {/if}
                            {if !empty($pagesInactive)}
                                <optgroup label="Inactive Pages">
                                {section name=pdata loop=$pagesInactive}
                                    <option value="{$pagesInactive[pdata].id}"{if $smarty.post.editID == $pagesInactive[pdata].id} selected="selected"{/if}>{$pagesInactive[pdata].titleNav}</option>
                                {/section}
                                </optgroup>
                            {/if}
                            {if !empty($pagesOrphan)}
                                <optgroup label="Orphan Pages">
                                {section name=pdata loop=$pagesOrphan}
                                    <option value="{$pagesOrphan[pdata].id}"{if $smarty.post.editID == $pagesOrphan[pdata].id} selected="selected"{/if}>{$pagesOrphan[pdata].titleNav}</option>
                                {/section}
                                </optgroup>
                            {/if}
                        </select>
                        <p>Assign to specific content block?</p>
                        <input type="radio" name="blockAssociation" id="id_blockAssociation2" value="0" checked="checked" />
                        <label for="id_blockAssociation2">No</label>
                        <input type="radio" name="blockAssociation" id="id_blockAssociation1" value="1" />
                        <label for="id_blockAssociation1">Yes</label>
                        <p>New file or existing file?</p>
                        <input type="radio" name="preExistingResource" id="id_preExistingResource1" value="0" checked="checked" />
                        <label for="id_preExistingResource1">New</label>
                        <input type="radio" name="preExistingResource" id="id_preExistingResource2" value="1" />
                        <label for="id_preExistingResource2">Existing</label>
                        <br /><br />
                        <input type="submit" name="action" id="id_resourceAddNext" value="Next" />
                    </fieldset>
                </form>
            </div>
        </div>
    {/if}

{/strip}