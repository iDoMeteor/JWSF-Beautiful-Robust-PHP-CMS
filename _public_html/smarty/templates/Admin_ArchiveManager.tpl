{strip}

    {* /* $Revision: 1.1.1.1 $ */ *}

<div class="c_ModuleAdmin">
    <form method="post" action="{$smarty.const.PathCurrentRel|escape}">
        {if $smarty.get.action eq 'select-data'}
            <fieldset>
                <legend>
                    Select a data set to delete or restore
                </legend>
                {page->dataBackupFiles assign='files'}
                <table>
                    <tr class="c_odd">
                        <th colspan="4">File Name</th>
                    </tr>
                    <tr class="c_even">
                        <th>Restore</th>
                        <th>Time</th>
                        <th>Date</th>
                        <th>Delete</th>
                    </tr>
                {foreach key=id item=file from=$files}
                    <tr class="c_odd">
                        <td colspan="4">{$file}</td>
                    </tr>
                    {assign var='file' value=$file|replace:'JWSF-DB-':''}
                    {assign var='file' value=$file|replace:'.sql.gz':''}
                    <tr class="c_even">
                        <td>
                            <input type="image" name="restore-data" src="" title="" value="{$id}" />
                        </td>
                        <td>
                            {$file|date_format:"%H:%M:%S"}
                        </td>
                        <td>
                            {$file|date_format:"%A, %B %e, %Y"}
                        </td>
                        <td>
                            <input type="image" name="delete-data" src="" title="" value="{$id}" />
                        </td>
                    </tr>
                {foreachelse}
                    <tr>
                        <td colspan="3">
                            No database backups found!
                        </td>
                    </tr>
                {/foreach}
                </table>
            </fieldset>
    {elseif $smarty.get.action eq 'select-files'}
            <fieldset>
                <legend>
                    Select a file set to delete or restore
                </legend>
                {page->fileBackupFiles assign='files'}
                <table>
                    <tr class="c_odd">
                        <th colspan="4">File Name</th>
                    </tr>
                    <tr class="c_even">
                        <th>Restore</th>
                        <th>Time</th>
                        <th>Date</th>
                        <th>Delete</th>
                    </tr>
                {foreach key=id item=file from=$files}
                    <tr class="c_odd">
                        <td colspan="4">{$file}</td>
                    </tr>
                    {assign var='file' value=$file|replace:'JWSF-FS-':''}
                    {assign var='file' value=$file|replace:'.tar.gz':''}
                    <tr class="c_even">
                        <td>
                            <input type="image" name="restore-files" src="" title="" value="{$id}" />
                        </td>
                        <td>
                            {$file|date_format:"%H:%M:%S"}
                        </td>
                        <td>
                            {$file|date_format:"%A, %B %e, %Y"}
                        </td>
                        <td>
                            <input type="image" name="delete-files" src="" title="" value="{$id}" />
                        </td>
                    </tr>
                {foreachelse}
                    <tr>
                        <td colspan="3">
                            No file system backups found!
                        </td>
                    </tr>
                {/foreach}
                </table>
            </fieldset>
        {/if}
    </form>

</div>
{/strip}
