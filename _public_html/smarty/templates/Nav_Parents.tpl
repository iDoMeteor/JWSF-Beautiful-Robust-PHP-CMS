{strip}

    {* Bread Crumb *}
        {config->NavShowBreadCrumb assign=showBreadCrumb}
        {if (($showBreadCrumb == 'true') && !empty($navParents))}
            <div id="id_containerNavigationParents">
				<h4>Trail:</h4>
                <ul>
                    {foreach from=$navParents key=key item=value}
                        <li>
							{page->id assign=idCur}
							{if $value.id != $idCur}
                                <a class="c_button" href="{$value.href}">
							{/if}
                            {$value.titleNav}
							{if $value.id != $idCur}
                                </a>
							{/if}
                        </li>
                    {/foreach}
                </ul>
            </div>
        {/if}

{/strip}
