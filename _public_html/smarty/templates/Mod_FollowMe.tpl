{strip}

    {* /* $Revision: 1.3 $ */ *}

    {php}
        $GLOBALS['jwsfPageModules']->loadModule('FollowMe');
    {/php}

        <div id="id_containerFollowMe">
            <h3>Follow Me</h3>
            {* loop *}
            {page->comments assign=comments}
            {foreach from=$followMeTypes key=tk item=ti}

                <h4>{$ti}</h4>
                <ul>
                    {foreach from=$followMeData key=dk item=di}
                        {if $tk == $di.idType}
                            <li>
                                <a title="{$di.description}" href="{$di.link}">
                                    {$di.title}
                                </a>
                            </li>
                        {/if}
                    {/foreach}
                </ul>
            {/foreach}
        </div>
{/strip}
