{strip}

    {* /* $Revision: 1.1.1.1 $ */ *}

    {if $smarty.post.linkNext eq 'Next'}
        <div class="c_ModuleAdmin" id="id_containerAddLink2">
            <h2>Add Links</h2>
            <div id="id_containerFormLinksAdd">
                <form method="post" action="{$smarty.const.PathCurrentRel|escape}">
                    <input type="hidden" name="linkPageID" value="{$smarty.get.editID}" />
                    <fieldset>
                        {if $smarty.post.preExistingResource == 1}
                            <legend>Manifest Details</legend>
                            <label for="linkSrc">Enter your link address</label>
                            <input type="text" name="linkSrc" id="id_linkSrc" />
                            <label for="linkName">Desired Link Name (or blank to use link address)</label>
                            <input type="text" name="linkName" id="id_linkName" />
                            <label for="linkTitle">Short description (for rollover tip)</label>
                            <input type="text" name="linkTitle" id="id_linkTitle" />
                        {elseif $smarty.post.preExistingResource == 0}
                            <legend>Select Existing Link</legend>
                            <select name="linkExists" id="id_linkExists">
                                {* loop *}
                                    <optgroup label="Available_Directory">
                                        {* loop *}
                                            <option value="link_id">title</option>
                                            <option value="link_name.ext">link_name.ext</option>
                                            <option value="link_name.ext">link_name.ext</option>
                                        {* /loop *}
                                    </optgroup>
                                {* /loop *}
                            </select>
                        {/if}
                        {if (($smarty.post.blockAssociation eq "1")
                                && ($smarty.post.editID != 0)
                                )}
                            <label for="id_linkPage">Select Associated Content Block</label>
                            <select name="contentBlock" id="id_contentBlock">
                                <option value="0"> --Page Only-- </option>
                                {if count($data) > 0}
                                    {section loop=$data name=info}
                                        <option value="{$data[info].cid}">{$data[info].header}</option>
                                    {/section}
                                {/if}
                            </select>
                        {/if}
                        <br /><br />
                        <input type="submit" name="saveResource" id="id_saveResource" value="Save" />
                    </fieldset>
                </form>
            </div>
        </div>
    {elseif $smarty.get.action eq "add-links"}
        <div class="c_ModuleAdmin" id="id_containerAddLinks1">
            <h2>Add Links</h2>
            <div id="id_containerFormLinksAdd">
                <form method="post" action="{$smarty.const.PathCurrentRel|escape}">
                    <fieldset>
                        <legend>Link Association</legend>
                        <label for="id_editID">Select Associated Page</label>
                        <select name="editID" id="id_editID">
                            <option value="0"> --Only Links Page-- </option>
                            {if !empty($pagesActive)}
                                <optgroup label="Active Pages">
                                {section name=pdata loop=$pagesActive}
                                    <option value="{$pagesActive[pdata].id}"{if $smarty.post.editID == $pagesActive[pdata].id} selected="selected"{/if}>{$pagesActive[pdata].titleNav}</option>
                                {/section}
                                </optgroup>
                            {/if}
                            {if !empty($pagesInactive)}
                                <optgroup label="Inactive Pages">
                                {section name=pdata loop=$pagesInactive}
                                    <option value="{$pagesInactive[pdata].id}"{if $smarty.post.editID == $pagesInactive[pdata].id} selected="selected"{/if}>{$pagesInactive[pdata].titleNav}</option>
                                {/section}
                                </optgroup>
                            {/if}
                            {if !empty($pagesOrphan)}
                                <optgroup label="Orphan Pages">
                                {section name=pdata loop=$pagesOrphan}
                                    <option value="{$pagesOrphan[pdata].id}"{if $smarty.post.editID == $pagesOrphan[pdata].id} selected="selected"{/if}>{$pagesOrphan[pdata].titleNav}</option>
                                {/section}
                                </optgroup>
                            {/if}
                        </select>
                        <p>Assign to specific content block?</p>
                        <input type="radio" name="blockAssociation" id="id_blockAssociation2" value="0" checked="checked" />
                        <label for="id_blockAssociation2">No</label>
                        <input type="radio" name="blockAssociation" id="id_blockAssociation1" value="1" />
                        <label for="id_blockAssociation1">Yes</label>
                        <p>New file or existing file?</p>
                        <input type="radio" name="preExistingResource" id="id_preExistingResource1" value="1" checked="checked" />
                        <label for="id_preExistingResource1">New</label>
                        <input type="radio" name="preExistingResource" id="id_preExistingResource2" value="0" />
                        <label for="id_preExistingResource2">Existing</label>
                        <br /><br />
                        <input type="submit" name="action" id="id_resourceAddNext" value="Next" />
                    </fieldset>
                </form>
            </div>
        </div>
    {/if}

{/strip}