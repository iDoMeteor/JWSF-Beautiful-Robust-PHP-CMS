{strip}

    {* /* $Revision: 1.1.1.1 $ */ *}

    {if $smarty.get.action eq 'add-files'}
        {jwsfDisplay resource_name='Admin_FilesAdd.tpl'}
    {elseif $smarty.get.action eq 'add-links'}
        {jwsfDisplay resource_name='Admin_LinksAdd.tpl'}
    {elseif $smarty.get.action eq 'edit-files'}
        {jwsfDisplay resource_name='Admin_FilesEdit.tpl'}
    {elseif $smarty.get.action eq 'edit-links'}
        {jwsfDisplay resource_name='Admin_LinksEdit.tpl'}
    {/if}
    
{/strip}