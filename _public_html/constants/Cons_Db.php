<?php

/***** Database Connection Parameters ******************************************
 *
 *  $Revision: 1.7 $
 *
 *  This file, aside from themes, images, etc.. should be the only file in the
 *  JWSF that ever needs editing, other than during the development process.
 *  What this means is that this file should either be read-only, or in a
 *  location not accessed by the upgrader, so that we don't ever have to worry
 *  about overwriting configuration files.
 *
 *  Any variable 'constants' are now being moved to the database.
 *
 ******************************************************************************/


// Site uses database
DEFINE ('UseDb', true);
// Your mySQL server host address
DEFINE ('dbHost', "localhost");
// The name of the database to use
DEFINE ('dbName', "lookinto_dev");
// Your database username
DEFINE ('dbUser', "lookinto_scripty");
// Your database password
DEFINE ('dbPass', "mysqlDBrox");


?>
