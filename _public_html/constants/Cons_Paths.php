<?php

/*******************************************************************************
 *  $Revision: 1.2 $
 *
 ******************************************************************************/


/***** Current Protocol *****/

if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == "on"))
	DEFINE ('CurrentProtocol', 'https://');
else
	DEFINE ('CurrentProtocol', 'http://');


/***** Current FQDN *****/

// Fix borken HTTP_HOSTS
if (isset($_SERVER['HTTP_X_FORWARDED_HOST'])) {

	if ($pos = strpos($_SERVER['HTTP_X_FORWARDED_HOST'], ',')) {
		// Get first result from forwarded host list
		$forwarded = substr ($_SERVER['HTTP_X_FORWARDED_HOST'], 0, $pos);
	} else {
		// There was only one in the list
		$forwarded = $_SERVER['HTTP_X_FORWARDED_HOST'];
	}

	DEFINE ('FQDN', $forwarded);

} else {

	// Standard environment
    if (isset($_SERVER['HTTP_HOST'])) {
    	DEFINE ('FQDN', $_SERVER['HTTP_HOST']);
    } elseif (isset($_ENV['HTTP_HOST'])) {
    	DEFINE ('FQDN', $_ENV['HTTP_HOST']);
    } else {
        die ('Cannot determine http host!');
    }
}


/***** Current Relative URL *****/
// This needs to be fixed for blink
    if (isset($_SERVER['REQUEST_URI'])) {
    	DEFINE ('RelativeURL', $_SERVER['REQUEST_URI']);
    } elseif (isset($_ENV['REQUEST_URI'])) {
    	DEFINE ('RelativeURL', $_ENV['REQUEST_URI']);
    } else {
        die ('Cannot determine request uri!');
    }


/***** Complete Root URL *****/
DEFINE ('RootURL', CurrentProtocol . FQDN);


/***** Current Complete URL *****/
DEFINE ('PathCurrentRel', CurrentProtocol . FQDN . RelativeURL);


/***** Current Absolute Filesystem Location *****/
DEFINE ('PathCurrentAbs', dirname($_SERVER['SCRIPT_FILENAME']));



?>
