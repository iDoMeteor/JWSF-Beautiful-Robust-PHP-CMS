<?php

/********************** JWSF Smarty Form Elements ******************************
 *
 *  These form element functions are meant to be called as a precursor to the
 *  display of the Smarty template.  Each function follows the following steps
 *  in order:
 *      *)  Accept a list of optional and mandatory parameters
 *      *)  Validate those parameters (jwsfFormValidation.php required)
 *      *)  Check $_POST for existence of array element of the same name
 *              and set as default value if true and valid
 *      *)  Register itself with $GLOBALS['smarty'] with the name it was passed
 *              as a required parameter.
 *
 *  ** Important Restrictions **
 *      $name/$id fields limited to 246 characters because that is ridiculously
 *          long enough!  Also, we impose a limit of 255 characters to CSS ID
 *          tags and we add three in most functions, and 9 in the label func, so
 *          246 is all you get for $name, $for, and $id values.  And, if you
 *          actually use anywhere near that you should be slapped with a fish.
 *      $maxlength default is 255 characters, and the maximum is 320 (which also
 *          just happens to be the maximum length of a valid email address)
 *      Absolutely ALL other text fields are limited to 255 characters except
 *          for the which creates a text area allows 65,535 characters.  Any
 *          violations of these rules will result in the value violated being
 *          null'd out.
 *      Size, maxlength, and tabindex fields are all limited to 255
 *
 *  <form> and <button> may not conform to these rules, we'll see.
 *
 *  Mandatory parameters will be described in each function, as will usage
 *  examples and any other pertinent information.
 *
 *  There is not a whole lot of error checking incorporated yet, since we assume
 *  that you at least know a little bit about what your doing.  ;>  This feature
 *  will be implemented soon though, and make sure fields are of the right type
 *  and size, etc..  As for the questionable usage of directly accessing $_POST
 *  variables, the base JWSF system has already sanitized all incoming data
 *  buffers (which may not be working 100% so throwing in some htmlentities).
 *
 *  Attributes are generally assigned in the order of top priority followed by
 *  most used, or that _should_ be used most!  :)  Since everything has a
 *  default value predetermined, you only have to pass values as far along the
 *  chain as you want.. but the deeper down the chain you get, the more you
 *  should begin to wonder what you are doing there because there is probably
 *  an easier/shorter solution that getting all the way to the class attribute.
 *
 *  TODO:
 *      Create a cheat sheet
 *      Move limits to constants
 *      Tighten up validation to ensure that only 100% valid code may be
 *          generated
 *
 ******************************************************************************/

// Pre-Defined Tables for Form Elements
DEFINE ('TableMonths', 'JWSF_predefMonths');


/***** jwsfGenerateForm - Generate Form Open Tag *******************************
 *
 *  This function returns the opening form tag and provides defaults for all
 *  values except for CSS ID.  No ID attribute will be output if one is not
 *  provided during the function call.
 *
 *  Mandatory Parameters:
 *      None
 *
 *  Default values are as follows:
 *      $id     =   null
 *      $title  =   'User Input Form'
 *      $acion  =   $REQUEST['URI'] (self, with all GET parameters)
 *      $class  =   null
 *      $method =   'post'
 *      $enctype =  'application/x-www-form-urlencoded'
 *
 *  If you wish to change any of the defaults, then all values must be passed
 *  up to and including the last non-default value.  ie; if you wish to change
 *  the form method, you must use 'jwsfGenerateForm ($id, $title, $method)'.
 *
 *  Example Usage:
 *      $GLOBALS['smarty']->assign ('smartyVarName',
 *          jwsfGenerateForm (
 *                              $id,
 *                              $title,
 *                              $action,
 *                              $class,
 *                              $method,
 *                              $enctype
 *                            ));
 *
 *  Requires:
 *      Use of jwsfCloseFieldset or manually closing the <form> tag
 ******************************************************************************/

function jwsfGenerateForm (
                            $id = null,
                            $title = 'User Input Form',
                            $action = null,
                            $class = null,
                            $method = 'post',
                            $enctype = 'application/x-www-form-urlencoded'
                            ) {
/*
Taking this out with new smarter PathCurrentRel
    if (isset($_REQUEST['url']))
        $defaultAction = PathCurrentRel . $_GET['url'];
    elseif (isset($_GET['id']))
        $defaultAction = PathCurrentRel . '?id=' . $_GET['id'];
    elseif (isset($_GET['mod']))
        $defaultAction = PathCurrentRel . '?mod=' . $_GET['mod'];
    else
        $defaultAction = PathCurrentRel;
*/
    // The default form action is to submit to itself
	$action = (($action == NULL) || (strlen($action) > 255))
        ? htmlentities(PathCurrentRel) : htmlentities($action);

    // Contstruct the form tag
	$retVal = '<form';
	if (($id != null) && (strlen($id) < 246))
        $retVal .= ' id="' . $id . '"';
	if (($title != null) && (strlen($title) < 255))
    	$retVal .= ' title="' . $title . '"';
	$retVal .= ' action="' . $action . '"';
	if (($class != null) && (strlen($class) < 255))
        $retVal .= ' class="' . $class . '"';
    if (($method == 'post') || ($method == 'get'))
    	$retVal .= ' method="' . $method . '"';
    if (($enctype == 'application/x-www-form-urlencoded')
            || ($enctype == 'multipart/form-data'))
        $retVal .= ' enctype="' . $enctype . '"';
    $retVal .= '>';


	return $retVal;
}


/***** jwsfCloseForm - Generate Closing Form Tag *******************************
 *
 *  This may seem extraneous, but it makes for good continuity and I like that.
 *
 *  Example Usage:
 *      $GLOBALS['smarty']->assign ('smartyVarName', jwsfCloseForm());
 ******************************************************************************/

function jwsfCloseForm () {
	return '</form>';
}


/***** jwsfGenerateFieldsetWithLegend ******************************************
 *
 *  This function returns an opening fieldset tag along with a complete legend
 *  if one is provided.
 *
 *  Mandatory Parameters:
 *      None
 *
 *  Default Values:
 *      $legend =   null (returns only legend)
 *
 *  Example Usage:
 *      $GLOBALS['smarty']->assign ('smartyVarName',
 *           jwsfGenerateFieldsetWithLegend($legend));
 *
 *  Requires:
 *      Use of jwsfCloseFieldset or manually closing the <fieldset> tag
 ******************************************************************************/

function jwsfGenerateFieldsetWithLegend (
                                            $legend = null
                                            ) {

    // Construct the fieldset
	$retVal = '<fieldset>';

    // Construct the legend, if supplied
    if (($legend != null) && (strlen($legend) < 255))
        $retVal .= '<legend>' . $legend . '</legend>';

    return $retVal;
}


/***** jwsfCloseFieldset - Generate Closing Fieldset Tag ***********************
 *
 *  This may seem extraneous, but it makes for good continuity and I like that.
 *
 *  Example Usage:
 *      $GLOBALS['smarty']->assign ('smartyVarName', jwsfCloseFieldset());
 ******************************************************************************/

function jwsfCloseFieldset () {
	return '</fieldset>';
}


/***** jwsfGenerateLabel *******************************************************
 *
 *  This function returns a complete label for ('id_' . $for) and with CSS id of
 *  ('id_label_' . $for).  I did allow for more control than that, however, I
 *  decided to revoke those privileges.  If you want to style labels as a class,
 *  I suggest that you style _all_ label tags that are children of this form, or
 *  of the fieldset if you require finer control.  As for the naming of the
 *  $for, I have decided that you must use the formula that the rest of this
 *  file follows or hax0r this file up urself.
 *
 *  Mandatory Parameters:
 *      $for
 *      $label
 *
 *  Default Values:
 *      $for        =   null    (this is the name used to generate the
 *                                  element which this describes)
 *                              (returns false)
 *      $label      =   null    (this is the content for the label tag)
 *                              (returns false)
 *
 *  Example Usage:
 *      $GLOBALS['smarty']->assign ('smartyVarName',
 *           jwsfGenerateLabel (
 *                                      $for,
 *                                      $label
 *                                    ));
 *          * Denotes Required Parameters
 *
 *  Requires:
 *      The $for variable should be the same as used to create the element
 *          that this label describes
 *
 *  Notes:
 *
 *  TODO:
 *
 ******************************************************************************/

function jwsfGenerateLabel (
                                $for = null,
                                $label = null
                                ) {

    $id = null;

    // Check mandatory values
    if (($for == null) || (strlen($for) > 246))
        return false;

    if (($label == null) || (strlen($label) > 255))
        return false;

    // Format $id
    $id = $for . 'Label';
    if (substr($id, 0, 3) != 'id_')
        $id = 'id_' . $id;

    // Construct label tag
    $retVal = '<label for="id_' . $for . '" id="' . $id . '">';
    $retVal .= $label;
    $retVal .= '</label>';

    return $retVal;
}



/***** jwsfGenerateInputBox ****************************************************
 *
 *  This function returns a complete input tag of type text, hidden, or password
 *  and checks for a value already submitted via post of the same name
 *  ($_POST[$name]) to use as the default value (except for hidden fields, to
 *  prevent tampering).  If no value is passed for the CSS ID, it will be
 *  created as 'id_' . $name.  The default is to create a text field, so you
 *  can pass a null value there if you want a text box. This function will not
 *  put extraneous tags on hidden or password fields fields, although we will
 *  invalidate any passwords over 254 characters.
 *
 *  Mandatory Parameters:
 *      $name
 *
 *  Default Values:
 *      $name       =   null    (returns false)
 *      $type       =   'text'  (must be text, password, or hidden)
 *      $maxlength  =   254     (only applied to type text)
 *      $title      =   'Please enter your response here'
 *                              (not applied to type hidden)
 *      $value      =   null or $_POST[$name]
 *                              (type hidden or password will NOT
 *                                  be read from $_POST)
 *                              (max length for type hidden is 255)
 *      $id         =   'id_' . $name
 *                              (not applied to type hidden)
 *      $size       =   null    (if not between 1 and 255 will be set to null)
 *                              (not applied to type hidden)
 *      $class      =   null    (not applied to type hidden)
 *
 *  Example Usage:
 *      $GLOBALS['smarty']->assign ('smartyVarName',
 *           jwsfGenerateInputBox(
 *                                      *$name,
 *                                      $type,
 *                                      $maxlength,
 *                                      $title,
 *                                      $value,
 *                                      $id,
 *                                      $size,
 *                                      $class
 *                                    ));
 *          * Denotes Required Parameters
 *
 *  Requires:
 *
 *  TODO:
 *      Additional attributes have not been included because if you are going
 *      to use disabled, read-only, etc.. then you should probably be doing it
 *      with Javascript anyways.
 *
 ******************************************************************************/

function jwsfGenerateInputBox (
                                $name = null,
                                $type = 'text',
                                $maxlength = 255,
                                $title = 'Please enter your response here',
                                $value = null,
                                $id = null,
                                $size = null,
                                $class = null
                                ) {

    // Check for mandatory parameters
    if (($name == null) || (strlen($name) > 246))
        return false;

    // Reset to default if type is invalid
    if (!(($type == 'text')
            || ($type == 'hidden')
            || ($type == 'password')))
        $type = 'text';

    // Make sure maxlength is an integer between 1 and 320

    if ((($type == 'text') || ($type == 'password'))
            && (($maxlength == null)
            || ($maxlength < 1)
            || ($maxlength > 320)))
        $maxlength = 255;
    elseif (($type != 'text') && ($type != 'password'))
        $maxlength = null;

    // Check title length
    if (($type != 'hidden') && ($title == null) || (strlen($title) > 255))
        $title = 'Please enter your response here';
    elseif (($title != null) && ($type == 'hidden'))
        $title = null;

    // Set default value if already submitted and type is text
    if (($type == 'text')
            && isset($_POST[$name])
            && (strlen($_POST[$name]) <= $maxlength))
        $value = htmlentities($_POST[$name]);
    // or check value passed by script
    elseif (($type == 'text')
            && isset($value)
            && (strlen($value) > 255))
        $value = null;
    // If type hidden, ensure a value is set
    elseif (($type == 'hidden')
            && ($value == null)
            || (strlen($value) > 255))
        return false;
    // Ensure that we are not filling in a password field
    elseif (($type == 'password')
            && ($value != null))
        $value = null;

    // Set default id for text and password fields
    if (($id == null)
            || (strlen($id) > 255)
            && ($type != 'hidden'))
        $id = 'id_' . $name;
    elseif (($id != null)
            && ($type == 'hidden'))
        $id = null;

    // Make sure size is an integer between 1 and 255
    if (($size != null)
            && !is_int($size)
            || ($size < 1)
            || ($size > 255)
            || ($type == 'hidden'))
        $size = null;

    // Check class for text and password fields
    if (($class != null)
            && (strlen($class) > 255)
            || ($type == 'hidden'))
        $class = null;


    // Construct input tag
	$retVal = '<input';
	$retVal .= ' name="' . $name . '"';
	if ($type != 'hidden')
        $retVal .= ' tabindex="' . $GLOBALS['tabindex']++ . '"';
    $retVal .= ' type="' . $type . '"';
	if ($maxlength != null)
    	$retVal .= ' maxlength="' . $maxlength . '"';
	if ($title != null)
    	$retVal .= ' title="' . $title . '"';
	if ($value != null)
        $retVal .= ' value="' . $value . '"';
	if ($id != null)
    	$retVal .= ' id="' . $id . '"';
	if ($size != null)
        $retVal .= ' size="' . $size . '"';
	if ($class != null)
        $retVal .= ' class="' . $class . '"';
	$retVal .= ' />';

	return $retVal;
}


/***** jwsfGenerateInputArea ***************************************************
 *
 *  This function returns a complete textarea tag and checks for a value
 *  already submitted via post of the same name ($_POST[$name]) to use as the
 *  default value.  If no value is passed for the CSS ID, it will be created as
 *  id_$name.
 *
 *  Mandatory Parameters:
 *      $name
 *
 *  Default Values:
 *      $name       =   null    (returns false)
 *      $rows       =   10
 *      $cols       =   40
 *      $title      =   'Please enter your response here'
 *      $value      =   null or $_POST[$name]
 *      $id         =   'id_' . $name
 *      $class      =   null
 *
 *  Example Usage:
 *      $GLOBALS['smarty']->assign ('smartyVarName',
 *           jwsfGenerateInputArea(
 *                                      *$name,
 *                                      $rows,
 *                                      $cols,
 *                                      $title,
 *                                      $value,
 *                                      $id,
 *                                      $class
 *                                    ));
 *          * Denotes Required Parameters
 *
 *  Requires:
 *      That $rows > 1 and <= 200
 *      That $cols > 5 and <= 200
 *
 *  TODO:
 *      Additional attributes have not been included because if you are going
 *      to use disabled, read-only, etc.. then you should probably be doing it
 *      with Javascript anyways.
 *
 ******************************************************************************/

function jwsfGenerateInputArea (
                                        $name = null,
                                        $rows = 10,
                                        $cols = 40,
                                        $title = 'Please enter your response here',
                                        $value = null,
                                        $id = null,
                                        $class = null
                                        ) {

    // Check for mandatory parameters
    if (($name == null) || (strlen($name) > 246)) return false;
    if (!is_int($rows) || ($rows < 2) || ($rows > 200)) return false;
    if (!is_int($cols) || ($cols < 6) || ($cols > 200)) return false;

    // Set default id
    if ($id == null)
        $id = 'id_' . $name;

    // Set default value if already submitted
    if (isset($_POST[$name]) && (strlen($_POST[$name]) < 65535))
        $value = htmlentities($_POST[$name]);

    // Construct textarea tag
	$retVal = '<textarea';
	$retVal .= ' name="' . $name . '"';
    $retVal .= ' tabindex="' . $GLOBALS['tabindex']++ . '"';
	$retVal .= ' rows="' . $rows . '"';
    $retVal .= ' cols="' . $cols . '"';
	$retVal .= ' id="' . $id . '"';
	if ($class != null)
        $retVal .= ' class="' . $class . '"';
	$retVal .= '>';
	if ($value != null)
        $retVal .= $value;
	$retVal .= '</textarea>';

	return $retVal;
}


/***** jwsfGenerateCheckRadio **************************************************
 *
 *  This function returns a complete input tag of type checkbox or radio  and
 *  checks for a value already submitted via post of the same name
 *  ($_POST[$name]) to determine if the box has been user selected before.  If
 *  the 'multiple' flag is passed as true then the function will add '[]' to the
 *  end of the name attribute's value to create a $_POST[$name][] array.  Also
 *  if 'multiple' flag is set to true, this array will be cycled through to see
 *  if multiple boxes have been user selected on a previous submission.
 *  If no value is passed for the CSS ID, it will be created as 'id_' . $name.
 *
 *  Mandatory Parameters:
 *      $name
 *      $value
 *
 *  Default Values:
 *      $name       =   null        (returns false)
 *      $value      =   null        (required, not modifiable by $_POST)
 *                                  (limited to 255 characters)
 *      $type       =   'checkbox'  (must be checkbox or radio)
 *      $title      =   'Please enter your response here'
 *      $checked    =   false       (true, false, or null should be passed)
 *      $multiple   =   false       (true, false, or null should be passed)
 *                                  (only applied to type checkbox)
 *      $id         =   'id_' . $name
 *      $class      =   null
 *
 *  Example Usage:
 *      $GLOBALS['smarty']->assign ('smartyVarName',
 *           jwsfGenerateInputTextBox(
 *                                      *$name,
 *                                      *$value,
 *                                      $type,
 *                                      $title,
 *                                      $checked,
 *                                      $multiple,
 *                                      $id,
 *                                      $class
 *                                    ));
 *          * Denotes Required Parameters
 *
 *  Requires:
 *      Multiple checkboxes with the same name should be named 'varName', the
 *          script will add the '[]'s for you if you specify 'multiple' as true.
 *      Only 255 items will be supported in the $_POST array if the type is
 *          checkbox to prevent buffer overflow attempts.  Anything more than
 *          that will return false.
 *
 *  TODO:
 *      Additional attributes have not been included because if you are going
 *      to use disabled, read-only, etc.. then you should probably be doing it
 *      with Javascript anyways.
 *      Add ability to send a label value in to be wrapped around the output tag
 *
 ******************************************************************************/

function jwsfGenerateCheckRadio (
                                    $name = null,
                                    $value = null,
                                    $type = 'checkbox',
                                    $title = 'Please make a selection',
                                    $checked = false,
                                    $multiple = false,
                                    $id = null,
                                    $class = null
                                    ) {

    // Check for mandatory parameters
    if (($name == null) || (strlen($name) > 246))
        return false;
    if (($value == null) || (strlen($value) > 255))
        return false;

    // If type is not valid, set default
    if (!(($type == 'checkbox') || ($type == 'radio')))
        $type = 'checkbox';

    // Set default id
    if ($id == null)
        $id = 'id_' . $name;

    // Set checked status to true if $value is contained in $_POST
    //  Need to fix for multiples
    if (isset($_POST[$name])
                && ($_POST[$name] == $value))
            $checked = true;
    elseif (!empty($_POST))
            $checked = false;

    // Construct input tag
	$retVal = '<input';
	$retVal .= ' name="' . $name . '"';
    $retVal .= ' value="' . $value . '"';
    $retVal .= ' tabindex="' . $GLOBALS['tabindex']++ . '"';
    if ($checked == true)
        $retVal .= ' checked="checked"';
    $retVal .= ' type="' . $type . '"';
	$retVal .= ' title="' . $title . '"';
	$retVal .= ' id="' . $id . '"';
	if ($class != null)
        $retVal .= ' class="' . $class . '"';
	$retVal .= ' />';

	return $retVal;
}


/***** jwsfGenerateInputButton *************************************************
 *
 *  This function returns a complete input tag of type button, image, or reset.
 *  If no value is passed for the CSS ID, it will be created as 'id_' . $name.
 *
 *  Mandatory Parameters:
 *
 *  Default Values:
 *      $name       =   'submit'
 *      $value      =   'Submit'
 *      $type       =   'submit'    (must be submit, reset, image, [ack] button)
 *      $title      =   'Submit this form'
 *      $src        =   null        (or valid URI to image which will
 *                                      be checked for existence or null'd out)
 *                                  (only relative URIs are supported, offsite
 *                                      submit images should never be used)
 *      $alt        =   'Submit'    (only applied to type image)
 *      $id         =   'id_' . $name
 *      $class      =   null
 *
 *  Example Usage:
 *      $GLOBALS['smarty']->assign ('smartyVarName',
 *           jwsfGenerateInputButton(
 *                                      $name,
 *                                      $value,
 *                                      $type,
 *                                      $title,
 *                                      $src,
 *                                      $alt,
 *                                      $id,
 *                                      $class
 *                                    ));
 *          * No Required Parameters
 *
 *  Requires:
 *
 *  Notes:
 *      Default action with all null values passed will create a submit button
 *      Align attribute has not been included because you can achieve the same
 *          results with CSS
 *
 *  TODO:
 *
 ******************************************************************************/

function jwsfGenerateInputButton (
                                $name = 'submit',
                                $value = 'Submit',
                                $type = 'submit',
                                $title = 'Submit this form',
                                $src = null,
                                $alt = 'Submit',
                                $id = null,
                                $class = null
                                ) {

    // I just think you should be reasonable for corn's sake!
    if (strlen($name) > 100) return false;

    // Return false if type is not valid for this function
    if (!(($type == 'submit')
            || ($type == 'reset')
            || ($type == 'image')
            || ($type == 'button')))
        return false;

    // Set default id
    if ($id == null)
        $id = 'id_' . $name;

    // If $src is set, check for file existence
    if (($src != null) && !file_exists($_SERVER['DOCUMENT_ROOT'] . $src))
        $src = null;

    // Check length of $alt
    if (strlen($alt) > 255)
        $alt = substr($alt, 0, 255);


    // Construct input tag
	$retVal = '<input';
        $retVal .= ' tabindex="' . $GLOBALS['tabindex']++ . '"';
	$retVal .= ' name="' . $name . '"';
    $retVal .= ' value="' . $value . '"';
    $retVal .= ' type="' . $type . '"';
  	$retVal .= ' title="' . $title . '"';
    if (($type == 'image') && ($src != null)) {
        $retVal .= ' src="' . $src . '"';
        $retVal .= ' alt="' . $alt . '"';
    }
	$retVal .= ' id="' . $id . '"';
	if ($class != null)
        $retVal .= ' class="' . $class . '"';
	$retVal .= ' />';

    return $retVal;
}


/***** jwsfGenerateSelectFromArray *********************************************
 *
 *  This function returns a complete select tag with options.  If no value is
 *  passed for the CSS ID, it will be created as id_$name.
 *
 *  Mandatory Parameters:
 *      $array
 *      $name
 *
 *  Default Values:
 *      $array      =   null    (returns false)
 *      $name       =   null    (returns false)
 *      $size       =   1       (drop box)
 *      $title      =   'Select from these options'
 *      $id         =   'id' . $name
 *      $class      =   null
 *      $multiple   =   false   (boolean)
 *
 *  Example Usage:
 *      $GLOBALS['smarty']->assign ('smartyVarName',
 *           jwsfGenerateFieldsetWithLegend(
 *                                              *$array,
 *                                              *$name,
 *                                              $size,
 *                                              $title,
 *                                              $id,
 *                                              $class,
 *                                              $multiple
 *                                          ));
 *          * Denotes Required Parameters
 *
 *  Requires:
 *      A pre-constructed associative array of values and labels to fill out the
 *      options, the first record should be the default select option/prompt.
 *      The format of the should be:
 *          array(value => label)
 *      If there are not at least two elements in the array, this function will
 *      return false since it would be pointless to have a select box with less
 *      than two options.
 *          $arrayName[$value] = $label;
 *          array(2) {
 *            [1]=>
 *            string(10) "Site Owner"
 *            [2]=>
 *            string(10) "Web Master"
 *          }
 *
 *  TODO:
 *      jwsfGenerateSelectFromTable
 *      jwsfGenerateSelectFromObject
 *      Integrate <optgroup>
 ******************************************************************************/

function jwsfGenerateSelectFromArray (
                                        $array = null,
                                        $name = null,
                                        $default = 0,
                                        $size = 1,
                                        $title = 'Select from these options',
                                        $id = null,
                                        $class = null,
                                        $multiple = false
                                        ) {

    // Verify mandatory parameters have been passed
    if (($array == null)
        || (!is_array($array))
        || (count($array) < 1)
        || ($name == null)
        || (strlen($name) > 100))
            return false;

    // Verify default
    if (array_key_exists($default, $array) == false)
        $default = 0;

    // Set default CSS ID
    if ($id == null)
        $id = 'id_' . $name;

    // Make sure $size is valid
    if (($size < 1) || ($size > 30))
        $size = 1;

    // Construct opening select tag
    $retVal = '<select';
	$retVal .= ' name="' . $name . '"';
    $retVal .= ' tabindex="' . $GLOBALS['tabindex']++ . '"';
	$retVal .= ' size="' . $size . '"';
	$retVal .= ' title="' . $title . '"';
	$retVal .= ' id="' . $id . '"';
	if ($class != NULL)
        $retVal .= ' class="' . $class . '"';
    if ($multiple == true)
        $retVal .= ' multiple="multiple"';
	$retVal .= '>';

    // Construct option list
    foreach ($array as $value => $label) {
    	$retVal .= '<option value="' . $value . '"';
    	if ((isset($_POST[$name]) && ($_POST[$name] == $value))
            || ($default == $value))
            $retVal .= ' selected="selected"';
    	$retVal .= '>' . $label . '</option>';
    }

    // Close the select tag
    $retVal .= '</select>';

    return $retVal;
}


/***** jwsfGenerateSelectRange *************************************************
 *
 *  This function returns a complete select tag with option values and labels
 *  that are integers from low to high.
 *
 *  Mandatory Parameters:
 *      $name
 *
 *  Default Values:
 *      $name       =   null    (returns false)
 *      $low        =   0
 *      $high       =   255
 *      $size       =   1       (drop box)
 *      $title      =   'Select from these options'
 *      $id         =   'id' . $name
 *      $class      =   null
 *      $multiple   =   false   (boolean)
 *
 *  Example Usage:
 *      $GLOBALS['smarty']->assign ('smartyVarName',
 *           jwsfGenerateSelectRange(
 *                                              *$name,
 *                                              $default,
 *                                              $low,
 *                                              $high,
 *                                              $size,
 *                                              $title,
 *                                              $id,
 *                                              $class,
 *                                              $multiple
 *                                          ));
 *          * Denotes Required Parameters
 *
 ******************************************************************************/

function jwsfGenerateSelectRange (
                                        $name = null,
                                        $default = 0,
                                        $low = 0,
                                        $high = 255,
                                        $size = 1,
                                        $title = 'Select from these options',
                                        $id = null,
                                        $class = null,
                                        $multiple = false
                                        ) {

    // Verify mandatory parameters have been passed
    if (($name == null)
        || (strlen($name) > 100))
            return false;

    // Set default access level
    if (($default == null) && !is_numeric($default))
        $default = 0;

    // Set default CSS ID
    if ($id == null)
        $id = 'id_' . $name;

    // Make sure $size is valid
    if (($size < 1) || ($size > 30))
        $size = 1;

    // Construct opening select tag
    $retVal = '<select';
	$retVal .= ' name="' . $name . '"';
    $retVal .= ' tabindex="' . $GLOBALS['tabindex']++ . '"';
	$retVal .= ' size="' . $size . '"';
	$retVal .= ' title="' . $title . '"';
	$retVal .= ' id="' . $id . '"';
	if ($class != NULL)
        $retVal .= ' class="' . $class . '"';
    if ($multiple == true)
        $retVal .= ' multiple="multiple"';
	$retVal .= '>';

    // Construct option list
    foreach (range($low, $high) as $value => $label) {
    	$retVal .= '<option value="' . $value . '"';
    	if ((isset($_POST[$name]) && ($_POST[$name] == $value))
            || ($value == $default))
            $retVal .= ' selected="selected"';
    	$retVal .= '>' . $label . '</option>';
    }

    // Close the select tag
    $retVal .= '</select>';

    return $retVal;
// End jwsfGenerateSelectRange
}


/***** jwsfGenerateSelectBool **************************************************
 *
 *  This function returns a complete select tag with option values and labels
 *  for a true/false selector.
 *
 *  Mandatory Parameters:
 *      $name
 *
 *  Default Values:
 *      $name       =   null    (returns false)
 *      $default    =   1    (returns false)
 *      $size       =   1       (drop box)
 *      $title      =   'Select from these options'
 *      $id         =   'id' . $name
 *      $class      =   null
 *
 *  Example Usage:
 *      $GLOBALS['smarty']->assign ('smartyVarName',
 *           jwsfGenerateSelectRange(
 *                                              *$name,
 *                                              $default,
 *                                              $size,
 *                                              $title,
 *                                              $id,
 *                                              $class,
 *                                          ));
 *          * Denotes Required Parameters
 *
 ******************************************************************************/

function jwsfGenerateSelectBool (
                                        $name = null,
                                        $default = 0,
                                        $size = 1,
                                        $title = 'Select from these options',
                                        $id = null,
                                        $class = null
                                        ) {

    // Verify mandatory parameters have been passed
    if (($name == null)
        || (strlen($name) > 100))
            return false;
    // Verify default
    if (($default != 1) && ($default != 0))
        $default = 0;

    // Set default CSS ID
    if ($id == null)
        $id = 'id_' . $name;

    // Make sure $size is valid
    if (($size < 1) || ($size > 30))
        $size = 1;

    // Construct opening select tag
    $retVal = '<select';
	$retVal .= ' name="' . $name . '"';
    $retVal .= ' tabindex="' . $GLOBALS['tabindex']++ . '"';
	$retVal .= ' size="' . $size . '"';
	$retVal .= ' title="' . $title . '"';
	$retVal .= ' id="' . $id . '"';
	if ($class != NULL)
        $retVal .= ' class="' . $class . '"';
	$retVal .= '>';

    // Construct option list
    	$retVal .= '<option value="0"';
    	if (isset($_POST[$name]) && ($_POST[$name] == 0))
            $retVal .= ' selected="selected"';
    	elseif ($default == 0)
            $retVal .= ' selected="selected"';
    	$retVal .= '>False</option>';
    	$retVal .= '<option value="1"';
    	if (isset($_POST[$name]) && ($_POST[$name] == 1))
            $retVal .= ' selected="selected"';
    	elseif ($default == 1)
            $retVal .= ' selected="selected"';
    	$retVal .= '>True</option>';

    // Close the select tag
    $retVal .= '</select>';

    return $retVal;
// End jwsfGenerateSelectBool
}


/***** Generate File Upload Form ***********************************************
 *
 *  This function generates a self-sustaining file upload form for one file with
 *  a default maximum size of around one meg.
 *
 ******************************************************************************/

function jwsfGenerateFileUploadForm (
                                            $name = 'fileUpload',
                                            $maxFileSize = 1048576,
                                            $title = 'Select a file to upload',
                                            $id = null,
                                            $class = null
                                            ) {

    // Check mandatory values
    if (!isset($name)
            || ($name == null)
            || (strlen($name) > 252))
        return false;
    if (!is_numeric($maxFileSize))
        return false;
    if (($id == null)
            || (strlen($id) > 254))
        $id = 'id_' . $name;

    // Construct form
    $retVal = '<form action="';
    $retVal .= $_SERVER['REQUEST_URI'];
    $retVal .= '" id="';
    $retVal .= $id;
    $retVal .= '" method="post" enctype="multipart/form-data">';
    $retVal .= '<fieldset>';
    if (isset($title) && (strlen($title) <= 255)) {
        $retVal .= '<legend>';
        $retVal .= $title;
        $retVal .= '</legend>';
    }
    $retVal .= '<input name="MAX_FILE_SIZE" value="';
    $retVal .= $maxFileSize;
    $retVal .= '" type="hidden" />';
    $retVal .= '<input name="';
    $retVal .=  $name;
    $retVal .= '" tabindex="';
    $retVal .= $GLOBALS['tabindex']++;
    $retVal .= '" type="file" />';
    $retVal .= '<input type="submit"';
    $retVal .= ' tabindex="';
    $retVal .= $GLOBALS['tabindex']++;
    $retVal .= '" />';
    $retVal .= '</fieldset>';
    $retVal .= '</form>';

    return $retVal;
}





/***** Generate Select Months **************************************************
 *
 *  This generates a select box with value = # and label = month from the db.
 *
 ******************************************************************************/

function jwsfGenerateSelectMonths (
                                            $name = 'month',
                                            $default = 1,
                                            $title = 'Select a month',
                                            $size = 1,
                                            $id = null,
                                            $class = null
                                            ) {

    $months = array();
    if (($id == null)
            || (strlen($id) > 254))
        $id = 'id_' . $name;


    $sql = 'SELECT id, item FROM ' . TableMonths . ';';
    $result = $GLOBALS['db']->query($sql, __FILE__, __LINE__);
    while ($row = $GLOBALS['db']->fetch(__FILE__, __LINE__)) {
        $months[$row['id']] = $row['item'];
    }

    // Construct and return select and option tags
    $retVal = jwsfGenerateSelectFromArray (
                                                $months,
                                                $name,
                                                $default,
                                                $size,
                                                $title,
                                                $id,
                                                $class
                                            );
    return $retVal;
}


/***** Generate Select Years Forward *******************************************
 *
 *  This function generates a select box with the current year, plus a given
 *  number of years forward (useful for expiration dates, etc).
 *
 ******************************************************************************/

function jwsfGenerateSelectYearsForward (
                                            $default = 0,
                                            $numyears = 15,
                                            $name = 'year',
                                            $title = 'Select a year',
                                            $size = 1,
                                            $id = null,
                                            $class = null
                                            ) {

    if (($id == null)
            || (strlen($id) > 254))
        $id = 'id_' . $name;

    $year = $GLOBALS['jwsfConfig']->GenerateSelectYearsForwardStart;
    $years = array();
    for ($i = 0; $i < $numyears; $i++){
        $years[$year] = $year++;
    }

    // Construct and return select and option tags
    $retVal = jwsfGenerateSelectFromArray (
                                                $years,
                                                $name,
                                                $default,
                                                $size,
                                                $title,
                                                $id,
                                                $class
                                            );
    return $retVal;
}


/***** Generate Credit Card Radios *********************************************
 *
 *  This generates a radio menu of credit card types (currently A/M/V)
 *
 ******************************************************************************/

function jwsfGenerateCreditCardRadios ($name = 'cardType') {

    $retVal = null;

    // Visa
    $retVal .= jwsfGenerateCheckRadio (
                                    $name,
                                    'Visa',
                                    'radio',
                                    $title = 'Pay with Visa',
                                    '',
                                    '',
                                    'id_visa'
                                    );
    $label = '<img src="' . ImageCardsVisa . '" alt="Pay with Visa" title="Pay with Visa" />';
    $retVal .= jwsfGenerateLabel ('id_visa', $label);

    // MasterCard
    $retVal .= jwsfGenerateCheckRadio (
                                    $name,
                                    'MC',
                                    'radio',
                                    $title = 'Pay with MasterCard',
                                    '',
                                    '',
                                    'id_mc'
                                    );
    $label = '<img src="' . ImageCardsMasterCard . '" alt="Pay with MasterCard" title="Pay with MasterCard" />';
    $retVal .= jwsfGenerateLabel ('id_mc', $label);

    // American Express
    $retVal .= jwsfGenerateCheckRadio (
                                    $name,
                                    'Amex',
                                    'radio',
                                    $title = 'Pay with American Express',
                                    '',
                                    '',
                                    'id_amex'
                                    );
    $label = '<img src="' . ImageCardsAmex . '" alt="Pay with American Express" title="Pay with American Express" />';
    $retVal .= jwsfGenerateLabel ('id_visa', $label);

    return $retVal;

}


/***** Generate State/Province Select ******************************************
 *
 *  This function generates a select box of all valid (I think) provinces and
 *  states.
 *
 *  TODO:
 *      I would like this to be in the database cuz that's how I am.
 *
 ******************************************************************************/

function jwsfGenerateStateProvinceSelect (
                                            $default = 'MI',
                                            $name = 'state',
                                            $title = 'Select a state or province',
                                            $size = 1,
                                            $id = null,
                                            $class = null
                                            ) {

    $states = array ('AL'=>'Alabama','AK'=>'Alaska','AB'=>'Alberta','AS'=>'American Samoa','AZ'=>'Arizona',
    'AR'=>'Arkansas','AA'=>'Armed Forces Americas (except Canada)',
    'AE'=>'Armed Forces Europe, Africa, Middle East, Canada','AP'=>'Armed Forces Pacific',
    'BC'=>'British Columbia','CA'=>'California','CO'=>'Colorado','CT'=>'Connecticut',
    'DE'=>'Delaware','DC'=>'District of Columbia','FM'=>'Federated States of Micronesia',
    'FL'=>'Florida','GA'=>'Georgia','GU'=>'Guam','HI'=>'Hawaii','ID'=>'Idaho','IL'=>'Illinois',
    'IN'=>'Indiana','IA'=>'Iowa','KS'=>'Kansas','KY'=>'Kentucky','LA'=>'Louisiana','ME'=>'Maine',
    'MB'=>'Manitoba','MH'=>'Marshall Islands','MD'=>'Maryland','MA'=>'Massachusetts',
    'MI'=>'Michigan','MN'=>'Minnesota','MS'=>'Mississippi','MO'=>'Missouri','MT'=>'Montana',
    'NE'=>'Nebraska','NV'=>'Nevada','NB'=>'New Brunswick','NH'=>'New Hampshire',
    'NJ'=>'New Jersey','NM'=>'New Mexico','NY'=>'New York','NF'=>'Newfoundland and Labrador',
    'NC'=>'North Carolina','ND'=>'North Dakota','MP'=>'Northern Mariana Islands',
    'NT'=>'Northwest Territories','NS'=>'Nova Scotia','NU'=>'Nunavut','OH'=>'Ohio','OK'=>'Oklahoma',
    'ON'=>'Ontario','OR'=>'Oregon','PW'=>'Palau','PA'=>'Pennsylvania','PE'=>'Prince Edward Island',
    'PR'=>'Puerto Rico','PQ'=>'Quebec','RI'=>'Rhode Island','SK'=>'Saskatchewan',
    'SC'=>'South Carolina','SD'=>'South Dakota','TN'=>'Tennessee','TX'=>'Texas','UT'=>'Utah',
    'VT'=>'Vermont','VI'=>'Virgin Islands','VA'=>'Virginia','WA'=>'Washington','WV'=>'West Virginia',
    'WI'=>'Wisconsin','WY'=>'Wyoming','YT'=>'Yukon');

    if (isset($_POST[$name]))
        if (array_search($_POST[$name], $states) == true)
            $default = $_POST[$name];
        else
            $default = 'MI';
    elseif (array_search($default, $states) == false)
            $default = 'MI';

    // Construct and return select and option tags
    $retVal = jwsfGenerateSelectFromArray (
                                                $states,
                                                $name,
                                                $default,
                                                $size,
                                                $title,
                                                $id,
                                                $class
                                            );
    return $retVal;
}


/***** Generate Country Select *************************************************
 *
 *  This function generates a select box with a whole lot of countries.
 *
 *  TODO:
 *      Database!
 *
 ******************************************************************************/

function jwsfGenerateCountrySelect (
                                            $default = 'US',
                                            $name = 'country',
                                            $title = 'Select a country',
                                            $size = 1,
                                            $id = null,
                                            $class = null
                                            ) {

    $countries = array ('US'=>'USA','CA'=>'CANADA','UK'=>'UNITED KINGDOM','AF'=>'AFGHANISTAN','AL'=>'ALBANIA',
    'DZ'=>'ALGERIA','AD'=>'ANDORRA','AO'=>'ANGOLA','AI'=>'ANGUILLA','AG'=>'ANTIGUA AND BARBUDA',
    'AR'=>'ARGENTINA','AM'=>'ARMENIA','AW'=>'ARUBA','AU'=>'AUSTRALIA','AT'=>'AUSTRIA',
    'AZ'=>'AZERBAIJAN','BS'=>'BAHAMAS','BH'=>'BAHRAIN','BD'=>'BANGLADESH','BB'=>'BARBADOS',
    'BY'=>'BELARUS','BE'=>'BELGIUM','BZ'=>'BELIZE','BJ'=>'BENIN','BM'=>'BERMUDA','BT'=>'BHUTAN',
    'BO'=>'BOLIVIA','BA'=>'BOSNIA AND HERZEGOWINA','BW'=>'BOTSWANA','BR'=>'BRAZIL',
    'BN'=>'BRUNEI DARUSSALAM','BG'=>'BULGARIA','BF'=>'BURKINA FASO','BI'=>'BURUNDI',
    'KH'=>'CAMBODIA','CM'=>'CAMEROON','CA'=>'CANADA','CV'=>'CAPE VERDE','KY'=>'CAYMAN ISLANDS',
    'CF'=>'CENTRAL AFRICAN REPUBLIC','TD'=>'CHAD','CL'=>'CHILE','CN'=>'CHINA','CO'=>'COLOMBIA',
    'KM'=>'COMOROS','CG'=>'CONGO','CK'=>'COOK ISLANDS','CR'=>'COSTA RICA',
    'HR'=>'CROATIA LOCAL NAME:(HRVATSKA)','CU'=>'CUBA','CY'=>'CYPRUS','CZ'=>'CZECH REPUBLIC',
    'DK'=>'DENMARK','DJ'=>'DJIBOUTI','DM'=>'DOMINICA','DO'=>'DOMINICAN REPUBLIC','EC'=>'ECUADOR',
    'EG'=>'EGYPT','SV'=>'EL SALVADOR','EE'=>'ESTONIA','FK'=>'FALKLAND ISLANDS (MALVINAS)',
    'FJ'=>'FIJI','FI'=>'FINLAND','FR'=>'FRANCE','GF'=>'FRENCH GUIANA','PF'=>'FRENCH POLYNESIA',
    'GA'=>'GABON','GM'=>'GAMBIA','GE'=>'GEORGIA','DE'=>'GERMANY','GH'=>'GHANA','GR'=>'GREECE',
    'GL'=>'GREENLAND','GP'=>'GUADELOUPE','GT'=>'GUATEMALA','HT'=>'HAITI','HN'=>'HONDURAS',
    'HK'=>'HONG KONG','HU'=>'HUNGARY','IS'=>'ICELAND','IN'=>'INDIA','ID'=>'INDONESIA','IR'=>'IRAN',
    'IQ'=>'IRAQ','IE'=>'IRELAND','IL'=>'ISRAEL','IT'=>'ITALY','JM'=>'JAMAICA','JP'=>'JAPAN',
    'JO'=>'JORDAN','KZ'=>'KAZAKHSTAN','KE'=>'KENYA','KI'=>'KIRIBATI','KR'=>'KOREA','KW'=>'KUWAIT',
    'KG'=>'KYRGYSTAN','LA'=>'LAOS','LV'=>'LATVIA','LB'=>'LEBANON','LS'=>'LESOTHO','LR'=>'LIBERIA',
    'LY'=>'LIBYA','LI'=>'LIECHTENSTEIN','LT'=>'LITHUANIA','LU'=>'LUXEMBOURG','MO'=>'MACAO',
    'MK'=>'MACEDONIA','MG'=>'MADAGASCAR','MW'=>'MALAWI','MY'=>'MALAYSIA','MV'=>'MALDIVES',
    'ML'=>'MALI','MT'=>'MALTA','MQ'=>'MARTINIQUE','MR'=>'MAURITANIA','MU'=>'MAURITIUS',
    'MX'=>'MEXICO','MD'=>'MOLDOVA','MC'=>'MONACO','MN'=>'MONGOLIA','MS'=>'MONTSERRAT',
    'MA'=>'MOROCCO','MZ'=>'MOZAMBIQUE','MM'=>'MYANMAR','NA'=>'NAMIBIA','NR'=>'NAURU',
    'NP'=>'NEPAL','NL'=>'NETHERLANDS','AN'=>'NETHERLANDS ANTILLES','NC'=>'NEW CALEDONIA',
    'NZ'=>'NEW ZEALAND','NI'=>'NICARAGUA','NE'=>'NIGER','NG'=>'NIGERIA','NU'=>'NIUE',
    'NO'=>'NORWAY','OM'=>'OMAN','PK'=>'PAKISTAN','PA'=>'PANAMA','PG'=>'PAPUA NEW GUINEA',
    'PY'=>'PARAGUAY','PE'=>'PERU','PH'=>'PHILIPPINES','PN'=>'PITCAIRN','PL'=>'POLAND','PT'=>'PORTUGAL',
    'QA'=>'QATAR','RE'=>'REUNION','RO'=>'ROMANIA','RU'=>'RUSSIA','RW'=>'RWANDA','KN'=>'SAINT KITTS AND NEVIS',
    'WS'=>'SAMOA','SM'=>'SAN MARINO','SA'=>'SAUDI ARABIA','SN'=>'SENEGAL','SC'=>'SEYCHELLES',
    'SL'=>'SIERRA LEONE','SG'=>'SINGAPORE','SK'=>'SLOVAKIA','SI'=>'SLOVENIA','SB'=>'SOLOMON ISLANDS',
    'ZA'=>'SOUTH AFRICA','ES'=>'SPAIN','LK'=>'SRI LANKA','SR'=>'SURINAME','SW'=>'SWEDEN',
    'CH'=>'SWITZERLAND','TW'=>'TAIWAN','TH'=>'THAILAND','TK'=>'TOKELAU','TO'=>'TONGA',
    'TT'=>'TRINIDAD AND TOBAGO','TN'=>'TUNISIA','TR'=>'TURKEY','TV'=>'TUVALU','UG'=>'UGANDA',
    'UA'=>'UKRAINE','AE'=>'UNITED ARAB EMIRATES','UK'=>'UNITED KINGDOM','TZ'=>'UNITED REPUBLIC OF TANZANIA',
    'UY'=>'URUGUAY','US'=>'USA','UZ'=>'UZBEKISTAN','VU'=>'VANUATU','VA'=>'VATICAN CITY STATE',
    'VE'=>'VENEZUELA','VN'=>'VIETNAM','VG'=>'VIRGIN ISLANDS (BRITISH)','YE'=>'YEMEN','ZR'=>'ZAIRE',
    'ZM'=>'ZAMBIA','ZW'=>'ZIMBABWE');

    if (isset($_POST[$name]))
        if (array_search($_POST[$name], $countries) == true)
            $default = $_POST[$name];
        else
            $default = 'US';
    elseif (array_search($default, $countries) == false)
            $default = 'US';

    // Construct and return select and option tags
    $retVal = jwsfGenerateSelectFromArray (
                                                $countries,
                                                $name,
                                                $default,
                                                $size,
                                                $title,
                                                $id,
                                                $class
                                            );
    return $retVal;
}


/***** JWSF Post Vars as Hidden Fields ****************************************/

function jwsfPostAsHidden () {

    // A single dimensional array of hidden input tags
    $retVal = array();
    if (isset($_POST)) {
        foreach ($_POST as $key => $value) {
            $retVal[$key] = jwsfGenerateInputBox (
                                $key,
                                'hidden',
                                '',
                                '',
                                $value
                                );
        }
        return $retVal;
    } else
        return false;

}




?>
