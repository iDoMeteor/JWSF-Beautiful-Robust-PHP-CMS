<?php



function generate_numbers_array ($begin, $end) {
    if (is_numeric($begin) && is_numeric($end)) {
        $retVal = array();
        $retVal = range ($begin, $end);
        return $retVal;
    } else {
        return false;
    }
}


function sanitize_incoming($untrusted) {
    $key = NULL;
    $value = NULL;
    $trusted = array();
    if (is_array($untrusted)) {
        foreach ($untrusted as $key => $value) {
            /* This ensures everything gets washed */
            if (is_array($value)) {
                sanitize_incoming ($value);
            } else {
                // remove whitespace
                $key = trim($key);
                $value = trim($value);
                // escape dangerous characters
                if (get_magic_quotes_gpc()) {
                    $key = addslashes($key);
                    $value = addslashes($value);
                }
                // make it squeaky clean
                if (!is_numeric($key)) {
                   $key =  mysql_real_escape_string($key);
                }
                if (!is_numeric($value)) {
                   $value =  mysql_real_escape_string($value);
                }
                $trusted[$key] = $value;
            }
        }
        return $trusted;
    } else {
        // remove whitespace
        $trusted = trim($untrusted);
        // escape dangerous characters
        if (get_magic_quotes_gpc()) {
            $trusted = addslashes($trusted);
        }
        // make it squeaky clean
        if (!is_numeric($trusted)) {
           $trusted =  mysql_real_escape_string($trusted);
        }
        return $trusted;
    }
    return false;
}

// Standard redirect wrapper
function redirect ($url) {
    header ("Location: $url");
    exit();
}


// Standard send user to site root function
function redirect_site_root () {
	header ("Location: http://" . $_SERVER['HTTP_HOST']);
	$redirect_string = '<a href="' . CurrentProtocol . FQDN . '" target="_top">Click here to return to the home page</a><br>\n';
	exit();
}

// SQL Time to Unix Time
function sql_to_unix_time($ts) {
		return mktime(substr($ts, 11,2), substr($ts, 14,2), substr($ts, 17,2), substr($ts, 5,2), substr($ts, 8,2), substr($ts, 0,4));
}

// Format Single Digit With Leading Zero
function fix_single_digit ($digit) {
	if (($digit < 10) && ($digit >= 0)) {
		$retVal = "0" . $digit;
		return $retVal;
	} else {
		return $digit;
	}
}

// TODO: Fix descending
function array_sort_by_any_key (&$array, $field) {
    $sortArr = array();
    $resultArr = array();

    foreach ( $array as $key => $value ) {
        $sortArr[ $key ] = $value[ $field ];
    }
    if ( isset($descending) ) {
        arsort( $sortArr );
    } else {
        asort( $sortArr );
    }
    foreach ( $sortArr as $key => $value ) {
        $resultArr[ $key ] = $array[ $key ];
    }
    /* May not need to return anything since using
        array as reference */
    return $resultArr;
}


function array_remove_element_by_key (&$array, $key) {
    /* This function removes and then returns the
        removed array element from the original array */
    return array_splice($array_of_items,($key+1),1);
}
    

function submit_post_request($url, $data, $optional_headers = null) {
 $params = array('http' => array(
              'method' => 'POST',
              'content' => $data
           ));
 if ($optional_headers !== null) {
    $params['http']['header'] = $optional_headers;
 }
 $ctx = stream_context_create($params);
 $response = @file_get_contents($url, 0, $ctx);
 if ($response === false) {
    $GLOBALS['messagesError'][] = "Problem reading data from $url";
    return false;
 }
 return $response;
}


/***** Email Smarty Template ***************************************************
 *
 *  @to             an array of email addresses or a single email as string
 *  @from           a properly formatted address
 *  @subject        a string
 *  @template       a smarty template file name with extension
 *  @files          an optional array of file names to attach
 *  TODO:
 *      Finish file attachment routine
 *
 * ****************************************************************************/

function email_smarty_template ($to, $from, $subject, $template, $files=null) {

    // Check recipient format
    if (is_array($to)) {
        // Validate & turn into string
        if (!empty($to)) {
            $array = $to;
            $to = '';
            foreach ($array as $v) {
                $to .= $v . ',';
            }
            $to = substr($to, 0, -1);
        } else {
            return false;
        }
    } elseif (validateEmail(true, $to) == false) {
        return false;
    }
    // Get BCC list from database
    $GLOBALS['jwsfConfig']->getBccList();
    if (!empty($GLOBALS['jwsfConfig->bccList'])) {
        $bcc = 'Bcc: ';
        foreach ($GLOBALS['jwsfConfig']->bccList as $v) {
            $bcc .= $v . ',';
        }
        $bcc = substr($bcc, 0, -1);
        $bcc .= "\r\n";
    } else {
        $bcc = null;
    }

    // Format Headers
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/plain; charset=us-ascii' . "\r\n";
    $headers .= 'From: '
        . $from
        . "\r\n";
    $headers .= 'Reply-To: '
        . $from
        . "\r\n";
    if ($bcc != null) {
        $headers .= $bcc;
    }
    $headers .= "\r\n";
    $subject = $GLOBALS['jwsfConfig']->SiteTitleShort
       . ' :: '
       .  $subject;

    // Fetch from Smarty Template
    $message = $GLOBALS['smarty']->returnString($template);

    // Mail it
    if (mail($to,
            $subject,
            $message,
            $headers
            ) == true) {
       return true;
    } else {
       return false; 
    }

}

?>
