<?php

/***** JWSF Form Validation Functions ******************************************
 *
 *  All of these return true upon success or false if the passed value is
 *  invalid.  The first parameter for each function is a boolean value which
 *  determines whether or not the value is required to have a non-null value.
 *
 ******************************************************************************/


/***** Semantic Fields ********************************************************/

/***** JWSF Custom Email Validation ********************************************
 *
 *  This routine checks for up to 64 valid email user name characters in any
 *  combination, followed by an ampersand, followed by either a valid IP address
 *  or up to 255 valid domain name characters, provided that it ends with 2 - 10
 *  valid domain name characters preceded by a period, ensuring the whole string
 *  is not longer than 320 characters.
 ******************************************************************************/
function validateEmail ($req, $value) {
    // Required?
    if (($req == true) && ($value == null)) return false;

    if ((strlen($value) > 320)
            || (eregi('^([a-zA-Z0-9_\-\.]{1,64})@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,10}|[0-9]{1,3})$', $value) == false))
        return false;
    else
        return true;

}


/***** American Zip Code Validator *********************************************
 *
 *  This function checks for exactly five digits, possibly followed by a hyphen
 *  and four more digits and no longer than 10 characters total.
 ******************************************************************************/
function validateZip ($req, $value) {
    // Required?
    if (($req == true) && ($value == null)) return false;

    if ((strlen($value) > 10)
            || (eregi('^\d{5}([-]\d{4})?$', $value) == false))
        return false;
    else
        return true;

}


/***** Canadian Postal Code Validator *****************************************/

function validatePostal ($req, $value) {
    // Required?
    if (($req == true) && ($value == null)) return false;

    if ((strlen($value) > 10)
            || (eregi('^\b[A-Z-[DFIOQUWZ]]\d[A-Z-[DFIOQU]]\ +\d[A-Z-[DFIOQU]]\d\b$', $value) == false))
        return false;
    else
        return true;

}

/***** IP Validation ***********************************************************
 *
 *  This checks for four sets of numeric digits between 0 and 255 seperated
 *  by periods.
 ******************************************************************************/
function validateIP ($req, $value) {
    // Required?
    if (($req == true) && ($value == null)) return false;

    if ((strlen($value) > 15)
            || (eregi('^(?<First>2[0-4]\d|25[0-5]|[01]?\d\d?)\.(?<Second>2[0-4]\d|25[0-5]|[01]?\d\d?)\.(?<Third>2[0-4]\d|25[0-5]|[01]?\d\d?)\.(?<Fourth>2[0-4]\d|25[0-5]|[01]?\d\d?)$', $value) == false))
        return false;
    else
        return true;

}


/***** JWSF International Telephone Number Validator ***************************
 *
 *  This will validate any valid international phone number just about any way
 *  a user tries to pass it by, within reason that is.  Try it.
 ******************************************************************************/
function validateTelephone ($req, $value) {
    // Required?
    if (($req == true) && ($value == null)) return false;

    if ((strlen($value) > 20)
            || (eregi('^(?<CountryCode>(((\+?\s*\d{3})|[1]){1})?)\s*[(]?(?<AreaCode>\d{3})[)]?\s*(?<Number>\d{3}(?:-|\s*)\d{4})$', $value) == false))
        return false;
    else
        return true;

}





/***** Database Field Types ***************************************************/


/***** JWSF Tiny Text Validator ************************************************
 *
 *  This makes sure that the string not longer than 255 characters
 ******************************************************************************/
function validateTinyText ($req, $value) {
    // Required?
    if (($req == true) && ($value == null)) return false;
    // Length?
    if (strlen($value) > 255) return false;
    else return true;

}

/***** JWSF Regular Text Validator ************************************************
 *
 *  This makes sure that the string not longer than 65,535 characters
 ******************************************************************************/
function validateRegularText ($req, $value) {
    // Required?
    if (($req == true) && ($value == null)) return false;
    // Length?
    if (strlen($value) > 65535) return false;
    else return true;

}

/***** JWSF Medium Text Validator ************************************************
 *
 *  This makes sure that the string not longer than 16,777,215 characters
 ******************************************************************************/
function validateMediumText ($req, $value) {
    // Required?
    if (($req == true) && ($value == null)) return false;
    // Length?
    if (strlen($value) > 16777215) return false;
    else return true;

}

/***** JWSF Varchar/Char Validator *********************************************
 *
 *  This makes sure that the string not longer X
 ******************************************************************************/
function validateVarChar ($req, $value, $size) {
    // Required?
    if (($req == true) && ($value == null)) return false;
    // Length?
    if (strlen($value) > $size) return false;
    else return true;

}

/***** JWSF Tiny Integer Validator *********************************************
 *
 *  This is fairly loose, as it allows any valid tiny integer value and does not
 *  check whether or not it is signed, so it would let a value over 127 fly even
 *  if the database field was signed, thus it would be outside the valid range.
 *  Just try to know what you're doing when you're doing it if you use this,
 *  it's biggest purpose it to check our work, but untrusted incoming..
 *  Valid values = -128 - +255
 *
 *  TODO:
 *      May want to incorporate some of the following..
 *          is_int
 *          is_string && === (string)(int) $value
 *          is_float && === (float)(int) $value
 *
 ******************************************************************************/
function validateTinyInt ($req, $value) {
    // Required?
    if (($req == true) && ($value == null)) return false;
    if (!is_numeric(trim($value))) return false;
    if (($value < -128) || ($value > 255)) return false;
    else return true;

}

/***** JWSF Small Integer Validator ********************************************
 *
 *  Same as above but valid from -32678 - +65535
 ******************************************************************************/
function validateSmallInt ($req, $value) {
    // Required?
    if (($req == true) && ($value == null)) return false;
    if (!is_numeric(trim($value))) return false;
    if (($value < -32678) || ($value > 65535)) return false;
    else return true;

}

/***** JWSF Medium Integer Validator *******************************************
 *
 *  Same as above, but -8388608 - +16777215
 ******************************************************************************/
function validateMediumInt ($req, $value) {
    // Required?
    if (($req == true) && ($value == null)) return false;
    if (!is_numeric(trim($value))) return false;
    if (($value < -8388608) || ($value > 16777215)) return false;
    else return true;

}

/***** JWSF Regular Integer Validator ******************************************
 *
 *  Same as above, but -2147483648 - +4294967295
 ******************************************************************************/
function validateRegularInt ($req, $value) {
    // Required?
    if (($req == true) && ($value == null)) return false;
    if (!is_numeric(trim($value))) return false;
    if (($value < -2147483648) || ($value > 4294967295)) return false;
    else return true;

}

/***** JWSF Big Integer Validator **********************************************
 *
 *  Same as above, but -9223372036854775808 - +18446744073709551615
 ******************************************************************************/
function validateBigInt ($req, $value) {
    // Required?
    if (($req == true) && ($value == null)) return false;
    if (!is_numeric(trim($value))) return false;
    if (($value < -9223372036854775808) || ($value > 18446744073709551615)) return false;
    else return true;

}

?>
