<?php
// Force the browser to refresh every time, or let it cache?
// This is set the global constants file
// Should move this out to functions
// Should use most recent date instead of whichever this process grabs
if (MC_AlwaysRefresh == true) {
	// always modified
    $mc_page_data['stamp'] = gmdate("D, d M Y H:i:s") . " GMT";
	header("Last-Modified: " . $mc_page_data['stamp']);
	// HTTP/1.1
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	// HTTP/1.0
	header("Pragma: no-cache");
} else {
	// Check for DBP compatibility
	if (isset($mc_page_data['stamp'])) {
		$mc_stamp_file = $_SERVER['DOCUMENT_ROOT'] . $_SERVER['REDIRECT_URL'] . MC_FnameContent;
		if (is_file($mc_stamp_file)) {
		    $mc_page_data['stamp'] = gmstrftime("%A %d-%b-%y %T %Z", filemtime($mc_stamp_file)) . " GMT";
		} else {
		    $mc_page_data['stamp'] = gmdate("D, d M Y H:i:s") . " GMT";
		}
	}
 	header ("Last-Modified: " . $mc_page_data['stamp']);
}
?>
