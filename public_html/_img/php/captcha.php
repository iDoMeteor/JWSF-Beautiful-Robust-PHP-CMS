<?php


/*******************************************************************************
 *  JWSF Captcha Image Generator
 *
 *  $Revision: 1.1.1.1 $
 *
 ******************************************************************************/

define_include_location();

DEFINE ('SessionDefaultName', 'JWSF');
DEFINE ('SessionStorage', PathIncAbs . '/' . 'sessions');

session_name (SessionDefaultName);
session_save_path (SessionStorage);
session_start ();

$_SESSION['captchaValidation'] = $_SESSION['captcha'][0]  + $_SESSION['captcha'][1];

header("Content-type: image/png");
$string = $_SESSION['captcha'][0] . ' + ' . $_SESSION['captcha'][1];
$image = imagecreate(50, 25);
$bg = imagecolorallocate($image, 255, 255, 255);
$txt = imagecolorallocate($image, 0, 0, 0);
imagestring($image, rand(1,5), 5, 5, $string, $txt);
imagepng($image);
imagedestroy($image);

function define_include_location () {

	$siteroot = dirname('../../../');
	if ($pos = strpos($siteroot, '/admin')) {
		$siteroot = substr ($siteroot, 0, $pos);
	}
	$base = strrchr($siteroot, '/');

	// The *real* site root
	DEFINE ('SiteRoot', $siteroot);

	if (file_exists(SiteRoot . '/../_public_html'))
		$incLocation = realpath (SiteRoot . '/../_public_html');
	elseif (file_exists(SiteRoot . '/_public_html'))
		$incLocation = SiteRoot . '/_public_html';
	elseif (file_exists(SiteRoot . '/../' . $base))
		$incLocation = realpath (SiteRoot . '/../' . $base);
	elseif (file_exists(SiteRoot . '/' . $base))
		$incLocation = SiteRoot . '/' . $base;
	else
		die ('Mandatory files could not be found');

	DEFINE ('PathIncAbs', $incLocation);

}

?>