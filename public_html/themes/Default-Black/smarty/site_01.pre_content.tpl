{strip}

    {* /* $Revision: 1.1.1.1 $ */ *}

	{* Outermost Container 1 - Presentation Frame *}
        <div id="id_container1">

		{* Major Section 1 - Header Containment *}
            <div id="id_container1-Row1">

                {jwsfDisplay resource_name='Nav_SkipTo.tpl'}

                {jwsfDisplay resource_name='User_Greeting.tpl'}

                {jwsfDisplay resource_name='User_Authentication.tpl'}

                {jwsfDisplay resource_name='Site_Logo.tpl'}

                {jwsfDisplay resource_name='Page_Image.tpl'}

            </div>
		{* End Major Section 1 - Header *}

		{* Major Section 2 - Quickies (secondary header) *}
            <div id="id_container1-Row2">

                {jwsfDisplay resource_name='Nav_Tertiary.tpl'}

                {jwsfDisplay resource_name='Nav_Parents.tpl'}

            </div>
		{* End Major Section 2 *}


		{* Major Section 3 - Column Containment *}
            <div id="id_container1-Row3">

		    {* Column 1 - System Messages and Content *}
                <div id="id_container1-Row3-Col1">

                {jwsfDisplay resource_name='Site_Messages.tpl'}

                {jwsfDisplay resource_name='Page_Title.tpl'}

			{* Display page content when not in admin area *}
				{if (strpos($smarty.server.PHP_SELF, '/admin') === false)}
					{page->cTemplate assign=cTemplate}
			        {jwsfDisplay resource_name=$cTemplate}
				{/if}

{/strip}
