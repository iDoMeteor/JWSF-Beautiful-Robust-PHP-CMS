{strip}

    {* /* $Revision: 1.1.1.1 $ */ *}

    {*
        Column 1 Container begins in pre-content template and contains
        everything in the content template and ends just below here.
    *}

                </div>
            {* End Column 1 *}

		    {* Column 2 - Primary Navigation *}
                <div id="id_container1-Row3-Col2">

                    {jwsfDisplay resource_name='Nav_Primary.tpl'}

                </div>
            {* End Column 2 *}

		    {* Column 3 *}
                <div id="id_container1-Row3-Col3">

                    {jwsfDisplay resource_name='Page_Resources.tpl'}

                    {jwsfDisplay resource_name='Mod_PagesRecentlyModified.tpl'}

		            {jwsfDisplay resource_name='Nav_Related.tpl'}

                    {jwsfDisplay resource_name='Mod_WhosOnline.tpl'}

                </div>
            {* End Column 3 *}

            </div>
        {* End Major Section 3 - Columns *}

		{* Major Section 4 - Footer Nav *}
            <div id="id_container1-Row4">

                {jwsfDisplay resource_name='Nav_Secondary.tpl'}

                {jwsfDisplay resource_name='Site_ThemeSelect.tpl'}

            </div>
        {* End Major Section 4 - Footer Nav *}

        </div>
    {* End Outermost Container 1 *}

	{* Outermost Container 2 - Disclaimers *}
        <div id="id_container2">

            {jwsfDisplay resource_name='Site_PaymentsAccepted.tpl'}

            {jwsfDisplay resource_name='Site_PerfectCode.tpl'}

            {jwsfDisplay resource_name='Site_LastModified.tpl'}

            {jwsfDisplay resource_name='Site_LoadTimer.tpl'}

        </div>
	{* End Major Section 4 - Disclaimers *}

    </body>
</html>
{/strip}
