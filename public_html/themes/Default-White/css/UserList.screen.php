/***** User List *****/

table.c_userList {
    border: 1px solid #aaaaaa;
    border-spacing: 0px;
    margin-left: auto;
    margin-right: auto;
    width: 90%;
}

table.c_userList caption {
    background-color: #aaaaaa;
    border: 1px solid #aaaaaa;
    font-weight: bold;
    margin-top: 40px;
}

table.c_userList th {
    font-size: .9em;
}

table.c_userList td {
    padding: 5px 0px;
}

table.c_userList .c_userEven {
    background-color: #dddddd;
}

table.c_userList .c_userEven td {
    border-top: 1px solid #aaaaaa;
    padding-top: .2em;
}

table.c_userList .c_userOdd td {
    padding-bottom: .2em;
}

table.c_userList .c_userOdd {
    background-color: #eeeeee;
}

table.c_userList .c_userColumnLeft {
    margin: 0px 0px;
    padding: 0px 0px;
    width: 50%;
}

table.c_userList .c_userColumnRight {
    margin: 0px 0px;
    padding: 0px 0px;
    text-align: right;
    width: 50%;
}
