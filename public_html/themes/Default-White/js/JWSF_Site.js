function JWSF_BlockToggle() {
	var obj,cS,args=JWSF_BlockToggle.arguments;document.JWSF_RetVal=(typeof(args[0].href)!='string')?true:false;
	for(var i=1;i<args.length;i++){obj=document.getElementById(args[i]);
		if(obj){cS=JWSF_GetCSSProp(obj,'display');
			if(!obj.JWSF_OD){obj.JWSF_OD=(cS!='none'&&cS!='')?cS:(obj.tagName.toUpperCase()=='TR' && cS!=='none')?'':
			(obj.tagName.toUpperCase()=='TR' && typeof(obj.currentStyle)!='object')?'table-row':'block';}
			obj.style.display=(cS!='none')?'none':obj.JWSF_OD}}
}
function JWSF_GetCSSProp(obj,cP,jP){
	if(typeof(obj)!='object'){var obj=document.getElementById(obj);}
	if(typeof(obj.currentStyle)!='object'){
		return (typeof(document.defaultView)=='object')?
		document.defaultView.getComputedStyle(obj,'').getPropertyValue(cP):
		obj.style.getPropertyValue(cP);}
	else{
		return (navigator.appVersion.indexOf('Mac')!=-1)?
		obj.currentStyle.getPropertyValue(cP):
		obj.currentStyle.getAttribute((jP)?jP:cP);}
}
function JWSF_AddBookmark(url, sitename)
{
    if (window.external.AddFavorite) {
        window.external.AddFavorite ('{$smarty.const.CurrentProtocol}{$smarty.const.FQDN}/?id={$rss[info].id}', '{page->titleFull}');
    } else {
        alert ('Press Control+D to Bookmark This Page!');
    }
}
