<?php

/*******************************************************************************
 * Jason White's Slim Frame Content Management System
 * Version: 1.0 alpha
 *
 * $Revision: 1.3 $
 *
 * You can reach me via email at:
 *      jwsf@square-guys.com
 * I appreciate any comments you may have, will
 * entertain feature and bug requests, and would
 * generally love to know if you enjoy this system!
 *
 *	TODO:
 *		Fix register_globals requirement!
 *
 ******************************************************************************/


/***** Strict Error Handling **************************************************/
// Trying new PHP5 setting of E_STRICT rather than E_ALL
error_reporting(E_ALL);


/***** Start Load Timer *******************************************************/
$loadBegin = load_timer_start();

/***** Find JWSF System Files **************************************************
 *
 *	This is defined at the bottom of this file
 *
 ******************************************************************************/
define_include_location();


/***** Required Files Loader ***************************************************
 *  This function, declared at the bottom of this file,
 *  loads the Slim Frame CMS base system.  There is no
 *  return value and all error detection is handled
 *  from within.
 ******************************************************************************/
load_components();


/***** Instantiate Database Connection *****************************************
 *  This creates a new database object for handling
 *  connections to the database.  This operation is
 *  handled by the ClassDBI_MySQL.php file and
 *  database connection parameters are stored in the
 *  DB.php constants file.  Thes settings in the
 *  constants file need to be modified for the system
 *  to connect to its database.
 ******************************************************************************/
$GLOBALS['db'] =& new mySqlObject ();


/***** Load Dynamic Configuration **********************************************
 *  This loads all of the global configuration options from the database.  These
 *  are all controlled via the Configuration menu in the administration area.
 *  Adding a new record into the configuration database will cause that option
 *  to automagically appear in the configuration menu.  All that you need to
 *  do to use that is implement it into the code base anywhere you like, using
 *      $GLOBALS['jwsfConfig']->optionName
 *  or, in Smarty
 *      config->optionName
 *
 *  This needs to be loaded here so that configuration options that affect
 *  things like users and themes, which are deeply integrated into the system,
 *  are handled before any decisions are made with regards to how they are
 *  presented (or not) to the user.
 *
 *  TODO:
 *      Integrate and seperate..
 *          Integrate a few new tables, and seperate
 *          strings, bool, and numerals... this would
 *          imply a need to create configuration sub-menus
 *          so that the pages are not unwieldy and scary.
 *          While this will complicate things a bit in the code,
 *          it will provide much more flexibility and control
 *          via the administration area.
 *
 ******************************************************************************/
$GLOBALS['jwsfConfig'] =& new configObject ();


/***** User Authorization Check ************************************************
 *  Determine if user is logging in, already logged in,
 *  or logging out.  If user is logging in, or logged in,
 *  then the user object should be loaded.
 ******************************************************************************/
$GLOBALS['userObject'] =& new userObject ();

/***** Load Dynamic Page ******************************************************/
$GLOBALS['jwsfPage'] =& new pageObject ();
$jwsfPage->populate();


/***** Admin Checks ********************************************************
 ******************************************************************************/

$GLOBALS['jwsfPage']->adminArea = false;
if ($GLOBALS['adminArea'] == true) {
    if (($GLOBALS['jwsfConfig']->AdminRequiresLogin == 'false')
            || (($GLOBALS['jwsfConfig']->AdminRequiresLogin == 'true')
                && ($GLOBALS['userObject']->checkAdmin() == false))
            ) {
        // added first clause and removing redirect as single file test
        // might not need a full redir
        // redirect('/');
        $GLOBALS['jwsfPage']->adminArea = true;

        /***** Process Admin Simple Actions *******************************************/
        if (isset($_GET['action'])) {

            switch ($_GET['action']) {
                case 'flush-smarty':
        			$GLOBALS['smarty']->clear_compiled_tpl();
        			$GLOBALS['smarty']->clear_all_cache();
        			$GLOBALS['messagesSuccess'][] = 'Smarty has been flushed';
                    break;
                case 'generate-pdfs':
                    $GLOBALS['jwsfPage']->pdfGenerateAll();
                	break;
                case 'update-check':
                	break;
            // End switch
            }
        // End action check
        }

        /***** Load Admin Module *******************************************************
         *  Process the module passed via GET or set default.  We must process these
         *  early so that any changes they impart will take affect before loading.
         *  Which, unfortunately is no longer an option because some modules require
         *  config and user data to function.  The only option is to throw in a redirect after
         *  the module is processed if required... must look into that.
         ******************************************************************************/
        $GLOBALS['jwsfAdminModule'] =& new jwsfModuleObject;
        $GLOBALS['jwsfAdminModule']->loadAdminModule ();

    // End authorization check
    }
// End admin stuffs
}


/***** Process Modules *********************************************************
 *  Load page specific modules from the database and
 *  process the associated code by including the file
 *  if the user has appropriate access privileges
 *
 *  If the module is required to output any specialized
 *  data, the templates are stored in the smarty template
 *  folder in the format module_ModuleFileName.tpl and will
 *  be loaded dynamically after the content template has
 *  been processed.
 *
 *  We load and process these here because modules should
 *  not affect anything globally, but may require access
 *  to global data.  Also, any data that will be available
 *  for output should be assigned to Smarty from within
 *  the module itself, and thusly we must load the forms
 *  after Smarty has been initialized but before the
 *  error template is displayed.
 *
 *	This module should be loaded before dhtml loader so module specific themes
 *	can be semantic
 ******************************************************************************/
$GLOBALS['jwsfPageModules'] =& new jwsfModuleObject;
$jwsfPageModules->loadPageModules ();



/***** Load Dynamic Configuration **********************************************
 *
 *  This reads the configuration database, the session variables (not to be
 *  trusted!   ..well, is anything?), and the request global (note could be
 *  GET?POST?SESSION/or the database!  This is where setting, changing and
 *  loading of theme files happen.
 *
 *  This loads here because loading it earlier ended up breaking the user
 *  selectable themes.  This is a candidate for shuffling up or down a couple
 *  of stages, but not too far!
 *
 *  TODO:
 *      Theme engine needs work.. should be able to put custom
 *      smarty templates in, theme related images should not be global,
 *      ie; /_img/logo.gif should really be /_themes/ThemeName/images.
 *
 ******************************************************************************/
$jwsfConfig->loadDhtml ();


/***** Load Dynamic Navigation *************************************************
 *
 *  This loads the primary and secondary navigational arrays.
 *
 *	This needs to be loaded after the page object.
 *
 ******************************************************************************/
$GLOBALS['jwsfNavigation'] =& new navigationObject ();


/* ******* Register Data Required by Smarty ************************************
 *  This section ensures that the data acquired above
 *  is passed on to smarty for usage.  The intention
 *  is to pass anything that may be of use to the
 *  template designer, without passing anything not
 *  directly related to the final output to the
 *  browser for security's sake.  We do this here
 *  so that Smarty gets everything fully loaded.
 *
 *	TODO:
 *		Move all smarty assignments to the place where the data is set to get
 *		rid of this big ugly section and prepare for more dynamic themes!
 *
 ******************************************************************************/

if (isset($jwsfPage))
    $GLOBALS['smarty']->register_object('page', $jwsfPage);
if (isset($GLOBALS['userObject']))
    $GLOBALS['smarty']->register_object('user', $GLOBALS['userObject']);
if (isset($jwsfPage->content))
    $GLOBALS['smarty']->assign('data', $jwsfPage->content);
if (isset($jwsfPage->rss))
    $GLOBALS['smarty']->assign('rss', $jwsfPage->rss);

/***** Check Email Page To Recipient ******************************************/
if (isset($_POST['formEmailPageSubmit'])) {
    $GLOBALS['jwsfPage']->emailPage();
}

/***** Assign System Messages *************************************************/
if (isset($GLOBALS['messagesError']))
    $GLOBALS['smarty']->assign('messagesError', $GLOBALS['messagesError']);
if (isset($GLOBALS['messagesSuccess']))
    $GLOBALS['smarty']->assign('messagesSuccess', $GLOBALS['messagesSuccess']);
if (isset($GLOBALS['messagesNotice']))
    $GLOBALS['smarty']->assign('messagesNotice', $GLOBALS['messagesNotice']);


/* ******* Initiate Browser ****************************************************
 *  This declares namespace, meta tags, css and
 *  javascript locations.  It uses data passed
 *  in via the 'page' object, the 'ua', 'favicon',
 *  'css' and 'js' variables.
 $GLOBALS['smarty']->addToBuffer(SiteTemplateMetaData);
 ***** Load Pre-Content Site-Wide Template ************************************
 $GLOBALS['smarty']->addToBuffer(SiteTemplatePreContent);
*/

/***** Display Module Output ***************************************************
 *  Load page specific module templates from the smarty
 *  template folder if the user has appropriate access
 *  privileges.
 *
 *  If the module is required to output any specialized
 *  data, the templates are stored in the smarty template
 *  folder in the format module_semanticName.tpl and will
 *  be loaded dynamically after the content template has
 *  been processed.
 ******************************************************************************/

$modulesPreContent = array();
$modulesPostContent = array();
foreach ($jwsfPageModules->modules as $key => $value) {
    if (file_exists(PathSmarty
                    . 'templates/'
                    . PathModulesTemplatePre
                    . $value[1]
                    . '.tpl')
                    ) {
        switch ($value[0]) {
            // Location 0 has executed but has no output, thus no Smarty reg..
            case '1':
                $modulesPreContent[] = 'Mod_' . $value[1] . '.tpl';
                break;
            case '2':
                $modulesPostContent[] = 'Mod_' . $value[1] . '.tpl';
                break;
        }
    }
}
$GLOBALS['smarty']->assign('modulesPre', $modulesPreContent);
$GLOBALS['smarty']->assign('modulesPost', $modulesPostContent);


/***** Display Admin Module Output ********************************************/
if ($GLOBALS['jwsfPage']->adminArea == true) {
    if (isset($_REQUEST['mod'])
            && (eregi('[a-z0-9_.-]{1,250}', $_REQUEST['mod']) == true)
            && file_exists(PathSmarty
                            . 'templates/'
                            . PathModulesAdminPre
                            . $_REQUEST['mod']
                            . PathModulesTemplateExt)
            ) {
        $GLOBALS['jwsfPage']->adminModule = PathModulesAdminPre
                            . $_REQUEST['mod']
                            . PathModulesTemplateExt;
    } elseif (is_file(PathSmarty
                            . 'templates/'
                            . PathModulesAdminPre
                            . AdminModuleDefault
                            . PathModulesTemplateExt)
                && (!isset($_GET['action']))
            ) {
        $GLOBALS['jwsfPage']->adminModule = PathModulesAdminPre
                            . AdminModuleDefault
                            . PathModulesTemplateExt;
    // End processing actions
    }
// End admin module output
}


/***** End Load Timer *********************************************************/
load_timer_end($loadBegin);


/***** Load Site Wide Smarty Template **************************************************/
// $GLOBALS['smarty']->addToBuffer(SiteTemplatePostContent);
$GLOBALS['smarty']->addToBuffer(SiteTemplate);

/***** Close Database Connection **********************************************/
$GLOBALS['db']->close();

$GLOBALS['smarty']->output = utf8_encode($GLOBALS['smarty']->output);

if (isset($_GET['url'])
        && ($_GET['url'] != 'rss')
        && ($GLOBALS['jwsfConfig']->UseTidy == 'true')
        ) {
	$config = array(
					'doctype' => 'auto',
					'drop-font-tags' => true,
					'error-file' => './error-tidy.log',
					'indent' => true,
					'indent-attributes' => false,
					'indent-spaces' => 2,
					'input-encoding' => 'utf8',
					'hide-comments' => false,
			        'output-xhtml' => true,
			        'quote-ampersand' => true,
					'output-xhtml' => true,
					'output-encoding' => 'utf8',
			        'preserve-entities' => true,
			        'show-errors' => false,
			        'show-warnings' => false,
					'tab-size' => 2,
					'tidy-mark' => false,
					'wrap' => 0
			        );
	$GLOBALS['smarty']->output
		= tidy_repair_string($GLOBALS['smarty']->output, $config);
}
$GLOBALS['smarty']->output =
                str_ireplace('<br>', '<br />', $GLOBALS['smarty']->output);
echo $GLOBALS['smarty']->output;

/******************************************************************************
 ************************** End Script Execution ******************************
 ******************************************************************************/


/*************************** Required Functions *******************************/


/***** Required Files Location *************************************************
 *  Directory name of include directory relative
 *  to $_SERVER['DOCUMENT_ROOT'].  This should not
 *  be stored in a publicly accessible location.
 *  This would need to be modified for a custom
 *  installation, but if you used the script
 *  installer, everything should be fine.
 *
 *      More intelligent.. use blink as trial
 *		Function this -> bottom of this page
 *		Remove need for trailing /
 *		Update path constants
 *
 *	NOTE:
 *		relative may be differ..  from server/script_name and same for
 *      server/request_uri
 ******************************************************************************/

function define_include_location () {

	$siteroot = dirname($_SERVER['SCRIPT_FILENAME']);
	if ($pos = strpos($siteroot, '/admin')) {
        $GLOBALS['adminArea'] = true;
		$siteroot = substr ($siteroot, 0, $pos);
	} else {
      $GLOBALS['adminArea'] = false;
    }
	$base = strrchr($siteroot, '/');

	// The *real* site root
	DEFINE ('SiteRoot', $siteroot);

	if (file_exists(SiteRoot . '/../_public_html'))
		$incLocation = realpath (SiteRoot . '/../_public_html');
	elseif (file_exists(SiteRoot . '/_public_html'))
		$incLocation = SiteRoot . '/_public_html';
	elseif (file_exists(SiteRoot . '/../' . $base))
		$incLocation = realpath (SiteRoot . '/../' . $base);
	elseif (file_exists(SiteRoot . '/' . $base))
		$incLocation = SiteRoot . '/' . $base;
	else
		die ('Mandatory files could not be found');

	DEFINE ('PathIncAbs', $incLocation);

}


/***** Component Loader ********************************************************
 *  This function loads all the required files for
 *  system execution.  Nothing below here should be
 *  changed.  Any needed changes or enhancements that
 *  need to be applied site-wide can just be dropped
 *  in its respective location and will be dynamically
 *  loaded upon page refresh.
 *
 *  TODO: Error handling! **
 *        Move array to constants
 ******************************************************************************/
function load_components () {
    /* These are the names, in order, of the component directories to load
        The constants MUST be the first to load */
    $components = array('constants', 'classes', 'functions');

    /* Load components */
    $files = array();
    foreach ($components as $component) {
        $files = dir_as_file_array (PathIncAbs . '/' . $component . '/');
        foreach ($files as $file) {
        	require_once (PathIncAbs . '/' . $component . '/' . $file);
        }
    }
}


/***** Load Directory Contents as Sorted Array ********************************/
function dir_as_file_array ($dir) {
	$retVal = array();
	if (is_dir($dir)) {
    	$handle=opendir($dir);
    	while ($file = readdir($handle)) {
    		/* Eliminate the junk */
    		if (($file != ".") &&
                ($file != "..") &&
        		($file != "CVS") &&
                ($file != "index.php") &&
                ($file != ".htaccess") &&
                ($file != "file_id.diz") &&
		(substr($file, 0, 1) != '.') &&
		(substr($file, -1) != '~') &&
		(substr($file, -3) != '.bak') &&
		(substr($file, -3) != '.old') &&
		(substr($file, -3) != '.tmp') &&
		(substr($file, -3) != '.log') &&
		(substr($file, -3) != '.swp')
		) {
    			$retVal[] = $file;
    		}
    	}
    	/* Clean up and sort */
    	closedir($handle);
    	if (count($retVal) < 1) {
    		return false;
    	} else {
    		sort($retVal);
    		return $retVal;
    	}
    } else {
    	return false;
    }
// End dir_as_file_array
}


/***** Start Load Timer *******************************************************/

function load_timer_start() {
    $loadBegin = microtime();
    $loadArray = explode(" ", $loadBegin);
    $loadBegin = $loadArray[1] + $loadArray[0];
    return $loadBegin;
}


/***** End Load Timer *********************************************************/

function load_timer_end($loadBegin) {

    // Move this into Smarty?
    $loadEnd = microtime();
    $loadArray = explode(" ", $loadEnd);
    $loadEnd = $loadArray[1] + $loadArray[0];
    $load = $loadEnd - $loadBegin;
    $load = round($load,3);
    if ($GLOBALS['jwsfConfig']->SiteShowLoad == 'true') {
    	$loadArray = array($load, $GLOBALS['numQueries']);
        $GLOBALS['smarty']->assign('jwsfLoad', $loadArray);
    }

}

?>
