-- phpMyAdmin SQL Dump
-- version 2.11.9.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 26, 2009 at 12:11 AM
-- Server version: 5.0.81
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `lookinto_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `JWSF_ConfigNumeric`
--

DROP TABLE IF EXISTS `JWSF_ConfigNumeric`;
CREATE TABLE `JWSF_ConfigNumeric` (
  `id` tinyint(4) unsigned NOT NULL auto_increment,
  `name` tinytext collate utf8_unicode_ci NOT NULL,
  `value` tinytext collate utf8_unicode_ci NOT NULL,
  `description` text collate utf8_unicode_ci NOT NULL,
  `stamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='This table holds site configuration' AUTO_INCREMENT=21 ;

--
-- Dumping data for table `JWSF_ConfigNumeric`
--

INSERT INTO `JWSF_ConfigNumeric` (`id`, `name`, `value`, `description`, `stamp`) VALUES
(1, 'PasswordLengthMinimum', '7', 'The minimum length of allowed user passwords.', '2009-01-11 02:55:22'),
(2, 'PasswordLengthMaximum', '255', 'The longest a system password can be.', '2009-01-11 02:55:22'),
(3, 'GenerateSelectYearsForwardStart', '2009', 'This is for CC expiration form element.. should be made dynamic!', '2009-01-09 17:33:06'),
(4, 'NavMaxDepthPrimary', '3', 'This sets the depth level for the navigational hierarchy of the primary navigational menu.', '2009-03-13 21:37:04'),
(5, 'PageDefaultId', '1', 'This sets the page id of the page that will load as the front page, or when a page that does not exist is requested.', '2009-01-09 18:02:28'),
(6, 'PageDefaultPid', '0', 'The default parent page id when creating new pages.', '2009-01-09 18:02:28'),
(7, 'PageDefaultAccessLevel', '0', 'The default level an anonymous user functions at, and also the default lower access level when creating new pages', '2009-01-09 18:10:08'),
(8, 'PageDefaultSo', '10', 'The default sort order pages receive.  0 means reverse chronological, 255 means chronological.', '2009-01-09 21:05:08'),
(9, 'PageDefaultContentLimit', '0', '0 = unlimited, anything else will be limited to that number of content blocks', '2009-01-09 18:14:14'),
(10, 'PageDefaultLocation', '1', 'The default navigation location of new pages: 0 = hidden, 1 = primary, 2 = secondary, 3 = primary and secondary', '2009-01-09 18:17:56'),
(12, 'PageExpireTime', '864000', 'This tells search engines how often the content is likely to be refreshed, but is not really heavily used.  Default is 864000 (10 days).  The measurement is in seconds.', '2009-01-09 18:19:50'),
(13, 'PageDefaultPriority', '5', 'Google site map priority, valid values are 0 - 10.  Search engines to rank your pages against each other, not other sites.  Default is 5.', '2009-01-09 19:45:33'),
(14, 'PageDefaultFrequency', '5', 'Valid values are the corresponding keys defined in JWSF_predefSiteMapFrequency: 1 = always, 2 = hourly, 3 = daily, 4 = weekly, 5 = monthly, 6 = yearly, 7 = never.  Lying will not help your search engine results, so use the most appropriate flag.\r\n', '2009-01-10 10:23:00'),
(15, 'AdminLevelLow', '254', 'Users of this level or higher will be considered to be administrators.  255 is the default just for safety.  In reality, you may have multiple levels of administrators.', '2009-03-12 20:56:31'),
(16, 'MaxFailedLogins', '3', 'The number of times in a row a user fails to authenticate.  The counter is reset each time a user authenticates successfully.', '2009-03-12 20:55:57'),
(17, 'UserDefaultLevel', '1', 'This is the default access level assigned when creating new users.', '2009-03-12 20:56:31'),
(18, 'UserDefaultActive', '1', 'Determines active status when creating new users.', '2009-01-14 21:17:03'),
(19, 'DefaultResolutionAdminArea', '1', 'Setting this to various settings will impact the default ''zoom'' level of the adminstration area.  Setting it to 0 will reset all sizes to default.', '2009-03-12 20:56:31'),
(20, 'NavMaxDepthBreadCrumb', '3', 'This limits the number of entries in the navigation bread crumb trail.', '2009-03-07 19:31:32');

-- --------------------------------------------------------

--
-- Table structure for table `JWSF_ConfigStrings`
--

DROP TABLE IF EXISTS `JWSF_ConfigStrings`;
CREATE TABLE `JWSF_ConfigStrings` (
  `id` tinyint(4) unsigned NOT NULL auto_increment,
  `name` tinytext collate utf8_unicode_ci NOT NULL,
  `value` tinytext collate utf8_unicode_ci NOT NULL,
  `description` text collate utf8_unicode_ci NOT NULL,
  `stamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='This table holds site configuration' AUTO_INCREMENT=28 ;

--
-- Dumping data for table `JWSF_ConfigStrings`
--

INSERT INTO `JWSF_ConfigStrings` (`id`, `name`, `value`, `description`, `stamp`) VALUES
(1, 'MIMEPreferred', 'application/xhtml+xml', 'This tells the browser how to interpret the documents that the system serves.  This should not be changed unless you *really* know what you''re doing!', '2009-03-08 12:35:19'),
(2, 'MIMEFallback', 'text/html; charset=UTF-8', 'This tells the browser how to interpret the documents that the system serves if it cannot parse the preferred type.  This should not be changed unless you *really* know what you''re doing!', '2009-03-08 14:10:27'),
(3, 'SiteTitle', 'Jason White Slim Frame CMS Development Area', 'This is displayed in the title bar of every page, and at the top of the page content if ''showPageTitle'' config switch is set to ''true''.', '2009-01-09 17:19:29'),
(4, 'HomeLinkAlt', 'Home', 'This text is displayed if there is no logo graphic displayed as a way to get back to the site root.  A graphic may not be displayed on many browsers that are not capable of displaying graphics.', '2009-01-09 17:19:29'),
(5, 'HomeLinkTitle', 'Click here to return to the main page!', 'This text will be displayed when a user holds their mouse over the logo header graphic as a clickable link to return to the site root.', '2009-01-09 17:19:29'),
(6, 'SiteTitleShort', 'JWSF Web Site', 'Used in certain areas for brevity, if full SiteTitle is long (ie; email subject preludes).  This must be set, even if to the same value as SiteTitle.', '2009-03-12 20:57:24'),
(7, 'PageDefaultTitleNav', 'New Page', 'This is the default page navigation title, which will appear in the ''create new page'' ''navigation title'' form field by default and must be changed to add a new page', '2009-01-09 18:22:13'),
(8, 'PageDefaultTitle', 'JW Slim Frame Page', 'This is the default page navigation title, which will appear in the ''create new page'' ''navigation title'' form field by default and must be changed to add a new page', '2009-01-09 18:22:13'),
(9, 'PageDefaultKeywords', 'jason, white, slim, frame, jwsf, cms, content, management, system, w3c, compliant, xhtml, php, css, javascript, mysql, marquette, michigan', 'These are the default page keywords which some (but not many) search engines may use.  They are no longer configurable on a page-by-page basis because search engines basically ignore them.', '2009-01-09 18:26:50'),
(10, 'PageDefaultDescription', 'The Jason White Slim Frame (JWSF) Content Management Systems is a highly extensible W3C compliant web site framework.', 'This is the default page page description, which will appear in the ''create new page'' ''description'' form field by default and may be changed on a page by page basis.  These descriptions will be used by search engines to summarize your pages.', '2009-01-09 18:26:50'),
(14, 'newUserActivationSubject', 'JWSF Site :: Action Required', 'This is the subject line of the email sent to users in their activation letter.', '2009-03-06 17:53:53'),
(13, 'PageDefaultImgUri', '', 'If this is set, it will place this image on any page that does not have a page-image set.  Maybe.', '2009-02-26 16:22:13'),
(15, 'NewUserSubjectWelcome', 'JWSF Site :: Welcome!', 'This is the subject line of the email sent to the users in their welcome email.', '2009-03-12 20:57:24'),
(16, 'NewUserEmailFrom', 'registration@lookintomycode.com', 'This is the reply-to address in the welcome and activation emails.', '2009-01-11 05:02:33'),
(17, 'NewUserSubjectActivation', 'JWSF Site :: Action Required', 'This is the subject line of the activation email.', '2009-03-06 17:53:53'),
(18, 'NewUserEmailTagline', 'The Jason White Slim Frame System', 'This text will be placed on the lines following ''Sincerely,'' in the welcome, activation, and probably any other, emails sent from the system.', '2009-01-11 04:28:05'),
(19, 'defaultAdminMod', 'UserEditor', 'The name of the default admin module to load when accessing /admin with no parameters.', '2009-01-12 15:17:18'),
(20, 'TimeZoneAbv', 'EST', 'The three letter time zone code that you would wish your system to use.  Currently only used by sitemap generator.', '2009-02-23 00:55:56'),
(21, 'PageDefaultAccessLevelHigh', '255', '', '2009-02-23 03:19:00'),
(22, 'PageDefaultSemanticURI', 'null', 'This is most certainly left set with no value.  Trust me on that one.', '2009-03-15 19:30:28'),
(23, 'PageDefaultImgURI', 'null', 'The absolute path to a default page image (ie; ''/_img/pages/default.png).', '2009-03-15 19:30:28'),
(24, 'TimeZone', 'America/Detroit', 'This is the full timezone string that you would like to use.', '2009-03-12 16:31:45'),
(25, 'SmartyContentDefault', 'content_00.tpl', 'This is the name of the default content template.', '2009-03-15 23:24:09'),
(26, 'SiteLanguage', 'en-us', 'This is the default site language string and *must* be a valid xml language string as it is used in the RSS feed.', '2009-10-11 12:15:12'),
(27, 'SiteRSSLimit', '20', 'This defines how many articles will appear in the RSS feed.  Feed is sorted in reverse chronological order.', '2009-10-11 13:37:17');

-- --------------------------------------------------------

--
-- Table structure for table `JWSF_ConfigSwitches`
--

DROP TABLE IF EXISTS `JWSF_ConfigSwitches`;
CREATE TABLE `JWSF_ConfigSwitches` (
  `id` tinyint(4) unsigned NOT NULL auto_increment,
  `name` tinytext collate utf8_unicode_ci NOT NULL,
  `value` tinytext collate utf8_unicode_ci NOT NULL,
  `description` text collate utf8_unicode_ci NOT NULL,
  `stamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='This table holds site configuration' AUTO_INCREMENT=50 ;

--
-- Dumping data for table `JWSF_ConfigSwitches`
--

INSERT INTO `JWSF_ConfigSwitches` (`id`, `name`, `value`, `description`, `stamp`) VALUES
(1, 'SiteAlwaysRefresh', 'false', 'Setting this to true disables browser caching.  This should be set to false except during development.', '2009-03-07 14:29:25'),
(2, 'ThemeLoad', 'true', 'This determines whether or not any thematic information will be loaded at all.  If disabled, under no circumstances will any CSS or Javascript be loaded.', '2009-07-27 21:26:51'),
(3, 'ThemeActive', 'Default-White', 'This setting determines the default theme, if it is available.  If it is unavailable, the system will fall back to the skeleton theme.  Setting this does not affect a user''s selected theme, only the default.', '2009-03-19 11:01:12'),
(4, 'ThemeLoadCss', 'true', 'Disabling this will prevent CSS from loading in any theme.', '2009-03-07 14:40:27'),
(5, 'ThemeLoadJs', 'true', 'Setting this will disable Javascript from loading in any theme.', '2009-03-07 14:40:27'),
(6, 'ThemeAllowUserSelect', 'true', 'Setting this to true allows users to select any theme they like.  Setting it to false forces them into the default theme selected above.', '2009-03-07 14:27:53'),
(7, 'SiteUseFavIcon', 'true', 'Allow favicon.ico, if file exists?', '2009-03-11 13:13:13'),
(8, 'PageAllowComments', 'false', 'Whether or not to allow comments on pages.  If this is not set to true, commenting will be completely turned off.', '2009-03-07 15:00:06'),
(9, 'PageAllowRatings', 'false', 'Whether or not to allow ratings on pages.  If this is not set to true, commenting will be completely turned off.', '2009-03-07 15:00:06'),
(10, 'UsersAllow', 'true', 'Whether or not to use the JWSF User Authentication System.  Setting this to true will insert the authentication form and check user access levels on every page.', '2009-03-07 14:20:24'),
(12, 'SiteShowTemplateNo', 'false', 'If this is set to true, each page will show it''s content template number at the bottom of the content area.', '2009-03-28 23:09:50'),
(13, 'PageShowTitle', 'true', 'If this is set to true, the full page title will be output at the top of the content area on each page.  If set to false, the full page title will only be displayed in the title bar.', '2009-03-07 16:29:57'),
(17, 'SiteShowPaymentLogo', 'false', 'Setting this to true will show the standard credit card logos in the site footer, setting it to false will not.', '2009-03-11 15:51:26'),
(18, 'PageDefaultActive', 'true', 'Whether the default page is active or not', '2009-01-09 18:12:49'),
(19, 'PageDefaultAllowComments', 'false', 'Whether or not to allow comments by default on new pages', '2009-01-09 18:12:49'),
(20, 'PageDefaultAllowRatings', 'false', 'Whether or not to allow ratings on new pages by default', '2009-01-09 18:13:23'),
(21, 'PageDefaultCollapse', 'false', 'Whether or not new page content blocks are collapsed by default.  If so, all content will shrink, showing just the block headers until the user clicks on them.', '2009-01-09 19:43:28'),
(22, 'SiteShowLoad', 'true', 'This determines whether or not to show page load time and number of queries in the footer.', '2009-03-07 16:27:58'),
(23, 'UserShowInfo', 'true', 'This determines whether or not to display the ''Welcome, User Name'' text.', '2009-03-07 16:27:58'),
(25, 'AdminRequiresLogin', 'false', 'If this is set to true, a user must be logged in to an administrative level account to access the admin area.  Note, some features will require login to function, but not all.', '2009-03-12 21:27:03'),
(26, 'AdminSeesAll', 'true', 'If this is set to true, users with administrative level access (AdminLevelLow) will see all pages in the navigation, regardless of their active/access level settings.', '2009-03-07 17:40:14'),
(27, 'UseSiteMap', 'true', 'If this value is set to false, the sitemap.xml file will not be utilized and should be removed from the system.  This should only be disabled if it fails due to host configuration.', '2009-02-24 11:43:30'),
(28, 'NavAllowNonSemanticHrefs', 'true', 'If this is set to true, all pages in the navigational hierarchy will be clickable.  If it is set to false, only pages that have a semantic URI will be clickable.  This is useful if you desire having sub-menus without having an informative parent page... which I recommend against, but what can I say.. some people don''t like to generate content I guess.', '2009-03-09 14:52:37'),
(29, 'NavShowBreadCrumb', 'true', 'If this is set to true, a container will be output with a list of navigation ending with the currently selected page and with its parent and that parent''s parent and so on up the chain to the top-most level which would be the first item in the list.  If set to false, no bread crumb trail will be created.', '2009-02-26 02:21:39'),
(30, 'NavShowRelated', 'true', 'If this is set to true, a related content container will be output on each page *if* that page has siblings or children *and* either ShowSiblings or ShowChildren (or both) are set to true.  If set to false, no container will be created regardless of the ShowSiblings/ShowChildren settings.', '2009-02-25 09:40:42'),
(31, 'NavShowSiblings', 'true', 'If this is set to true a list of pages will be output which all have the same parent as the selected page (selected page excluded).  This applies only to non-top level pages.  If set to false, no sibling related content will be output.', '2009-03-11 21:32:51'),
(32, 'NavShowChildren', 'true', 'If this is set to true, related sub-content will be output inside of the related content container.  This would be a list of pages that have the selected page as their parent (and no deeper).  This applies to all pages including top-level pages.  If set to false, child relationships will not be created.', '2009-02-25 09:40:42'),
(33, 'NavUsePrimary', 'true', 'If this is set to true, the primary navigational unit will be output.  By default this will display a three-level hierarchy (changeable in numerics).  Linearly speaking, it will default to being displayed in the post-content container.  CSS-wise, it will traditionally be a left-handed roll-over menu although it may also be used for a roll-over menu in the header.  If set to false, no primary navigation code will be parsed.', '2009-03-04 20:43:01'),
(34, 'NavUseSecondary', 'true', 'If this is set to true, the secondary navigational unit will be output.  By default this will output a single level hierarchy.  Linearly speaking, it will default to being displayed in the footer container.  CSS-wise, it will traditionally be the footer.  If set to false, no secondary navigation code will be parsed.', '2009-02-25 09:36:43'),
(35, 'NavUseTertiary', 'true', 'If this is set to true, the tertiary navigational unit will be output.  By default this will display a single hierarchy.  Linearly speaking, it will default to being displayed last in the pre-content container.  CSS-wise, it would traditionally be a quick link menu bar between the logo and content areas although it may also be used for anything depending on how you CSS it.  If set to false, no tertiary navigation code will be parsed.', '2009-02-25 09:38:41'),
(36, 'NavShowSkipTo', 'true', 'If this is set to true, the very first thing output to the browser will be a ''Skip to Navigation'' link.  Very handy for users of alternative browsers, since by default primary navigation is below the content.', '2009-02-25 23:16:05'),
(37, 'UsePageImages', 'true', 'If this is set to true and a page image or default page image is set, then page-by-page images will be output (in the header by default).  If set to false, no page-specific images will be output automagically.', '2009-02-25 23:35:02'),
(38, 'SmartyCaching', 'false', 'If this is set to true, Smarty caching will be turned on.  Which is really a very good thing *if* modules are properly programmed to regenerate their cache on an appropriate time basis.  It will also reduce server load.  Just realize that your updates will not take immediate effect unless you turn this off while editing, or use the ''Flush Smarty Cache'' feature.  If you are experiencing issues with things not updating quickly enough, turn this off.', '2009-03-03 00:59:00'),
(39, 'UseTrackSQL', 'false', 'If this is set to true (a good default), every SQL query will be logged as sent to the database in the tracking database.  Note that with larger, more dynamic sites this may have quite an impact on performance under heavy usage.', '2009-03-12 16:46:44'),
(40, 'NavForceNonSemantic', 'false', 'If this is set to true, all links will be put out with /?id=XX for internal links.  Otherwise, normal parsing of friendly URLs will proceed.  Default is, of course, false (friendly URLs enabled).', '2009-03-07 14:33:32'),
(41, 'ThemesUseDynamic', 'true', 'If this is set to true, the dynamic layout editor will be available, and the dynamic layout database will be consulted for layout information.', '2009-03-03 17:20:23'),
(43, 'UseTinyMCE', 'true', 'If this is set to true, the content editor will use the fancy editor.  If set to false, you will have to input special code by hand.', '2009-03-06 16:44:52'),
(42, 'SessionsUse', 'true', 'If this is set to true, no JWSF will not attempt to create a session (no cookies mode).  User selectable themes and the user system will probably not function.', '2009-03-04 14:14:26'),
(44, 'UseTidy', 'false', 'If this is set to false, output will not be parsed with Tidy.', '2009-10-25 11:02:05'),
(46, 'SmartyFallbackAdvanced', 'true', 'When set to true, the advanced system smarty templates are used when theme specific smarty templates are not found.  These should be identical to the standard fall back templates, but with the addition of JWSF_BlockToggle functions to help with contextual help and other little niceties.  Using the advanced fallback templates, your themes should have the JWSF_Site.js file included. **NOTE** Changing this will require you to flush the smarty cache!', '2009-10-09 20:44:42'),
(47, 'ProtectionCaptcha', 'true', 'Setting this value to true will enable the robot check for publicly accessible forms that are captcha enabled.', '2009-07-15 01:09:35'),
(48, 'SiteRssSort', 'true', 'This value determines how the RSS feed is organized.  To sort by last created choose false, to sort by last modified, choose true.', '2009-10-11 09:00:54'),
(49, 'SiteEngageSocial', 'true', 'If this is set to true, each page will have an unordered list of sharing links such as ''permalink'', ''share this'', ''email this'', etc.', '2009-10-22 18:53:41');

-- --------------------------------------------------------

--
-- Table structure for table `JWSF_DynamicLayoutItems`
--

DROP TABLE IF EXISTS `JWSF_DynamicLayoutItems`;
CREATE TABLE `JWSF_DynamicLayoutItems` (
  `id` tinyint(3) unsigned NOT NULL auto_increment,
  `rid` tinyint(3) unsigned NOT NULL COMMENT 'This defines the item parent, which is the id of the LayoutMeta row which uses these values.',
  `module` tinytext collate utf8_unicode_ci NOT NULL,
  `column` tinyint(3) unsigned NOT NULL,
  `so` tinyint(3) unsigned NOT NULL,
  `description` tinytext collate utf8_unicode_ci NOT NULL,
  `stamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='This table stores dynamic layout item assignments' AUTO_INCREMENT=1 ;

--
-- Dumping data for table `JWSF_DynamicLayoutItems`
--


-- --------------------------------------------------------

--
-- Table structure for table `JWSF_DynamicLayoutMeta`
--

DROP TABLE IF EXISTS `JWSF_DynamicLayoutMeta`;
CREATE TABLE `JWSF_DynamicLayoutMeta` (
  `id` tinyint(3) unsigned NOT NULL auto_increment,
  `title` tinytext collate utf8_unicode_ci NOT NULL,
  `description` tinytext collate utf8_unicode_ci NOT NULL,
  `stamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='This table stores information related to dynamic themes' AUTO_INCREMENT=1 ;

--
-- Dumping data for table `JWSF_DynamicLayoutMeta`
--


-- --------------------------------------------------------

--
-- Table structure for table `JWSF_DynamicLayoutRows`
--

DROP TABLE IF EXISTS `JWSF_DynamicLayoutRows`;
CREATE TABLE `JWSF_DynamicLayoutRows` (
  `id` tinyint(3) unsigned NOT NULL auto_increment,
  `pid` tinyint(3) unsigned NOT NULL,
  `num_columns` tinyint(3) unsigned NOT NULL,
  `output_empty` tinyint(1) NOT NULL,
  `so` tinyint(3) unsigned NOT NULL,
  `description` tinytext collate utf8_unicode_ci NOT NULL,
  `stamp` timestamp NOT NULL default '0000-00-00 00:00:00' on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `JWSF_DynamicLayoutRows`
--


-- --------------------------------------------------------

--
-- Table structure for table `JWSF_LanguageStrings-EN`
--

DROP TABLE IF EXISTS `JWSF_LanguageStrings-EN`;
CREATE TABLE `JWSF_LanguageStrings-EN` (
  `id` tinyint(4) unsigned NOT NULL auto_increment,
  `name` tinytext collate utf8_unicode_ci NOT NULL,
  `value` tinytext collate utf8_unicode_ci NOT NULL,
  `description` text collate utf8_unicode_ci NOT NULL,
  `stamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='This table holds site configuration' AUTO_INCREMENT=6 ;

--
-- Dumping data for table `JWSF_LanguageStrings-EN`
--

INSERT INTO `JWSF_LanguageStrings-EN` (`id`, `name`, `value`, `description`, `stamp`) VALUES
(1, 'MIMEPreferred', 'application/xhtml+xml', '', '2009-01-07 18:59:32'),
(2, 'MIMEFallback', 'text/html; charset=utf-8', '', '2009-01-07 18:59:32'),
(3, 'SiteTitle', 'Jason White Slim Frame CMS Development Area', '', '2009-01-07 19:00:08'),
(4, 'HomeLinkAlt', 'Home', '', '2009-01-07 19:00:08'),
(5, 'HomeLinkTitle', 'Click here to return to the main page!', '', '2009-01-07 19:00:29');

-- --------------------------------------------------------

--
-- Table structure for table `JWSF_ModuleContactUsBCC`
--

DROP TABLE IF EXISTS `JWSF_ModuleContactUsBCC`;
CREATE TABLE `JWSF_ModuleContactUsBCC` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `item` tinytext character set latin1 NOT NULL COMMENT 'Name',
  `value` varchar(320) character set latin1 NOT NULL COMMENT 'Email Address',
  `stamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='This table contains the list of address that will be BCC''d' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `JWSF_ModuleContactUsBCC`
--

INSERT INTO `JWSF_ModuleContactUsBCC` (`id`, `item`, `value`, `stamp`) VALUES
(1, 'Jason', 'jawhite@nmu.edu', '2009-01-08 01:41:07');

-- --------------------------------------------------------

--
-- Table structure for table `JWSF_ModuleContactUsLog`
--

DROP TABLE IF EXISTS `JWSF_ModuleContactUsLog`;
CREATE TABLE `JWSF_ModuleContactUsLog` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `recipient` tinytext collate utf8_unicode_ci NOT NULL,
  `subject` tinytext collate utf8_unicode_ci NOT NULL,
  `name` tinytext collate utf8_unicode_ci NOT NULL,
  `email` varchar(320) collate utf8_unicode_ci NOT NULL,
  `comments` text collate utf8_unicode_ci NOT NULL,
  `company` tinytext collate utf8_unicode_ci NOT NULL,
  `telephone` varchar(20) collate utf8_unicode_ci NOT NULL,
  `address` tinytext collate utf8_unicode_ci NOT NULL,
  `address2` tinytext collate utf8_unicode_ci NOT NULL,
  `city` tinytext collate utf8_unicode_ci NOT NULL,
  `state` tinytext collate utf8_unicode_ci NOT NULL,
  `country` tinytext collate utf8_unicode_ci NOT NULL,
  `zip` varchar(10) collate utf8_unicode_ci NOT NULL,
  `fax` varchar(20) collate utf8_unicode_ci NOT NULL,
  `ip` varchar(15) collate utf8_unicode_ci NOT NULL,
  `stamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='This table logs all successful communication from the contac' AUTO_INCREMENT=10 ;

--
-- Dumping data for table `JWSF_ModuleContactUsLog`
--

INSERT INTO `JWSF_ModuleContactUsLog` (`id`, `recipient`, `subject`, `name`, `email`, `comments`, `company`, `telephone`, `address`, `address2`, `city`, `state`, `country`, `zip`, `fax`, `ip`, `stamp`) VALUES
(1, '1', '2', 'Old Man', 'erwmlw@yahoo.com', 'Looks cleaner and i like the logo', '', '', '', '', '', 'MI', 'US', '', '', '', '2009-02-27 13:40:58'),
(2, '1', '4', 'test', 'test@test.com', 'test', '', '', '', '', '', 'MI', 'US', '', '', '', '2009-03-04 20:45:21'),
(3, '1', '4', 'jason', 'jason@nmu.edu', 'asdasf', '', '', '', '', '', 'MI', 'US', '', '', '', '2009-03-06 16:55:26'),
(4, '1', '4', 'test', 'test@test.com', 'test', '', '', '', '', '', 'MI', 'US', '', '', '', '2009-03-06 17:32:22'),
(5, '1', '4', 'Super Admin', 'super@admin.com', '\r\n', '', '', '', '', '', '0', '0', '', '', '', '2009-03-07 19:50:44'),
(6, '1', '4', 'test', 'test@test.com', '\r\ntest', '', '', '', '', '', 'MI', 'US', '', '', '', '2009-03-19 23:17:51'),
(7, '1', '3', 'auqgtpke', 'oopugy@soympc.com', 'xYF2pY  <a href="http://pknvcomkazan.com/">pknvcomkazan</a>, [url=http://rksbxanhinun.com/]rksbxanhinun[/url], [link=http://twibfxseqxrw.com/]twibfxseqxrw[/link], http://gbturmaailzo.com/', 'SutxTjIQTHWSr', 'bxZGUCazTLCveQ', 'bNXPpKXdDrVmQAj', 'GlDAKNWnkck', 'eIGLFVDJPxdPJR', 'MI', 'US', 'bBKlnBqGsp', 'xFmjmdkahPMeohYq', '', '2009-06-21 09:20:28'),
(8, '1', '4', 'jason', 'me@square-guy.com', '\r\nasfd', '', '', '', '', '', 'MI', 'US', '', '', '97.83.149.156', '2009-07-13 20:51:15'),
(9, '1', '1', 'Susan G. Wideman', 'susan@widemanlaw.com', 'Do you design websites?  If so I would like to talk with you? ', 'The Wideman Law Center', '906-226-4600', '', '', '', 'MI', 'US', '', '906-226-8216', '24.213.40.154', '2009-09-23 17:06:31');

-- --------------------------------------------------------

--
-- Table structure for table `JWSF_ModuleContactUsLogFail`
--

DROP TABLE IF EXISTS `JWSF_ModuleContactUsLogFail`;
CREATE TABLE `JWSF_ModuleContactUsLogFail` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `recipient` tinytext collate utf8_unicode_ci NOT NULL,
  `subject` tinytext collate utf8_unicode_ci NOT NULL,
  `name` tinytext collate utf8_unicode_ci NOT NULL,
  `email` varchar(320) collate utf8_unicode_ci NOT NULL,
  `comments` text collate utf8_unicode_ci NOT NULL,
  `company` tinytext collate utf8_unicode_ci NOT NULL,
  `telephone` varchar(20) collate utf8_unicode_ci NOT NULL,
  `address` tinytext collate utf8_unicode_ci NOT NULL,
  `address2` tinytext collate utf8_unicode_ci NOT NULL,
  `city` tinytext collate utf8_unicode_ci NOT NULL,
  `state` tinytext collate utf8_unicode_ci NOT NULL,
  `country` tinytext collate utf8_unicode_ci NOT NULL,
  `zip` varchar(10) collate utf8_unicode_ci NOT NULL,
  `fax` varchar(20) collate utf8_unicode_ci NOT NULL,
  `ip` varchar(15) collate utf8_unicode_ci NOT NULL,
  `stamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='This table logs all successful communication from the contac' AUTO_INCREMENT=1 ;

--
-- Dumping data for table `JWSF_ModuleContactUsLogFail`
--


-- --------------------------------------------------------

--
-- Table structure for table `JWSF_ModuleContactUsRecips`
--

DROP TABLE IF EXISTS `JWSF_ModuleContactUsRecips`;
CREATE TABLE `JWSF_ModuleContactUsRecips` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `item` tinytext character set latin1 NOT NULL COMMENT 'Name',
  `value` varchar(320) character set latin1 NOT NULL COMMENT 'Email Address',
  `stamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='This table contains the list of valid contact recipients.' AUTO_INCREMENT=5 ;

--
-- Dumping data for table `JWSF_ModuleContactUsRecips`
--

INSERT INTO `JWSF_ModuleContactUsRecips` (`id`, `item`, `value`, `stamp`) VALUES
(1, 'Jason', 'jason@aplusecs.com', '2009-01-08 01:40:48');

-- --------------------------------------------------------

--
-- Table structure for table `JWSF_ModuleContactUsSubjs`
--

DROP TABLE IF EXISTS `JWSF_ModuleContactUsSubjs`;
CREATE TABLE `JWSF_ModuleContactUsSubjs` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `item` tinytext character set latin1 NOT NULL COMMENT 'Name',
  `stamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='This table contains the list of valid contact subjects.' AUTO_INCREMENT=6 ;

--
-- Dumping data for table `JWSF_ModuleContactUsSubjs`
--

INSERT INTO `JWSF_ModuleContactUsSubjs` (`id`, `item`, `stamp`) VALUES
(1, 'Question', '2008-08-21 13:35:21'),
(2, 'Comment', '2008-08-21 13:35:21'),
(3, 'Concern', '2008-08-21 13:35:29'),
(4, 'Bug Report', '2009-01-04 05:41:19');

-- --------------------------------------------------------

--
-- Table structure for table `JWSF_PageContent`
--

DROP TABLE IF EXISTS `JWSF_PageContent`;
CREATE TABLE `JWSF_PageContent` (
  `id` tinyint(4) unsigned NOT NULL auto_increment,
  `pid` tinyint(4) unsigned NOT NULL,
  `acsLow` tinyint(3) unsigned NOT NULL,
  `acsHigh` tinyint(3) unsigned NOT NULL,
  `so` tinyint(3) unsigned NOT NULL,
  `active` tinyint(1) NOT NULL,
  `cssClass` tinytext collate utf8_unicode_ci NOT NULL,
  `cssID` tinytext collate utf8_unicode_ci NOT NULL,
  `header` tinytext collate utf8_unicode_ci NOT NULL,
  `content` text collate utf8_unicode_ci NOT NULL,
  `imgURI` tinytext collate utf8_unicode_ci NOT NULL COMMENT 'This is experimental, good luck!',
  `stamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `headerURI` tinytext collate utf8_unicode_ci NOT NULL COMMENT 'Setting this turns the header into a clickable link.',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='This table tracks page content' AUTO_INCREMENT=165 ;

--
-- Dumping data for table `JWSF_PageContent`
--

INSERT INTO `JWSF_PageContent` (`id`, `pid`, `acsLow`, `acsHigh`, `so`, `active`, `cssClass`, `cssID`, `header`, `content`, `imgURI`, `stamp`, `headerURI`) VALUES
(1, 1, 0, 255, 2, 1, '', '', 'Developer Area', '<p>The system is almost bug free now and it is unlikely that it will break down on you in any way.&nbsp; If it is temporarily broken, it is most likely just a glitch in my automatic upgrade and should be quite temporary.</p>\r\n<p>The database for public versions of the system automatically resets every two hours so you may make any changes you like and they will be reset shorty.</p>', '', '0000-00-00 00:00:00', ''),
(2, 1, 0, 255, 1, 1, '', '', 'Live Demo', '<p>Welcome to Jason White''s Slim Frame live demonstration area.  This is a full fledged, fully functional site operating solely for you to play with.  You may log in with any of the following user names and password combinations, or you may register a new user from the registration link.</p>\r\n<dl> <dt> Anonymous </dt> <dd> If you are not logged in, you are an anonymous user with no ''Your Settings'' area.. You can only access content with a ''low level'' access setting of 0 (public) and if you use the contact script you must enter your name and email address (other fields are presented as well). </dd> <dt> regular@user.com / regular1 </dt> <dd> This is a basic user that has access to additional content, a ''Your Settings'' page to control their name, password, etc.. and when using the contact page your personal information does not need to be entered. </dd> <dt> normal@admin.com / admin001 </dt> <dd> This user can access the /admin area (either directly or via the menu link), but is prohibited from performing the more delicate administration features.</dd> <dt> super@admin.com / admin001 </dt> <dd> This is the super user.  It can do anything.</dd></dl><p>If any of these are not working, someone else may have been playing around before you and changed a password or deleted a user.  Don''t worry, it will reset within 2 hours and be nice and fresh.</p>', '', '2009-10-11 16:16:48', ''),
(3, 2, 0, 255, 0, 1, '', '', 'I Love Fish!', '<p>This is the default contact page and should be changed via the administration area.  It should at least include basic real-world contact information.</p><p>Hey, so I said I love fish and I do.&nbsp; I really do.<br></p>', '', '0000-00-00 00:00:00', ''),
(4, 8, 0, 255, 1, 1, '', '', 'The Jason White Slim Frame', '<p>The Jason White Slim Frame (JWSF) is the heart and soul of every web based application (aka: website) that I have created since approximately the year 2000.  The JWSF Content Management System (CMS) is a feature packed W3C compliant Content Management System (CMS) for browser-based applications and web sites.  Able to drive simple dynamic sites, XML driven eCommerce sites and even mobile device applications, the JWSF system generates 100% W3C compliant XHTML 1.1 and CSS 2.1 code using PHP 5, Smarty templates, MySQL 5, and Javascript.  All code has been written from scratch by Jason (with exception to Smarty and TinyMCE).</p>', 'test.gif', '2009-10-11 11:39:37', ''),
(5, 8, 0, 255, 4, 1, '', '', 'Standards, Graceful Degradation and Best Practices', '<p>Standards for browser related code are defined by the <a style="" href="http://w3c.org" mce_href="http://w3c.org">World Wide Web Consortium (W3C)</a> and this system strives to generate each page with 100% compliance.  Standards ensure continuity between browsers regardless of platform or display capabilities and therefore ensure that your content can reach the broadest possible audience effectively.&nbsp; It also helps search engines understand and index your content properly.<br> <br> Graceful degradation is absolutely essential and unfortunately is often neglected by many web designers.  What happens to your site design and content if someone visits your site and is not using a fancy new browser?  Can they still navigate?  Does your content show up?  Many of the more intelligent web browsing citizens out there use browser add-ons to block potentially malicious javascript, cookies, flash files and other web extensions which can drastically affect how your site renders when they visit it.  Also, there are <b>many</b> users on the Internet using text-based, mobile, braile, and aural browsers that rely on your site being intelligently designed and maintained so that they may access your content as well.  Is it fair to deny someone who browses differently than you?  JWSF based applications are sure to display your content regardless of the output medium!</p>', '', '2009-08-18 11:41:14', ''),
(6, 7, 0, 255, 1, 1, '', '', 'How the JWSF CMS Evolved', '<p>I started hand-coding web sites using Notepad and Pico in 1994.  During the winter of 1998-99 I started using PHP, MySQL and Javascript to create dynamic sites.  During the spring of 2000 I developed a methodology for a skinnable template system moving away from Dreamweaver templates in favor of PHP/MySQL/CSS driven templates and launched the first web site on the Internet (that I know of) with user-selectable skins in the fall of 2001. <br /> <br /> The principles behind that original template system drive the JWSF CMS to this day.  Over the years it has seen several names, the primary one being the MC WireFrame (MCWF).  In the year of 2004, Jason upgraded the system from XHTML 1.0 Transitional compliancy to XHTML 1.1 Strict compliancy.  During the winter of 2007-2008, the system was completely re-written using OOP, upgraded to PHP and MySQL versions 5 and renamed to JWSF</p>', '', '2009-03-13 19:38:21', ''),
(7, 8, 0, 255, 5, 1, '', '', 'Languages, Templates and Stylesheets', '<p>The JW Slim Frame uses PHP 5, XHTML 1.1, CSS 2.1, EMCA/JavasScript 1.2, MySQL 5.0, Smarty 2.6 and some Perl 5 under the hood to perform all sorts of magic.  The entire project is hand-coded by yours truly using gedit or vim running under Ubuntu or Lunar Linux.<br> <br> All XHTML is output by the PHP Smarty template engine using hand-coded templates and several hand-written Smarty plug-ins to generate the hierarchical navigation menus. <br> <br> Presentation of the content is handled by hand-written Cascading StyleSheets for maximum speed and compatibility.  Without CSS, all content is displayed perfectly in a text-browser compatible format.</p>', '', '2009-08-18 11:41:14', ''),
(94, 100, 0, 255, 2, 1, '', '', 'Module Ideas', '<ul>\r\n<li>XML Messaging plugins - things like weather, twitter, rss</li>\r\n<li>Voting system</li>\r\n<li>File sharing</li>\r\n<li>Email to friend</li>\r\n<li>etc..</li>\r\n</ul>', '', '2009-03-09 16:39:58', ''),
(11, 3, 0, 255, 0, 1, '', '', 'Developed on the Shores of Superior', 'The JWSF Slim Frame CMS is developed on the south shore of the mighty Lake Superior.\r\n<br /><br />\r\nYou can use <a href="maps.google.com">Google Maps</a> to get the link code to insert your own location below:\r\n<br />\r\n<iframe width="420" height="275" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;hl=en&amp;geocode=&amp;q=marquette,mi&amp;ie=UTF8&amp;s=AARTsJoXeaMz1sfPqRIh75fvtyewPV60Rg&amp;ll=48.166085,-86.748047&amp;spn=16.123955,36.914062&amp;t=h&amp;z=4&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?f=q&amp;hl=en&amp;geocode=&amp;q=marquette,mi&amp;ie=UTF8&amp;ll=48.166085,-86.748047&amp;spn=16.123955,36.914062&amp;t=h&amp;z=4&amp;source=embed" style="color:#0000FF;text-align:left">View Larger Map</a></small>', '', '2009-03-11 16:09:54', ''),
(12, 7, 0, 255, 3, 1, '', '', 'Roadmap to the Future', '<p>Version 2.0 will add features and focus heavily on implementing AJAX and fancier themes.</p>', '', '2009-03-13 19:38:21', ''),
(13, 7, 0, 255, 2, 1, '', '', 'Current Version', '<p>The current stable version is 1.0 alpha.  It is extremely usable and flexible for my own personal usage.  There are a couple system-wide features that are only partially implemented.<br /><br /> Version 1.0 is the target right now and will soon be available as a public beta.</p>', '', '2009-03-13 19:38:21', ''),
(124, 109, 0, 255, 1, 1, '', '', 'Base Code', '<p>An example from the navigation class:</p>\r\n<p>/***** Populate Primary Navigation Object *****<br />&nbsp;*&nbsp; Load navigation<br />&nbsp;*&nbsp; What we should be expecting back here is a<br />&nbsp;*&nbsp; multi-dimensional area of relavent navigation properties<br />&nbsp;*&nbsp; $this-&gt;[#][''id'']<br />&nbsp;*&nbsp; $this-&gt;[#][''pid'']<br />&nbsp;*&nbsp; $this-&gt;[#][''acsLow'']<br />&nbsp;*&nbsp; $this-&gt;[#][''acsHigh'']<br />&nbsp;*&nbsp; $this-&gt;[#][''so'']<br />&nbsp;*&nbsp; $this-&gt;[#][''active'']<br />&nbsp;*&nbsp; $this-&gt;[#][''location'']<br />&nbsp;*&nbsp; $this-&gt;[#][''titleNav'']<br />&nbsp;*&nbsp; $this-&gt;[#][''titleFull'']<br />&nbsp;*&nbsp; $this-&gt;[#][''semanticURI'']<br />&nbsp;*<br />&nbsp;*&nbsp; This is set to the semantic uri if possible, otherwise a GET string with<br />&nbsp;*&nbsp; the page id set.<br />&nbsp;*&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $this-&gt;[#][''href'']<br />&nbsp;*<br />&nbsp;*&nbsp; What we want to build here is a multi-dimensional array of valid<br />&nbsp;*&nbsp; page IDs for use by our Smarty plugin.&nbsp; Page data should only be<br />&nbsp;*&nbsp; added to the array if the page and all of its parents are active<br />&nbsp;*&nbsp; and the user has qualified for access to the page and all of its<br />&nbsp;*&nbsp; parents.<br />&nbsp;*<br />&nbsp;*&nbsp; The checkAccess function weeds out those pages which snuck by<br />&nbsp;*&nbsp; the SQL select requirements but should not actually be displayed<br />&nbsp;*&nbsp; because they are sub-pages of pages that the user does not have<br />&nbsp;*&nbsp; access to.&nbsp; Theoretically, every page SHOULD be set correctly,<br />&nbsp;*&nbsp; but in reality this may not occur so this is here to catch those<br />&nbsp;*&nbsp; oversights.<br />&nbsp;*<br />&nbsp;*&nbsp; This sets the navItems property to an array that will be passed to the<br />&nbsp;*&nbsp; Smarty plugin which builds the XHTML nested navigational list<br />&nbsp;*&nbsp; (jwsfNestedUListFromObject).<br />****************************************/<br /><br />&nbsp;&nbsp;&nbsp; function loadPrimary () {<br /><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; if ($GLOBALS[''jwsfConfig'']-&gt;NavUsePrimary == ''true'') {<br /><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $this-&gt;navPrimary = array();<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; foreach ($this-&gt;navArray as $k =&gt; $v) {<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; switch ($v[''location'']) {<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; case 1:<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; case 3:<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; case 5:<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; case 7:<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $this-&gt;navPrimary[] = $v;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; }<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; }<br /><br />&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; $GLOBALS[''smarty'']-&gt;assign(''navPrimary'', $this-&gt;navPrimary);<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; }<br /><br />&nbsp;&nbsp;&nbsp; // End loadPrimary<br />&nbsp;&nbsp;&nbsp; }</p>\r\n<p>&nbsp;</p>\r\n<hr />\r\n<p>/***** Build Tertiary Navigation *********<br />&nbsp;*<br />&nbsp;*&nbsp; This is identical to the secondary navigation builder except it picks<br />&nbsp;*&nbsp; the tertiary elements.<br />&nbsp;*<br />&nbsp;**********************************/<br /><br />&nbsp;&nbsp;&nbsp;&nbsp; function loadTertiary () {<br /><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; if ($GLOBALS[''jwsfConfig'']-&gt;NavUseTertiary == ''true'') {<br /><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $this-&gt;navTertiary = array();<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; foreach ($this-&gt;navArray as $k =&gt; $v) {<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; switch ($v[''location'']) {<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; case 4:<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; case 5:<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; case 6:<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; case 7:<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $this-&gt;navTertiary[] = $v;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; }<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; }<br /><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $this-&gt;navTertiary<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; = array_sort_by_any_key($this-&gt;navTertiary, ''titleNav'');<br /><br />&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; $GLOBALS[''smarty'']-&gt;assign(''navTertiary'', $this-&gt;navTertiary);<br /><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; }<br />&nbsp;&nbsp;&nbsp; // End buildSecondary<br />&nbsp;&nbsp;&nbsp; }</p>', '', '2009-03-13 19:42:24', ''),
(125, 109, 0, 255, 2, 1, '', '', 'Smarty Templates', '<p>{* Column 2 - Primary Navigation *}<br />&nbsp;&nbsp;&nbsp; &lt;div id="id_containerColumn2"&gt;<br /><br />&nbsp;&nbsp;&nbsp; {* Navigation *}<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;div id="id_containerNavigationPrimary"&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {jwsfNestedUListFromObject navList=$navPrimary}<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;/div&gt;<br /><br />&nbsp;&nbsp;&nbsp; &lt;/div&gt;<br />{* End Column 2 *}</p>\r\n<hr />\r\n<p>{* Tertiary Navigation *}<br />&nbsp;&nbsp;&nbsp; &lt;div id="id_containerNavigationTertiary"&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;ul&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {foreach from=$navTertiary key=key item=value}<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;li&gt;<br />&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; {page-&gt;id assign=idCur}<br />&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; {if $value.id != $idCur}<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;a class="c_button" href="{$value.href}"&gt;<br />&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; {/if}<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {$value.titleNav}<br />&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; {if $value.id != $idCur}<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;/a&gt;<br />&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; {/if}<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;/li&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {/foreach}<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;/ul&gt;<br />&nbsp;&nbsp;&nbsp; &lt;/div&gt;</p>\r\n<hr />\r\n<p>{* Page Title Display *}<br />&nbsp;&nbsp;&nbsp; {config-&gt;PageShowTItle assign=showTitle}<br />&nbsp;&nbsp;&nbsp; {if $showTitle == ''true''}<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;div id="id_containerPageTitle"&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;h1&gt;{page-&gt;titleFull}&lt;/h1&gt;<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;/div&gt;<br />&nbsp;&nbsp;&nbsp; {/if}</p>', '', '2009-03-13 19:42:24', ''),
(14, 12, 0, 255, 1, 1, '', '', 'The Change Log..', '<p>To gain full perspective these notes should be cross-referenced with CVS commit messages and source code comments.</p>', '', '0000-00-00 00:00:00', ''),
(15, 13, 0, 255, 1, 1, '', '', 'Documentation for All', '<p>I am trying to assemble a complete array of documentation for end-users, developers, designers and anything else that I can think of.&nbsp; As my primary navigation menu was getting quite full, I have set the configuration depth limit to 2 levels.&nbsp; All related documentation can be found in the ''directly related content'' section.</p>\r\n<p>If there is interest in any particular area of documentation which does not have enough content for your needs, please let me know and I will get right to work on it.</p>', '', '0000-00-00 00:00:00', ''),
(93, 100, 0, 255, 1, 1, '', '', 'Get Involved!', '<p>The current status of JWSF is ''alpha'' and source code is not yet being publicly released.&nbsp; However, I am interested in accepting submissions for modules and themes.&nbsp; Look over the documentation, and contact me to let me know what you are interested in working on.&nbsp; All work will be appropriately credited, and version 2.0 of the system is open for name changes.</p>', '', '2009-03-09 16:39:58', ''),
(16, 14, 0, 255, 1, 1, '', '', 'A list of things to do..', '<ul>\r\n<li>fix session features - create dumps dir if not exist</li><li>while ur at it, make database stuff smart like that too!<br></li><li>special chars in headers<br></li><li>add page meta fields for link icon, link image.. then link background image and text field for php overlay.. standard bg images via css of course<br></li><li>allow multiple rss feeds based on page id and/or children<br></li><li>fix external img url</li><li>''steal an image'' downloader<br></li><li>auto-generate parent page content based on children contents (meta switch?)<br></li><li>Make php 5.3 and 6 friendly!</li><li>*bug* active page parented by inactive page throws error in navigation class<br></li><li>host tracker/page view stats<br></li><li>Fix page image default null value issue<br></li><li>Show multiple pages modules (news/bloggy style)</li>\r\n<li>Add Google sitemap pinger</li>\r\n<li>Add ShowSiteMap to secondary nav</li>\r\n<li>News</li>\r\n<li>Fix sort order.. alphabetical, reverse chron, chron?</li>\r\n<li>Update load timer</li>\r\n<li>Page view counter</li>\r\n<li>Fix new bug in page deletions.. at least for orphans</li>\r\n<li> mhash check </li>\r\n<li> Form field require/output options from admin.. this may be a while </li>\r\n<li>File uploader, assign to page, name files timestamp-ip-origName, use db and fs, report when files missing from one or the other, do not output link for non-existent files </li>\r\n<li> Per-page images for nav bg/rollover/hilight with ability to choose whether to assign to the anchor or the list item tag </li>\r\n<li> Usage tracking module </li>\r\n<li>Give the admin templates some love </li>\r\n<li> Allow users to hide themselves from who''s online </li>\r\n<li> Auto-prepend/append formatting to semantic url options.. things like date, parent path, etc </li>\r\n<li> User Roles - Level = Semantic; User Permissions = Sub-set of Configuration-&gt;Numerics </li>\r\n<li>Modules - break modules into sub-dirs: admin, admin/modules, section[1,2,3,5], column[1-3] or something </li>\r\n<li> Pimp out that theme engine! </li>\r\n<li> All languages strings to db! </li>\r\n<li> Pagination </li>\r\n<li> Comments/moderation </li>\r\n<li> Allow users level X to have personal posting area.. top posters in nav? </li>\r\n<li> Email to a friend </li>\r\n<li>Page modules, sidebar modules (need to return standardized results) </li>\r\n<li> RSS </li>\r\n<li> Share this feature</li>\r\n<li>welcome message -&gt; config strings </li>\r\n<li> Multi-level admin area </li>\r\n<li>Validate sitemap.. possibly change to w3c schema </li>\r\n<li>OpenID - strike that, openid fail</li>\r\n<li> Double check flood protections </li>\r\n<li> Integrate SMS tools </li>\r\n<li> Integrate a search already </li>\r\n<li>Add database backup and restore </li>\r\n<li> Fix content limit </li>\r\n<li>re-write db interace and add others (postgres, etc) </li>\r\n<li>Tighten up naming conventions, and again.  and again.  Until they are perfect! </li>\r\n<li>Link and Download plug-ins </li>\r\n<li> Move module assignment to pages, be sure to allow more than one module per page </li>\r\n<li>Allow modules to be assigned to various locations on the page, rather than just in the content area </li>\r\n<li> Login help, put welcome/activation emails into smarty templates, put register link into authentication div, implement ''remember me'', add ''forgot password'' </li>\r\n<li> Allow switching of required/non-req fields in user registration </li>\r\n<li>Implement comments, ratings </li>\r\n<li>Form plugin.. store forms in database with automatic generic validation, ability to add custom server side validation (and js of course) </li>\r\n</ul>', '', '2009-10-25 23:58:06', ''),
(59, 14, 0, 255, 2, 1, '', '', 'Version 2.0', '<p>Goals and To Do List for Version 2.0</p>\r\n<ul>\r\n<li>Name change if public adopts</li>\r\n<li> im/pm/sysop </li>\r\n<li> comment moderation </li>\r\n<li> permissions based security system with groups, inheritance, etc </li>\r\n<li> Content revision history </li>\r\n<li> Profiles - ability to quickly feature sets; example:             \r\n<ul>\r\n<li> Single User Mode </li>\r\n<li> Multi User Mode </li>\r\n<li> Blog Site </li>\r\n<li> Commercial Site </li>\r\n</ul>\r\n</li>\r\n<li> More explicitly pre-configured user roles </li>\r\n<li> AJAX/Javascript and Flash themes, focusing heavily on enhancing administrative user interface. </li>\r\n</ul>', '', '0000-00-00 00:00:00', ''),
(17, 15, 0, 255, 1, 1, '', '', 'New User Registration Information', '<p>Thank you for your interest in signing up for an account with our web site, which will allow you to access special content and features. <br /><br /> Please fill out the required information in the following short form.  You may skip any fields which do not begin with a *. <br /><br /> We do require a valid email address to register with this system, as a way to verify that you are actually human and as a very good way to provide a unique and easy to remember login user name for your account.  Any time you would like to login, simply enter your email address and password into the login form located on each page and press enter. <br /><br /> If you successfully enter your authorization credentials, you access level will be appropriately elevated and you will have access to new content.  If you make a mistake during your entry, you will receive an error message at the top of the content area.  If you have any problems during registration or when trying to log in, please use the <a href="../contact">Contact Form</a> to let us know and we will respond as quickly as possible! <br /><br /> Thank you once again, we hope you enjoy the system.</p>', '', '2009-03-09 23:09:03', ''),
(18, 1, 0, 255, 3, 1, '', '', 'Administration Area', '<p>Click on the ''Admin'' link to the left to configure the system, create and edit pages, etc.</p>', '', '0000-00-00 00:00:00', ''),
(118, 107, 0, 255, 0, 1, '', '', 'Error and Message Handling', '<ul>\r\n<li>Three levels of output messages; success, notice/warning, error</li>\r\n<li>Very strict PHP parsing mode with 0 errors or warnings</li>\r\n<li>No server generated message output to browser in production mode</li>\r\n</ul>', '', '2009-03-13 17:34:20', ''),
(23, 12, 0, 255, 2, 1, 'New CSS Class', 'New CSS ID', 'Changes', '<ul>\r\n<li>20091009 - After ignoring this (log) for a bit.. I''ve changed a lot, but today I consolidated the three chunks of site templates into one big phatty and added a location option to modules..which allows them to be hidden (no output) or display before/after the content (per module selection)<br></li><li>20090313 - The last several days I have been debugging things from some massive code changes.&nbsp; Also, I have been testing out the new TinyMCE and Tidy additions, which are pretty splendid indeed (Thanks Dathorn for adding lilbtidy!)&nbsp; A lot of minor changes, some major ones.&nbsp; Source code and CVS logs are more detailed.</li>\r\n<li>20090307 - Made configuration names continuous</li>\r\n<li>20090307 - Extended smarty fetch, now automatically adds to global output buffer</li>\r\n<li>20090307 - Integrated Tidy</li>\r\n<li>20090306 - Per-theme Smarty templates integrated</li>\r\n<li>20090306 - Made path related things smarter </li>\r\n<li>20090306 - Implemented TinyMCE</li>\r\n<li>20090306 - Fixed possible extra / in sitemap </li>\r\n<li>20090303 - Modded CSS dynamic file handling </li>\r\n<li> 20090303 - Lots of minor changes to the Default- interfaces and a bit to layout </li>\r\n<li> 20090302 - Performed a major site upgrade on a site that hasn''t been touched in several weeks, took a couple hours.  Mostly due to the heavy output changes (anticipated) </li>\r\n<li> 20090301 - Lots of styling, some clean ups, etc over the last couple days </li>\r\n20090227 - Drastically modified smarty template outputs, getting close to final conventions\r\n<li> 20090226 - The last 36 hours have some some exciting new changes and editions!      \r\n<ul>\r\n<li> Tertiary navigation! </li>\r\n<li> Bread Crumb! </li>\r\n<li> Related content panels (two types!) </li>\r\n<li> Completely revamped DIV output design </li>\r\n<li> Smarty site-wide templates completely revamped! </li>\r\n<li> Currently loaded page is no longer clickable.. anywhere! </li>\r\n<li> A ton of new configuration switches! </li>\r\n<li> Created template code for message/warning and success levels (instead of just errors) </li>\r\n<li> Changed document declaration a bit </li>\r\n</ul>\r\n</li>\r\n<li> 20090224 - Finally updated CSS tags in navigation Smarty plugin and made semantic (friendly) URIs smarter </li>\r\n<li> 20090222 - Sitemaps are finally ''done'' </li>\r\n<li> 20090220 - Lots of changes here and there over the past month and launch of two new sites running JWSF.  More details in CVS logs, but I was very busy so they are sparse.  However, I have been heavily updating/enhancing source code documentation </li>\r\n<li> 20090114 - After about 4 days of non-stop action, I believe the user editor is complete.. minus the search feature.. there have been a lot of small changes, some of which are noted in the cvs logs </li>\r\n<li> 20090114 - Finally fixed issue which caused an admin user to reload the page after changing the default theme for it to take effect (for them), now it is instantaneous the way it used to be :) </li>\r\n<li> 20090110 - Added failedLoginsTotal and successfulLoginsTotal </li>\r\n<li> 20090110 - Upgraded page editor, inactive content editor, and orphan content editor to be continuous with changes to page creator </li>\r\n<li> 20090109 - Split page creation into nice fieldsets </li>\r\n<li> 20090109 - Wrote sitemaps into admin interface </li>\r\n<li> 20090109 - Implemented image location field for pages and content blocks </li>\r\n<li> 20090109 - Removed page-by-page keyword setting and moved it to a global configuration option since search engines no longer reference these except in the rarest of occasions </li>\r\n<li> 20090109 - Changed admin configuration forms to definition lists rather than a table which will allow for better css styling, and still looks fine in text browsers </li>\r\n<li> 20090109 - Split configuration area into three sections - Numerics, Strings, and Switches </li>\r\n<li> 20090109 - Eliminated almost all constant files - moved to configuration databases or appropriate class/function files </li>\r\n<li> 20090108 - public_html/index.php and admin/index.php are finally dynamic and no longer need to be edited per-installation as long as installation guidelines are followed.  yay. </li>\r\n<li> 20090108 - Corrected hard-coding issues and code duplication in user registration process - phew! </li>\r\n<li> 20090107 - Integrated CVS revision control </li>\r\n<li> 20090107 - Removed a few constant files -&gt; database </li>\r\n<li> 20090107 - Renamed ''_themes'' to ''themes'' </li>\r\n<li> 20090107 - Structured an ''uploads'' directory </li>\r\n<li> 20090107 - Added ''fixperms'' shell script - does not address database parameters file! </li>\r\n</ul>', '', '2009-10-09 19:39:49', ''),
(120, 75, 0, 255, 0, 1, '', '', 'Configuration Enables Diversity', '<p>Almost every feature in JWSF has an on/off switch located in the ''Switches'' area of the ''Configuration'' under /admin.&nbsp; Each one also has contextual help which explains the impact that changing the value will have.&nbsp; There is also a ''Numerics'' and ''Strings'' configuration area.&nbsp; Both of these areas are also important to famaliarize yourself with, and also have contextual help.</p>\r\n<p>In general, the switches will have direct impact on what is output to the browser.&nbsp; Anyone designing themes and modules should take into consideration that the value of these settings may change at any time.&nbsp; It would be wise to design your themes for a full feature set so that things do not break if someone suddenly decides to turn an option on.</p>\r\n<p>However, I do not always follow this rule.&nbsp; On occasion I design fairly rigid themes that require certain features to be enabled and disabled.&nbsp; It does not cause any issue, you just need to plan out the impact of your decisions as you decide to make changes.</p>', '', '2009-03-11 22:23:45', ''),
(38, 47, 0, 255, 0, 1, '', '', 'Need Support? Look No Further!', '<p>I provide remote technical assistance in a variety of ways.  For Windows users, the best bet is probably <a href="http://crossloop.com/aplusecs">the Crossloop method</a>.  You install a little program that will track our session time and provide a secure way for you to allow me temporary visual access to your computer and will allow me to fix any problem you may be having (as long as you can connect to the Internet of course) without either of us leaving our present location. <br /><br /> For users of other operating systems, there are a variety of methods we can use.  I have ~10 years telecommuting experience and have used most remote access programs available, including; SSH, Telnet, Timbuk2, RAS, DUN, RDC, etc.. <br /><br /> Let me fix your computer issues and I''ll provide a free performance tune-up as well!</p>', '', '2009-03-11 10:33:15', ''),
(39, 8, 0, 255, 7, 1, '', '', 'Semantics and Syntax', '<p>I believe that if a thing is worth doing, it is worth doing correctly.  I strive to deliver only the very best in the web sites that I create; in content, aesthetics, and in the code output to the browser.  I have spent many hours studying standards, browser capabilities, error handling, and more to ensure that anyone with any browser will be able to view my sites and that they will load quickly, correctly, and with a pleasing visual layout. <br><br> The code behind JWSF adheres to W3C standards and uses syntactically correct PHP/MySQL/XHTML/CSS/Javascript code.&nbsp; Also, in the default configuration all output is also<b> semantically</b> correct.  This is very important to users of alternative browsers, as well as search engines and web robots.</p>', '', '2009-08-18 11:41:14', ''),
(40, 28, 0, 255, 0, 1, '', '', 'Current Status', 'I believe that I have the code down to just one or two CSS tag generating inconsistencies right now.  The goal of my system is to provide a CSS designer with a great deal of flexibility with a minimum of excess code generation.  Basically, I feel that the default generated source code should be as minimalistic as possible so that it loads quickly and properly for non-fancy browsers.\r\n<br /><br />\r\nMy first and foremost suggestion for designers is to know how to write compliant code by hand.  Trust me, it is *the* way to go.  I develop all of my themes this way, and one tool I have found to be invaluable as of late is the web developer toolbar''s live CSS editor (ctrl+shift+e) for FireFox.  Get that, and use the Information panel to show the ID &amp; Class details and you will be well on your way to creating some great themes for the JWSF system.\r\n<br /><br />\r\nMy designs use as few images as possible, but if you want to get real fancy and need some extra tags in the code for layout then you should use Javascript to add them dynamically.  This will ensure that users of less-capable browsers will not need to download the extra code and imagery when viewing the site (especially key for internationally targetted audiences).', '', '2009-02-26 04:28:57', ''),
(41, 29, 0, 255, 0, 1, '', '', 'The Key to AdOOPtion', 'The key to a system''s adoption by a community of users is its ability to be easily expanded to encompass many different users'' independent needs.  The JWSF system  is extremely modular and provides a very nice base framework from which to work.  You get all the benefits of standards compliant frame generation along with an integrated database, user system, content control and a couple of site wide features which may be toggled on and off depending on your intent.\r\n<br /><br />\r\nModules can be created which utilize these base features and integrate literally anything.  The key to module development is understanding the globally available objects, functions, and at least a basic understanding of the underlying database layout.\r\n<br /><br />\r\nUsing these system wide features will allow anyone to quickly create useful and diverse functionally.  One can always assume an existing database connection, and adding additional tables and linking them into the base system in a relational manner is actually quite easy.  Your modules can be completely independent of the base system, or tightly interwoven, it is all up to your, your skill set, and your desire.\r\n<br /><br />\r\nThe intent of the developer guide, however, is not targetted towards the creation of modules, but rather towards contributing to the core JWSF code which is contained in the release packages.  It is my intent to accept public contributions to the code base of the core system, after I release the first stable version.\r\n<br /><br />\r\nI believe that this will allow a select group of talented programmers to become part of a team which I will lead to JWSF version 2.0 which will feature a whole slew of new core features, modules, themes and most importantly full ajax integration.  All base level code changes will be reviewed by yours truly and be accepted, rejected or be marked for additional changes.  The code is updated using CVS for now, but will most likely be moving to GIT before the first release.\r\n<br /><br />\r\nI will expand on this section as time allows, but feel free to contact me if you have any questions.', '', '2009-02-17 19:09:43', ''),
(42, 48, 0, 255, 0, 1, '', '', 'Providing Effective System Control', '<p>The JWSF system tries to provide a simple and elegant solution to administer the features and content of the web site that it generates.  My real goal is to allow a site owner to easily update basic content, correct typos, etc.. while not providing enough power (say via Dreamweaver and FTP) to break the aesthetics which they probably spent hundreds or thousands of dollars on. <br><br> This section will become tightly integrated to the video tutorials with the goal of providing a very complete set of examples and explanations that will enable nearly any level of user to maintain their site in a very powerful and easy to understand manner.</p>', '', '2009-10-11 19:29:11', ''),
(133, 116, 0, 255, 2, 1, '', '', 'Error Free Dynamic Sitemaps', '<p>Every time you edit or update a page, your sitemap.xml file will be updated with the latest changes.&nbsp; Dates, pages, custom frequencies and priorities, you name it.&nbsp; I wrote the sitemap routine to generate a sitemap.xml that is 100% compliant with the <a style="" title="The Official Sitemap Standard" href="http://sitemaps.org" mce_href="http://sitemaps.org">sitemap.org</a> schema.&nbsp; Attached is a screen shot of a sample google validation page.</p>\r\n<p>*Note: The low number of index''d pages are because I just recently set the system up at that location.</p>', 'sitemap.png', '0000-00-00 00:00:00', ''),
(99, 105, 0, 255, 0, 1, '', '', 'JWSF Default Modules', '<p>This is where you will soon be able to find documentation about the ''default module set'' that will be available in all JWSF standard installations. Things like new user registration, the multi-level contact script, etc..</p>', '', '0000-00-00 00:00:00', ''),
(45, 18, 0, 255, 0, 1, '', '', 'Coding with Clarity', '<p>The JWSF system tries to be ''self-documenting'' as much as possible.  I also try to maintain continuity throughout the entire system with concerns to naming things. <br /><br /> At this point, there are three predominate themes.. the_lowercase_with_underscores method, camelCaseLowerLeading and CamelCaseUpperLeading.  I am trying to bring these all into cohesion and the final release will be completely continuous with just one method. <br /><br /> Self-documenting means that almost all variable, constant, id, class, and function names are given meaningful names that make the item, by nature, self-documenting.  I feel that it should help enable those who look through the source code to understand it, as well as making it easier for me to maintain.</p>', '', '2009-03-14 00:55:52', ''),
(46, 27, 0, 255, 0, 1, '', '', 'Coming Soon', 'I will be putting together some screen recordings that I have been archiving into some useful video tutorials.. for end-users, administrators, and CSS designers.  If you have immediate need for something in particular, don''t hesitate to ask!', '', '2009-01-18 05:07:21', ''),
(47, 33, 0, 255, 0, 1, '', '', 'JWSF is Fully Flash Compatible', '<p>By default, there is no Flash in the base system because it is not truly web-friendly.  I really enjoy flash animations and in the proper context flash can really enhance a web site.  However, search engines cannot read content inside of a flash file.&nbsp; So if you are say, using a flash navigational system for your web site, Google will not be able to successfully navigate your site and find out what it has to offer if you do not properly serve up something for non-flash capable browsers.</p>\r\n<p>However, thanks to per-theme Smarty templates, you&nbsp; may easily add flash content anywhere you like.&nbsp; You could use a flash logo to get some excitement up top, or you could replace the navigational menu with a nice flash version that reads pages from the database, or just paste some YouTube video into your content.<br /><br /> If you choose to use Flash, please make sure you provide fall-back content!</p>', '', '2009-03-11 22:32:31', ''),
(123, 111, 0, 255, 0, 1, '', '', 'My Favorite Database', '<p>Having used, designed and administered a lot of different databases in my time, MySQL has been my favorite ever since I discovered it.&nbsp; SQLite is pretty cool though, and of course Oracle is splendid for extremely large data sets.</p>\r\n<p>I enjoy writing SQL statements, although I do also thoroughly enjoy phpMyAdmin.&nbsp; Updating my MySQL interface has been long overdue and I have recently made some improvements which make the PHP source code easier to read by removing full SQL statements.&nbsp; This interface should also be helpful to developers that are not as familiar with SQL.</p>\r\n<p>The reason that I kept the statements in the source code for so long is that it was great for trouble shooting and development.&nbsp; However, most of the statements are quite stable now and overall moving to a more abstract interface has improved code readability.</p>\r\n<p>There are no SELECT * statements in JWSF, and that means that the database abstraction is not as robust as some people may be used to with other frameworks.&nbsp; You may not know it, but almost all of them silently use all sorts of extraneous data calls when you are not looking.&nbsp; Not JWSF, this system calls only the relevant data for the purpose at hand.</p>\r\n<p>My use of the database is precise and efficient, but you are of course free to use any SQL structures you like when developing your own modules.&nbsp; Honestly, you should not need to interface with the database unless you are writing modules or system code.&nbsp; Template and CSS designers will interface with Smarty and XHTML.</p>\r\n<p>More information about interfacing with the JWSF database will be provided in the documentation area.</p>\r\n<p>Ex:<br />$fields = array(''name'', ''value'', ''description'');<br />$clause = ''ORDER BY name'';<br /> $db-&gt;select($fields, $table, $clause, __FILE__, __LINE__);</p>', '', '2009-03-14 09:18:07', ''),
(49, 35, 0, 255, 0, 1, '', '', 'My Certifications', '<p>Below is a screen shot of my BrainBench certifications.  You may view my public profile by going to <a style="" href="http://brainbench.com" mce_href="http://brainbench.com">BrainBench.com</a> and entering <b>4794028</b> in the transcript id field. <br><br>You may search their results for the categories that I have tested in and&nbsp; take note that I rank either top ten in the state of Michigan and/or top ten in the country on many.&nbsp; I will soon provide those rankings on this page.</p>', '', '2009-10-11 19:35:41', ''),
(77, 35, 0, 255, 0, 1, '', '', 'The Certs', '<p><br mce_bogus="1"></p>', 'brainbenchcerts.png', '2009-10-11 19:35:41', ''),
(50, 49, 0, 255, 1, 1, '', '', 'Blue Demon', '<p>Intel Core 2 Quad Core w/8GB of RAM.  Boots Lunar Linux with the latest stable Linux kernel, Ubuntu 64 with Studio mods, and Vista 64 Ultimate (on occasion).  1GB nVidia card drives two 22" LCDs and plenty of disk space, with an aesthetically pleasing case.  I use dwm, xmonad and awesome window managers.</p>', '', '2009-03-11 10:31:35', ''),
(51, 49, 0, 255, 2, 1, '', '', 'Ronin', '<p>10" Sony Vaio TZ w/2GB RAM.  Elegant and light, precisely what a laptop should be.  Usually running Ubuntu with Vista on the other partition but I am getting ready to convert it to full on Lunar.  Currently running xmonad, but switching to dwm soon.</p>', '', '2009-03-11 10:31:35', ''),
(52, 49, 0, 255, 3, 1, '', '', 'Ghe-t0w', '<p>This is an old hand me down that had 2GB RAM but one of the sticks fried so it is back down to 1GB.  This machine runs OpenBSD with awesome or dwm.. when I am not working my Citrix telephone-sales-guy job and running Windows XP (*blech*).</p>', '', '2009-03-11 10:31:35', ''),
(53, 49, 0, 255, 4, 1, '', '', 'Duhr', '<p>This is an old old machine that coughs when you start it up, doesn''t really fit together properly and has hard drives bouncing around loose inside of it and serves as my personal file server for things that I archive but never use.  It runs Ubuntu server, headless.. but also has it''s original XP installed on it as well for no reason.  It is soon to be a dedicated boxrox0ring machine.</p>', '', '2009-03-11 10:31:35', ''),
(54, 49, 0, 255, 5, 1, '', '', 'Webserv', '<p>This machine probably does have a web server running on it.  ;)  It runs Ubuntu server headlessly as well and also has it''s original Windows operating system (I think) bootable.  This machine also has webmin for easy point and click control (Duhr runs eBox..I like webmin better, much more power).  It is pretty old, probably a little better than Duhr processor-wise.  It serves as my DNS caching server (to reduce network latency).  This will also soon be a dedicated boxrox0ring machine.</p>', '', '2009-03-11 10:31:35', ''),
(55, 45, 0, 255, 0, 1, '', '', 'Information Is Valuable', '<p>Operating a web site is a much more serious affair than many people realize.  Web sites and those that develop them very often overlook all but the most basic implications created by the design and hosting of a web site on the Internet.  Well, not me! <br /><br /> The JWSF system implements many advanced security features as well as sophisticated logging which can be used to trace hacking attempts.  The code layout uses strict file permissions and by default all sensitive code is stored in a location readable only by the Apache web server and not by the web browser. <br /><br /> The database is also thoroughly protected and all variable data which may be entered into the system from an untrusted source is verified on the server side (using client-side only validation is *not* secure!).  In stable releases the database interface will not output any error information to the browser to prevent data mining (it is pretty hard to get that kind of error out of my system any way). <br /><br />If you would like any additional information about the secure features of the JWSF system, please contact me directly or look through the code.</p>', '', '2009-03-11 22:34:35', ''),
(57, 34, 0, 255, 1, 1, '', '', 'A Bit About the Author', '<p>Programming has been a personal passion since childhood.&nbsp; By nature of the beast, most of my life has been spent at the keyboard and natural curiousity has driven me to learn about computers on a very comprehensive level.&nbsp; Security became a concern early in my life and is integrated deeply into all aspects of my projects.</p>\r\n<p>As a profession, it is my preference to administer projects, networks, databases and teams.&nbsp; As a hobby, it pleases me to create compliant, elegant web sites that are easy for end users to maintain and I also operate a small businesses for my outside work; which I try to minimize to friends, long-term clients, and occasional Internet contacts.</p>\r\n<p>In my free time you are likely to find me standing in a river fly-fishing for trout and salmon.&nbsp; When not fishing, you can find me at the shore of Lake Superior playing ball with my two dogs.</p>\r\n<p>Most days are spent with a headset and keyboard working away at networks, end user support, and web site maintenance.&nbsp; Most nights are spent productively hacking away at my secret codes in front of the fire.&nbsp; Pulling all nighters and multiple day coding binges is default status (same could be said about fishing at times).</p>\r\n<p>I also contribute to <a style="" title="Lunar Linux" href="http://lunar-linux.org/" mce_href="http://lunar-linux.org/">Lunar Linux</a> by writing modules for the moonbase as time permits.</p>', '', '2009-10-11 19:26:03', ''),
(87, 80, 0, 255, 0, 1, '', '', 'Enhance Your Critical Decision Making Process', '<p>In the past, it has been my privilege to provide consultation services to business and professionals when they require additional perspective while making critical decisions.</p>\r\n<p>My ability to read people, situations and technlogy allows for the visualization of choices and solutions to multi-faceted issues which intelligent people who have not studied technology and interpersonal relationships so intensely may overlook.</p>\r\n<p>My experience includes:</p>\r\n<ul>\r\n<li>Cultivation of employee candidate profiles</li>\r\n<li>Network design</li>\r\n<li>Internet services</li>\r\n<li>Media production</li>\r\n<li>USOEC Boxing events</li>\r\n</ul>', '', '2009-03-11 10:30:15', ''),
(88, 94, 0, 255, 0, 1, '', '', 'Screen Real Estate', '<p>Working fast and having information at my finger tips and an eye flick away is imperative to my workflow.&nbsp; Dual screens have been mandatory to my work since about ''98, although I am quite productive on my 10" vaio when I need to be.&nbsp; Currently, I surround myself with two 22"s, a 19", and my 10" vaio.&nbsp; My long term goal is to&nbsp; have an 8x2 or 3x3 flat screen panel setup.</p>\r\n<p>I use some version of Linux or BSD on any machine that I use for a production work environment because .. well, because it is better in every way.</p>\r\n<p>Most of my days are spent inside a text editor with syntax highlighting, a web browser, multiple terminal sessions, ssh connections and some remote desktoping here and there.</p>', '', '2009-03-12 10:51:04', ''),
(89, 94, 0, 255, 2, 1, '', '', 'Things I use Every Day', '<ul>\r\n<li>bash</li>\r\n<li>screen</li>\r\n<li>xmonad</li>\r\n<li>vim</li>\r\n<li>gedit</li>\r\n<li>lynx</li>\r\n<li>Firefox</li>\r\n<li>ssh/scp</li>\r\n<li>PHP</li>\r\n<li>MySQL</li>\r\n<li>phpMyAdmin</li>\r\n<li>cvs (i know, very ''90s of me)</li>\r\n<li>irssi</li>\r\n<li>Meebo</li>\r\n</ul>', '', '2009-03-12 10:51:04', '');
INSERT INTO `JWSF_PageContent` (`id`, `pid`, `acsLow`, `acsHigh`, `so`, `active`, `cssClass`, `cssID`, `header`, `content`, `imgURI`, `stamp`, `headerURI`) VALUES
(121, 68, 0, 255, 2, 1, '', '', 'The Result', '<p>The result is that my coding style is fairly unique.&nbsp; In the past, I wrote in a very hackerish style which would have been difficult for people to digest, although it was very efficient and wistful.&nbsp; The code that I generate now is much more clear and I try to use very didactic names for everything and this allows the code to be extremely easy to comprehend even if you are approaching it for the first time.</p>\r\n<p>I do not use PEAR, PECL or ZEND.&nbsp; Nor do I search around the Internet looking for code that other people have generated and try to twist it to my purposes.&nbsp; I write code specific to my intent which frees the server from parsing a whole lot of code that never gets used.&nbsp; JWSF is not based on any way on any other code, templates or methodologies other than what I have derived myself.</p>\r\n<p>With that in mind, expect some interesting things if you delve into the code.&nbsp; Rest assured though, there is a lot of documentation and the code itself is self-documenting.</p>\r\n<p>All code stays within an 80 character width, except Smarty templates.</p>', '', '2009-03-14 08:53:28', ''),
(90, 37, 0, 255, 4, 1, '', '', 'Education', '<p><b>Northern Michigan University</b>, 1998 - 2004</p>\r\n<ul>\r\n<li>Concentration: Electronic Imaging, Photography</li>\r\n</ul>\r\n<p><b>Oakland Community College</b>, 1996 - 1997</p>\r\n<ul>\r\n<li>Concentration: Programming</li>\r\n</ul>\r\n<p><b>University of Detroit Mercy</b>, 1994 - 1996</p>\r\n<ul>\r\n<li>Concentration: Architecture, Programming</li>\r\n</ul>\r\n<p><b>Oakland Technical Center - Southwest</b>, 1992 - 1994</p>\r\n<ul>\r\n<li>Certification: Computer Aided Architecture</li>\r\n</ul>', '', '0000-00-00 00:00:00', ''),
(92, 37, 0, 255, 6, 1, '', '', 'Future Goals', '<ul>\r\n<li>Attainment of Master''s Degree from Norwich University''s Information Assurance distance education program</li>\r\n<li>Building of micro-devices</li>\r\n<li>Robotics programming and design</li>\r\n</ul>', '', '0000-00-00 00:00:00', ''),
(91, 37, 0, 255, 5, 1, '', '', 'Civic Achievements', '<ul>\r\n<li>Participant in Child and Family Services of the Upper Peninsula Child Mentor program</li>\r\n<li>Pro-bono web sites for non-profits</li>\r\n<li>Shrine clown at MI C.H.I.P.S. program</li>\r\n<li>Soup kitchen volunteer</li>\r\n<li>Produced USOEC Boxing event as a Fund-raiser for Big Bay Health Camp</li><li>Produced Shakespeare''s Macbeth as a Masonic Fund-raiser</li><li>Board of Directors - Marquette Masonic Assocation<br></li>\r\n</ul>', '', '0000-00-00 00:00:00', ''),
(95, 100, 0, 255, 3, 1, '', '', 'Theme Ideas', '<ul>\r\n<li>Blog profile</li>\r\n<li>Artist portofolio</li>\r\n<li>Musician site</li>\r\n<li>Fisihing enthusiast</li>\r\n<li>Ghost in the Shell</li>\r\n<li>Retro</li>\r\n<li>etc..</li>\r\n</ul>', '', '2009-03-09 16:39:58', ''),
(96, 13, 0, 255, 2, 1, '', '', 'PHP Documenter', '<p>I am currently trying to get all of my source code phpdoc friendly, and will make the link available soon.&nbsp; There is abundant of documentation throughout the entire code base.</p>', '', '0000-00-00 00:00:00', ''),
(97, 104, 0, 255, 0, 1, '', '', 'Multiple Demonstration Models', '<p>I will soon be setting up multiple demonstrations pre-configured with various roles in mind.&nbsp; For now, this installation itself serves as a live demonstration and you are free to try out any of the features and use the <a style="" href="../undefined/admin" mce_href="../undefined/admin">admin section</a> without fear of impacting the site permanently.&nbsp; The database resets itself every two hours.</p>\r\n<p>The official demo url is currently:</p>\r\n<p><a style="" href="http://demo.lookintomycode.com" mce_href="http://demo.lookintomycode.com">http://demo.lookintomycode.com</a><br mce_bogus="1"></p>\r\n<p>and http://lookintomycode.com is currently running as an exact mirror of demo.lookintomycode.com.</p>\r\n<p>A standard small business brochure site with multiple themes:</p>\r\n<p><a style="" href="http://demo1.lookintomycode.com" mce_href="http://demo1.lookintomycode.com">http://demo1.lookintomycode.com</a><br mce_bogus="1"></p><p>A complex visual design with flash and java applied:</p><p><a style="" mce_href="http://bagel.lookintomycode.com" href="http://bagel.lookintomycode.com">http://bagel.lookintomycode.com</a><br></p>', '', '2009-10-11 16:45:39', ''),
(98, 91, 0, 255, 0, 1, '', '', 'Theme Packs', '<p>Theme packs are tarballs (files stuck together with tar, then compressed with gzip) with the following directory structure:</p>\r\n<p style="padding-left: 30px;">Theme-Name</p>\r\n<p style="padding-left: 60px;">css<br />images<br />js<br />smarty</p>\r\n<p>So the full (relative to site root) path name to a theme-specific image might look like ''/themes/Default-White/images/logo.png''.</p>\r\n<p>More details to come.</p>', '', '2009-03-09 17:02:41', ''),
(100, 68, 0, 255, 0, 1, '', '', 'Why I Write By Hand', '<p>Coding everything by hand, from scratch, is beneficial for many reasons which are dear to me.&nbsp; #1 is efficiency.&nbsp; Coding things from scratch allows me to ensure the minimal output of code execution and output, which results in happier site visitors.</p>\r\n<p>#2 is learning.&nbsp; I do not use code repositories or pre-packaged classes that may be doing who knows what in the background or have security vulnerabilities which are publicly exploited.&nbsp; I know every single thing that happens behind my web sites.&nbsp; Not only that, but instead of using pre-built interfaces to things.&nbsp; This enables me to understand things on a fundamentally more primitive level and helps ease the transition of switching languages.</p>\r\n<p>There are endless benefits to writing code by hand, but these are the two that please me the most.</p>', '', '2009-03-14 08:53:28', ''),
(58, 18, 0, 255, 0, 1, '', '', 'CSS ID And Class Names', '<p>All CSS tags are named semantically so that it is easy to identify their purpose and find their location.  Additionally:</p>\r\n<ul>\r\n<li> ID tags all begin with id_ </li>\r\n<li> Class tags all begin with c_ </li>\r\n<li> All DIV elements which serve only to group related nested elements and therefore provide containment mechanisms for positioning/styling of have tag values of ''containerSemanticPurpose'' -  ie; id_containerHeader, id_containerNavigation </li>\r\n<li> Hierarchical groups are have tags with the following convention: \r\n<ul>\r\n<li> level 1 items: c_semanticName_00 </li>\r\n<li> level 2 items: c_semanticName_01 </li>\r\n<li> .... </li>\r\n<li> level X items: c_semanticName_(X-1) </li>\r\n</ul>\r\n</li>\r\n<li> Repetitive groups of similar items utilize the following convention:  \r\n<ul>\r\n<li> The upper-most level of related items will provide a large containment div with label: id_containerSemanticGroupName (a DIV) </li>\r\n<li> Each smaller section of items will be contained by: id_containerSectionName_XX where XX will be two digits beginning with 00. </li>\r\n</ul>\r\n</li>\r\n</ul>\r\n<p>This naming scheme allows very fine grained generic control over things with CSS and Javascript.  For instance, you can style a whole area (say the content area), then style say the sixth block of content by addressing the individual tags in that block like so: <br /><br /> #id_containerContent { background-color: black; } <br /> #id_containerContentBlock_06 { background-color: white } <br /> #id_containerContentBlock_06 h2 { background-color: gray } <br /><br /> Javascript/AJAX developers will take much joy in this predictability and genericism.</p>', '', '2009-03-14 00:55:52', ''),
(60, 28, 0, 255, 0, 1, '', '', 'An Outline', 'Here the most basic outline I can provide:\r\n<br /><br />\r\n<code>\r\nbody<br />\r\n--frame ------- everything but disclaimers<br />\r\n----section1 -- logo, login, search, page image<br />\r\n----section2 -- tertiary nav, user greeting/bread crumb<br />\r\n----section3 -- column container<br />\r\n------col1 ---- content, system messages, related content<br />\r\n------col2 ---- primary nav<br />\r\n------col3 ---- extras<br />\r\n----section4 -- theme selector, footer nav<br />\r\n--disclaimers - copyright, w3c, jwsf, etc<br />\r\n</code>', '', '2009-02-26 04:28:57', ''),
(61, 28, 0, 255, 0, 1, '', '', 'Quick Tips', '<ul>\r\n<li>\r\nStart from a skeleton theme and work your way up\r\n</li>\r\n<li>\r\nIf you design your template with any amount of rigidity, it would behoove you to hide any elements that you are counting on being turned off.  This will prevent your theme from breaking should someone turn that feature on without realizing the possible ramifications.  It would be best to test your theme under multiple conditions (various features turned on/off in admin area).\r\n</li>\r\n<li>\r\nTest your layout in at least a couple browsers, one in text mode, without javascript and without css.  The Internet is a very diverse place and it is not your place to exercise technological prejudice.\r\n</li>\r\n</ul>', '', '2009-02-26 04:28:57', ''),
(62, 69, 0, 255, 0, 1, '', '', 'General Requirements', '<p>This is just a loose compilation of notes to be clarified later:</p>\r\n<ul>\r\n<li>Assumes to be running from root of domain </li>\r\n<li> Mod_rewrite permission in .htaccess (highly recommended) </li>\r\n<li> PHP 5 </li>\r\n<li> Apache 2 </li>\r\n<li> MySQL 5 </li>\r\n<li> FTP or shell access </li>\r\n<li> CVS or GIT access may be desired for developers </li>\r\n<li> Smarty (included) </li>\r\n</ul>\r\n<p>If you come across anything I''ve missed, do let me know! :)</p>', '', '2009-03-13 17:15:52', ''),
(66, 82, 0, 255, 0, 1, '', '', 'Base System and Modules', 'Are tricky.. I will document later.', '', '2009-02-28 23:11:25', ''),
(67, 82, 0, 255, 0, 1, '', '', 'Dynamic Form Creation', 'Will be a module/ajax combo coming later.. maybe version 1.5.', '', '2009-02-28 23:11:25', ''),
(69, 89, 0, 255, 1, 1, '', '', 'JWSF Is Highly Extensible', '<p>In the early days of my system, the push was on cutting edge features and speed of code production because I was really supposed to be creating ''Art'' rather than anything programmatic.  That lead to a lot of really cool features, but they became completely useless and unmaintainable over time. <br><br> In rebuilding this system, I have tried to maximize modularity, customization, and the ability to quickly configure the system for a wide variety of intended uses.  With that in mind, there are three major types of enhancements which allow for extremely powerful and flexible manipulation of the system.</p>', '', '2009-10-11 19:26:41', ''),
(70, 89, 0, 255, 2, 1, '', '', 'Modules', '<p>Modules, AKA; extensions, plug-ins.  Modules can be dropped into place and the administration area will automatically make them available for usage in the system at a selectable location.  If you drop a matching admin module in the proper location, you can have a complete administrative interface to your extension that will automatically show up in the admin area.  The <a style="" href="../modules" mce_href="../modules">Modules</a> page will contain a directory of official JWSF approved modules, as well as a section of publicly contributed modules of any kind.  Base-level instructions will also be provided. <br><br> Note that most custom modules should have theme extensions (since they most likely create output)!</p>', '', '2009-10-11 19:26:41', ''),
(71, 89, 0, 255, 3, 1, '', '', 'Profiles', '<p>Profiles are sets of pre-defined configuration settings (possibly including themes and modules).  If you install a profile and activate it, it will quickly convert your site accordingly.</p>', '', '0000-00-00 00:00:00', ''),
(72, 89, 0, 255, 4, 1, '', '', 'Themes', '<p>&nbsp;This was one of the first features I built into the original carnation of my CMS.  Unfortunately it got lost along the way (or just rusty perhaps) and it is back and better than ever.  I''ve even added per-theme smarty templates and images!  Of course, the system still fails back to a sane skeleton for unsupported features. <br><br> One important note is that changing system configuration (even adding/removing pages) can drastically affect a theme if it is not designed with re-configuration in mind!  That''s not called a bug, that''s call a feature.&nbsp; Use it wisely grasshopper!</p>', '', '2009-10-11 19:26:41', ''),
(73, 127, 0, 255, 1, 1, '', '', 'JWSF Target Audience', '<p>JWSF serves two primary audiences:</p>\r\n<ul>\r\n<li>Production web designers, small to medium design firms</li>\r\n<li>Office personnel, site owners</li>\r\n</ul>\r\n<p>The following page contains brief descriptions of how and why these two audiences could benefit from integrating JWSF into their infrastructure.</p>\r\n<p>However, I feel it is important to also note what JWSF is not intended to be.&nbsp; JWSF is not trying to be WordPress, Joomla, PHPNuke, or any of those.&nbsp; They have many more cons than pros in my book and using pre-packaged systems years ago is what drove me to create and use my own system.&nbsp; These other systems also target a more ''plug and play'' type of audience.&nbsp; They come with a ton of useful features and goodies, but you have to do things <b>their</b> way.&nbsp; JWSF provides a simple method to interact with standard features, but you are free to develop your own module code, smarty templates, etc in any way you see fit.</p>\r\n<p>JWSF <b>is</b> intended to replace systems like those mentioned above, <b>if</b> you find yourself trying to contort them into an end result that is different from their creator''s intention.&nbsp; For instance, Wordpress is supposed to be a <b>blog</b> but many people try to mold it into shape for small business sites, ecommerce sites, etc..</p>\r\n<p>The intention of JWSF is to provide a simple yet extensible platform from which to build tiny one page sites all the way through complex ecommerce sites or handheld intranet services.&nbsp; JWSF provides a very robust themeing system to allow designers to showcase their work and allows clients to easily choose between interface designs, or even easily switch between seasonal designs without incurring any additional billable time.</p>\r\n<p>JWSF also provides a simple and persistent database connection, integrated user system and has some base features which a majority of sites can benefit from.&nbsp; These features can also be easily turned off to avoid feature-overkill on smaller sites.</p>\r\n<p>JWSF is a ''framework'' from which powerful sites can be built without having the feeling of ''bloat-ware''.</p>', '', '2009-10-11 19:32:16', ''),
(119, 108, 0, 255, 0, 1, '', '', 'Directory Structure', '<p>/_public_html - not readable by web</p>\r\n<p style="padding-left: 30px;">/classes<br />/constants - Cons_Db.php should be chmod 440<br />/modules<br />/sessions - writable by apache<br />/smarty</p>\r\n<p style="padding-left: 60px;">/cache - writable by apache<br />/internals<br />/plugins<br />/templates<br />&nbsp;&nbsp;&nbsp; /advanced - javascript additions<br />/templates_c - writable by apache</p>\r\n<p>&nbsp;</p>\r\n<hr />\r\n<p>/public_html - world readable</p>\r\n<p>&nbsp;</p>\r\n<p style="padding-left: 30px;">/_img</p>\r\n<p style="padding-left: 60px;">/content<br />/logos<br />/misc<br />/pages</p>\r\n<p style="padding-left: 30px;">/admin - use .htaccess auth on production sites<br />/uploads</p>\r\n<p style="padding-left: 60px;">/audio<br />/documents<br />/images<br />/video</p>\r\n<p style="padding-left: 30px;">/themes</p>\r\n<p style="padding-left: 60px;">/Default-Black<br />/Default-White<br />/Skeleton<br />/Skeleton-Black</p>\r\n<p style="padding-left: 90px;">/css<br />/images<br />/js<br />/smarty<br />(these are in each theme folder)</p>\r\n<p>An astute observer will note that all senstive information is stored in a location which, by default, should be a non-world readable location.</p>', '', '0000-00-00 00:00:00', ''),
(74, 53, 0, 255, 0, 1, '', '', 'Using JWSF Smarty Data', '<p>In this section I will document all of the data structures that the base JWSF system makes available to Smarty and under what conditions. <br /><br /> Module developers should provide appropriate documentation for any smarty data that they assign.</p>', '', '2009-03-13 18:39:33', ''),
(75, 48, 0, 255, 0, 1, '', '', 'The Keys to Success', '<p>The responsibility for a successful web site ultimately falls on the shoulders of the site owner.  To effectively utilize the web as a tool a web site must look and feel alive.  It should provide pertinent, targeted information in a concise way that is easy to find, read and share. <br><br> This means that occasional minor maintenance of content needs to be performed and many businesses cannot afford to keep someone on staff (or pay hourly rates for a web design firm) to update their sites frequently.  In such cases it is imperative to provide the site owner easy to use tools to make the routine changes necessary to keep their site current and active. <br><br> Getting friendly with the administration area is key in this context.  It is designed to be straight forward and simple to use.  Contextual help is provided and soon will be linked to video tutorials.  This should enhance your clients'' positive response and confidence in performing simple administrative tasks inherent to web site ownership.</p>', '', '2009-10-11 19:29:11', ''),
(76, 54, 0, 255, 0, 1, '', '', 'Integrate JWSF!', '<p>Are you a web development firm l0ooking to adopt a powerful new framework to enable rapid site design?  Do you appreciate the goals and features which are provided by my system? <br /><br /> If you are interested in my work and would like to provide an in-depth and comprehensive training experience, I am open to providing training seminars.  Rather than just using a couple demos, skimming over some documentation or seeing sites that I have squeaked out, get the real low-down on the philosophy and power behind this system so that your team can tap it in ways that I do not have time to implement (I''m busy adding features, not using them!).</p>', '', '2009-03-09 23:06:48', ''),
(83, 37, 0, 255, 1, 1, '', '', 'Profile', '<p>Security minded network administrator with diverse experience in hardware, software and wetware.  Innovative leader able to guide dynamic technical teams.  Expert programmer with ability to quickly adapt to new languages and environments while under pressure.  Friendly and articulate demeanor while interfacing with a non-technical audience.</p>', '', '0000-00-00 00:00:00', ''),
(79, 95, 0, 255, 1, 1, '', '', 'Ready to Get Twisted?', 'Seriously, this is a bit of a mind-bender.  At least, coding it is (which I am doing atm).  However, this feature unleashes some serious design power.  However, it does have some caveats.\r\n', '', '2009-03-05 01:55:34', ''),
(80, 95, 0, 255, 2, 1, '', '', 'The Premise', '*sigh*  I have broken.  Sort of.  Haha!\r\n<br /><br />\r\nOk, now that I have that out of my system.  My goal when finally committing to thinking of my code as a CMS was to keep it as light as possible.  Thus it was named WireFrame and SlimFrame and other Frames.\r\n<br /><br />\r\nI finally broke a little bit recently and realized that I needed to open up some things a bit more for designability.  Up until that point, I had made semantic, syntactic, minimalistic perfection priority 1.  However, in rebelion against breaking my golden rule, I started scheming..\r\n<br /><br />\r\nMy consort Charles led me to discover 960.gs, a new design method.  I thought it was a pretty cool idea, but see it as having major semantic abuse issues, as well as not being truly universal (for instance, I still think 720px is the best universal fixed-width target).  So I started to wonder... how could I enable more powerful design features?\r\n<br /><br />\r\nPreviously, I had the site-wide smarty templates configured in a fixed manner, but allowed for custom content and module templates.  This allowed me to force semantic correctness and minimize extraneous presentation DIVs.  My theory was that any extra crap *perdon* like that should be added with Javascript (to ensure graceful degradation with no extraneous code download for un-supported features).\r\n<br /><br />\r\nSo, I said ''hey, how about per-theme Smarty templates?''.. sounded good. But tricky.  Well, I have done it.  What this means is that the default Smarty template directory will contain a template for absolutely everything that is output by the system.  These templates should be absolutely minimal, and as semantically correct as possible.\r\n<br /><br />\r\nSo now let''s take that a step further.  What we have now are templates that we can go into and move code around and create different layers of DIV tags consulting the JWSF Smarty API documentation, Globals documentation, etc.. Which is cool, if you are a fairly geeky designer.\r\n<br /><br />\r\nSo, how about I include a couple themes with default 960.gs compatible Smarty-theme templates?', '', '2009-03-05 01:55:34', ''),
(81, 95, 0, 255, 3, 1, '', '', 'I''ll Do You ^2 Better!', 'How about create your own layout right there in /admin via some simple forms?  Sure, I am going to build and include a couple full 960.gs compatible themes for you, but I am going to straight step the theme game up and let you design your own dynamic theme layout with a configurable amount of rows, presentation containers within those rows, and Smarty output template assignment to any cell.  All that, and you can choose *per-row* whether or not to output empty cells (for presentational purposes)!\r\n<br /><br />\r\nIf you can not see the power in that, you are in the wrong place baby!', '', '2009-03-05 01:55:34', ''),
(82, 95, 0, 255, 4, 1, '', '', 'The Caveats', 'To break things down succinctly:\r\n<ul>\r\n<li>\r\nIt exacerbates me to think of the horrible abuse this will put my beautiful semantic output through.. let alone the mind-warping it may cause for noobs.\r\n</li>\r\n<li>\r\nIt may quite possibly impact performance up to 2x (we''ll see)\r\n</li>\r\n<li>\r\nIt will probably not be very friendly if you are making mistakes until i have thoroughly tested it on multiple themes/configurations\r\n</li>\r\n<li>\r\nA theme based on the dynamic layout engine will probably be so heavily styled that any changes in configuration (and possibly navigation) could completely disfigure the aesthetic\r\n</li>\r\n</ul>\r\nHowever, those are your issues to deal with (mostly)!  So make sure your themes are fully-degradable or else you will make me shake my head disapprovingly!', '', '2009-03-05 01:55:34', ''),
(84, 37, 0, 255, 2, 1, '', '', 'Areas of Strength', '<ul>\r\n<li>Ability to create and test security procedures</li>\r\n<li>Appreciation for aesthetics in code and design</li>\r\n<li>Advanced theories in network and software design</li>\r\n<li>Confidence with Linux, BSD, Windows and OS X</li>\r\n<li>Comfort with any ANSII SQL compliant database</li>\r\n<li>Expert in Internet client and server technologies</li>\r\n<li>History of productive and&nbsp; secure telecommuting</li>\r\n<li>Trained in Human Interface Design</li>\r\n<li>Usage of automation to lessen repetitive tasks</li>\r\n</ul>', '', '0000-00-00 00:00:00', ''),
(85, 37, 0, 255, 3, 1, '', '', 'Professional Experience', '<p><b>Network Administrator</b>, MediRide EMS, 2001 - Present</p>\r\n<ul>\r\n<li>Confidential</li>\r\n</ul>\r\n<p><b>Owner</b>, A+ Emergency Computer Solutions, 2007 - Present</p>\r\n<ul>\r\n<li>Coded calling card reseller program with XML and Moneris eCommerce interface for content management systemInstalled and remotely maintained several secure Windows networks</li>\r\n<li>Forensic analysis of equipment for use in court</li>\r\n<li>Produced secure mobile-friendly intranet web services</li>\r\n<li>Provided data and disaster recovery services</li>\r\n</ul>\r\n<p><b>Owner</b>, Marquette Collective, 2002 - 2005</p>\r\n<ul>\r\n<li>Developed sophisticated content management system enabling rapid production of powerful database driven web sites with PHP and MySQL</li>\r\n<li>Used aforementioned content management system to create a hand-held mobile solution for geological field surveyors</li>\r\n<li>Developed custom image and video galleries with rating system</li>\r\n<li>Developed user contributed sports-related image, video and trading card galleries</li>\r\n</ul>\r\n<p><b>Web Master/Network Administrator</b>, NMU JOBSearch Center, 1998 - 2001</p>\r\n<ul>\r\n<li>Performed massive site conversion from ASP to PHP</li>\r\n<li>Instituted site''s first graphical user interface with PHP templates</li>\r\n<li>Remotely administered 14 Windows clients and 1 Windows NT Server</li>\r\n<li>Designed sophisticated database with PHP back-end that is still used today</li>\r\n</ul>\r\n<p>Other previous experiences include owning a network service company for three years in the greater-metro Detroit area, and technical positions with Oakland County''s Information Technology Department and Competitive Computer Systems in Birmingham.</p>', '', '0000-00-00 00:00:00', ''),
(122, 110, 0, 255, 0, 1, '', '', 'Compliant, Hand Generated CSS', '<p>There are many wonderful tools available for designers to take advantage of today and you may feel free to use them.&nbsp; May the force be with you.</p>\r\n<p>It is my preference to mimick a design flat on a live system with the content in place and a blank style sheet.&nbsp; I then create a skeletal structure with all avaiable class and ids, neatly ordered and laid out with indentation that represents the document structure.&nbsp; Also included will be any default XHTML tags that I would like to restyle.</p>\r\n<p>From there I start at the outermost layers and work my way in, one layer at a time, hand writing the rules for each element.&nbsp; It helps that I read the entire CSS 2.1 W3C Recommendation about 5 years ago.</p>\r\n<p>From my many years of practice, I can generate cross-browser, compliant code without using <strong>any</strong> CSS browser tricks, reset sheets, or !important.&nbsp; Junk in my book, the lot of it.&nbsp; You will not find any themes packaged in the releases which contain such methods.</p>\r\n<p>Of course, you are free to use any means necessary in your own designs.&nbsp; The default themes should provide an execellent skeletal structure from which to work from.&nbsp; They include many empty tags sets to get you going and should be streamlined if you intend to use them in a production environment.</p>\r\n<p>Using the skeleton CSS tags from the Skeleton* and Default* theme sets, you shuold not even need to worry about examining the XHTML code for tag names.&nbsp; As noted elsewhere on the site, I highly suggest the Web Developer Toolbar for Firefox as your CSS editing tool for JWSF.</p>', '', '2009-03-13 15:34:29', ''),
(101, 105, 0, 255, 0, 1, '', '', 'Module Layout', '<p>All modules should have their self-contained PHP file located in /_public_html/modules, and a default output template in /_public_html/smarty/templates.&nbsp; If your module likes to use Javascript then it should also have output templates in /_public_html/smarty/templates/advanced.&nbsp; </p><p>You may optionally also add default css moduleName.screen.css files in the /public_html/themes/Default-White(Black)/css directories.&nbsp; If your module is theme-specific, then put the CSS in that theme''s folder, as well as the smarty template.<br></p>\r\n<p>Modules may be assigned to execute without output, or to output immediately before or after the primary page content (using the default template set anyway).<br></p><p>Modules have an extensive set of resources available to them by accessing JWSF System API&nbsp; and Smarty API features.&nbsp; Documentation for those interested in said features, will be coming soon.</p>', '', '2009-10-10 11:17:55', ''),
(102, 93, 0, 255, 0, 1, '', '', 'JWSF Image Handling', '<p>Images are such a mutli-faceted dilema.&nbsp; Currently, you may assign assign a default ''page image'', assign each page a specific image, and you may also assign each content block an image.&nbsp; You do this by putting a URL into the image box of the item.&nbsp; This can be relative to site root and should be located under /_img/pages or /_img/content for now.</p>\r\n<p>I will soon be adding an upload feature, but it will not do any resizing because I don''t think that''s what web servers are for.&nbsp; If you want to add an image, you should know enough about editing, sizing and compressing them for the web to do so before you upload it.</p>\r\n<p>I will soon be writing a full-featured upload manager and am waiting until then to integrate the file uploads.</p>', 'compress.gif', '0000-00-00 00:00:00', ''),
(103, 84, 0, 255, 0, 1, '', '', 'Seeking Reviewers', '<p>I am currently looking for a few people to do a pre-beta review of my system.&nbsp; Interested parties please use the contact form!</p>', '', '2009-03-09 18:02:20', ''),
(106, 8, 0, 255, 3, 1, '', '', 'XHTML is Strict', '<p>Recently there has been a lot of disrespect from the public towards XHTML, everyone is hyped about HTML 5.&nbsp; I do not care to engage in a flame war here, but I firmly believe the HTML 4/5 is <b>not</b> the way.</p>\r\n<p>XHTML is really just XML with a default style sheet (which browsers have taking to contorting rather than coding solutions to their bugs).&nbsp; Overall, it is a pretty good default.&nbsp; The great thing about XHTML is if it does not output code that is broken.&nbsp; This is one of the things that has always turned people away from XHTML.&nbsp; It is one of the features that I enjoy.</p>\r\n<p>There is no reason that a carefully coded web site should not be outputting valid code everytime.&nbsp; Unless there is a bug of course, and XHTML actually makes it easier to trouble shoot code because if something is amiss, the end result will most likely have a very small discontinuity which will cause the browser to prevent rendering the document.&nbsp; A sure sign your code is broken!&nbsp; So, fix your code, learn about semantics and appreciate proper syntax.&nbsp; Use XHTML (or XML).</p>', '', '2009-08-18 11:41:14', ''),
(108, 107, 0, 255, 0, 1, '', '', 'Search Engine Optimization', '<ul>\r\n<li>100% Compliant, automatic site map generation (sitemap.xml)</li>\r\n<li>Page specific descriptions</li>\r\n<li>Page specific site map priority and frequency</li>\r\n<li>Site-wide keywords (no longer used by reputable search engines)</li>\r\n<li>CSS and JS are publicly accessible</li>\r\n<li>Robots.txt</li>\r\n<li>Canonical URLs (host must support mod_rewrite of course)</li>\r\n</ul>', '', '2009-03-13 17:34:20', ''),
(109, 107, 0, 255, 0, 1, '', '', 'Smarty', '<p>Smarty is the PHP templating framework from the PHP developers which allows a programmer to provide output designers easy yet powerful control over the output which is generated for the browser.&nbsp; These templates are then generally passed on to the CSS and graphic designers.&nbsp; Of course, this may all be the same person, such as myself.</p>\r\n<p>What it really does is provide the glue which binds program to presentation.&nbsp; In other words, it formats the XHTML output.</p>\r\n<p>Use of Smarty templates allows me to quickly rearrange the order in which content is displayed and insert dynamic values easily into the content without having to clog up my PHP code with a bunch of nasty XHTML tags.</p>\r\n<p>I take Smarty usage somewhat to the extreme.&nbsp; I use an outer layer of Smarty templates for presentational organization.&nbsp; These templates control the&nbsp; order in which more specific Smarty templates will be added to the output buffer.</p>\r\n<p>The inner layer of Smarty templates are very compartamentalized.&nbsp; This allows for very focused attention to each particular item''s potential output.</p>\r\n<p>I don''t stop there.&nbsp; Every theme may have it''s own set of Smarty templates and therefore drastically affect browser output on a theme by theme basis.</p>\r\n<p>There are two default Smarty template sets, one with and one without a few basic Javascript controls.&nbsp; You may choose which fall back you would like to use and if your chosen theme is lacking its own Smarty template for a feature, it will pull from your chosen fall-back location.</p>\r\n<p>This can get heady quickly, for more information please browse through documentation and source-code.</p>', '', '2009-03-13 17:34:20', ''),
(110, 107, 0, 255, 0, 1, '', '', 'User System', '<ul>\r\n<li>Dual SHA512 password encryption schema</li>\r\n<li>Comprehensive session hijacking protection</li>\r\n<li>Detailed user tracking without compromising privacy</li>\r\n<li>Immediate user access updates even if logged in</li>\r\n<li>Per user control panel</li>\r\n<li>Automatic password failure lock out</li>\r\n<li>Email verification for new users</li>\r\n<li>Dual-level access control for every page, content block, and module enables very flexible and dyanmic method of controlling privileges</li>\r\n</ul>', '', '2009-03-13 17:34:20', ''),
(111, 107, 0, 255, 0, 1, '', '', 'Dynamic Pages and Navigation', '<ul>\r\n<li>6 layers of dynamic navigation; Primary (hierarchical), Secondary (footer), Tertiary (header), Parents (bread crumb trail), Siblings (indirectly related), Children (directly related)</li>\r\n<li>Each navigational unit may be turned on or off</li>\r\n<li>No data is drawn from the database which a user should not have access to</li>\r\n<li>Functions with user system disabled</li>\r\n<li>Updates go into effect immediately</li>\r\n<li>Redundant security checks</li>\r\n<li>Very clear and predictable output</li>\r\n<li>Per page content templates (just a feature, not required)</li>\r\n<li>Per page css files by id or canonical name</li>\r\n<li>Per page javascript files (as above)</li>\r\n<li>Each page may have a specific image or fall back to default (which may be none)</li>\r\n<li>Same with each content block</li>\r\n<li>Upper and lower access level restrictions for each page, block and module</li>\r\n<li>and much more..</li>\r\n</ul>', '', '2009-03-13 17:34:20', ''),
(112, 107, 0, 255, 0, 1, '', '', 'Configuration', '<ul>\r\n<li>100% Form based administration</li>\r\n<li>38 ''switches'' to turn features on and off instantly</li>\r\n<li>Many customizable text strings</li>\r\n<li>Many customizable numeric values</li>\r\n<li>Detailed contextual help</li>\r\n<li>100% customizable output</li>\r\n<li>Coming Soon: default language database selection and 100% string configurability</li>\r\n</ul>', '', '2009-03-13 17:34:20', ''),
(113, 107, 0, 255, 0, 1, '', '', 'Themes and Layout Engine', '<p>Themes</p>\r\n<ul>\r\n<li>Select default theme</li>\r\n<li>User selectable themes (may be turned off)</li>\r\n<li>Each theme may have custom CSS, Javascript, Images or Smarty Templates</li>\r\n<li>Each page may have custom CSS, Javascript, Images or Smarty templates</li>\r\n<li>Each modules may have custom CSS, Javascript, Images or Smarty templates</li>\r\n<li>JWSF Provides sane fallbacks for anything which does not have a theme-specific replacement</li>\r\n<li>Repositionable modules</li>\r\n</ul>\r\n<p>Layout Engine</p>\r\n<ul>\r\n<li>Create custom output layouts</li>\r\n<li>Selectable number of ''row'' containers</li>\r\n<li>Each row has selectable number of ''column'' containers</li>\r\n<li>Per row handling of empty containers</li>\r\n<li>Choose default fall back template set</li>\r\n<li>960.gs compatible pre-configured layouts</li>\r\n</ul>', '', '2009-03-13 17:34:20', ''),
(114, 107, 0, 255, 0, 1, '', '', 'Compliancy and Graceful Degradation', '<ul>\r\n<li>Generates 100% W3C Compliant XHTML and CSS</li>\r\n<li>Option to parse final output with HTMLTidy</li>\r\n<li>100% Server side input validation</li>\r\n<li>Meta data and header output thoroughly researched</li>\r\n<li>Fall back meta data and headers</li>\r\n<li>No base feature requires Javascript or CSS</li>\r\n<li>Fall back for non-semantic URLs</li>\r\n<li>Full MVC</li>\r\n</ul>', '', '2009-03-13 17:34:20', ''),
(116, 107, 0, 255, 0, 1, '', '', 'Modules', '<p>These are modules that I have previously written for the system, only a couple are available in the default installation.</p>\r\n<ul>\r\n<li>Interactive user-contributed video and image trading card gallery</li>\r\n<li>Localized weather (xml/soap)</li>\r\n<li>Moneris e-payment gateway (xml)</li>\r\n<li>Paypal e-payment gateway (xml)</li>\r\n<li>Telcan/Callture calling card reseller program (xml)</li>\r\n<li>Contact scripts</li>\r\n<li>Handheld field technician''s well survey program</li>\r\n<li>Ski area management system</li>\r\n<li>User registrations</li>\r\n<li>Standard image and video galleries with ratings and comments</li>\r\n</ul>\r\n<p>&nbsp;</p>', '', '2009-03-13 17:34:20', ''),
(117, 107, 0, 255, 0, 1, '', '', 'Tidy and TinyMCE', '<p>These are two fairly exciting features that I have recently (much to my end-user''s joy) added.&nbsp; I have been using TinyMCE for the past few days and I really enjoy it, although I need to figure out how to prevent it from inserting inline-style controls because that is a big no-no in my mind.</p>\r\n<p>TinyMCE is a not-so-tiny Javascript WYSIWIG editor.&nbsp; Basically, it takes large text areas and inserts a familiar looking set of toolbars into them&nbsp; so that users may add lists, bold, links and things like that without having to see or use any XHTML code.</p>\r\n<p>Tidy on the other hand, cleans up XHTML code to ensure that the output sent to the browser is correct.&nbsp; Honestly, I have found that using it programmtically can be just as good at breaking things as fixing them.&nbsp; Still, it has its advantages in the proper context.</p>\r\n<p>Using the Tidy option has two benifits.&nbsp; It can pretty-print the source code to the browser for easier debugging (since by default all code output to the browser by JWSF is stripped of white space and line breaks).&nbsp; The second beneifit is that it can catch those little ampersands and other special characters that may slip by if you edit in guru mode (that is, without TinyMCE).</p>\r\n<p>For overall defaults, I suggest TinyMCE on, Tidy off.</p>', '', '2009-03-13 17:34:20', ''),
(126, 112, 0, 255, 0, 1, '', '', 'XHTML 1.1', '<p>Aside from Isaac Asimov and fly fishing books, I do not read much in the way of tangible materials.&nbsp; With exception to W3C Recommendations.&nbsp; I print those out and sleep with them.&nbsp; Well, I used to.</p>\r\n<p>I can generate compliant XHTML 1.1 in my sleep.&nbsp; If XHTML 1.1 is not your cup of tea, you can easily perform a couple slight modifications to a couple configuration strings and change the DOCTYPE declaration in the template (or change the Tidy settngs) and easily convert the entire system to XML, XHTML 1.0, HTM 4, or even HTML 3.2 if you really wanted to.&nbsp; If converting to anything but XML, you should not even have to change any of the output since XHTML 1.1 basically just enforces proper syntax of the others.&nbsp; But your templates could be sloppier if that pleases you.</p>\r\n<p>The system output is something that has been a large focus for me.&nbsp; I try to keep it as slim and trim as possible, relying on CSS and ECMAScript to take care of enhancements.&nbsp; All of the meta data, headers, and SEO specific output has been carefully researched and is, in its default status, perfect in my eyes.</p>', '', '2009-03-13 16:51:00', ''),
(128, 92, 0, 255, 0, 1, '', '', 'Profiles Coming Soon', '<p>Profiles are, for the most part, pre-configured sets of configuration options exported as a .sql file, added to the profiles dir, and can be executed at will.&nbsp; Profiles may also be part of theme packs.</p>\r\n<p>The profile feature is currently in development, so stayed tuned.</p>', '', '2009-03-13 19:47:38', ''),
(129, 113, 0, 255, 0, 1, '', '', 'Interacting with JWSF', '<p>The JWSF provides many API functions, class methods and properties, hooks, or whatever you like to call it.&nbsp; Each base class provide a variety of methods and properties with which you may interact when developing modules and themes.</p>\r\n<p>What I consider to be the ''System API'' is basically a clear definition of the methods and properties available in each of the core system classes, excluding the database class.&nbsp; That one gets a section of its own, for clarity and convienence.</p>\r\n<p>The Smarty API documents all of the data that the core system registers with Smarty.&nbsp; It will also document a few Smarty tips and tricks that I have discovered, as well as a brief Smarty tutorial.</p>\r\n<p>These sections are intended to provide a quick reference and are not going to be as up to date as the source code. If you want the most current data for development purposes, please refer to the source code.</p>', '', '2009-03-13 20:15:54', ''),
(130, 114, 0, 255, 1, 1, '', '', 'Straight or Abstract Queries', '<p>There are two methods of composing your SQL statements to send to the database.&nbsp; The first option is to use the query method to send a full SQL statement of your own design straight to the database.&nbsp; I usually set $sql = to my statement, and you must pass two additional parameters as well (see fourth and fifth below).</p>\r\n<dl> <dt>db::query()</dt> <dl> $sql = ''SELECT name, value FROM TableName LIMIT 1''; </dl><dl>$result = $db-&gt;query($sql, __FILE__, __LINE__);</dl><dl>while ($row = $db-&gt;fetch(__FILE__, __LINE__))</dl><dl>&nbsp;&nbsp;&nbsp; $this-&gt;$row[''name''] = $row[''value'']; </dl><dl>}</dl></dl>\r\n<p>The second, more abstract way is to use four seperate methods for your four primary query types.&nbsp; This way keeps your code a bit easier to read.&nbsp; The four methods are:</p>\r\n<dl> <dt>db::delete()</dt> <dl> $db-&gt;delete ($id, $table, __FILE__, __LINE__);<br /></dl> <dt>db::insert()</dt> <dl> $data = array (''value'' =&gt; $_POST[''value'']); </dl><dl>$db-&gt;insert ($data, TablePageMetaData, __FILE__,__LINE__); </dl> <dt>db::select()</dt> <dl> $fields = array(''name'', ''value'', ''description''); </dl><dl>$clause = ''ORDER BY name''; </dl><dl>$db-&gt;select($fields, $table, $clause, __FILE__, __LINE__); </dl> <dt>db::update()</dt> <dl> $data = array (''value'' =&gt; $_POST[$name]); </dl><dl>$clause = ''WHERE name="'' . $name . ''" LIMIT 1'';</dl><dl>$db-&gt;update ($data, $table, $clause, __FILE__,__LINE__); </dl> </dl>\r\n<p>Each of these takes five parameters.&nbsp; The first is an array, either a single dimensional array of field names, or a multi-dimensional array of field name/value pairs.&nbsp; The context obviously determines the difference.</p>\r\n<p>The second parameter will be the table name upon which to execute the query.&nbsp; These are usually passed using constants for now, as all tables have a constant name.&nbsp; There is an occasion or two where I pass the table in from a variable.</p>\r\n<p>The third parameter is whatever clause you would like to append to the SQL statement.&nbsp; For instance, ''WHERE id=1''.&nbsp; The semi-colon will automatically be appended.&nbsp; Note that you can use sub-select, joins, SELECT AS, etc in your arrays and clause variables.</p>\r\n<p>The fourth and fifth parameters should be the same for every one of these methods (parameters two and three in first method).&nbsp; __FILE__ and __LINE__.&nbsp; This provides a convienent method of debugging since it identifies precisely where your SQL issue is generated rather than spitting out the file and line number of the database class.</p>\r\n<p>These last two parameters may soon be condensed to one single parameter which would be the entire backtrace.&nbsp; I am still researching the performance impacts this may have, but my educated guess is that it will be nill.&nbsp; So, expect that change coming soon.</p>', '', '0000-00-00 00:00:00', ''),
(131, 114, 0, 255, 2, 1, '', '', 'After Your Query', '<p>Ok, so you''ve sent your query off to the mystical land of MySQL.&nbsp; What now?</p>\r\n<p>The following list provides a brief summary of additional methods available.&nbsp; For the most part they are just simple wrappers to the complimentary PHP function.</p>\r\n<dl> <dt>&nbsp; new mySqlObject </dt> <dd> Instantiates a new instance (connection) of the database object.  There is one created and available for your usage by default, you may access it with $GLOBALS[''db''].  You should only need to create a new one if you are getting real tricky.  For convience, the rest of the examples will be short-handed as db-&gt;xx rather than $GLOBALS[''db'']-&gt;xx.  This also initializes the global query counter if it does not exist.  Takes no parameters. </dd> <dt> db::connect </dt> <dd> Actually performs the database connection and selection, using values as set in /_public_html/constants/ConsDb.php.  Takes no parameters. </dd> <dt> db::fetch </dt> <dd> This returns a row from your query, if it produced results.  It is a wrapper for mysql_fetch_assoc and takes no parameters.  Results from your previous query will be stored in db-&gt;result and that is the parameter passed to mysql_fetch_assoc.  You will generally use this in a while statement. ex:<br /></dd><dd>while ($row = $db-&gt;fetch(__FILE__, __LINE__)) { <br />&nbsp;&nbsp;&nbsp; $this-&gt;$row[''name''] = $row[''value'']; <br />} <br /><br /></dd> <dt> db::numRows </dt> <dd> This will return the number of rows your last query affected, if your query provides those details. </dd> <dt> db::lastId </dt> <dd> Returns the row id created by the last insert query. </dd> <dt> db::safeSql </dt> <dd> This performs ritualistic cleansing of every query passed to the database.  It is a wrapper to a nifty little function that I wrote myself.  Fear not SQL injection attacks.  You should never need to call this function yourself. </dd> <dt> db::close </dt> <dd> Closes the database connection and is called at the end of program execution.  Another feature you should never need to be concerned with. </dd> <dt> db::isError </dt> <dd> This is a custom error trigger.  It is deprecated and shall soon be ditched. </dd> </dl>', '', '0000-00-00 00:00:00', ''),
(132, 99, 0, 255, 0, 1, '', '', 'Quite a Bit of Info', '<p>This section is going to generate a lot of documentation.&nbsp; Please be patient while I work on finding time for this.&nbsp; Look through the source code for now, post questions in the forums, or contact me directly if you need to.</p>', '', '2009-03-13 21:30:25', '');
INSERT INTO `JWSF_PageContent` (`id`, `pid`, `acsLow`, `acsHigh`, `so`, `active`, `cssClass`, `cssID`, `header`, `content`, `imgURI`, `stamp`, `headerURI`) VALUES
(134, 115, 0, 255, 0, 1, '', '', 'From Start to Finish', '<h3>Friendly URLs and .htaccess</h3>\r\n<p>The entire JWSF system is generate by two fairly minimal index.php files.&nbsp; They are almost identical, the one in /admin has a couple extra goodies.</p>\r\n<p>If your apache web server supports url rewriting with mod_rewrite from .htaccess, then you can use canonical URLs, aka dynamic url rewriting, semantic URLs, or friendly URLs.&nbsp; Basically, it means human (and search engine) friendly things after yourdomain.name/ in the URL bar.</p>\r\n<p>This is the recommended treatment.&nbsp; Semantic URLs are very valuable.</p>\r\n<p>If you are using them, then before script execution begins apache will do a couple tricks and then load up the index file.</p>\r\n<h3>Entering index.php</h3>\r\n<p>The very first thing the script does is set the PHP error level.&nbsp; By default it is set to E_STRICT, which will throw every error it encounters as well as warn about deprecated feature usage.&nbsp; It is very handy, but should not be used for production systems unless logging only to a file.</p>\r\n<p>Next the load timers are initialized.</p>\r\n<p>The next two functions attempt to make educated guesses about where the include files are located, and then load them into memory.</p>\r\n<h3>Start Loading Objects<br /></h3>\r\n<p>Now that we have all the goodies loaded, instantiate the database connection.</p>\r\n<p>Once we are connected to the database, magic starts heppening.&nbsp; The configuration object is created which loads all the configuration values from the database and also sets up minimal global environment.</p>\r\n<p>Now that configuration data is available, we check to see if the user system is on and if so instantiate the user object.&nbsp; When the object is created, ti performs all sorts of good stuff which determines the current operating status of user related features and privileges.</p>\r\n<p>Time to load the page data, by creating a new page object and populating it.&nbsp; Immediately after loading the page meta data and content, the system reads any page specific modules which have been assigned.</p>\r\n<p>It is at this time that we also determine what theme to use and which theme specific CSS and Javascript fles are available and eligible to be loaded for the particular page.&nbsp; It must be loaded after the page object so that it can be smart enough to grab page specific CSS and Javascript.</p>\r\n<p>The last object to load is the navigational object and it determines which navigational items need to be created for the page and user.&nbsp; Creating the object is all you need to do, it (like most things) assigns data to smarty if and when it creates data intended for use in browser output.</p>\r\n<p>That is the end of our business logic.&nbsp; Short, sweet, elegant.</p>\r\n<h3>Take Care of Smarty<br /></h3>\r\n<p>All that is left is to register a few objects with Smarty (ones that may change during script execution) and assign the system messages.</p>\r\n<p>The system now starts adding the outer layer of smarty templates to the buffer.&nbsp; For the most part, they are simple little things which you can go into and cut and paste things around if you would like to change the order in which thinks load (if you are not using dynamic layouts).&nbsp; The outer layer should just load a bunch of little chunks which should, once all assembled, include a template for all possible feature outputs.</p>\r\n<h3>Shutting Down<br /></h3>\r\n<p>Now that the buffer has been filled (which contains the entire XHTML content that will be output to the browser), we close the database, format it all to UTF-8 (just in case), and if UseTidy is on, pass the whole shibang through Tidy to pretty it up.</p>\r\n<p>echo $smarty-&gt;output;</p>\r\n<p>And that is all she wrote, next page please.</p>', '', '2009-03-14 00:51:39', ''),
(135, 116, 0, 255, 1, 1, '', '', 'Google Webmaster Guidelines', '<p>Google has a detailed document outlining recommendations for creating what it considers to be a high quality site that is search engine and user friendly.</p>\r\n<p>This page will compare JWSF features to each of their specifications, in case this is of value to you.&nbsp; To give you the short version, JWSF is 100% Google friendly.&nbsp; I will start off with a screen shot of the dynamically created JWSF sitemap.xml, validating 100% in Google Sitemap Validator, and move on to the webmaster specifications.</p>', '', '0000-00-00 00:00:00', ''),
(136, 116, 0, 255, 3, 1, '', '', 'When Your Site is Ready (for launch)', '<dl> <dt>Submit it to Google at <a style="" href="http://www.google.com/addurl/?continue=/addurl" mce_href="http://www.google.com/addurl/?continue=/addurl">http://www.google.com/addurl.html</a>.</dt> <dd> This is the responsibility of the web master. </dd> <dt>Submit a <a style="" href="http://google.com/support/webmasters/bin/answer.py?answer=40318" mce_href="http://google.com/support/webmasters/bin/answer.py?answer=40318">Sitemap</a> using <a style="" href="http://www.google.com/webmasters/tools/" mce_href="http://www.google.com/webmasters/tools/" target="new">Google Webmaster Tools</a>. Google uses your Sitemap to learn about the structure of your site and to increase our coverage of your webpages.</dt> <dd> There is a way to send Google a ping which will tell it to re-read your sitemap.  Before using that feature, be sure to add your site my properly as outlined above as doing it otherwise will cause Google to forever fail to ping your sitemap. </dd> <dt>Make sure all the sites that should know about your pages are aware your site is online.</dt> <dd> This seems like a chicken before the egg scenario, most inbound links accumulate after you launch your site. </dd> </dl>', '', '0000-00-00 00:00:00', ''),
(137, 116, 0, 255, 4, 1, '', '', 'Design and Content Guidelines', '<dl> <dt> Make a site with a clear hierarchy and text links. Every page should be reachable from at least one static text link. </dt> <dd> The JWSF navigational system ensures this happens.  Using the related content features will help achieve this if your NavPrimaryDepth is set to less than the full depth of your sub-pages.  The ShowSiteMap feature will output a full user-friendly site map in the secondary navigation if you choose to use it. </dd> <dt> Offer a site map to your users with links that point to the important parts of your site. If the site map is larger than 100 or so links, you may want to break the site map into separate pages. </dt> <dd> See previous.  If your site goes over 100 pages and you would like a paginated user site map, let me know. </dd> <dt> Create a useful, information-rich site, and write pages that clearly and accurately describe your content. </dt> <dd> This is up to your and/or your content editor.  There is nothing JWSF can do to help you here. </dd> <dt> Think about the words users would type to find your pages, and make sure that your site actually includes those words within it. </dt> <dd> This is up to your and/or your content editor.  There is nothing JWSF can do to help you here. </dd> <dt> Try to use text instead of images to display important names, content, or links. The Google crawler doesn''t recognize text contained in images. </dt> <dd> The intent of JWSF is to provide 100% machine readable text for all content.  It is possible to break this through the course of your content editing or design process, but you should most certainly try not to. </dd> <dt> Make sure that your  elements and alt attributes are descriptive and accurate. </dt> <dd> See previous.  Note, each JWSF page has a custom title element, which then gets the site-wide title element appended to it. </dd> <dt> Check for broken links and correct HTML. </dt> <dd> In the default configuration, only external links could possibly be ''broken'' if you are using the .htaccess as I do.  Correct HTML is not a concern, JWSF is 100% XHTML 1.1 by default. </dd> <dt> If you decide to use dynamic pages (i.e., the URL contains a "?" character), be aware that not every search engine spider crawls dynamic pages as well as static pages. It helps to keep the parameters short and the number of them few. </dt> <dd> JWSF does not use Get parameters except in the admin area so this is not a concern unless you are coding modules (poorly perhaps?).  If you choose not to (or can not) use friendly URLs, every page will have ?id= in the URL, but that is the only one and there is no way around that. </dd> <dt> Keep the links on a given page to a reasonable number (fewer than 100). </dt> <dd> Again, this is up to you.  The JWSF page and navigational systems can handle much more than 100, but really, that is quite a lot of pages and you may desire seperating them into sub-sites.  The web is supposed to be quick and easy to navigate. </dd> </dl>', '', '0000-00-00 00:00:00', ''),
(138, 116, 0, 255, 5, 1, '', '', 'Technical Guidelines', '<dl> <dt> Use a text browser such as Lynx to examine your site, because most search engine spiders see your site much as Lynx would. If fancy features such as JavaScript, cookies, session IDs, frames, DHTML, or Flash keep you from seeing all of your site in a text browser, then search engine spiders may have trouble crawling your site. </dt> <dd> This is one of the primary reasons that I have been touting graceful degradation for so many years.  Every site I have ever created (since ''95) has practiced this method.  Also, I use text browsers quite often, which one depends on context (termcap).  The do not mention, but I feel it is important to note, graceful degradation is a huge concern for mobile users.  JWSF, in default configuration, follows the principles to the core. </dd> <dt> Allow search bots to crawl your sites without session IDs or arguments that track their path through the site. These techniques are useful for tracking individual user behavior, but the access pattern of bots is entirely different. Using these techniques may result in incomplete indexing of your site, as bots may not be able to eliminate URLs that look different but actually point to the same page. </dt> <dd> None of that garbage in JWSF! </dd> <dt> Make sure your web server supports the If-Modified-Since HTTP header. This feature allows your web server to tell Google whether your content has changed since we last crawled your site. Supporting this feature saves you bandwidth and overhead. </dt> <dd> Personally, I only use Apache.  JWSF however, performs quite extensive and intelligent methods of determining the last modified date of every page that it generates.  It then uses that information in the XHTML meta data, and also by sending custom response headers from PHP with the correct data in the correct format.  Together with the dynamic sitemap.xml generator, JWSF satifisfies this very thoroughly.  These features also help tell end-user browsers when they should refresh cached data, which I believe to be valuable (believe it or not, some people still use dial-up!). </dd> <dt> Make use of the robots.txt file on your web server. This file tells crawlers which directories can or cannot be crawled. Make sure it''s current for your site so that you don''t accidentally block the Googlebot crawler. Visit http://www.robotstxt.org/faq.html to learn how to instruct robots when they visit your site. You can test your robots.txt file to make sure you''re using it correctly with the robots.txt analysis tool available in Google Webmaster Tools. </dt> <dd> JWSF includes a default robots.txt file which disallows robot browsing in /admin.  Of course, they do not have to obey, so you should definitely use authentication methods of some kind for your /admin area.  JWSF allows browsing of all other publicly accessible locations because there should be absolutely 0 sensitive information there anyway and Google will penalize you if you try to block it from reading your CSS or Javascript because that is indicative of trying to subversively change your content to lubricate your SEO.<br> Another important feature of JWSF is that by default, every single directory has an index.php file which will redirect a user to the site root.  This provides basic protection from directory browsing hacks/intelligence gathering. </dd> <dt> If your company buys a content management system, make sure that the system can export your content so that search engine spiders can crawl your site. </dt> <dd> I believe this page documents why using JWSF would be a good idea. :) </dd> <dt> Use robots.txt to prevent crawling of search results pages or other auto-generated pages that don''t add much value for users coming from search engines. </dt> <dd> Is this redundant Mr. Google? </dd> <dt> Test your site to make sure that it appears correctly in different browsers. </dt> <dd> This is the responsibility of your theme designer.  JWSF default configuration rolls out only with themes that work in all grade A browsers with no goofy tricks. </dd> </dl>', '', '0000-00-00 00:00:00', ''),
(139, 116, 0, 255, 6, 1, '', '', 'Quality Guidelines', '<p>*Note* - I am not going to comment on this section.&nbsp; By default, JWSF is engineered to allow the creation of high quality sites.&nbsp; The rest of the Google recommendation is related to intentionally sneaky behavior that is not found in JWSF in its default configuration but it could be contorted by unscrupulous users.&nbsp; I hope that no one ever perverts my intention in such a manner.&nbsp; It is good reading for the uninitiated though.</p>\r\n<p><br></p>\r\n<hr>\r\n<p>These quality guidelines cover the most common forms of deceptive or manipulative behavior, but Google may respond negatively to other misleading practices not listed here (e.g. tricking users by registering misspellings of well-known websites). It''s not safe to assume that just because a specific deceptive technique isn''t included on this page, Google approves of it. Webmasters who spend their energies upholding the spirit of the basic principles will provide a much better user experience and subsequently enjoy better ranking than those who spend their time looking for loopholes they can exploit.</p>\r\n<p><br></p>\r\n<p>If you believe that another site is abusing Google''s quality guidelines, please report that site at <a style="" href="https://www.google.com/webmasters/tools/spamreport" mce_href="https://www.google.com/webmasters/tools/spamreport">https://www.google.com/webmasters/tools/spamreport</a>. Google prefers developing scalable and automated solutions to problems, so we attempt to minimize hand-to-hand spam fighting. The spam reports we receive are used to create scalable algorithms that recognize and block future spam attempts.</p>\r\n<p><b>Quality guidelines - basic principles</b></p>\r\n<ul>\r\n<li>Make pages primarily for users, not for search engines. Don''t deceive your users or present different content to search engines than you display to users, which is commonly referred to as "cloaking."</li>\r\n<br>\r\n<li>Avoid tricks intended to improve search engine rankings. A good rule of thumb is whether you''d feel comfortable explaining what you''ve done to a website that competes with you. Another useful test is to ask, "Does this help my users? Would I do this if search engines didn''t exist?"</li>\r\n<br>\r\n<li>Don''t <a style="" href="http://google.com/support/webmasters/bin/answer.py?answer=66356" mce_href="http://google.com/support/webmasters/bin/answer.py?answer=66356">participate in link schemes</a> designed to increase your site''s ranking or PageRank. In particular, avoid links to web spammers or "bad neighborhoods" on the web, as your own ranking may be affected adversely by those links.</li>\r\n<br>\r\n<li>Don''t use unauthorized computer programs to submit pages, check rankings, etc. Such programs consume computing resources and violate our <a style="" href="http://www.google.com/accounts/TOS" mce_href="http://www.google.com/accounts/TOS">Terms of Service</a>. Google does not recommend the use of products such as WebPosition Goldâ„¢ that send automatic or programmatic queries to Google.</li>\r\n<br> \r\n</ul>\r\n<p><b>Quality guidelines - specific guidelines</b></p>\r\n<ul>\r\n<li>Avoid <a style="" href="http://google.com/support/webmasters/bin/answer.py?answer=66353" mce_href="http://google.com/support/webmasters/bin/answer.py?answer=66353">hidden text or hidden links</a>.</li>\r\n<br>\r\n<li>Don''t use <a style="" href="http://google.com/support/webmasters/bin/answer.py?answer=66355" mce_href="http://google.com/support/webmasters/bin/answer.py?answer=66355">cloaking or sneaky redirects</a>.</li>\r\n<br>\r\n<li>Don''t send <a style="" href="http://google.com/support/webmasters/bin/answer.py?answer=66357" mce_href="http://google.com/support/webmasters/bin/answer.py?answer=66357">automated queries to Google</a>.</li>\r\n<br>\r\n<li>Don''t <a style="" href="http://google.com/support/webmasters/bin/answer.py?answer=66358" mce_href="http://google.com/support/webmasters/bin/answer.py?answer=66358">load pages with irrelevant keywords.</a><br mce_bogus="1"></li>\r\n<br>\r\n<li>Don''t create multiple pages, subdomains, or domains with substantially <a style="" href="http://google.com/support/webmasters/bin/answer.py?answer=66359" mce_href="http://google.com/support/webmasters/bin/answer.py?answer=66359">duplicate content</a>.</li>\r\n<br>\r\n<li>Don''t create pages with malicious behavior, such as phishing or installing viruses, trojans, or other <a style="" href="http://google.com/support/webmasters/bin/answer.py?answer=45432" mce_href="http://google.com/support/webmasters/bin/answer.py?answer=45432">badware</a>.</li>\r\n<br>\r\n<li>Avoid <a style="" href="http://google.com/support/webmasters/bin/answer.py?answer=66355" mce_href="http://google.com/support/webmasters/bin/answer.py?answer=66355">"doorway" pages created just for search engines</a>, or other "cookie cutter" approaches such as affiliate programs with <a style="" href="http://google.com/support/webmasters/bin/answer.py?answer=66361" mce_href="http://google.com/support/webmasters/bin/answer.py?answer=66361">little or no original content</a>.</li>\r\n<br>\r\n<li>If your site participates in an affiliate program, make sure that your site adds value. <a style="" href="http://google.com/support/webmasters/bin/answer.py?answer=66361" mce_href="http://google.com/support/webmasters/bin/answer.py?answer=66361">Provide unique and relevant content </a>that gives users a reason to visit your site first.</li>\r\n<br> \r\n</ul>\r\n<p>If you determine that your site doesn''t meet these guidelines, you can modify your site so that it does and then <a style="" href="http://www.google.com/support/webmasters/bin/answer.py?answer=35843" mce_href="http://www.google.com/support/webmasters/bin/answer.py?answer=35843">submit your site for reconsideration</a>.</p>', '', '0000-00-00 00:00:00', ''),
(144, 125, 0, 255, 0, 1, '', '', 'JWSF Application Layers', '<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; File System&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; Database &nbsp; &nbsp;&nbsp;&nbsp; System Level<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; \\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; /<br />JWSF Modules and Classes&nbsp;&nbsp; &lt;-&gt;&nbsp;&nbsp;&nbsp; DB Class&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; JWSF Logic<br />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; \\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; \\<br />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; \\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; \\<br />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; \\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Smarty&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Output Logic<br />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; \\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp; |<br />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; \\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; XHTML&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Semantic Output<br />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; \\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; |<br />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; \\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; CSS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Presentation Layer<br />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; \\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; |<br />&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Client-Side Scripting&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; User Interaction</p>', '', '0000-00-00 00:00:00', ''),
(145, 105, 0, 255, 3, 1, '', '', 'Example', '<p>This is taken from the ''Recently Updated Pages'' module I just wrote.</p>\r\n<p><br></p>\r\n<hr>\r\n<p><br></p>\r\n<p>Module PHP:</p>\r\n<p>&lt;?php<br><br>/***** Pages Recently Modified *****<br>&nbsp;*<br>&nbsp;*&nbsp; $Revision: 1.1.1.1 $<br>&nbsp;*<br>&nbsp;***********************************/<br><br>$recent = array();<br>$max = 5;</p>\r\n<p>$recent = array_sort_by_any_key<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ($GLOBALS[''jwsfNavigation'']-&gt;navArray, ''stamp'');<br>$recent = array_slice($recent, -$max);<br>$GLOBALS[''smarty'']-&gt;assign(''RecentlyModified'', $recent);<br><br>?&gt;</p>\r\n<p><br></p>\r\n<hr>\r\n<p><br></p>\r\n<p>And the template:</p>\r\n<p>{strip}<br><br>{* /* $Revision: 1.1.1.1 $ */ *}<br><br>{if !empty($RecentlyModified)}<br>&nbsp;&nbsp;&nbsp; &lt;div id="id_containerPagesRecentlyModified"&gt;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;h2&gt;Recently Updated Pages&lt;/h2&gt;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;ul&gt;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {section loop=$RecentlyModified name=pageR step=-1}<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;li&gt;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;a href="{$RecentlyModified[pageR].href}" <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; title="{$RecentlyModified[pageR].titlefull}"&gt;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {$RecentlyModified[pageR].titleNav}<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;/a&gt;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;/li&gt;<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {/section}<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;/ul&gt;<br>&nbsp;&nbsp;&nbsp; &lt;/div&gt;<br>{/if}<br><br>{/strip}</p>', '', '2009-10-10 11:17:55', ''),
(147, 76, 0, 255, 0, 1, '', '', 'A Rough Draft', '<p>This is not the final guide of course, but a quick draft for those who are wondering what the basic steps for installation of JWSF are.&nbsp; The basic concept is outlined below:</p>\r\n<ol>\r\n<li>Create database and add user with select, delete, update, insert and index prileges.</li>\r\n<li>Transfer tarball to target</li>\r\n<li>Unpack tarball</li>\r\n<li>Dump provided SQL into database</li>\r\n<li>Edit _public_html/constants/Cons_Db.php</li>\r\n<li>Modify provided .htaccess if necessary</li>\r\n<li>Run ./fixperms to make sure settings are correct</li>\r\n<li>Point browser to target</li>\r\n<li>Cruise through admin configuration area</li>\r\n<li>Add/remove content as desired</li>\r\n<li>Design some CSS and add custom images</li>\r\n<li>Booyah!</li>\r\n</ol>', '', '0000-00-00 00:00:00', ''),
(149, 126, 0, 255, 0, 1, '', '', 'The Timer Tells the Tale', '<p>JWSF is quick.&nbsp; So is my web host, so that helps.&nbsp; I recently optimized a lot of things on the back side and thought I would finally time things out to see where else I needed to focus.&nbsp; I am going to focus on the couple parts that could be improved but may not update this, so check your page load times to see how successful I have been!</p>\r\n<p>The average page load time for a full page with a custom theme, three levels of navigation, two related content panels, recently updated pages and all that good stuff is 350/1000 parts of a second.&nbsp; That is 0.035 seconds that it takes the server to create all the output and send it out to you.</p>\r\n<p>Here is the meat:</p>\r\n<p>0.000&nbsp;&nbsp;&nbsp; define_include_location<br />0.022&nbsp;&nbsp;&nbsp; load_components<br />0.025&nbsp;&nbsp;&nbsp; mysql object<br />0.026&nbsp;&nbsp;&nbsp; config ojbect<br />0.026&nbsp;&nbsp;&nbsp; user object - logged out<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 0.07&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; loggin in<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 0.028&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; logged in<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 0.034&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; logging out<br />0.027&nbsp;&nbsp; page object w/populate<br />0.027&nbsp;&nbsp; modules no mod<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 0.030&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; modules w/contact mod<br />0.027&nbsp;&nbsp; dhtml<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 0.027&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; when changing theme<br />0.031&nbsp;&nbsp; navigation<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 0.031&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; page w/lots nav<br />0.035&nbsp;&nbsp; buffering smarty</p>', '', '0000-00-00 00:00:00', ''),
(155, 131, 0, 255, 0, 1, '', '', 'New Features!', '<p>I''ve added a couple new features, an example of which can be seen on the <a style="" mce_href="/doc" href="/doc">documentation page</a>.&nbsp; The ''Page Resources'' feature allows for easy addition of various file types and links through an easy to use interface in the administration area.&nbsp; This makes it easy for users to dynamically add/remove attachments and links without having to do anything technical while providing site-wide continuity in the presentation of such additions.</p><p>The other new features are a bit more technical.&nbsp; The ''Archive Manager'' is now fully functional, allowing for indepent saving/restoring of file or database information.&nbsp; The new ''State Manager'' allows you to save the current state of the entire system in one click.</p><p>A couple of <b>caveats</b> in regard to the archiving features:</p><ul><li>Both DB Restore and Restore State will completely overwrite the database which means that any structural changes will also be lost</li><li>The File Restore feature will overwrite any existing files, but will not remove any files which may not be in the archive</li><li>The Restore State feature will remove all files and restore the file system to the state that it was in at the time it was saved, meaning any new files uploaded will be lost</li></ul><p>I am pretty excited about these new features, so enjoy! :)<br></p>', '', '2009-10-11 16:10:54', ''),
(163, 131, 0, 255, 0, 1, '', '', 'Article 4', '<p>Article 1 should scroll off<br mce_bogus="1"></p>', '', '2009-08-14 09:26:30', ''),
(162, 131, 0, 255, 0, 1, '', '', 'Article 3', '<p>More<br mce_bogus="1"></p>', '', '2009-08-14 09:26:10', ''),
(161, 131, 0, 255, 0, 1, '', '', 'Article 2', '<p>Test 2<br mce_bogus="1"></p>', '', '2009-08-14 09:25:53', ''),
(160, 131, 0, 255, 0, 1, '', '', 'Article 1', '<p>A test<br mce_bogus="1"></p>', '', '2009-08-14 09:24:38', ''),
(164, 131, 0, 255, 0, 1, '', '', 'RSS Reader', '<p>I have just completed an RSS feeder for the CMS.&nbsp; It generates completely valid code, check the Valid RSS icon @ the bottom of the page.<br mce_bogus="1"></p>', '', '2009-10-13 19:51:17', '');

-- --------------------------------------------------------

--
-- Table structure for table `JWSF_PageMeta`
--

DROP TABLE IF EXISTS `JWSF_PageMeta`;
CREATE TABLE `JWSF_PageMeta` (
  `id` tinyint(4) unsigned NOT NULL auto_increment,
  `pid` tinyint(4) unsigned NOT NULL COMMENT 'pid 255=no display, 254 = footer navigation',
  `views` int(10) unsigned NOT NULL,
  `acsLow` tinyint(3) unsigned NOT NULL,
  `acsHigh` tinyint(3) unsigned NOT NULL,
  `so` tinyint(4) unsigned NOT NULL,
  `active` tinyint(1) unsigned NOT NULL,
  `sitemapPriority` tinyint(3) unsigned NOT NULL default '5',
  `sitemapFrequency` tinyint(4) NOT NULL,
  `headerCollapse` tinyint(1) NOT NULL COMMENT 'This determines whether or not Javascript should collapse all the headers on the page by default or not.',
  `location` varchar(1) collate utf8_unicode_ci NOT NULL,
  `contentLimit` tinyint(4) unsigned NOT NULL,
  `allowComments` tinyint(1) unsigned NOT NULL,
  `allowRatings` tinyint(1) unsigned NOT NULL,
  `titleNav` tinytext collate utf8_unicode_ci NOT NULL,
  `titleFull` tinytext collate utf8_unicode_ci NOT NULL,
  `semanticURI` tinytext collate utf8_unicode_ci NOT NULL,
  `imgURI` tinytext collate utf8_unicode_ci NOT NULL COMMENT 'This is experimental, good luck!',
  `cTemplate` tinytext collate utf8_unicode_ci NOT NULL,
  `description` tinytext collate utf8_unicode_ci NOT NULL,
  `stamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='This table tracks page meta data' AUTO_INCREMENT=134 ;

--
-- Dumping data for table `JWSF_PageMeta`
--

INSERT INTO `JWSF_PageMeta` (`id`, `pid`, `views`, `acsLow`, `acsHigh`, `so`, `active`, `sitemapPriority`, `sitemapFrequency`, `headerCollapse`, `location`, `contentLimit`, `allowComments`, `allowRatings`, `titleNav`, `titleFull`, `semanticURI`, `imgURI`, `cTemplate`, `description`, `stamp`) VALUES
(1, 0, 413, 0, 255, 0, 1, 5, 5, 0, '3', 0, 0, 1, 'Home', 'Slim Frame Development Center', '', '', 'content_00.tpl', 'JWSF Slim Frame is a W3C compliant, XHTML 1.1, CSS 2.1, PHP/MySQL 5 mobile friendly system for managing accessible web sites!', '2009-10-25 23:56:48'),
(2, 0, 30, 0, 255, 0, 1, 5, 5, 0, '7', 0, 0, 1, 'Contact Me!', 'Slim Frame Contact Page', 'contact', '', 'content_00.tpl', 'Contact the JWSF Developer via Email Using This Form', '2009-10-23 11:52:47'),
(3, 34, 5, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Location', 'Slim Frame Development Headquarters', 'location', '', 'content_00.tpl', '', '2009-10-24 05:10:05'),
(93, 13, 3, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Images', 'JWSF Image Handling', 'images', '', 'content_00.tpl', '', '2009-10-23 11:40:19'),
(4, 0, 1, 0, 255, 0, 1, 5, 5, 0, '2', 0, 0, 1, 'Privacy Policy', 'Slim Frame Privacy Policy', 'privacy', '', 'content_00.tpl', 'The Standard JWSF Privacy Policy', '2009-10-13 21:32:18'),
(5, 0, 1, 0, 255, 0, 1, 5, 5, 0, '2', 0, 0, 1, 'Terms of Use', 'Slim Frame Terms and Conditions', 'terms', '', 'content_00.tpl', 'The Standard JWSF Terms and Conditions Page', '2009-10-23 11:13:31'),
(6, 0, 2, 0, 255, 0, 1, 5, 5, 0, '2', 0, 0, 1, 'Link to Us!', 'Slim Frame Link-Back Page', 'link', '', 'content_00.tpl', 'This page provides the proper XHTML code you need if you would like to provide a link to our site from yours.', '2009-10-23 10:56:19'),
(7, 8, 2, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Evolution', 'JWSF: A Work In Progress', 'evolution', '', 'content_00.tpl', '', '2009-10-23 12:34:50'),
(8, 0, 24, 0, 255, 0, 1, 5, 1, 0, '1', 0, 0, 1, 'About JWSF', 'Learn About the Jason White Slim Frame CMS', 'about', '', 'content_00.tpl', 'Learn about what the JWSF is and why you should use it.', '2009-10-23 17:08:20'),
(11, 0, 0, 0, 255, 0, 1, 5, 5, 0, '1', 0, 1, 1, 'Admin', 'Slim Frame Administration Area', 'admin', '', 'content_00.tpl', '', '2009-03-15 23:25:18'),
(75, 13, 2, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Configuration', 'How To Configure JWSF', 'configuration', '', 'content_00.tpl', '', '2009-10-14 11:06:44'),
(76, 13, 2, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Installation', 'How To Install JWSF', 'install', '', 'content_00.tpl', '', '2009-10-23 11:54:57'),
(12, 13, 4, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Change Log', 'Slim Frame Change Log', 'changelog', '', 'content_00.tpl', 'A chronological list of (important) changes made to JWSF.', '2009-10-24 05:08:50'),
(13, 8, 2, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Documentation', 'Slim Frame Documentation', 'doc', '', 'content_00.tpl', '', '2009-10-23 10:49:33'),
(14, 13, 2, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'To Do', 'Slim Frame Things to Do!', 'todo', '', 'content_00.tpl', '', '2009-10-23 11:04:55'),
(89, 13, 2, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Add-Ons', 'JWSF Modules and Themes', 'extensions', '', 'content_00.tpl', 'JWSF is highly extensible through modules, plug-ins and themes.', '2009-10-23 12:54:20'),
(84, 8, 2, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Reviews', 'Public Reviews of The JWSF', 'reviews', '', 'content_00.tpl', '', '2009-10-23 11:58:01'),
(15, 0, 1, 0, 0, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Register', ' New User Registration Page', 'register', '', 'content_00.tpl', '', '2009-10-23 12:05:20'),
(18, 13, 2, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Name Conventions', 'Slim Frame Naming Conventions', 'conventions', '', 'content_00.tpl', '', '2009-10-18 06:13:35'),
(49, 34, 3, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'My Network', 'My Network Rox', 'network', '', 'content_00.tpl', '', '2009-10-23 12:02:45'),
(104, 8, 8, 0, 255, 0, 1, 5, 5, 0, '5', 0, 0, 1, 'Live Demos', 'Multiple Live JWSF Demos', 'demos', '', 'content_00.tpl', 'JWSF Live Demos show off various features of the JWSF content management system that you can test and change.', '2009-10-23 11:14:56'),
(88, 8, 0, 0, 255, 0, 0, 5, 5, 0, '5', 0, 0, 1, 'Downloads', 'JWSF Related Downloads', 'downloads', '', 'content_00.tpl', '', '2009-03-15 23:25:18'),
(25, 0, 0, 0, 255, 0, 1, 0, 6, 0, '0', 0, 0, 1, 'Login Help', 'Login Help', 'help-login', '', 'content_00.tpl', 'If you are having trouble logging in to our web site, use this page to provide some helpful insight.', '0000-00-00 00:00:00'),
(27, 13, 2, 0, 255, 0, 1, 5, 5, 0, '1', 0, 1, 1, 'Video Tutorials', 'JWSF Video Tutorials for Users and Administrators', 'tutorials', '', 'content_00.tpl', 'The Jason White Slim Frame (JWSF) Content Management Systems is a highly extensible W3C compliant web site framework.', '2009-10-23 12:37:33'),
(28, 13, 4, 0, 255, 0, 1, 5, 5, 0, '1', 0, 1, 1, 'Design Guide', 'JWSF Guide for CSS Designers', 'designguide', '', 'content_00.tpl', 'The Jason White Slim Frame (JWSF) Content Management Systems is a highly extensible W3C compliant web site framework.', '2009-10-25 10:08:27'),
(29, 13, 5, 0, 255, 0, 1, 5, 5, 0, '1', 0, 1, 1, 'Developer Guide', 'JWSF Guide for Plug-In Developers', 'developers', '', 'content_00.tpl', 'The Jason White Slim Frame (JWSF) Content Management Systems is a highly extensible W3C compliant web site framework.', '2009-10-23 17:06:37'),
(116, 8, 8, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'JWSF vs Google', 'JWSF is Google Friendly', 'google', '', 'content_00.tpl', '', '2009-10-23 11:36:43'),
(111, 68, 2, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'MySQL', 'JWSF SQL Technique', 'mysql', '', 'content_00.tpl', '', '2009-10-23 11:08:02'),
(112, 68, 5, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'XHTML', 'JWSF XHTML Output', 'xhtml', '', 'content_00.tpl', '', '2009-10-24 17:23:15'),
(33, 13, 2, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Flash', 'Where is the Flash?', 'flash', '', 'content_00.tpl', '', '2009-10-23 11:20:39'),
(34, 0, 5, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'About Me', 'A Little Bit About The Author', 'jason', '', 'content_00.tpl', 'Learn about me, the author of JWSF.', '2009-10-23 11:21:26'),
(35, 34, 8, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Certifications', 'My BrainBench Certifications and Scores', 'certifications', '', 'content_00.tpl', 'My Brain Bench and other professional certifications.', '2009-10-23 20:38:46'),
(95, 13, 4, 0, 255, 0, 1, 5, 5, 0, '1', 0, 1, 1, 'Dynamic Layouts', 'JWSF Dynamic Layout Engine', 'dynamic-layout', '', 'content_00.tpl', '', '2009-10-17 15:47:36'),
(37, 34, 9, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Resume', 'My Resume', 'resume', '', 'content_00.tpl', '', '2009-10-24 05:09:25'),
(100, 8, 1, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Participate', 'How To Get Involved', 'participate', '', 'content_00.tpl', '', '2009-10-14 11:06:35'),
(94, 34, 4, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Workflow', 'A Typical Day in My Workflow Pattern', 'workflow', '', 'content_00.tpl', '', '2009-10-24 05:09:16'),
(44, 0, 0, 1, 255, 0, 1, 5, 5, 0, '3', 0, 0, 0, 'Your Settings', 'Update Your Account Settings', 'settings', '', 'content_00.tpl', 'The Jason White Slim Frame (JWSF) Content Management Systems is a highly extensible W3C compliant web site framework.', '2009-03-15 23:25:18'),
(45, 8, 2, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Security', 'JWSF Takes Security Seriously', 'security', '', 'content_00.tpl', '', '2009-10-23 12:17:50'),
(109, 68, 2, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'PHP', 'JWSF PHP Style', 'php', '', 'content_00.tpl', '', '2009-10-23 10:45:05'),
(110, 68, 24, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'CSS', 'JWSF CSS Style', 'css', '', 'content_00.tpl', '', '2009-10-25 23:07:03'),
(47, 34, 3, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Remote Support', 'I Provide Remote Support with Crossloop', 'crossloop', '', 'content_00.tpl', '', '2009-10-23 12:36:57'),
(108, 13, 3, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Files', 'JWSF File System Structure', 'file-system-structure', '', 'content_00.tpl', '', '2009-10-22 08:28:51'),
(48, 13, 2, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Administration', 'JWSF End User Administration Documentation', 'end-user-documentation', '', 'content_00.tpl', 'Documentation related to the administrative area of JWSF.  This is a good place to start if you maintain content using the JWSF CMS.', '2009-10-23 17:06:36'),
(99, 113, 6, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'System', 'JWSF System Interface', 'api-system', '', 'content_00.tpl', 'JWSF System API descripes how to interact with the core JWSF components.', '2009-10-14 11:08:22'),
(53, 113, 1, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Smarty', 'Using Smarty with JWSF', 'smarty-api', '', 'content_00.tpl', '', '2009-10-23 15:36:14'),
(82, 13, 1, 0, 255, 0, 1, 5, 5, 0, '1', 0, 1, 1, 'Forms', 'JWSF Form Handling', 'forms', '', 'content_00.tpl', '', '2009-10-14 11:06:43'),
(54, 8, 2, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Training', 'JWSF Training and Seminars', 'training', '', 'content_00.tpl', '', '2009-10-23 12:04:52'),
(113, 13, 4, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'APIs', 'Programmatic Interaction', 'apis', '', 'content_00.tpl', 'This area documents the JWSF Application Programming Interfaces.  In other words, how to programmatically interact with JWSF.', '2009-10-23 10:55:13'),
(114, 113, 3, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Database', 'JWSF Database Interface', 'database-api', '', 'content_00.tpl', '', '2009-10-16 07:25:13'),
(115, 13, 2, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Execution', 'JWSF Linear Execution', 'execution', '', 'content_00.tpl', '', '2009-10-21 13:01:03'),
(131, 0, 23, 0, 255, 0, 1, 5, 5, 0, '5', 3, 0, 1, 'News', 'Recent Events', 'news', '', 'content_00.tpl', '', '2009-10-23 17:06:42'),
(68, 8, 27, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Coding Style', 'JWSF Coding Methodology', 'code-methodology', '', 'content_00.tpl', 'A little bit about my personal coding style.', '2009-10-25 21:13:21'),
(92, 89, 1, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Profiles', 'JWSF Profiles', 'profiles', '', 'content_00.tpl', '', '2009-10-21 02:15:19'),
(69, 13, 1, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Requirements', 'JWSF Technical Requirements', 'requirements', '', 'content_00.tpl', '', '2009-10-14 11:06:41'),
(125, 8, 2, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Topology', 'JWSF Application Stack', 'topology', '', 'content_00.tpl', '', '2009-10-23 12:12:14'),
(107, 8, 2, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Features', 'JWSF Feature Set', 'features', '', 'content_00.tpl', '', '2009-10-23 12:20:44'),
(80, 34, 4, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Consulting', 'Consulting Services', 'consulting', '', 'content_00.tpl', '', '2009-10-24 05:09:56'),
(105, 89, 0, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Modules', 'JWSF Modules', 'modules', '', 'content_00.tpl', '', '2009-10-10 11:17:55'),
(91, 89, 0, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Themes', 'JWSF Themes', 'theme-packs', '', 'content_00.tpl', '', '2009-03-15 23:25:18'),
(126, 8, 1, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Performance', 'JWSF is Fast', 'performance', '', 'content_00.tpl', '', '2009-10-14 11:06:36'),
(127, 8, 6, 0, 255, 0, 1, 5, 5, 0, '1', 0, 0, 1, 'Audience', 'JWSF Target Audience', 'audience', '', 'content_00.tpl', 'This page describes the target user base for the JWSF content management system.', '2009-10-23 17:08:19'),
(132, 0, 257, 0, 255, 0, 1, 5, 5, 0, '7', 0, 0, 1, 'RSS', 'The Official JWSF RSS Feed', 'rss', '', 'content_00.tpl', '', '2009-10-25 23:36:01'),
(133, 0, 329, 0, 255, 0, 1, 5, 5, 0, '1', 0, 1, 1, 'QDB Test', 'Quote Database Test for Blinkenshell', 'qdb', '', 'content_00.tpl', '', '2009-10-25 03:31:09');

-- --------------------------------------------------------

--
-- Table structure for table `JWSF_PageModules`
--

DROP TABLE IF EXISTS `JWSF_PageModules`;
CREATE TABLE `JWSF_PageModules` (
  `id` tinyint(4) unsigned NOT NULL auto_increment,
  `pid` tinyint(4) unsigned NOT NULL,
  `location` varchar(1) collate utf8_unicode_ci NOT NULL default '1' COMMENT 'This determines the location of the module output.  0 will prevent any output, 1 will be before content, and 2 will be after content.',
  `so` tinyint(4) unsigned NOT NULL,
  `active` tinyint(1) unsigned NOT NULL,
  `acsLow` tinyint(4) unsigned NOT NULL,
  `acsHigh` tinyint(4) unsigned NOT NULL,
  `module` tinytext collate utf8_unicode_ci NOT NULL,
  `stamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='This table tracks page content' AUTO_INCREMENT=8 ;

--
-- Dumping data for table `JWSF_PageModules`
--

INSERT INTO `JWSF_PageModules` (`id`, `pid`, `location`, `so`, `active`, `acsLow`, `acsHigh`, `module`, `stamp`) VALUES
(1, 2, '2', 0, 1, 0, 255, 'ContactUsMultiLevel', '2009-10-09 19:40:55'),
(2, 15, '2', 0, 1, 0, 255, 'SignUp', '2009-10-09 18:17:19'),
(4, 44, '2', 0, 1, 1, 255, 'UserSettings', '2009-10-09 18:17:19'),
(7, 133, '1', 0, 1, 0, 255, 'QDB', '2009-10-18 17:04:18');

-- --------------------------------------------------------

--
-- Table structure for table `JWSF_PageResourceAssignment`
--

DROP TABLE IF EXISTS `JWSF_PageResourceAssignment`;
CREATE TABLE `JWSF_PageResourceAssignment` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `rid` int(10) unsigned NOT NULL,
  `pid` int(10) unsigned NOT NULL,
  `cid` int(10) unsigned NOT NULL,
  `addedBy` tinytext collate utf8_unicode_ci NOT NULL,
  `modifiedBy` tinytext collate utf8_unicode_ci NOT NULL,
  `stamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=30 ;

--
-- Dumping data for table `JWSF_PageResourceAssignment`
--

INSERT INTO `JWSF_PageResourceAssignment` (`id`, `rid`, `pid`, `cid`, `addedBy`, `modifiedBy`, `stamp`) VALUES
(1, 1, 999, 0, 'super@admin.com', '', '2009-08-02 14:10:43'),
(2, 2, 2, 51, 'super@admin.com', '', '2009-08-08 21:03:27'),
(3, 3, 13, 0, 'super@admin.com', 'super@admin.com', '2009-08-14 08:54:10'),
(5, 4, 2, 999, 'super@admin.com', '', '2009-08-02 14:15:48'),
(6, 5, 13, 0, 'super@admin.com', 'super@admin.com', '2009-08-14 08:49:16'),
(7, 6, 999, 51, 'super@admin.com', '', '2009-08-02 14:10:43'),
(8, 7, 13, 0, 'super@admin.com', 'super@admin.com', '2009-08-14 08:49:51'),
(9, 5, 13, 96, 'super@admin.com', '97.83.149.156', '2009-08-12 21:17:18'),
(10, 999, 111, 123, 'super@admin.com', '', '2009-08-02 14:02:08'),
(11, 9, 13, 15, 'super@admin.com', '97.83.149.156', '2009-08-13 17:30:38'),
(12, 10, 0, 999, 'super@admin.com', '', '2009-08-02 14:15:48'),
(13, 11, 13, 0, 'super@admin.com', 'super@admin.com', '2009-08-14 08:54:52'),
(14, 999, 29, 0, 'super@admin.com', '', '2009-08-02 13:41:04'),
(15, 1, 13, 15, '97.83.149.156', '97.83.149.156', '2009-08-13 17:26:13'),
(16, 6, 13, 15, '97.83.149.156', '97.83.149.156', '2009-08-13 17:26:38'),
(17, 9, 0, 0, '97.83.149.156', '', '2009-08-13 17:25:25'),
(20, 16, 13, 0, 'super@admin.com', '', '2009-08-14 08:57:54'),
(19, 1, 13, 0, 'super@admin.com', '', '2009-08-14 08:56:31'),
(25, 20, 113, 129, '97.83.149.156', '', '2009-08-14 10:48:19'),
(27, 22, 13, 0, '97.83.149.156', '', '2009-08-14 11:31:54'),
(28, 23, 93, 0, '24.213.20.174', '', '2009-08-14 13:56:25'),
(29, 24, 114, 0, '24.213.18.146', '', '2009-08-18 11:42:19');

-- --------------------------------------------------------

--
-- Table structure for table `JWSF_PageResourceMeta`
--

DROP TABLE IF EXISTS `JWSF_PageResourceMeta`;
CREATE TABLE `JWSF_PageResourceMeta` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `active` tinyint(1) NOT NULL,
  `viewable` tinyint(1) NOT NULL,
  `title` tinytext collate utf8_unicode_ci NOT NULL,
  `description` text collate utf8_unicode_ci NOT NULL,
  `location` text collate utf8_unicode_ci NOT NULL,
  `addedBy` tinytext collate utf8_unicode_ci NOT NULL,
  `modifiedBy` tinytext collate utf8_unicode_ci NOT NULL,
  `stamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

--
-- Dumping data for table `JWSF_PageResourceMeta`
--

INSERT INTO `JWSF_PageResourceMeta` (`id`, `active`, `viewable`, `title`, `description`, `location`, `addedBy`, `modifiedBy`, `stamp`) VALUES
(1, 1, 1, 'An_Archive.tar.gz', '', '/uploads/archives/An_Archive.tar.gz', 'super@admin.com', '', '2009-08-02 11:42:30'),
(2, 1, 1, 'A Windows Zip File', 'pkzip was better than WinZip.', '/uploads/archives/AnotherArchive-RenamedOnUpload.zip', 'super@admin.com', '', '2009-08-15 09:10:02'),
(3, 1, 1, 'Silence', 'Johnny Cage style', '/uploads/audio/some_audio.mp3', 'super@admin.com', '', '2009-08-02 11:41:39'),
(4, 1, 1, 'CouldBe.doc', 'Download or view this file', '/uploads/documents/CouldBe.doc', 'super@admin.com', '', '2009-08-02 11:53:15'),
(5, 1, 1, 'Ick - Spread Sheet', 'This is a spread sheet', '/uploads/documents/Spreadsheet.xls', 'super@admin.com', '', '2009-08-02 11:53:55'),
(6, 1, 1, 'image.gif', 'Download or view this file', '/uploads/images/image.gif', 'super@admin.com', '', '2009-08-02 11:54:17'),
(7, 1, 1, 'moreimage.jpg', 'Download or view this file', '/uploads/images/moreimage.jpg', 'super@admin.com', '', '2009-08-02 11:54:27'),
(14, 1, 1, 'File Doesn''t Exist', 'This is another non-existent file.', '/uploads/pdf/missing.pdf', 'Me Again', '', '2009-08-03 13:53:22'),
(23, 1, 1, 'WGRD_LicKey_3543429_ORD222164.zip', 'Download or view this file', '', '24.213.20.174', '', '2009-08-14 13:56:25'),
(24, 1, 1, 'Menu Title', 'For rollover', '/uploads/archives/JWSF.tar.gz', '24.213.18.146', '', '2009-08-18 11:42:19'),
(11, 1, 1, 'doit.mpg', 'Here is a movie', '/uploads/video/doit.mpg', 'super@admin.com', '', '2009-08-13 17:30:48'),
(13, 1, 1, 'A Missing File', 'This file does not exist on the file system.', '/uploads/images/missing.png', 'Me', '', '2009-08-03 13:52:10'),
(22, 1, 1, 'AnEmpty.pdf', 'Download or view this file', '/uploads/pdf/AnEmpty.pdf', '97.83.149.156', '', '2009-08-14 11:31:54');

-- --------------------------------------------------------

--
-- Table structure for table `JWSF_predefCountries`
--

DROP TABLE IF EXISTS `JWSF_predefCountries`;
CREATE TABLE `JWSF_predefCountries` (
  `id` tinyint(3) unsigned NOT NULL auto_increment,
  `value` tinytext collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `JWSF_predefCountries`
--


-- --------------------------------------------------------

--
-- Table structure for table `JWSF_predefMonths`
--

DROP TABLE IF EXISTS `JWSF_predefMonths`;
CREATE TABLE `JWSF_predefMonths` (
  `id` tinyint(4) NOT NULL auto_increment,
  `item` tinytext collate utf8_unicode_ci NOT NULL,
  `abv` varchar(3) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `JWSF_predefMonths`
--

INSERT INTO `JWSF_predefMonths` (`id`, `item`, `abv`) VALUES
(1, 'January', 'Jan'),
(2, 'February', 'Feb'),
(3, 'March', 'Mar'),
(4, 'April', 'Apr'),
(5, 'May', 'May'),
(6, 'June', 'Jun'),
(7, 'July', 'Jul'),
(8, 'August', 'Aug'),
(9, 'September', 'Sep'),
(10, 'October', 'Oct'),
(11, 'November', 'Nov'),
(12, 'December', 'Dec');

-- --------------------------------------------------------

--
-- Table structure for table `JWSF_predefSiteMapFrequency`
--

DROP TABLE IF EXISTS `JWSF_predefSiteMapFrequency`;
CREATE TABLE `JWSF_predefSiteMapFrequency` (
  `id` tinyint(1) unsigned NOT NULL auto_increment,
  `value` tinytext collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `JWSF_predefSiteMapFrequency`
--

INSERT INTO `JWSF_predefSiteMapFrequency` (`id`, `value`) VALUES
(1, 'always'),
(2, 'hourly'),
(3, 'daily'),
(4, 'weekly'),
(5, 'monthly'),
(6, 'yearly'),
(7, 'never');

-- --------------------------------------------------------

--
-- Table structure for table `JWSF_predefStates`
--

DROP TABLE IF EXISTS `JWSF_predefStates`;
CREATE TABLE `JWSF_predefStates` (
  `id` tinyint(3) unsigned NOT NULL auto_increment,
  `item` tinytext collate utf8_unicode_ci NOT NULL,
  `abv` varchar(2) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `JWSF_predefStates`
--


-- --------------------------------------------------------

--
-- Table structure for table `JWSF_ThemeConfig`
--

DROP TABLE IF EXISTS `JWSF_ThemeConfig`;
CREATE TABLE `JWSF_ThemeConfig` (
  `id` tinyint(4) unsigned NOT NULL auto_increment,
  `name` tinytext collate utf8_unicode_ci NOT NULL,
  `value` tinytext collate utf8_unicode_ci NOT NULL,
  `description` text collate utf8_unicode_ci NOT NULL,
  `stamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='This table holds site configuration' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `JWSF_ThemeConfig`
--

INSERT INTO `JWSF_ThemeConfig` (`id`, `name`, `value`, `description`, `stamp`) VALUES
(1, 'ThemeActive', 'Default-Black', 'This setting determines the default theme, if it is available.  If it is unavailable, the system will fall back to the skeleton theme.  Setting this does not affect a user''s selected theme, only the default.', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `JWSF_TrackSQL`
--

DROP TABLE IF EXISTS `JWSF_TrackSQL`;
CREATE TABLE `JWSF_TrackSQL` (
  `id` int(4) unsigned NOT NULL auto_increment,
  `query` mediumtext collate utf8_unicode_ci NOT NULL,
  `address` varchar(15) collate utf8_unicode_ci NOT NULL,
  `stamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='This table tracks all SQL queries' AUTO_INCREMENT=1 ;

--
-- Dumping data for table `JWSF_TrackSQL`
--


-- --------------------------------------------------------

--
-- Table structure for table `JWSF_UserFailedActivations`
--

DROP TABLE IF EXISTS `JWSF_UserFailedActivations`;
CREATE TABLE `JWSF_UserFailedActivations` (
  `id` int(11) NOT NULL auto_increment,
  `email` text collate utf8_unicode_ci NOT NULL,
  `crypt` text collate utf8_unicode_ci NOT NULL,
  `address` varchar(15) collate utf8_unicode_ci NOT NULL,
  `stamp` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `JWSF_UserFailedActivations`
--

INSERT INTO `JWSF_UserFailedActivations` (`id`, `email`, `crypt`, `address`, `stamp`) VALUES
(1, 'test2@aplusecs.com', 'grfg2@ncyhfrpf.pbz', '', '2009-03-06 17:54:11'),
(2, 'test2@aplusecs.com', 'grfg2@ncyhfrpf.pbz', '', '2009-03-06 18:01:25'),
(3, 'test2@aplusecs.com', 'grfg2@ncyhfrpf.pbz', '', '2009-03-06 18:01:39'),
(4, 'test2@aplusecs.com', 'grfg2@ncyhfrpf.pbz', '', '2009-03-06 18:01:56');

-- --------------------------------------------------------

--
-- Table structure for table `JWSF_UserLevels`
--

DROP TABLE IF EXISTS `JWSF_UserLevels`;
CREATE TABLE `JWSF_UserLevels` (
  `id` tinyint(4) NOT NULL auto_increment,
  `item` tinytext collate utf8_unicode_ci NOT NULL COMMENT 'This is the short label (ie; admin, regular, editor)',
  `value` tinyint(4) unsigned NOT NULL COMMENT 'This is the numeric level',
  `description` tinytext collate utf8_unicode_ci NOT NULL COMMENT 'This is the long description',
  `stamp` timestamp NOT NULL default '0000-00-00 00:00:00' on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='This table defines user access levels' AUTO_INCREMENT=5 ;

--
-- Dumping data for table `JWSF_UserLevels`
--

INSERT INTO `JWSF_UserLevels` (`id`, `item`, `value`, `description`, `stamp`) VALUES
(1, 'Anonymous', 0, 'This is the access level assigned to users who are not logged in.', '2009-01-12 15:20:39'),
(2, 'Standard Registered User', 1, 'This is just a typical user.', '2009-01-12 15:20:39'),
(3, 'Super Admin', 255, 'mmm...power', '2009-01-13 06:01:30'),
(4, 'Upper Admin', 254, 'This is the second highest level, typically 255 should be reserved for the site owner or developer only.', '2009-01-14 09:42:20');

-- --------------------------------------------------------

--
-- Table structure for table `JWSF_Users`
--

DROP TABLE IF EXISTS `JWSF_Users`;
CREATE TABLE `JWSF_Users` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `active` tinyint(1) unsigned NOT NULL,
  `acslvl` tinyint(3) unsigned NOT NULL,
  `nameFirst` tinytext collate utf8_unicode_ci NOT NULL,
  `nameMiddle` tinytext collate utf8_unicode_ci NOT NULL,
  `nameLast` tinytext collate utf8_unicode_ci NOT NULL,
  `nameCompany` tinytext collate utf8_unicode_ci NOT NULL,
  `email` varchar(320) collate utf8_unicode_ci NOT NULL,
  `email2` varchar(320) collate utf8_unicode_ci NOT NULL,
  `telephone` varchar(13) collate utf8_unicode_ci NOT NULL,
  `telephone2` varchar(13) collate utf8_unicode_ci NOT NULL,
  `fax` varchar(13) collate utf8_unicode_ci NOT NULL,
  `address` tinytext collate utf8_unicode_ci NOT NULL,
  `address2` tinytext collate utf8_unicode_ci NOT NULL,
  `city` tinytext collate utf8_unicode_ci NOT NULL,
  `state` tinytext collate utf8_unicode_ci NOT NULL,
  `country` tinytext collate utf8_unicode_ci NOT NULL,
  `zip` varchar(10) collate utf8_unicode_ci NOT NULL,
  `secretAnswer` tinytext collate utf8_unicode_ci NOT NULL,
  `secretQuestion` tinytext collate utf8_unicode_ci NOT NULL,
  `passValue` tinytext collate utf8_unicode_ci NOT NULL,
  `sessionKey` tinytext collate utf8_unicode_ci NOT NULL,
  `language` tinytext collate utf8_unicode_ci NOT NULL,
  `language2` tinytext collate utf8_unicode_ci NOT NULL,
  `specialOffers` tinytext collate utf8_unicode_ci NOT NULL,
  `stampCreated` timestamp NOT NULL default '0000-00-00 00:00:00',
  `stampLogin` timestamp NOT NULL default '0000-00-00 00:00:00',
  `stampModified` timestamp NOT NULL default '0000-00-00 00:00:00' on update CURRENT_TIMESTAMP,
  `loginsFailed` tinyint(3) unsigned NOT NULL COMMENT 'This tracks the number of times a user''s account failed authentication since their last successful attempt',
  `loginsFailedTotal` int(10) unsigned NOT NULL COMMENT 'This tracks the total lifetime failed logins of a user',
  `loginsSuccessTotal` int(10) unsigned NOT NULL COMMENT 'This tracks the total lifetime successful logins by a user',
  `ip` tinytext collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=39 ;

--
-- Dumping data for table `JWSF_Users`
--

INSERT INTO `JWSF_Users` (`id`, `active`, `acslvl`, `nameFirst`, `nameMiddle`, `nameLast`, `nameCompany`, `email`, `email2`, `telephone`, `telephone2`, `fax`, `address`, `address2`, `city`, `state`, `country`, `zip`, `secretAnswer`, `secretQuestion`, `passValue`, `sessionKey`, `language`, `language2`, `specialOffers`, `stampCreated`, `stampLogin`, `stampModified`, `loginsFailed`, `loginsFailedTotal`, `loginsSuccessTotal`, `ip`) VALUES
(38, 0, 0, 'chris', '', 'schmidt', '', 'dude@gmail.com', '', '', '', '', '113 East Ohio', '', 'Marquette', 'MI', 'US', '49855', 'e13efc991a9bf44bbb4da87cdbb725240184585ccaf270523170e008cf2a3b85f45f86c3da647f69780fb9e971caf5437b3d06d418355a68c9760c70a31d05c7acb032e737fb7b828f0bc62264272c6b6de5d0a3d6bc9b1c2105404d6f2e5c0424d998b6b6fa187d876bcc34462d8d53d721b6e2dcbc64112739ab0bd68f834', 'dogs name', 'acb032e737fb7b828f0bc62264272c6b6de5d0a3d6bc9b1c2105404d6f2e5c0424d998b6b6fa187d876bcc34462d8d53d721b6e2dcbc64112739ab0bd68f834e13efc991a9bf44bbb4da87cdbb725240184585ccaf270523170e008cf2a3b85f45f86c3da647f69780fb9e971caf5437b3d06d418355a68c9760c70a31d05c7', '', '', '', 'true', '2009-05-13 12:04:52', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, ''),
(14, 0, 0, 'UserAwaiting', '', 'Activation', '', 'email0@email.com', 'asdf', '1111111111', '', '', 'This user should', 'check their email', 'and get activated!', 'MI', 'US', '12312', '4f6d5b539389b3af79908c82b22e059dc6508767a912bb54e78423f8a8802e40f78a8b2e4f9af3b185027dd964fc32a5056115ddf54fc9d13e3abf95ebf125b9e3f7fb2455363aa7917e23da792fea686ad984b7f45b042bd24855c47994095ee82b5eaed2b69ff8a7c9a99c74a1959d2b80b0d97a26339ef2a195c9bcdcfba', '', 'e3f7fb2455363aa7917e23da792fea686ad984b7f45b042bd24855c47994095ee82b5eaed2b69ff8a7c9a99c74a1959d2b80b0d97a26339ef2a195c9bcdcfba4f6d5b539389b3af79908c82b22e059dc6508767a912bb54e78423f8a8802e40f78a8b2e4f9af3b185027dd964fc32a5056115ddf54fc9d13e3abf95ebf125b9', '', '0', '', '', '2009-01-14 22:09:05', '0000-00-00 00:00:00', '2009-03-06 19:09:03', 1, 1, 0, ''),
(15, 1, 1, 'Regular', '', 'User', 'Unemployed', 'regular@user.com', 'no@address.net', 'adfasdfas', '', '', 'Address 1', '', 'City1', 'MI', 'US', '11111', 'b18c82eb5dcaba48feb0adafacab69b0bdf47a6cb6733bccb3ae23f8ef31892aa43c2ef37237acd204caddd15757dd4aa9bd7fc5f4bee41e7737729d214e4896451a7eaad07cc4c2219958263bee0243d58237898fb1ef7f256d8241880cf2a2ac28c07cfb4bb07e06602d0ced27b8a6a29e09330f55f590547fe07277caeb6', '', '451a7eaad07cc4c2219958263bee0243d58237898fb1ef7f256d8241880cf2a2ac28c07cfb4bb07e06602d0ced27b8a6a29e09330f55f590547fe07277caeb6b18c82eb5dcaba48feb0adafacab69b0bdf47a6cb6733bccb3ae23f8ef31892aa43c2ef37237acd204caddd15757dd4aa9bd7fc5f4bee41e7737729d214e4896', '0', '0', '', 'true', '2009-01-14 22:39:42', '2009-09-23 17:15:06', '2009-09-23 17:20:59', 0, 0, 7, '24.213.40.154'),
(16, 1, 254, 'Normal', '', 'Admin', 'Admin Company', 'normal@admin.com', '', '', '', '', '', '', '', 'MI', 'US', '', '309ed4198244cc4767ff712094f88e98b5bf0c59aa78ff678db2d923867f468deefc3130239693e2ccb171a0a3b696bd2abb96b9d01cece70ed31ccfc36cc6d25f56c61b2cab02e5d0689530e9ec66616bfbd57f228d5f8f4093e738fa509ecb18210b6b7c6b76d24c41103ed843a39e0dc19be496940688eb91b1ce61eb70e', '', '5f56c61b2cab02e5d0689530e9ec66616bfbd57f228d5f8f4093e738fa509ecb18210b6b7c6b76d24c41103ed843a39e0dc19be496940688eb91b1ce61eb70e309ed4198244cc4767ff712094f88e98b5bf0c59aa78ff678db2d923867f468deefc3130239693e2ccb171a0a3b696bd2abb96b9d01cece70ed31ccfc36cc6d2', '7425717.976+1118E', '0', '', '', '2009-01-14 22:40:40', '2009-04-08 14:24:07', '2009-04-08 14:24:45', 0, 0, 7, '204.38.20.182'),
(17, 1, 255, 'Super', '', 'Admin', '', 'super@admin.com', '', '', '', '', '', '', '', 'MI', 'US', '', '309ed4198244cc4767ff712094f88e98b5bf0c59aa78ff678db2d923867f468deefc3130239693e2ccb171a0a3b696bd2abb96b9d01cece70ed31ccfc36cc6d25e0e0be1e72d96eb91ac14f7d44ee31a5aa24b5c979cccfd35ca912a472eab2baf62b4996c6c2074e0f3b6ee42c144877001a9a8a8ae31157566b1911f770d3', '', '5e0e0be1e72d96eb91ac14f7d44ee31a5aa24b5c979cccfd35ca912a472eab2baf62b4996c6c2074e0f3b6ee42c144877001a9a8a8ae31157566b1911f770d3309ed4198244cc4767ff712094f88e98b5bf0c59aa78ff678db2d923867f468deefc3130239693e2ccb171a0a3b696bd2abb96b9d01cece70ed31ccfc36cc6d2', 'E87886545+1.26329', '0', '', '', '2009-01-14 22:41:07', '2009-10-23 05:32:20', '2009-10-23 05:33:02', 0, 2, 75, '84.27.107.25'),
(36, 0, 0, 'InetMarketolog', 'InetMarketolog', 'InetMarketolog', 'InetMarketolog', 'ionisanon@gmail.com', 'ionisanon@gmail.com', '123456', '123456', '123456', 'http://www.businesstalkforum.com/members/shakespearefan39.html', 'Mtskheta', 'Mtskheta', 'AL', 'US', '123456', '073f2a791a2e563a8a8663721483b775da007b358915547aacbc52292e3d0265854cf17b587c1482e9493c8e27262e03f8f008aaafee3ced02cb9885455ddeef7d1169445db67b6ac2112e0efaa1009f1c556d9777c5751d4639eeb09f34fcf9285ffd6c0307bd3ccb403864101ccec215b181ce585db927be29454e1dbb75e', '', '7d1169445db67b6ac2112e0efaa1009f1c556d9777c5751d4639eeb09f34fcf9285ffd6c0307bd3ccb403864101ccec215b181ce585db927be29454e1dbb75e073f2a791a2e563a8a8663721483b775da007b358915547aacbc52292e3d0265854cf17b587c1482e9493c8e27262e03f8f008aaafee3ced02cb9885455ddeef', '', '', '', 'true', '2009-05-06 23:47:51', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, ''),
(37, 0, 0, 'Turbcrect', 'Turbcrect', 'Turbcrect', 'Turbcrect', 'ignolokig@gmail.com', 'ignolokig@gmail.com', '123456', '123456', '123456', 'http://indiatourtips.com/india-travel-guide/member.php?u=412', 'Liberia', 'Liberia', 'AL', 'US', '123456', '21ef90c1a190108d590e74e3bef072430cf8340eb9d326db72dc5eeb308819e1fdd7bf86cbf1a2653990ed99ddc089a012ee32109ec2a1907f400a195171ba1ae823d4f7343416f83b47f45d8630d477cdaeaac504952af2cc71b0bc32e2977900525b4e0943fbc664fd30e041244e52d935e4bc7fe802f830710b2ddde533b', '', 'e823d4f7343416f83b47f45d8630d477cdaeaac504952af2cc71b0bc32e2977900525b4e0943fbc664fd30e041244e52d935e4bc7fe802f830710b2ddde533b21ef90c1a190108d590e74e3bef072430cf8340eb9d326db72dc5eeb308819e1fdd7bf86cbf1a2653990ed99ddc089a012ee32109ec2a1907f400a195171ba1a', '', '', '', 'true', '2009-05-10 09:47:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, '');
